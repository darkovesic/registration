<?php

define('APP_PATH', realpath(__DIR__ . '/..') . '/');

session_set_cookie_params(8640000);
/**
 * Read composer autoloader
 */
require_once APP_PATH . 'vendor/autoload.php';

/**
 * Read bootstrap application
 */
require_once APP_PATH . 'app/library/Comit/Application.php';

/**
 * Handle the request
 */
$application = new \Comit\Application();
$application->init();
echo $application->handle()->getContent();

/**
 *
 * @param grid_selector
 * @param pager_selector
 */
function add_nav_buttons(grid_selector, pager_selector) {
    $(grid_selector).jqGrid('navGrid', pager_selector,
        { 	//navbar options
            edit: false,
            add: false,
            del: false,
            view: false,
            search: true,
            searchicon: 'fa fa-search orange',
            refresh: true,
            nexticon: 'fa fa-refresh green'
        },
        {
            //edit record form
            //closeAfterEdit: true,
            recreateForm: true,
            beforeShowForm: function (e) {
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
            }
        },
        {
            //new record form
            closeAfterAdd: true,
            recreateForm: true,
            viewPagerButtons: false,
            beforeShowForm: function (e) {
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
            }
        },
        {
            //delete record form
            recreateForm: true,
            beforeShowForm: function (e) {
                var form = $(e[0]);
                if (form.data('styled')) return false;
                form.closest('.ui-jqdialog').find('.ui-jqdialog-titlebar').wrapInner('<div class="widget-header" />')
                form.data('styled', true);
            },
            onClick: function (e) {
                alert(1);
            }
        },
        {
            //search form
            recreateForm: true,
            afterShowSearch: function (e) {
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
                style_search_form(form);
            },
            afterRedraw: function () {
                style_search_filters($(this));
            },
            multipleSearch: true
        },
        {
            //view record form
            recreateForm: true,
            beforeShowForm: function (e) {
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
            }
        }
    );
}

/**
 *
 * @param form
 */
function style_search_filters(form) {
    form.find('.delete-rule').val('X');
    form.find('.add-rule').addClass('btn btn-xs btn-primary');
    form.find('.add-group').addClass('btn btn-xs btn-success');
    form.find('.delete-group').addClass('btn btn-xs btn-danger');
}

/**
 *
 * @param form
 */
function style_search_form(form) {
    var dialog = form.closest('.ui-jqdialog');
    var buttons = dialog.find('.EditTable');
    buttons.find('.EditButton a[id*="_reset"]').addClass('btn btn-sm btn-info').find('.ui-icon').attr('class', 'fa fa-retweet');
    buttons.find('.EditButton a[id*="_query"]').addClass('btn btn-sm btn-inverse').find('.ui-icon').attr('class', 'fa fa-comment-alt');
    buttons.find('.EditButton a[id*="_search"]').addClass('btn btn-sm btn-purple').find('.ui-icon').attr('class', 'fa fa-search');
}


//replace icons with FontAwesome icons like above
function updatePagerIcons(table) {
    var replacement =
    {
        'ui-icon-seek-first': 'fa fa-angle-double-left bigger-140',
        'ui-icon-seek-prev': 'fa fa-angle-left bigger-140',
        'ui-icon-seek-next': 'fa fa-angle-right bigger-140',
        'ui-icon-seek-end': 'fa fa-angle-double-right bigger-140'
    };
    $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
        var icon = $(this);
        var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

        if ($class in replacement) icon.attr('class', replacement[$class]);
    })
}

/**
 *
 * @param table
 */
function enableTooltips(table) {
    $('.navtable .ui-pg-button').tooltip({container: 'body'});
    $(table).find('.ui-pg-div').tooltip({container: 'body'});
}

/**
 *
 */
function updateTableIcons() {
    $('.ui-jqgrid-htable').find('.clearsearchclass').html('').addClass('fa fa-remove bigger-120 orange');
}

/**
 *
 * @param obj
 * @param options
 * @param rowObject
 */
var addStatusIcons = function (obj, options, rowObject) {

}

/**
 *
 * @param elem
 */
var datePick = function (elem) {
    $(elem).datepicker({
        duration: '',
        changeYear: true,
        showTime: $(this).hasClass('timepicker') ? true : false,
        constrainInput: false,
        stepMinutes: 1,
        stepHours: 1,
        dateFormat: 'D, d MM yy',
        altTimeField: '',
        time24h: true,
        onSelect: function (dateText, inst) {
            setTimeout(function () {
                $('#grid-table')[0].triggerToolbar();
            }, 100);
        }
    });
}
(function ($) {

    $(document).on("ready", function () {

        showDatePicker();

        grepSessionMessages();

        validateForm();

        setChosen();

        blogSlider();

        $("form").submit(function (e) {
            return getFormValidation($(this));
        });

        $(".file-wrapper input[type=file]")
            .bind("change focus click", SITE.fileInputs);


        $("div#awesome-dropzone").dropzone({
            url: "/auth/upload-avatar",
            clickable: true
        });

        $("#side-menu").metisMenu();

        $("select.languages").chosen();

        // Highlight the top nav as scrolling
        $("body").scrollspy({
            target: ".navbar-fixed-top",
            offset: 80
        });

        $(function () {
            $(window).bind("load resize", function () {
                if ($(this).width() < 769) {
                    $("body").addClass("body-small")
                } else {
                    $("body").removeClass("body-small")
                }
            })
        });

        $(document).on("keypress", "input,select,textarea", function (e) {
            if (e.which == 13) {
                if($(this).parents('form').length) {
                    $("#save-modal").click();
                    e.stopEventPropagation;
                    e.preventDefault();
                }
            }
        });

        $(document).on("keypress", window, function (e) {
            if (e.which == 13) {
                if($(this).parents('form').length) {
                    $("#save-modal").click();
                    e.stopEventPropagation;
                    e.preventDefault();
                }
            }
        });

        $(document).on("click", ".dropdown-menu li", function(){
            $(".dropdown-menu li").attr("data-status", "");
            $(this).attr("data-status", "active");

            var attr = $(this).attr('data-filter');
            var text;
            if (typeof attr !== typeof undefined && attr !== false) {
                text = $(this).find("a").text()
            } else {
                text = "Filter";
            }

            $("#drop1").find("span").html(text);

        });

        $(document).on("click", "#contributorCheck", function(){
            if ($("#contributorCheck").is(':checked')) {
                $("#contributor, #contributorAmount").parents('.form-group').show();

                if($("#contributor").val() == '') {
                    $("#contributor").val($("#guarantor").val());
                }

            } else {
                $("#contributor, #contributorAmount").parents('.form-group').hide();
            }

        });

        $(document).on("change", "#typeOfClient", function(){
            if ($(this).val() == 'LEGAL') {
                $("#invoice").parents('.form-group').show();
            } else {
                $("#invoice").parents('.form-group').hide();
            }

        });

        $(document).on("keyup.autocomplete", 'input#jmbg', function () {
            $(this).autocomplete({
                minLength: 12,
                dataType: "json",
                autoFocus: true,
                source: "/admin/clients/get-clients",
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.value);
                    $("#clientId").val(ui.item.id);
                    $("#firstName").val(ui.item.firstName);
                    $("#lastName").val(ui.item.lastName);
                    $("#phoneNumber").val(ui.item.phoneNumber);
                    $("#guarantor").val(ui.item.guarantor);
                    $("#guarantorPhone").val(ui.item.guarantorPhone);
                    $("#typeOfClient").val(ui.item.typeOfClient);

                    if(ui.item.smsNotification) {
                        $('.switchery').trigger('click');
                        $("#smsNotification").trigger("click");
                    }

                },
                change: function (event, ui) {
                    //$(this).val((ui.item ? ui.item.value : ""));
                    //if ($(this).val() == '') {
                    //    $("#personId").val('').trigger('change');
                    //}
                },
                _renderItem: function( ul, item ) {
                    return $( "<li>" )
                        .attr( "data-value", item.value )
                        .append( item.label )
                        .appendTo( ul );
                }
            });
        });

        $("#biography").summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']]
            ]
        });

        $(document).on("click", ".navbar-minimalize", function (e) {
            $("body").toggleClass("mini-navbar");
            smoothlyMenu();
        });

        $(document).on("click", ".bids-actions a:not(:last)", function (e) {
            e.preventDefault();
            $(".bids-actions a").removeClass("active");
            $(this).addClass("active");

            var action = $(this).attr("data-action");

            if (typeof action !== "undefined") {

                if (action == "five-minutes") {
                    var url = "/index/five-minutes-bids?page=1";
                } else {
                    var url = "/index/more-bid-requests?page=1";
                }

                var row = $(".bid-holder:first");

                $(".bid-holder:not(:first)").remove();

                $.get(url, function (response) {
                    var html = $(response).find(".bid-content").html();
                    row.replaceWith(html);
                });

            }
        });

        $("#keyword")
            .on("keyup", function () {
                if ($(this).val().length > 2) {
                    search($(this), $("#search-results"));
                }
            })
            .on("blur", function () {
                $("#search-results")
                    .animate({
                        opacity: "hide"
                    }, "slow")
            });

    });

    // Collapse ibox function
    $(document).on("click", ".collapse-link", function (e) {
        var ibox = $(this).closest("div.ibox");
        var button = $(this).find("i");
        var content = ibox.find("div.ibox-content");
        content.slideToggle(200);
        button.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down");
        ibox.toggleClass("").toggleClass("border-bottom");
        setTimeout(function () {
            ibox.resize();
            ibox.find("[id^=map-]").resize();
        }, 50);
    });

    $("#signinButton").click(function () {
        // signInCallback defined in step 6.
        auth2.grantOfflineAccess({'redirect_uri': "postmessage"}).then(signInCallback);
    });

    $(document).on("click", ".close-link", function (e) {
        var content = $(this).closest("div.ibox");
        content.remove();
    });

    $(window).bind("resize", function () {
        setChosen();
    }).trigger("resize");


    /**
     * Prevent form submit on enter
     */

    $(document).on("change", "input[type=file]", function() {
        $(this).next().html($(this).val());
    });

    /**
     * Choose role + sport category
     */
    $(document).on("click", ".role-box:not('input')", function (e) {

        $(this).find("input").prop("checked", true);

        $(".role-box").removeClass("active");
        $(this).addClass("active");

        $(".role-box").find("select").not($("select", this)).attr("disabled", true).hide();
        $("select", this).removeAttr("disabled").show();

    });

    $(document).on("click", "#load-more-bids", function (e) {

        e.preventDefault();

        var row = $(".bid-holder:last");
        var limit = $(this).attr("data-limit");
        var totalBidRequestInRow = $(".col-xs-12.col-md-4.col-sm-6", row).length;

        if (totalBidRequestInRow == 6 || totalBidRequestInRow == 12) {

            var currentAction = $(".bids-actions a.active").attr("data-action");
            var offset = $(row).attr("data-paginate");

            if (currentAction == "five-minutes") {
                var url = "/index/five-minutes-bids?page=" + (parseFloat(offset) + 1);
            } else {
                var url = "/index/more-bid-requests?page=" + (parseFloat(offset) + 1) + "&limit=" + parseFloat(limit);
            }

            $.get(url, function (response) {
                var html = $(response).find(".bid-content").html();
                row.append(html);
            });
        }

    });

    /**
     * AJAX submit modal form
     */
    $(document).on("click", "#save-modal", function (e) {

        e.preventDefault();

        var modal = $(this).parents("#modal");
        var form = $(modal).find("form");
        var isValid = getFormValidation($(form));
        var currentUrl = window.location.href;
        var button = $(this);

        var formData = new FormData();

        var formElements = form.find("input, select, textarea").not("input[type='file']");
        formElements.each(function () {
            var name = $(this).attr("name");
            if (!(undefined == name && name == null)) {
                formData.append(name, $(this).val());
            }
        });

        var files = $(form).find("input[type='file']");
        $(files).each(function () {
            if ("" != $(this).val()) {
                var name = $(this).attr("name");
                formData.append(name, $("#" + name)[0].files[0]);
            }
        });


        if (isValid) {
            $(button).addClass("disabled");
            $.ajax({
                url: $(form).attr("action"),
                type: "post",
                data: formData,
                dataType: "json",
                enctype: "multipart/form-data",
                processData: false,
                contentType: false,
                success: function (data) {

                    if (!data.error) {

                        if (typeof data.url !== "undefined") {
                            window.location = data.url;
                        } else {

                            if(typeof data.next !== "undefined") {
                                getCustomCreateForm('admin/payment-installments', 'purchase-id', data.next, e);
                            } else {
                                $.get(currentUrl, function (response) {
                                    var content = $(response).find("#content");
                                    $("#content").replaceWith(content);
                                    renderMessages(data.messages, "success");
                                    setChosen();
                                });

                            }

                            destroyModal(modal);

                        }

                    } else {
                        renderMessages(data.messages, "danger");
                        $(button).removeClass("disabled");
                        return false;
                    }
                },
                error: function (request, error) {
                    $.get(currentUrl, function (response) {
                        var content = $(response).find("#content");
                        var flashSession = $(response).find("#flash-session");
                        $("#content").replaceWith(content);
                        $("#flash-session").replaceWith(flashSession);
                        showPushMessage(t("An error was detected."), "error");
                        grepSessionMessages();
                        destroyModal(modal);
                    });
                }
            });
        }
    });

    $(document).on("click", ".destroy-modal", function (e) {
        e.preventDefault();
        var modal = $(this).parents("#modal");
        destroyModal(modal);
    });

})(jQuery);

/**
 *
 * @param {string} controller
 * @param {object} e
 */
function getCreateForm(controller, e) {
    var getUrl = "/" + controller + "/get-create-form";
    var postUrl = "/" + controller + "/create";
    getForm(getUrl, postUrl, e, "plus");
}

/**
 *
 * @param {string} controller
 * @param {int} id
 * @param {object} e
 */
function getUpdateForm(controller, id, e) {
    var getUrl = "/" + controller + "/get-update-form/" + id;
    var postUrl = "/" + controller + "/update/" + id;
    getForm(getUrl, postUrl, e, "edit");
}

/**
 *
 * @param {string} getUrl
 * @param {string} postUrl
 * @param {object} e
 */
function getCustomForm(getUrl, postUrl, e) {
    getForm(getUrl, postUrl, e, "plus");
}

/**
 *
 * @param {string} controller
 * @param {string} propertyName
 * @param {string} propertyValue
 * @param {object} e
 */
function getCustomCreateForm(controller, propertyName, propertyValue, e) {
    var getUrl = "/" + controller + "/get-create-form/" + propertyName + "/" + propertyValue;
    var postUrl = "/" + controller + "/create/" + propertyName + "/" + propertyValue;
    getForm(getUrl, postUrl, e, "plus");
}

/**
 *
 * @param {string} controller
 * @param {int} id
 * @param {object} e
 */
function getDeleteForm(controller, id, e) {

    e.preventDefault();

    //var event = e.srcElement || e.target;
    //var title = t("Delete");
    //
    //$(event).attr("data-backdrop", "static")
    //    .attr("data-toggle", "modal")
    //    .attr("data-target", "#modal");
    //
    //var modal = $("#modal");
    //var body = '<form id=ajax-form" action="/' + controller + '/delete/' + id + '" method="post">'
    //    + t("Are you sure that you want to delete this item?") + '</form>';
    //
    //$(modal).find(".modal-title").html("<i class ='fa fa-eraser'></i> " + title);
    //$(modal).find(".modal-body").html(body);
    //$(modal).find("#save-modal").removeClass("btn-success").addClass("btn-danger").html(t('Yes'));
    //$(modal).find("#close-modal").html(t('No'));
    //$("body").addClass("modal-open");
    //$(modal).show();
    var currentUrl = window.location.href;
    swal({
        title: t("Da li ste sigurni?!"),
        text: "",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: t("Odustani"),
        confirmButtonColor: "#DD6B55",
        confirmButtonText: t("Da, obriši!"),
        closeOnConfirm: true
    }, function () {
        $.ajax({
            url: "/" + controller + "/delete/" + id ,
            type: "post",
            data: {},
            dataType: "json",
            processData: false,
            contentType: false,
            success: function (data) {

                if (!data.error) {

                    if (typeof data.url !== "undefined") {
                        currentUrl = data.url;
                    }

                    $.get(currentUrl, function (response) {
                        var content = $(response).find("#content");
                        $("#content").replaceWith(content);
                        renderMessages(data.messages, "success");
                        setChosen();
                    });

                    destroyModal(modal);
                } else {
                    renderMessages(data.messages, "danger");
                    $(button).removeClass("disabled");
                    return false;
                }
            },
            error: function (request, error) {
                $.get(currentUrl, function (response) {
                    var content = $(response).find("#content");
                    var flashSession = $(response).find("#flash-session");
                    $("#content").replaceWith(content);
                    $("#flash-session").replaceWith(flashSession);
                    showPushMessage(t("An error was detected."), "error");
                    grepSessionMessages();
                    destroyModal(modal);
                });
            }
        });
    });
}

/**
 *
 * @param {string} url
 * @param {object} e
 */
function getAjaxForm(url, e) {

    e.preventDefault();

    var event = e.srcElement || e.target;
    var title = t("Potvrda");

    $(event).attr("data-backdrop", "static")
        .attr("data-toggle", "modal")
        .attr("data-target", "#modal");

    var modal = $("#modal");
    var body = '<form action="/' + url + '" method="post">'
        + t("Da li ste sigurni?") + '</form>';

    $(modal).find(".modal-title").html(title);
    $(modal).find(".modal-body").html(body);
    $(modal).find("#save-modal").html(t('Da'));
    $(modal).find("#close-modal").html(t('Ne'));
    $("body").addClass("modal-open");
    $(modal).show();
}

/**
 *
 * @param {string} getUrl
 * @param {string} postUrl
 * @param {object} e
 * @param {string} ico
 */
function getForm(getUrl, postUrl, e, ico) {

    e.preventDefault();
    toastr.clear();

    var event = e.srcElement || e.target;
    var title = "";
    var currentUrl = window.location.href;
    var modal = $("#modal");

    $(event).attr("data-backdrop", "static")
        .attr("data-toggle", "modal")
        .attr("data-target", "#modal");

    if (typeof title === "undefined" || title == "")
        title = $.trim($(event).text()) || $.trim($(event).parent().text());

    if (typeof title === "undefined" || title == "")
        title = $.trim($(event).attr("title")) || $.trim($(event).parent().attr("title"));

    if (typeof title === "undefined" || title == "")
        title = $.trim($(event).parents(".ibox").children(".ibox-title").find("h5").html());

    $.ajax({
        url: getUrl,
        type: "get",
        dataType: "json",
        success: function (data) {
            if (data.error) {
                if (data.messages) {
                    renderMessages(data.messages, "error");
                }
            } else {
                var body = renderForm(data, postUrl);
                $(modal).find(".modal-title").html("<i class='fa fa-" + ico + "'></i> " + title);
                $(modal).find(".modal-body").html(body);
                $("#amount").focus();
                $("#firstName").focus();
                validateForm();
                showDatePicker();
                initTextEditor();
                showExtraFields();
                $("body").addClass("modal-open");
                $(modal).show();
            }
        },
        error: function (request, error) {
            $.get(currentUrl, function (response) {
                var content = $(response).find("#content");
                $("#content").replaceWith(content);
                destroyModal(modal);
                setChosen();
                showPushMessage(t('An error was detected.'), 'error');
            });
        }
    });
}

/**
 *
 * @param {object} element
 * @returns {boolean}
 */
function getFormValidation(element) {
    var next = true;
    var required = $(element).find(".required");

    $(required).parents(".form-group").removeClass("has-error");
    $(element).parents(".form-group").find(".help-block").html("");
    $(required).each(function () {
        if ($(this).val() == "" || $(this).val() == "0") {
            $(this).parents(".form-group").addClass("has-error");
            $(this).parents(".form-group").find(".help-block").html(t("Obavezno Polje"));
            next = false;
        }
    });

    $("textarea").each(function () {
        if (html = $(this).next(".note-editor").find(".note-editable").html()) {
            $(this).html(html);
        }
    });

    return next;
}

/**
 *
 * @param {object} messages
 * @param {string} type
 */
function renderMessages(messages, type) {

    var modal = $("#modal");
    modal.find(".help-block").html(null);
    modal.find(".form-group").removeClass("has-error");
    $.each(messages, function (key, message) {
        if (!message.field) {
            showPushMessage(message.message, type);
            destroyModal(modal);
        } else {
            if (message.field == 'description') {
                $(".editor").parents('.form-group')
                    .addClass('has-error')
                    .find('.help-block').html(message.message);
            } else {
                var el = modal.find("#" + filterName(message.field));
                if (!el.length) {
                    var msg = '';
                    if (typeof message.message == "object") {
                        $.each(message.message, function (k, text) {
                            msg += '<div class="alert alert-' + type + ' alert-dismissable">'
                                + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                                + text
                                + '</div>';
                        });
                    } else {
                        msg = '<div class="alert alert-' + type + ' alert-dismissable">'
                            + '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'
                            + message.message
                            + '</div>';
                    }
                    $(modal).find(".flash-notification").append(msg);
                } else {
                    el.parents(".form-group")
                        .addClass("has-error")
                        .find(".help-block").html(message.message);
                }
            }
        }
    });
}

/**
 *
 * @param {object} data
 * @param {string} postUrl
 * @returns {string}
 */
function renderForm(data, postUrl) {
    var html = '<form id="ajax-form" class="form-horizontal" action="' + postUrl + '" method="post" enctype="multipart/form-data">';

    $.each(data, function (key, val) {

        var attributes = '';
        var required = '';

        if (val.label != '') {

            if (val.attributes != '') {
                $.each(val.attributes, function (attrKey, attrVal) {
                    if (attrKey === 'class') {
                        attrVal += ' form-control';
                    }
                    attributes += ' ' + attrKey + '="' + attrVal + '"';
                });
            } else {
                attributes = ' class="form-control"';
            }

            if (val.required) {
                required = '<span class="">*</span>';
            }

            html += '<div class="form-group">';
            html += '<label class="col-sm-4 control-label no-padding-right" for="' + filterName(val.name) + '">' + val.label + ': ' + required + '</label>';

            html += '<div class="col-sm-8">';

            if (val.type == 'text' || val.type == 'password') {
                html += '<input name="' + val.name + '" id="' + filterName(val.name) + '" type="' + val.type + '" value="' + val.default + '"' + attributes + ' />';
            } else if (val.type == 'date') {
                html += '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>'
                    + '<input name="' + val.name + '" id="' + filterName(val.name) + '" type="text" value="' + val.default + '"' + attributes + ' /></div>';
            } else if (val.type == 'email') {
                html += '<input name="' + val.name + '" id="' + filterName(val.name) + '" type="email" value="' + val.default + '"' + attributes + ' />';
            } else if (val.type == 'textarea') {
                html += '<textarea name="' + val.name + '" id="' + filterName(val.name) + '" style="resize:vertical;min-height:150px;" style="width:100%;" ' + attributes + '>' + val.default + '</textarea>';
            } else if (val.type == 'checkbox' || val.type == 'check') {
                html += '<input name="' + val.name + '" id="' + filterName(val.name) + '" type="checkbox" value="' + val.default + '" class="js-switch" ' + (val.default ? ' checked="checked"' : '') + ' />';
            } else if (val.type == 'file') {
                html += '<div class="upload"><i class="fa fa-upload"></i> ' + t(' Izaberi fajl');
                html += '<input name="' + val.name + '" id="' + filterName(val.name) + '" type="file"' + attributes + ' />';
                html += '<p></p>'
                html += '</div>';
            } else if (val.type == 'select' && val.options != undefined) {
                var arr = [];
                $.each(val.options, function (k, v) {
                    arr.push({key: k, value: v});
                });
                arr = $.grep(arr, function (elem, index) {
                    if (elem.key != 0 || elem.key != '') {
                        return elem.key
                    }
                });

                html += '<select name="' + val.name + '" id="' + filterName(val.name) + '"' + attributes + '>';
                $.each(arr, function (k, v) {

                    if ($.isPlainObject(v.value)) {
                        var optionGroup = v.key.substring(0, 1).toUpperCase() + v.key.substring(1);
                        html += '<optgroup label="' + optionGroup + '">';
                        $.each(v.value, function (optKey, optValue) {
                            if ($.trim(optKey) == val.default) {
                                selected = ' selected="selected"';
                            } else {
                                selected = '';
                            }

                            var value = optValue.substring(0, 1).toUpperCase() + optValue.substring(1);
                            html += '<option value="' + (0 != optKey ? optKey : '') + '"' + selected + '>' + value + '</option>';
                        });
                        html += '</optgroup>';
                    } else {
                        if ($.trim(v.key) == val.default) {
                            selected = ' selected="selected"';
                        } else {
                            selected = '';
                        }

                        var value = v.value.substring(0, 1).toUpperCase() + v.value.substring(1);
                        html += '<option value="' + (0 != v.key ? v.key : '') + '"' + selected + '>' + value + '</option>';
                    }
                });
                html += '</select>';
            }

            html += '<div class="help-block m-b-none"></div></div></div>';
        } else {
            if(val.type == 'hidden') {
                html += '<input name="' + val.name + '" id="' + filterName(val.name) + '" type="' + val.type + '" value="' + val.default + '"' + attributes + ' />';
            }
        }
    });

    html += '</form>';

    return html;
}

/**
 *
 */
function validateForm() {
    $("form .required").on("blur", function () {
        if ($(this).val() == "" || $(this).val() == "0") {
            $(this).parents(".form-group").addClass("has-error");
            $(this).parents(".form-group").find(".help-block").html(t("Obavezno Polje"));
        } else {
            $(this).parents(".form-group").removeClass("has-error");
            $(this).parents(".form-group").find(".help-block").html("");
        }
    });

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function (elem) {
        var check = elem;
        var switchery = new Switchery(elem, {color: "#1AB394"});
        elem.onchange = function () {
            $(check).attr('checked', check.checked);
            $(check).val(check.checked ? 1 : 0);
        }
    });
}

/**
 *
 * @param {object} modal
 */
function destroyModal(modal) {
    $(modal).find(".modal-title").html(null);
    $(modal).find(".modal-body").html(null);
    $(modal).find(".flash-notification").html(null);
    $(modal).find("#save-modal").removeClass("disabled");
    $(modal).hide();
    $("body").removeClass("modal-open");
    $(".modal-backdrop").remove();
}

/**
 *
 */
function showDatePicker() {

    $(".datepicker").datepicker({
        clearBtn: true,
        autoclose: true,
        format: "dd M yyyy",
        todayHighlight: true,
        weekStart: 1
    });

    $(".clockpicker").on("click", function () {
        $(this).clockpicker({autoclose: true}).clockpicker("show");
    });
}

/**
 *
 * @param {string} msg
 * @param {string} type
 */
function showPushMessage(msg, type) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "4000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "preventDuplicates": true
    };

    if (type == 'success') {
        toastr.success(msg);
    } else if (type == 'warning') {
        toastr.warning(msg);
    } else {
        toastr.error(msg);
    }
}

/**
 * {return} boolean
 */
function grepSessionMessages() {
    var sessionHolder = $("#flash-session");
    var returnValue = false;

    $(sessionHolder).hide();
    $(sessionHolder).find("div").each(function () {
        var msg = $(this).text();
        if ("" != msg) {
            returnValue = true;
            if ($(this).hasClass("alert-danger")) {
                showPushMessage(msg, "error");
            } else {
                showPushMessage(msg, "success");
            }
        }
    });

    return returnValue;
}

/**
 *
 */
function initTextEditor() {
    $("textarea").summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol', 'paragraph']]
        ]
    });
}

/**
 *
 * @param {string} string
 * @returns {string}
 */
function t(string) {
    var translations = {};

    if (string in translations) {
        return translations[string];
    }

    return string;
}

/**
 *
 */
function setChosen() {
    $(".chosen-select").chosen({
        placeholder_text_multiple: '---'
    });
}

/**
 *
 * @param {string} name
 * @returns {string}
 */
function filterName(name) {
    return name.replace(/\W+/gi, "-");
}

/**
 *
 */
function smoothlyMenu() {
    if (!$("body").hasClass("mini-navbar") || $("body").hasClass("body-small")) {
        // Hide menu in order to smoothly turn on when maximize menu
        $("#side-menu").hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $("#side-menu").fadeIn(500);
            }, 100);
    } else if ($("body").hasClass("fixed-sidebar")) {
        $("#side-menu").hide();
        setTimeout(
            function () {
                $("#side-menu").fadeIn(500);
            }, 300);
    } else {
        $("#side-menu").removeAttr("style");
    }
}

/**
 *
 * @param {object} elem
 * @param {object} appendTo
 */
function search(elem, appendTo) {

    var msgNoResults = "<div class='no-results'>" + t('No result found...') + "</div>";

    $.ajax({
        url: "/searches/search",
        data: {'term': elem.val()},
        type: "post",
        dataType: "json",
        success: function (response) {

            appendTo.html(null);

            if (response.length > 0) {
                $.each(response, function (index, item) {
                    var resultHtml = '<a href="' + item.url + '" >' + item.name + '</a>';
                    appendTo.append(resultHtml);
                });

            } else {
                appendTo.html(msgNoResults);
            }

            appendTo.animate({
                opacity: "show"
            }, "slow");
        },
        error: function () {
            appendTo
                .html(msgNoResults)
                .animate({
                    opacity: "show"
                }, "slow");
        }
    });
}


/**
 *
 * @param authResult
 */
function signInCallback(authResult) {
    if (authResult['code']) {

        // Send the code to the server
        $.ajax({
            type: "POST",
            url: "/auth/google-login",
            contentType: "application/octet-stream; charset=utf-8",
            success: function (result) {

                if (!result.error) {
                    window.location = '/' + result.url;
                } else {
                    renderMessages(result.messages, "danger");
                }
            },
            processData: false,
            data: authResult["code"]
        });
    }
}

/**
 *
 */
function facebookLogin() {

    FB.login(function (response) {

        if (response.authResponse) {

            FB.getLoginStatus(function (response) {

                if (response.status === "connected") {

                    $.ajax({
                        type: "POST",
                        url: "/auth/facebook-login",
                        data: response,
                        success: function (result) {

                            if (!result.error) {
                                window.location = '/' + result.url;
                            } else {
                                renderMessages(result.messages, "danger");
                            }
                        }
                    });

                } else if (response.status === "not_authorized") {
                    showPushMessage(t("We could not login via facebook account!"), "danger");
                }

            });
        }
    }, {scope: "publish_actions"});
}

function checkLoginState() {

    FB.getLoginStatus(function (response) {

        if (response.status === "connected") {
            $.ajax({
                type: "POST",
                url: "/auth/facebook-login",
                data: response,
                success: function (result) {

                    if (!result.error) {

                        if (result.url.length == 0) {
                            result.url = '/';
                        }

                        window.location = result.url;
                    } else {
                        renderMessages(result.messages, "danger");
                    }
                }
            });

        } else if (response.status === "not_authorized") {
            showPushMessage(t("We could not login via facebook account!"), "danger");
        }

    });
}


// A function to create the marker and set up the event window function
function createMarker(latlng, name) {
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        zIndex: Math.round(latlng.lat() * -100000) << 5
    });

    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
    google.maps.event.trigger(marker, 'click');
    return marker;
}

var SITE = SITE || {};

/**
 *
 */
SITE.fileInputs = function () {
    var $this = $(this),
        $val = $this.val(),
        valArray = $val.split('\\'),
        newVal = valArray[valArray.length - 1],
        $button = $this.siblings(".button"),
        $fakeFile = $this.siblings(".file-holder");
    if (newVal !== "") {
        $button.text("Photo Chosen");
        if ($fakeFile.length === 0) {
            $button.before('' + newVal + '');
        } else {
            $fakeFile.text(newVal);
        }
    }
};


/**
 *
 */
function initFunctions() {
    if (typeof initCalendar == "function") {
        initCalendar();
    }
}

function blogSlider () {

    var dotsHolder = $(".slider-dots");
    if(dotsHolder.length > 0) {
        var dots = $("a", dotsHolder);

        $(dotsHolder).each(function(index, element) {

            var dots = $("a", $(element));
            setTimeout(function () {
                triggerClick(dots, 1, element)
            }, 5000);
        });



    }
}

function triggerClick(selector, index, element) {

    var parent = $(element);
    var buttons = $(selector, parent);
    var mod = index++ % $(buttons).length;
    var activeDot = $(buttons[mod], parent);
    var holder = parent.parents(".col-sm-4");

    $(buttons, parent).removeClass("active");


    var dataSlide = activeDot.attr("data-dot");
    activeDot.trigger("click");
    if(!activeDot.hasClass("active")) {
        activeDot.addClass("active");
    }

    var activeSlide = $(holder).find('.slider-box').filter("[data-slide='" + dataSlide + "']");

    $(holder).find('.slider-box').hide();
    activeSlide.show();

    setTimeout(function () {
        triggerClick(selector, index, parent);
    }, 5000);
}


function showExtraFields()
{
    if($("#contributorCheck").length > 0) {
        if (!$('#contributorCheck').is(':checked')) {
            $("#contributor, #contributorAmount").parents('.form-group').hide();
        }
    }

    if($("#invoice").length > 0) {
        $("#invoice").parents('.form-group').hide();
    }

}

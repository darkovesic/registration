<?php

namespace Backend;

use Comit\Ajax\Handler as AjaxHandler;
use Comit\Error\HandlerInterface;
use Comit\Helper\Common as CommonSettings;
use Comit\Messenger\Manager as MessengerManager;
use Comit\Messenger\Message as MessengerMessage;
use Phalcon\DiInterface;
use Phalcon\Events\Event;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Loader;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\View;

/**
 * Class Backend Module
 *
 * @package Backend
 */
class Module implements ModuleDefinitionInterface
{

    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();
        $loader->registerNamespaces(array(
            'Sms\Controllers' => __DIR__ . '/controllers/'
        ));
        $loader->registerDirs(array(
            __DIR__ . '/controllers/',
        ));
        $loader->register();
    }

    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        // Init dispatcher
        $this->_initDispatcher($di);

        // Init view
        $this->_initView($di);
    }

    /**
     * @param DiInterface $di
     */
    protected function _initDispatcher(DiInterface $di)
    {
        /** @var EventsManager $eventsManager */
        $eventsManager = $di->getShared('eventsManager');

        $eventsManager->attach('dispatch:beforeException',
            function (Event $event, Dispatcher $dispatcher, \Exception $exception) use ($di) {

                /** @var MessengerManager $messengerManager */
                $messengerManager = $di->getShared('messengerManager');
                $messengerManager->appendMessage(new MessengerMessage(
                    $exception->getMessage(),
                    null,
                    MessengerMessage::DANGER
                ));

                switch ($exception->getCode()) {

                    case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND :
                    case Dispatcher::EXCEPTION_ACTION_NOT_FOUND :
                        $dispatcher->forward(array(
                            'controller' => 'errors',
                            'action' => 'show404',
                        ));

                        return false;

                    default :

                        /** @var HandlerInterface $errorHandler */
                        $errorHandler = $di->getShared('errorHandler');
                        $errorHandler->handleException($exception);

                        $dispatcher->forward(array(
                            'controller' => 'errors',
                            'action' => 'show500',
                        ));

                        return false;
                }
            }
        );

        $eventsManager->attach('dispatch', new AjaxHandler());

        /** @var Dispatcher $dispatcher */
        $dispatcher = $di->getShared('dispatcher');
        $dispatcher->setDefaultNamespace('Backend\Controllers');
        $dispatcher->setEventsManager($eventsManager);
    }

    /**
     * @param DiInterface $di
     */
    protected function _initView(DiInterface $di)
    {
        /** @var View $view */
        $view = $di->getShared('view');
        $view->setViewsDir(__DIR__ . '/views/');
        $view->setLayoutsDir('layouts/');
        $view->setTemplateAfter('sms');
        $view->setPartialsDir('');

        $locale = 'en_US';
        $user = null;

        /** @var \Phalcon\Session\AdapterInterface $session */
        if ($session = $di->getShared('session')) {
            if ($currentUser = $session->get('currentUser')) {
                $user = \User::findFirst(array(
                    \User::PROPERTY_ID . ' = :id:',
                    'bind' => array(
                        'id' => $currentUser['id']
                    )
                ));

                if (isset($currentUser['locale'])) {
                    $locale = $currentUser['locale'];
                }
            }
        }

        $view->setVar('currentUser', $user);
        $view->setVar('locale', $locale);
        $view->setVar('commonSettings', (new CommonSettings())->toSimpleObject());

        /** @var View\Simple $simpleView */
        $simpleView = $di->getShared('simpleView');
        $simpleView->setViewsDir(__DIR__ . '/views/');
    }

}

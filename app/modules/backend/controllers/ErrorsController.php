<?php

namespace Backend\Controllers;

use Comit\Controller\Base as BaseController;

/**
 * Class ErrorsController
 *
 * @package Backend\Controllers
 */
class ErrorsController extends BaseController
{

    /**
     *
     */
    public function show404Action()
    {
        $this->response->resetHeaders()
            ->setStatusCode(404)
            ->setHeader(404, 'Not found');

        $this->view->setTemplateAfter('single');
        $this->view->pick('errors/404');

        $this->_setTitle($this->_translate('Not found'));
    }

    /**
     *
     */
    public function show500Action()
    {
        $this->response->resetHeaders()
            ->setStatusCode(500)
            ->setHeader(500, 'Internal server error');

        $this->view->setTemplateAfter('single');
        $this->view->pick('errors/500');

        $this->_setTitle($this->_translate('Internal server error'));
    }

    /**
     *
     */
    public function show401Action()
    {
        $this->response->resetHeaders()
            ->setStatusCode(401)
            ->setHeader(401, 'Not authorized request');

        $this->view->setTemplateAfter('single');
        $this->view->pick('errors/401');

        $this->_setTitle($this->_translate('Not authorized request'));
    }

}

<?php

namespace Backend\Controllers;

use Comit\Controller\Base as BaseController;
use Comit\Form\Renderer\Json;
use Comit\Helper\DateTime;
use Comit\Messenger\Manager;
use Comit\Messenger\Message;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use mikehaertl\wkhtmlto\Pdf;

/**
 * Class DashboardController
 *
 * @package Backend\Controllers
 */
class RegistrationsController extends BaseController
{

    public function indexAction()
    {

    }

    /**
     * @return string
     */
    public function getCreateFormAction()
    {
        $client = new \Client();
        $form = $client->getForm();

        $date = new DateTime();

        $registration = new \Purchase();
        $formRegistration = $registration->getForm(array(
            \Purchase::PROPERTY_PURCHASE_TYPE_ID,
            \Purchase::PROPERTY_EXTRA,
            \Purchase::PROPERTY_CLIENT_ID,
            \Purchase::PROPERTY_QUANTITY,
        ));

        foreach ($formRegistration->getElements() as $element) {

            if($element->getName() === 'invoice') {
                $form->add($element, 'typeOfClient', false);
            } else {
                $form->add($element);
            }
        }

        $clientId = new Hidden('clientId');
        $form->add($clientId);

        $contributor = new Check('contributorCheck');
        $contributor->setLabel('Dodaj Saradnika');

        $form->add($contributor,'contributor', true);

        if($form->has('date')) {
            $form->get('date')->setDefault($date->format('d.m.Y'));
        }

        $renderer = new Json();
        return $renderer->render($form);
    }

    /**
     * Creates new Purchase
     */
    public function createAction()
    {

        if ($this->request->isPost()) {

            $postData = $this->request->getPost();

            $client = null;
            if(isset($postData['clientId']) && $postData['clientId']) {
                $client = \Client::findFirst(
                    array(
                        \Client::PROPERTY_ID . ' = :clientId:',
                        'bind' => array(
                            'clientId' => $postData['clientId']
                        )
                    )
                );
            }

            if(!$client) {
                $client = new \Client();
            }

            $purchase = new \Purchase();

            if (count($postData)) {

                $postData[\Purchase::PROPERTY_PURCHASE_TYPE_ID] = 1;
                $postData[\Purchase::PROPERTY_CLIENT_ID] = 0;
                $postData[\Purchase::PROPERTY_QUANTITY] = 1;
                $postData[\Purchase::PROPERTY_MODEL] = isset($postData[\Purchase::PROPERTY_MODEL]) ? $postData[\Purchase::PROPERTY_MODEL] : '';
                $postData[\Purchase::PROPERTY_AUTHORIZATION] = isset($postData[\Purchase::PROPERTY_AUTHORIZATION]) && $postData[\Purchase::PROPERTY_AUTHORIZATION] ?  : 0;
                $postData[\Client::PROPERTY_SMS_NOTIFICATION] = isset($postData[\Client::PROPERTY_SMS_NOTIFICATION]) && $postData[\Client::PROPERTY_SMS_NOTIFICATION] ? 1 : 0;

                $form = $client->getForm();
                $formPurchase = $purchase->getForm();

                if ($form->isValid($postData) && $formPurchase->isValid($postData)) {

                    foreach ($postData as $property => $value) {
                        $client->setProperty($property, $value);
                        $purchase->setProperty($property, $value);
                    }

                    $client->save();

                    $purchase->setClient($client);
                    $purchase->save();

                    if($this->request->hasFiles()) {

                        if(array_key_exists(0, $this->request->getUploadedFiles(true))) {
                            $file = $this->request->getUploadedFiles(true)[0];
                            $purchase->moveFile($file, $purchase->getPlates());
                        }
                    }

                    /** @var Manager $messengerManager */
                    $messengerManager = $this->getDI()->getShared('messengerManager');

                    $messengerManager->appendMessage(new Message(
                        $this->_translate('Uspesno ste uneli podatke!!!'),
                        '',
                        Message::SUCCESS
                    ));

                    $this->_setRedirectUrl('registrations/overview/' . $purchase->getId());
                }
            }
        }

    }

    /**
     * @param int $id
     * @return bool|string
     */
    public function getUpdateFormAction($id)
    {
        /** @var \Purchase $purchase */
        $purchase = \Purchase::findFirst(array(
            \Purchase::PROPERTY_ID . ' = :id:',
            'bind' => array(
                'id' => $id
            )
        ));

        $date = new DateTime();

        $formRegistration = $purchase->getForm(array(
            \Purchase::PROPERTY_PURCHASE_TYPE_ID,
            \Purchase::PROPERTY_EXTRA,
            \Purchase::PROPERTY_CLIENT_ID,
            \Purchase::PROPERTY_QUANTITY,
        ));



        if ($purchase && $client = $purchase->getClient()) {
            $form = $client->getForm();



            foreach ($formRegistration->getElements() as $element) {
                if($element->getName() == 'invoice') {
                    $form->add($element, 'typeOfClient', false);
                } else {
                    $form->add($element);
                }
            }

            foreach ($formRegistration->getElements() as $element) {

                if($element->getName() == 'invoice') {
                    $form->add($element, 'typeOfClient', false);
                } else {
                    $form->add($element);
                }
            }

            $contributor = new Check('contributorCheck');
            $contributor->setLabel('Dodaj Saradnika');

            if($purchase->getContributor()) {
                $contributor->setDefault('1');
            }

            $form->add($contributor,'contributor', true);

            if($form->has('date')) {
                $date = $purchase->getDate() ? $purchase->getDate() : new DateTime();
                $form->get('date')->setDefault($date->format('d.m.Y'));
            }

            $renderer = new Json();
            return $renderer->render($form);
        } else {
            return $this->_returnNotFound();
        }

    }

    /**
     * Updates matched Purchase
     *
     * @param $id
     * @return bool|void
     */
    public function updateAction($id)
    {
        if ($this->request->isPost()) {

            /** @var \Purchase $purchase */
            $purchase = \Purchase::findFirst(array(
                \Purchase::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $id
                )
            ));

            if (!$purchase || !$client = $purchase->getClient()) {
                return $this->_returnNotFound();
            }

            $postData = $this->request->getPost();

            if (count($postData)) {

                $form = $client->getForm();
                $formPurchase = $purchase->getForm();

                $postData[\Purchase::PROPERTY_PURCHASE_TYPE_ID] = $purchase->getPurchaseTypeId();
                $postData[\Purchase::PROPERTY_CLIENT_ID] = $purchase->getClientId();
                $postData[\Purchase::PROPERTY_QUANTITY] = $purchase->getQuantity();
                $postData[\Client::PROPERTY_SMS_NOTIFICATION] = isset($postData[\Client::PROPERTY_SMS_NOTIFICATION]) && $postData[\Client::PROPERTY_SMS_NOTIFICATION] ? 1 : 0;
                $postData[\Purchase::PROPERTY_MODEL] = isset($postData[\Purchase::PROPERTY_MODEL]) && $postData[\Purchase::PROPERTY_MODEL] ? $postData[\Purchase::PROPERTY_MODEL] : '';

                if ($form->isValid($postData) && $formPurchase->isValid($postData)) {

                    foreach ($postData as $property => $value) {
                        $client->setProperty($property, $value);
                        $purchase->setProperty($property, $value);
                    }

                    $client->save();
                    $purchase->save();

                    if($this->request->hasFiles()) {

                        if(array_key_exists(0, $this->request->getUploadedFiles(true))) {
                            $file = $this->request->getUploadedFiles(true)[0];
                            $purchase->moveFile($file, $purchase->getPlates());
                        }
                    }

                    /** @var Manager $messengerManager */
                    $messengerManager = $this->getDI()->getShared('messengerManager');

                    $messengerManager->appendMessage(new Message(
                        $this->_translate('Uspesno ste izmeni podatke!'),
                        '',
                        Message::SUCCESS
                    ));

                    $this->_setRedirectUrl('registrations/overview/' . $purchase->getId());

                }
            }
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function overviewAction($id)
    {

        /** @var \Purchase $purchase */
        $purchase = \Purchase::findFirst(array(
            \Purchase::PROPERTY_ID . ' = :id:',
            'bind' => array(
                'id' => $id
            )
        ));

        if (!$purchase) {
            return $this->_returnNotFound();
        }

        $this->view->purchase = $purchase;
    }

    /**
     * @param $purchaseId
     */
    public function printFileAction($purchaseId)
    {

        /** @var \Purchase $purchase */
        $purchase = \Purchase::findFirst(
            array(
                \Purchase::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $purchaseId
                )
            )
        );

        if(!$purchase) {
            $this->_returnNotFound();
        }

        $params = array(
            'today' => (new DateTime())->format('d.m.Y'),
            'purchase' => $purchase
        );

        /** @var $view \Phalcon\Mvc\View\Simple */
        $view = $this->getDI()->get('simpleView');
        $html = $view->render('registrations/print/print', $params);

        $pdf = new Pdf($html);
        $pdf->send();

    }

}

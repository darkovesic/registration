<?php

namespace Backend\Controllers;

use Comit\Controller\Base as BaseController;
use Comit\Form\Renderer\Json;
use Comit\Helper\DateTime;
use Comit\Messenger\Manager;
use Comit\Messenger\Message;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use mikehaertl\wkhtmlto\Pdf;

/**
 * Class DashboardController
 *
 * @package Backend\Controllers
 */
class TirePartsController extends BaseController
{

    public function indexAction()
    {
        $this->assets->collection('headJs')
            ->addJs(APP_PATH . 'assets/js/plugins/jq-grid.min.js')
            ->addJs(APP_PATH . 'assets/js/plugins/jqgrid.locale-en.js')
            ->addJs(APP_PATH . 'assets/js/comit/jquery.jqgrid.functions.js');

        $this->assets->collection('headCss')
            ->addCss(APP_PATH . 'assets/css/jq-grid.css');
    }

    /**
     * @return string
     */
    public function getCreateFormAction()
    {
        $client = new \Client();
        $form = $client->getForm();

        $date = new DateTime();

        $registration = new \Purchase();

        $formRegistration = $registration->getForm(array(
            \Purchase::PROPERTY_PURCHASE_TYPE_ID,
            \Purchase::PROPERTY_CLIENT_ID,
            \Purchase::PROPERTY_LEVEL,
            \Purchase::PROPERTY_PLATES,
            \Purchase::PROPERTY_REGISTERED_UNTIL,
            \Purchase::PROPERTY_CONTRIBUTOR,
            \Purchase::PROPERTY_CONTRIBUTOR_AMOUNT,
            \Purchase::PROPERTY_TECHNICAL,
            \Purchase::PROPERTY_TECHNICAL_AMOUNT
        ));

        foreach($formRegistration->getElements() as $element) {

            if($element->getLabel() == 'Model') {
                $element->setLabel('Vrsta');
            }

            if($element->getName() == 'invoice') {
                $form->add($element, 'typeOfClient', false);
            } else {
                $form->add($element);
            }

        }

        foreach ($formRegistration->getElements() as $element) {

            if($element->getName() == 'invoice') {
                $form->add($element, 'typeOfClient', false);
            } else {
                $form->add($element);
            }
        }

        if($form->has('date')) {
            $form->get('date')->setDefault($date->format('d.m.Y'));
        }

        $clientId = new Hidden('clientId');
        $form->add($clientId);

        $renderer = new Json();
        return $renderer->render($form);
    }

    /**
     * Creates new SpokenLanguage
     */
    public function createAction()
    {

        if ($this->request->isPost()) {

            $postData = $this->request->getPost();

            $client = null;
            if(isset($postData['clientId']) && $postData['clientId']) {
                $client = \Client::findFirst(
                    array(
                        \Client::PROPERTY_ID . ' = :clientId:',
                        'bind' => array(
                            'clientId' => $postData['clientId']
                        )
                    )
                );
            }

            if(!$client) {
                $client = new \Client();
            }

            $purchase = new \Purchase();

            if (count($postData)) {

                $postData[\Purchase::PROPERTY_PURCHASE_TYPE_ID] = 2;
                $postData[\Purchase::PROPERTY_CLIENT_ID] = 0;
                $postData[\Purchase::PROPERTY_PLATES] = ' ';
                $postData[\Purchase::PROPERTY_AUTHORIZATION] = isset($postData[\Purchase::PROPERTY_AUTHORIZATION]) && $postData[\Purchase::PROPERTY_AUTHORIZATION] ?  : 0;
                $postData[\Client::PROPERTY_SMS_NOTIFICATION] = isset($postData[\Client::PROPERTY_SMS_NOTIFICATION]) && $postData[\Client::PROPERTY_SMS_NOTIFICATION] ? 1 : 0;

                $clientForm = $client->getForm();
                $purchaseForm = $purchase->getForm();

                if ($clientForm->isValid($postData) && $purchaseForm->isValid($postData)) {

                    foreach ($postData as $property => $value) {
                        $client->setProperty($property, $value);
                        $purchase->setProperty($property, $value);
                    }
                    $client->save();

                    $purchase->setClient($client);
                    $purchase->save();

                    if($this->request->hasFiles()) {

                        if(array_key_exists(0, $this->request->getUploadedFiles(true))) {
                            $file = $this->request->getUploadedFiles(true)[0];
                            $purchase->moveFile($file, $purchase->getId());
                        }
                    }

                    /** @var Manager $messengerManager */
                    $messengerManager = $this->getDI()->getShared('messengerManager');

                    $messengerManager->appendMessage(new Message(
                        $this->_translate('Uspesno ste uneli podatke!!!'),
                        '',
                        Message::SUCCESS
                    ));

                    $this->_setRedirectUrl('tire-parts/overview/' . $purchase->getId());


                }
            }
        }

    }

    /**
     * @param int $id
     * @return bool|string
     */
    public function getUpdateFormAction($id)
    {
        /** @var \Purchase $purchase */
        $purchase = \Purchase::findFirst(array(
            \Purchase::PROPERTY_ID . ' = :id:',
            'bind' => array(
                'id' => $id
            )
        ));

        $formRegistration = $purchase->getForm(array(
            \Purchase::PROPERTY_PURCHASE_TYPE_ID,
            \Purchase::PROPERTY_CLIENT_ID,
            \Purchase::PROPERTY_LEVEL,
            \Purchase::PROPERTY_PLATES,
            \Purchase::PROPERTY_REGISTERED_UNTIL,
            \Purchase::PROPERTY_CONTRIBUTOR,
            \Purchase::PROPERTY_CONTRIBUTOR_AMOUNT,
            \Purchase::PROPERTY_TECHNICAL,
            \Purchase::PROPERTY_TECHNICAL_AMOUNT,
        ));

        if($purchase && $client = $purchase->getClient()) {
            $form = $client->getForm();

            foreach($formRegistration->getElements() as $element) {

                if($element->getLabel() == 'Model') {
                    $element->setLabel('Vrsta');
                }

                if($element->getName() == 'invoice') {
                    $form->add($element, 'typeOfClient', false);
                } else {
                    $form->add($element);
                }

            }

            foreach ($formRegistration->getElements() as $element) {

                if($element->getName() == 'invoice') {
                    $form->add($element, 'typeOfClient', false);
                } else {
                    $form->add($element);
                }
            }

            $renderer = new Json();
            return $renderer->render($form);
        } else {
            return $this->_returnNotFound();
        }

    }

    /**
     * Updates matched Purchase
     *
     * @param $id
     * @return bool|void
     */
    public function updateAction($id)
    {
        if ($this->request->isPost()) {

            /** @var \Purchase $purchase */
            $purchase = \Purchase::findFirst(array(
                \Purchase::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $id
                )
            ));

            if (!$purchase || !$client = $purchase->getClient()) {
                return $this->_returnNotFound();
            }

            $postData = $this->request->getPost();

            if (count($postData)) {

                $form = $client->getForm();
                $formPurchase = $purchase->getForm();

                $postData[\Purchase::PROPERTY_PURCHASE_TYPE_ID] = $purchase->getPurchaseTypeId();
                $postData[\Purchase::PROPERTY_CLIENT_ID] = $purchase->getClientId();
                $postData[\Purchase::PROPERTY_PLATES] = ' ';
                $postData[\Client::PROPERTY_SMS_NOTIFICATION] = isset($postData[\Client::PROPERTY_SMS_NOTIFICATION]) && $postData[\Client::PROPERTY_SMS_NOTIFICATION] ? 1 : 0;

                if ($form->isValid($postData) && $formPurchase->isValid($postData)) {

                    foreach ($postData as $property => $value) {
                        $client->setProperty($property, $value);
                        $purchase->setProperty($property, $value);
                    }

                    $client->save();
                    $purchase->save();

                    if($this->request->hasFiles()) {

                        if(array_key_exists(0, $this->request->getUploadedFiles(true))) {
                            $file = $this->request->getUploadedFiles(true)[0];
                            $purchase->moveFile($file, $purchase->getId());
                        }
                    }

                    /** @var Manager $messengerManager */
                    $messengerManager = $this->getDI()->getShared('messengerManager');

                    $messengerManager->appendMessage(new Message(
                        $this->_translate('Uspesno ste izmeni podatke!'),
                        '',
                        Message::SUCCESS
                    ));

                    $this->_setRedirectUrl('tire-parts/overview/' . $purchase->getId());

                }
            }
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function overviewAction($id)
    {
        /** @var \Purchase $purchase */
        $purchase = \Purchase::findFirst(array(
            \Purchase::PROPERTY_ID . ' = :id:',
            'bind' => array(
                'id' => $id
            )
        ));

        if (!$purchase) {
            return $this->_returnNotFound();
        }

        $this->view->purchase = $purchase;
    }


    /**
     * @param $purchaseId
     */
    public function printFileAction($purchaseId)
    {

        $purchase = \Purchase::findFirst(
            array(
                \Purchase::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $purchaseId
                )
            )
        );

        if(!$purchase) {
            $this->_returnNotFound();
        }
        //die('dsfs');

        $params = array(
            'today' => (new DateTime())->format('d.m.Y'),
            'purchase' => $purchase
        );

        /** @var $view \Phalcon\Mvc\View\Simple */
        $view = $this->getDI()->get('simpleView');
        $html = $view->render('tire-parts/print/print', $params);

        $pdf = new Pdf($html);
        $pdf->send();

        ///var/www/html/registration/app/modules/backend/views/tire-parts/print/print.volt

    }


}

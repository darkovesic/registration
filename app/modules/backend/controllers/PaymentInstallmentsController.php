<?php

namespace Backend\Controllers;

use Comit\Controller\Base as BaseController;
use Comit\Form\Form;
use Comit\Form\Renderer\Json;
use Comit\Helper\DateTime;
use Phalcon\Forms\Element\Text;
use Comit\Messenger\Manager;
use Comit\Messenger\Message;

class PaymentInstallmentsController extends BaseController
{

    public function registrationAction()
    {
        $this->view->today = (new DateTime())->format('d.m.Y');
    }

    public function partsAction()
    {
        $this->view->today = (new DateTime())->format('d.m.Y');
    }

    public function onestaPartsAction()
    {
        $this->view->today = (new DateTime())->format('d.m.Y');
    }

    /**
     * @return string
     */
    public function getCreateFormAction()
    {
        $params = $this->dispatcher->getParams();
        if(count($params) < 2 && $params[0] != 'purchase-id') {
            return $this->_returnNotFound();
        }

        $purchase = \Purchase::findFirst(
            array(
                \Purchase::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $params[1]
                )
            )
        );

        if(!$purchase) {
            return $this->_returnNotFound();
        }

        $paymentInstallments = $purchase->getPaymentInstallments();
        $date = new DateTime();

        /** @var \PaymentInstallment $lastPaymentInstalment */
        if($lastPaymentInstalment = $paymentInstallments->getLast()) {
            $date = clone $lastPaymentInstalment->getDate();

            if((int)$date->format('d') == 31) {
                $date->add(new \DateInterval('P3D'));
                $date = new DateTime("last day of {$date->format('F Y')}");
            } else {
                $date->add(new \DateInterval('P1M'));
            }
        }

        $paymentInstallment = new \PaymentInstallment();
        $form = $paymentInstallment->getForm(
            array(
                \PaymentInstallment::PROPERTY_PURCHASE_ID,
                \PaymentInstallment::PROPERTY_PAYMENT_DATE,
            )
        );

        if($form->has('date')) {
            $form->get('date')->setDefault($date->format('d.m.Y'));
        }

        $renderer = new Json();
        return $renderer->render($form);
    }

    /**
     * @return bool
     */
    public function createAction()
    {

        if ($this->request->isPost()) {

            $params = $this->dispatcher->getParams();
            if(count($params) < 2 && $params[0] != 'purchase-id') {
                return $this->_returnNotFound();
            }

            $purchase = \Purchase::findFirst(
                array(
                    \Purchase::PROPERTY_ID . ' = :id:',
                    'bind' => array(
                        'id' => $params[1]
                    )
                )
            );

            if(!$purchase) {
                return $this->_returnNotFound();
            }

            $postPayment = $this->request->getPost();

            if (count($postPayment)) {

                $paymentInstallment = new \PaymentInstallment();

                $form = $paymentInstallment->getForm(array(
                    \PaymentInstallment::PROPERTY_PAYMENT_DATE,
                    \PaymentInstallment::PROPERTY_PURCHASE_ID,
                ));

                $postPayment[\PaymentInstallment::PROPERTY_PURCHASE_ID] = $purchase->getId();

                if ($form->isValid($postPayment)) {

                    foreach ($postPayment as $property => $value) {
                        $paymentInstallment->setProperty($property, $value);
                    }

                    $paymentInstallment->save();

                    /** @var Manager $messengerManager */
                    $messengerManager = $this->getDI()->getShared('messengerManager');

                    $messengerManager->appendMessage(new Message(
                        $this->_translate('Uspesno ste uneli ratu!'),
                        '',
                        Message::SUCCESS
                    ));

                }
            }
        }
    }

    /**
     * @param $id
     * @return bool|string
     */
    public function getUpdateFormAction($id)
    {
        /** @var \PaymentInstallment $payment */
        $payment = \PaymentInstallment::findFirst(array(
            \PaymentInstallment::PROPERTY_ID . ' = :id:',
            'bind' => array(
                'id' => $id
            )
        ));

        if ($payment) {
            $form = $payment->getForm(array(
                \PaymentInstallment::PROPERTY_PAYMENT_DATE,
                \PaymentInstallment::PROPERTY_PURCHASE_ID,
            ));


            $renderer = new Json();
            return $renderer->render($form);
        } else {
            return $this->_returnNotFound();
        }

    }

    public function updateAction($id)
    {
        if ($this->request->isPost()) {

            /** @var \PaymentInstallment $payment */
            $payment = \PaymentInstallment::findFirst(array(
                \PaymentInstallment::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $id
                )
            ));

            if (!$payment) {
                return $this->_returnNotFound();
            }

            $postPayment = $this->request->getPost();

            if (count($postPayment)) {

                $form = $payment->getForm(array(
                    \PaymentInstallment::PROPERTY_PAYMENT_DATE,
                    \PaymentInstallment::PROPERTY_PURCHASE_ID,
                ));

                if ($form->isValid($postPayment)) {

                    foreach ($postPayment as $property => $value) {
                        $payment->setProperty($property, $value);
                    }

                    $payment->save();

                    /** @var Manager $messengerManager */
                    $messengerManager = $this->getDI()->getShared('messengerManager');

                    $messengerManager->appendMessage(new Message(
                        $this->_translate('Uspesno ste izmenili podatke!'),
                        '',
                        Message::SUCCESS
                    ));

                }
            }
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function addPaymentAction($id)
    {
        if ($this->request->isPost()) {

            /** @var \PaymentInstallment $payment */
            $payment = \PaymentInstallment::findFirst(array(
                \PaymentInstallment::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $id
                )
            ));

            if (!$payment) {
                return $this->_returnNotFound();
            }

            $payment->setPaymentDate(new DateTime());
            $payment->save();

            /** @var Manager $messengerManager */
            $messengerManager = $this->getDI()->getShared('messengerManager');

            $messengerManager->appendMessage(new Message(
                $this->_translate('Uspesno evidentirali uplatu!'),
                '',
                Message::SUCCESS
            ));


        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function deletePaymentAction($id)
    {
        if ($this->request->isPost()) {

            /** @var \PaymentInstallment $payment */
            $payment = \PaymentInstallment::findFirst(array(
                \PaymentInstallment::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $id
                )
            ));

            if (!$payment) {
                return $this->_returnNotFound();
            }

            $payment->setPaymentDate(null);
            $payment->save();

            /** @var Manager $messengerManager */
            $messengerManager = $this->getDI()->getShared('messengerManager');

            $messengerManager->appendMessage(new Message(
                $this->_translate('Uspesno uklonili uplatu!'),
                '',
                Message::SUCCESS
            ));


        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteAction($id)
    {
        $payment = \PaymentInstallment::findFirst(array(
            \PaymentInstallment::PROPERTY_ID . ' = :id:',
            'bind' => array(
                'id' => $id
            )
        ));

        if (!$payment) {
            return $this->_returnNotFound();
        }

        $payment->delete();
    }

    /**
     * @return array
     */
    public function getByPeriodAction($type = 1)
    {

        $today = (new DateTime())->format('d.m.Y');
        $limit = $this->request->getPost('rows') ? (int)$this->request->getPost('rows') : 10;
        $page = $this->request->getPost('page') ? (int)$this->request->getPost('page') : 1;
        $offset = ($page - 1) * $limit;
        $order_by = $this->request->getPost('sidx') ? $this->request->getPost('sidx') : 'createdDatetime';
        $order = $this->request->getPost('sord') ? $this->request->getPost('sord') : 'asc';

        $filters = $this->request->getPost('filters');
        $search = $this->request->getPost('_search');
        $from = $this->request->getPost('from') ? $this->request->getPost('from') : $today;
        $to = $this->request->getPost('to') ? $this->request->getPost('to') : $today;
        $statusPayment = $this->request->getPost('statusPayment');

        /** @var \Phalcon\Mvc\Model\Query\Builder $query */
        $modelsManager = $this->getDI()->get('modelsManager');
        $query = $modelsManager->createBuilder();
        $query->addFrom("\\PaymentInstallment", "pi")
            ->leftJoin('\Purchase', 'p.id = pi.purchaseId', 'p')
            ->leftJoin('\Client', 'c.id = p.clientId', 'c')
            ->where("pi.paymentDate IS NOT NULL AND p.purchaseTypeId = :type:", array(
                'type' => $type
            ));


        $orgAr = array(
            'name' => 'CONCAT_WS(" ", c.firstName, c.lastName)',
            'guarantor' => 'p.guarantor',
            'phoneNumber' => 'c.phoneNumber',
            'jmbg' => 'c.jmbg',
            'extra' => 'p.extra',
            'plates' => 'p.plates',
            'level' => 'p.level',
            'model' => 'p.model',
            'price' => 'p.price',
            'date' => 'p.date',
            'from' => 'pi.paymentDate',
            'to' => 'pi.paymentDate'
        );

        if($from) {
            $query->andWhere('pi.paymentDate >= :from:',array(
                'from' => (new DateTime($from))->getUnixFormat()
            ));
        }

        if($to) {
            $query->andWhere('pi.paymentDate <= :to:',array(
                'to' => (new DateTime($to))->getUnixFormat()
            ));
        }

        if (!empty($filters) && $search == 'true') {
            $filters = json_decode($filters);
            foreach ($filters->rules as $filter) {
                if ($filter->data && array_key_exists($filter->field, $orgAr)) {

                    $property = $orgAr[$filter->field];
                    $key = $filter->field;
                    $value = trim($filter->data);

                    switch ($filter->op) {
                        case 'eq' :
                            $query->andWhere("$property = :$key:", array($key => $value));
                            break;
                        case 'ne' :
                            $query->andWhere("$property != :$key:", array($key => $value));
                            break;
                        case 'cn' :
                            $query->andWhere("$property LIKE :$key:", array($key => "%$value%"));
                            break;
                        case 'lt' :
                            $query->andWhere("$property < :$key:", array($key => $value));
                            break;
                        case 'gt' :
                            $query->andWhere("$property > :$key:", array($key => $value));
                            break;
                    }
                }
            }
        }

        $searchCriteria = $query;
        $total = count($searchCriteria->getQuery()->execute());
        $query->limit($limit, $offset)->orderBy("{$orgAr[$order_by]} $order");

        $paymentInstallments = $query->getQuery()->execute();

        $json = array(
            'currpage' => $page,
            'totalpages' => $total > 0 ? ceil($total / $limit) : 1,
            'totalrecords' => $total,
        );

        $i = 0;
        $sum = 0;

        /** @var \PAymentInstallment $paymentInstallment */
        foreach($paymentInstallments as $paymentInstallment) {

            $purchase = $paymentInstallment->getPurchase();

            $client = $purchase->getClient();

            $registeredUntil = null;
            if($purchase->getRegisteredUntil()) {
                $registeredUntil = (new DateTime($purchase->getRegisteredUntil()))->format('d.m.Y');
            }

            $date = null;
            if($purchase->getCreatedDatetime()) {
                $date = (new DateTime($purchase->getCreatedDatetime()))->format('d.m.Y');
            }


            $color = '';
            if($purchase->getPrice() == $purchase->getTotalPaid()) {
                $color = "#62FF90";
            } elseif($purchase->isTodayPaymentDate()) {
                $color = '#FFD967';
            } elseif($purchase->isThereMissingPayments()) {
                $color = '#FF432E';
            }

            $pass = true;
            if($statusPayment) {
                switch($statusPayment) {
                    case 'PAID':
                        if($purchase->getPrice() > $purchase->getTotalPaid()) {
                            $pass = false;
                        }
                        break;
                    case 'TODAY':
                        if(!$purchase->isTodayPaymentDate()) {
                            $pass = false;
                        }
                        break;
                    case 'DUE':
                        if(!$purchase->isThereMissingPayments()) {
                            $pass = false;
                        }
                }
            }

            if($pass) {

                $sum += $paymentInstallment->getAmount();

                $json ['purchase'] [$i] [\Purchase::PROPERTY_ID] = $purchase->getId();
                $json ['purchase'] [$i] ['name'] = $client ? $client->getDisplayName() : '';
                $json ['purchase'] [$i] [\Purchase::PROPERTY_GUARANTOR] = $purchase ? $purchase->getGuarantor() : '';
                $json ['purchase'] [$i] [\Purchase::PROPERTY_PHONE_NUMBER] = $purchase ? $purchase->getPhoneNumber() : '';
                $json ['purchase'] [$i] [\Purchase::PROPERTY_EXTRA] = $purchase->getExtra();
                $json ['purchase'] [$i] [\Purchase::PROPERTY_PLATES] = $purchase->getPlates();
                $json ['purchase'] [$i] [\Client::PROPERTY_JMBG] = $client->getJmbg();
                $json ['purchase'] [$i] [\PaymentInstallment::PROPERTY_AMOUNT] = $paymentInstallment->getAmount();
                $json ['purchase'] [$i] [\Purchase::PROPERTY_LEVEL] = $purchase->getLevel();
                $json ['purchase'] [$i] [\Purchase::PROPERTY_MODEL] = $purchase->getModel();
                $json ['purchase'] [$i] [\Purchase::PROPERTY_REGISTERED_UNTIL] = $registeredUntil;
                $json ['purchase'] [$i] [\Purchase::PROPERTY_QUANTITY] = $purchase->getQuantity();
                $json ['purchase'] [$i] [\Purchase::PROPERTY_DATE] = $date;
                $json ['purchase'] [$i] [\Purchase::PROPERTY_PRICE] = $purchase->getPrice();
                $json ['purchase'] [$i] ['color'] = $color;
                $json ['purchase'] [$i] ['action']['id'] = $purchase->getId();

                $i++;

            }


        }

        $json['sum'] = $sum;

        return $json;
    }


}
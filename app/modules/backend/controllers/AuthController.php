<?php

namespace Backend\Controllers;

use Abraham\TwitterOAuth\TwitterOAuth;
use Comit\Controller\Base as BaseController;
use Comit\Helper\Common;
use Comit\Helper\DateTime;
use Comit\Helper\Image;
use Comit\Messenger\Manager as MessengerManager;
use Comit\Messenger\Message as MessengerMessage;
use Comit\Security\Acl;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphObject;
use Facebook\GraphUser;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\ElementInterface;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Tag;
use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Phalcon\Validation\Validator\Confirmation;

/**
 * Class AuthController
 *
 * @package Backend\Controllers
 */
class AuthController extends BaseController
{

    /**
     * @return \Phalcon\Http\ResponseInterface
     */
    public function loginAction()
    {
        if ($this->user) {
            return $this->response->redirect();
        }

//        $this->createUser();

        if ($this->request->isPost()) {

            $user = \User::authenticateUser(
                $this->request->getPost('username'),
                $this->request->getPost('password')
            );

            if ($user) {

                $this->_setLoggedUser($user);

                $user->setLastVisitDatetime(new DateTime());
                $user->setLastIp($this->request->getClientAddress());
                $user->save();

                $this->flashSession->success(
                    $this->_translate('Dobrodosao/la ' . $user->getDisplayName())
                );
                return $this->response->redirect('admin');

            } else {

                if ($this->request->isAjax()) {
                    $this->flashSession->error(
                        $this->_translate('Your session has expired. Please login again.')
                    );
                } else {
                    $this->flashSession->error(
                        $this->_translate('Either your username or password was incorrect. Please try again or reset your password.')
                    );
                }

                return $this->response->redirect('admin/auth/login');

            }
        }

        $this->view->setTemplateAfter('single');
        $this->_setTitle($this->_translate('Login'));
    }

    /**
     * Logout Action
     */
    public function logoutAction()
    {
        $this->session->remove('currentUser');

        $this->flashSession->success(
            $this->_translate('Dovidjenja!!!')
        );

        return $this->response->redirect('admin/auth/login');
    }

    /**
     * @return \Phalcon\Http\ResponseInterface
     * @throws \Abraham\TwitterOAuth\TwitterOAuthException
     */
    public function uploadAvatarAction()
    {
        /** @var \Comit\Messenger\Manager $messengerManager */
        $messengerManager = $this->getDI()->getShared('messengerManager');
        $messengerManager->appendMessage(
            new MessengerMessage(
                $this->_translate('No image provided!'),
                null,
                MessengerMessage::DANGER
            )
        );

        if (!$this->request->isPost()) {
            return null;
        }

        $postData = $this->request->getPost('image');

        if (!$postData) {
            return null;
        }

        $user = \User::findFirst(
            array(
                \User::PROPERTY_ID . ' = :userId:',
                'bind' => array(
                    'userId' => $this->session->get('currentUser')['id']
                )
            )
        );

        if (!$user) {
            return null;
        }

        $data = explode(',', $postData);
        $image = base64_decode($data[1]);
        $sourceImg = imagecreatefromstring($image);
        $path = $user->getUploadPath() . substr(sha1(rand()), 0, 8) . '.png';
        $success = imagepng($sourceImg, $path);
        imagedestroy($sourceImg);

        if ($success && $file = $this->_processAvatar($user, $path)) {

            $user->setAvatar(basename($file));
            $user->save();

            $this->dispatcher->setParam('img', $user->getAvatarUrl());

            $messengerManager->clear();
            $messengerManager->appendMessage(
                new MessengerMessage(
                    $this->_translate('Avatar has successfully changed!'),
                    null,
                    MessengerMessage::SUCCESS
                )
            );

        }
    }

    /**
     * Sets logged in User to the session
     *
     * @param \User $user
     */
    protected function _setLoggedUser(\User $user)
    {
        $userData = array(
            'id' => $user->getId(),
            'role' => $user->getRole() ? $user->getRole() : \User::ROLE_MEMBER,
            'locale' => 'en_US',
        );

        $this->session->set('currentUser', $userData);
    }

    /**
     * @param \User $user
     * @param string $avatar
     * @return null|string
     */
    protected function _processAvatar(\User $user, $avatar)
    {
        $size = getimagesize($avatar);
        $extension = image_type_to_extension($size[2]);
        $file = file_get_contents($avatar);
        $filePath = $user->getUploadPath() . substr(sha1(rand()), 0, 8) . $extension;

        if (file_put_contents($filePath, $file)) {
            Image::resizeAndCrop($filePath, 400, 400);
            unlink($avatar);
            return $filePath;
        }

        return null;
    }

    protected function createUser()
    {
        $user = new \User();
        $user->setFirstName('Darko');
        $user->setLastName('Vesic');
        $user->setUsername('darkovesic3@gmail.com');
        $user->setRole(Acl::ROLE_ADMIN);
        $user->setPassword('ruzalopuza');
        $user->setIsActive(1);
        $user->setGender(\User::GENDER_MALE);
        if(!$user->save()){
            foreach ($user->getMessages() as $message) {
                print_r($message->getMessage());
            }
            exit;
        }
    }

}

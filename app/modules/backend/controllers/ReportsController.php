<?php
namespace Backend\Controllers;

use Comit\Controller\Base as BaseController;
use Comit\Helper\DateTime;
use Phalcon\Forms\Element\Text;

class ReportsController extends BaseController
{
//
    public function contributorAction()
    {
        $this->view->firstDayOfMonth = (new DateTime())->format('01.m.Y');
        $this->view->lastDayOfMonth = (new DateTime())->format('t.m.Y');
    }

    public function technicalAction()
    {
        $technicals = array(0 => 'Izaberi');
        $technicals = $technicals + \Purchase::$constants;

        $this->view->technicals = json_encode($technicals);
        $this->view->firstDayOfMonth = (new DateTime())->format('01.m.Y');
        $this->view->lastDayOfMonth = (new DateTime())->format('t.m.Y');
    }

    public function contributorByPeriodAction()
    {

        $lastDayOfMonth = (new DateTime())->format('t.m.Y');
        $firstDayOfMonth = (new DateTime())->format('01.m.Y');
        $limit = $this->request->getPost('rows') ? (int)$this->request->getPost('rows') : 10;
        $page = $this->request->getPost('page') ? (int)$this->request->getPost('page') : 1;
        $offset = ($page - 1) * $limit;
        $order_by = $this->request->getPost('sidx') ? $this->request->getPost('sidx') : 'createdDatetime';
        $order = $this->request->getPost('sord') ? $this->request->getPost('sord') : 'asc';

        $filters = $this->request->getPost('filters');
        $search = $this->request->getPost('_search');
        $from = $this->request->getPost('from') ? $this->request->getPost('from') : $firstDayOfMonth;
        $to = $this->request->getPost('to') ? $this->request->getPost('to') : $lastDayOfMonth;


        /** @var \Phalcon\Mvc\Model\Query\Builder $query */
        $modelsManager = $this->getDI()->get('modelsManager');
        $query = $modelsManager->createBuilder();
        $query->addFrom("\\Purchase", "p")
            ->leftJoin('\Client', 'p.clientId = pi.id', 'pi')
            ->where("p.contributor IS NOT NULL AND p.contributor != ''");


        $orgAr = array(
            'name' => 'CONCAT_WS(" ", c.firstName, c.lastName)',
            'guarantor' => 'p.guarantor',
            'phone' => 'pi.phone',
            'jmbg' => 'pi.jmbg',
            'plates' => 'p.plates',
            'model' => 'p.model',
            'date' => 'p.date',
            'invoice'=>'p.invoice',
            'contributor' => '  p.contributor',
            'contributorAmount' => 'p.contributorAmount'
        );

        if ($from) {
            $query->andWhere('p.date >= :from:', array(
                'from' => (new DateTime($from))->getUnixFormat()
            ));
        }

        if ($to) {
            $query->andWhere('p.date <= :to:', array(
                'to' => (new DateTime($to))->getUnixFormat()
            ));
        }

        if (!empty($filters) && $search == 'true') {
            $filters = json_decode($filters);
            foreach ($filters->rules as $filter) {
                if ($filter->data && array_key_exists($filter->field, $orgAr)) {

                    $property = $orgAr[$filter->field];
                    $key = $filter->field;
                    $value = trim($filter->data);

                    switch ($filter->op) {
                        case 'eq' :
                            $query->andWhere("$property = :$key:", array($key => $value));
                            break;
                        case 'ne' :
                            $query->andWhere("$property != :$key:", array($key => $value));
                            break;
                        case 'cn' :
                            $query->andWhere("$property LIKE :$key:", array($key => "%$value%"));
                            break;
                        case 'lt' :
                            $query->andWhere("$property < :$key:", array($key => $value));
                            break;
                        case 'gt' :
                            $query->andWhere("$property > :$key:", array($key => $value));
                            break;
                    }
                }
            }
        }

        $searchCriteria = $query;
        $total = count($searchCriteria->getQuery()->execute());
        $query->limit($limit, $offset)->orderBy("{$orgAr[$order_by]} $order");

        $purchases = $query->getQuery()->execute();

        $json = array(
            'currpage' => $page,
            'totalpages' => $total > 0 ? ceil($total / $limit) : 1,
            'totalrecords' => $total,
        );

        $i = 0;
        $sum = 0;

        /** @var \Purchase $purchase */
        foreach ($purchases as $purchase) {

            $client = $purchase->getClient();

            $registeredUntil = null;
            if ($purchase->getRegisteredUntil()) {
                $registeredUntil = (new DateTime($purchase->getRegisteredUntil()))->format('d.m.Y');
            }

            $date = null;
            if ($purchase->getCreatedDatetime()) {
                $date = (new DateTime($purchase->getCreatedDatetime()))->format('d.m.Y');
            }


            $sum += $purchase->getContributorAmount();

            $json ['purchase'] [$i] [\Purchase::PROPERTY_ID] = $purchase->getId();
            $json ['purchase'] [$i] ['name'] = $client ? $client->getDisplayName() : '';
            $json ['purchase'] [$i] [\Purchase::PROPERTY_GUARANTOR] = $purchase ? $purchase->getGuarantor() : '';
            $json ['purchase'] [$i] [\Purchase::PROPERTY_PHONE_NUMBER] = $purchase ? $purchase->getPhoneNumber() : '';
            $json ['purchase'] [$i] [\Purchase::PROPERTY_EXTRA] = $purchase->getExtra();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_PLATES] = $purchase->getPlates();
            $json ['purchase'] [$i] [\Client::PROPERTY_JMBG] = $client->getJmbg();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_LEVEL] = $purchase->getLevel();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_MODEL] = $purchase->getModel();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_REGISTERED_UNTIL] = $registeredUntil;
            $json ['purchase'] [$i] [\Purchase::PROPERTY_QUANTITY] = $purchase->getQuantity();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_DATE] = $date;
            $json ['purchase'] [$i] [\Purchase::PROPERTY_CONTRIBUTOR] = $purchase->getContributor();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_CONTRIBUTOR_AMOUNT] = $purchase->getContributorAmount();;
            $json ['purchase'] [$i] [\Purchase::PROPERTY_PRICE] = $purchase->getPrice();
            $json ['purchase'] [$i] ['action']['id'] = $purchase->getId();

            $i++;


        }

        $json['sum'] = number_format($sum, 2);

        return $json;

    }

    public function technicalByPeriodAction()
    {

        $lastDayOfMonth = (new DateTime())->format('t.m.Y');
        $firstDayOfMonth = (new DateTime())->format('01.m.Y');
        $limit = $this->request->getPost('rows') ? (int)$this->request->getPost('rows') : 10;
        $page = $this->request->getPost('page') ? (int)$this->request->getPost('page') : 1;
        $offset = ($page - 1) * $limit;
        $order_by = $this->request->getPost('sidx') ? $this->request->getPost('sidx') : 'createdDatetime';
        $order = $this->request->getPost('sord') ? $this->request->getPost('sord') : 'asc';

        $filters = $this->request->getPost('filters');
        $search = $this->request->getPost('_search');
        $from = $this->request->getPost('from') ? $this->request->getPost('from') : $firstDayOfMonth;
        $to = $this->request->getPost('to') ? $this->request->getPost('to') : $lastDayOfMonth;

        /** @var \Phalcon\Mvc\Model\Query\Builder $query */
        $modelsManager = $this->getDI()->get('modelsManager');
        $query = $modelsManager->createBuilder();
        $query->addFrom("\\Purchase", "p")
            ->leftJoin('\Client', 'p.clientId = pi.id', 'pi')
            ->where('p.technical IS NOT NULL');


        $orgAr = array(
            'name' => 'CONCAT_WS(" ", c.firstName, c.lastName)',
            'guarantor' => 'p.guarantor',
            'phone' => 'pi.phone',
            'jmbg' => 'pi.jmbg',
            'plates' => 'p.plates',
            'model' => 'p.model',
            'technical' => 'p.technical',
            'date' => 'p.date',
            'contributor' => '  p.contributor',
            'contributorAmount' => 'p.contributorAmount'
        );

        if ($from) {
            $query->andWhere('p.date >= :from:', array(
                'from' => (new DateTime($from))->getUnixFormat()
            ));
        }

        if ($to) {
            $query->andWhere('p.date <= :to:', array(
                'to' => (new DateTime($to))->getUnixFormat()
            ));
        }

        if (!empty($filters) && $search == 'true') {
            $filters = json_decode($filters);
            foreach ($filters->rules as $filter) {
                if ($filter->data && array_key_exists($filter->field, $orgAr)) {

                    $property = $orgAr[$filter->field];
                    $key = $filter->field;
                    $value = trim($filter->data);

                    switch ($filter->op) {
                        case 'eq' :
                            $query->andWhere("$property = :$key:", array($key => $value));
                            break;
                        case 'ne' :
                            $query->andWhere("$property != :$key:", array($key => $value));
                            break;
                        case 'cn' :
                            $query->andWhere("$property LIKE :$key:", array($key => "%$value%"));
                            break;
                        case 'lt' :
                            $query->andWhere("$property < :$key:", array($key => $value));
                            break;
                        case 'gt' :
                            $query->andWhere("$property > :$key:", array($key => $value));
                            break;
                    }
                }
            }
        }

        $searchCriteria = $query;
        $total = count($searchCriteria->getQuery()->execute());
        $query->limit($limit, $offset)->orderBy("{$orgAr[$order_by]} $order");

        $purchases = $query->getQuery()->execute();

        $json = array(
            'currpage' => $page,
            'totalpages' => $total > 0 ? ceil($total / $limit) : 1,
            'totalrecords' => $total,
        );

        $i = 0;
        $sum = 0;

        /** @var \Purchase $purchase */
        foreach ($purchases as $purchase) {

            $client = $purchase->getClient();

            $date = null;
            if ($purchase->getCreatedDatetime()) {
                $date = (new DateTime($purchase->getCreatedDatetime()))->format('d.m.Y');
            }

            $sum += $purchase->getTechnicalAmount();

            $technicals = $purchase::$constants;

            $json ['purchase'] [$i] [\Purchase::PROPERTY_ID] = $purchase->getId();
            $json ['purchase'] [$i] ['name'] = $client ? $client->getDisplayName() : '';
            $json ['purchase'] [$i] [\Purchase::PROPERTY_PHONE_NUMBER] = $purchase->getPhoneNumber();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_PLATES] = $purchase->getPlates();
            $json ['purchase'] [$i] [\Client::PROPERTY_JMBG] = $client->getJmbg();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_TECHNICAL] = $technicals[$purchase->getTechnical()];
            $json ['purchase'] [$i] [\Purchase::PROPERTY_TECHNICAL_AMOUNT] = $purchase->getTechnicalAmount();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_DATE] = $date;
            $json ['purchase'] [$i] ['action']['id'] = $purchase->getId();

            $i++;

        }

        $json['sum'] = number_format($sum, 2);

        return $json;

    }

}
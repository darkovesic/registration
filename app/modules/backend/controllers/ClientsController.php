<?php

namespace Backend\Controllers;

use Comit\Controller\Base as BaseController;

/**
 * Class AuthController
 *
 * @package Backend\Controllers
 */
class ClientsController extends BaseController
{

    public function getClientsAction()
    {
        $term = $this->request->getQuery('term');
        $json = [];

        /** @var \Client[] $clients */
        $clients = \Client::find(
            array(
                \Client::PROPERTY_JMBG . ' LIKE :jmbg:',
                'bind' => array(
                    'jmbg' => '%'.$term.'%'
                )
            )
        );

        foreach ($clients as $key => $client) {

            /** @var \Purchase|null $purchase */
            $purchase = $client->getPurchases()->getLast();

            $json[$key]['id'] = $client->getId();
            $json[$key]['value'] = $client->getJmbg();
            $json[$key]['firstName'] = $client->getFirstName();
            $json[$key]['lastName'] = $client->getLastName();
            $json[$key]['phoneNumber'] = $purchase ? $purchase->getPhoneNumber() : '';
            $json[$key]['guarantor'] = $purchase ? $purchase->getGuarantor() : '';
            $json[$key]['guarantorPhone'] = $purchase ? $purchase->getGuarantorPhone() : '';
            $json[$key]['typeOfClient'] = $purchase ? $purchase->getTypeOfClient() : '';
            $json[$key]['smsNotification'] = $client->getSmsNotification();
            $json[$key]['label'] = $client->getDisplayName();
        }

        return $json;
    }
}
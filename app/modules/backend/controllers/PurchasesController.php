<?php

namespace Backend\Controllers;

use Comit\Controller\Base as BaseController;
use Comit\Form\Renderer\Json;
use Comit\Helper\DateTime;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
//
class PurchasesController extends BaseController
{

    public function listAction($type = 1)
    {

        $limit = $this->request->getPost('rows') ? (int)$this->request->getPost('rows') : 10;
        $page = $this->request->getPost('page') ? (int)$this->request->getPost('page') : 1;
        $offset = ($page - 1) * $limit;
        $order_by = $this->request->getPost('sidx') ? $this->request->getPost('sidx') : 'multisort';
        $order = $this->request->getPost('sord') ? $this->request->getPost('sord') : 'desc';

        $filters = $this->request->getPost('filters');
        $search = $this->request->getPost('_search');
        $from = $this->request->getPost('from');
        $to = $this->request->getPost('to');
        $statusPayment = $this->request->getPost('statusPayment');

        /** @var \Phalcon\Mvc\Model\Query\Builder $query */
        $modelsManager = $this->getDI()->get('modelsManager');
        $query = $modelsManager->createBuilder();
        $query->addFrom('\Purchase', "p")
            ->leftJoin('\Client', 'c.id = p.clientId', 'c')
            ->where("p.purchaseTypeId = :type:", array(
                'type' => $type
            ))
            ->andWhere('c.id IS  NOT NULL')
//        ->andWhere("p.date >= :date:",array(
//            'date'=> '2018-01-01'
//        ) )
        ;


        $orgAr = array(
            'name' => 'CONCAT_WS(" ", c.firstName, c.lastName)',
            'guarantor' => 'p.guarantor',
            'phoneNumber' => 'p.phoneNumber',
            'jmbg' => 'c.jmbg',
            'extra' => 'p.extra',
            'plates' => 'p.plates',
            'level' => 'p.level',
            'model' => 'p.model',
            'price' => 'p.price',
            'invoice' => 'p.invoice',
            'date' => 'p.date',
            'from' => 'p.createdDatetime',
            'to' => 'p.createdDatetime',
            'multisort' => 'multisort',
            'id' => 'p.id'
        );
        if ($from && $from == $to) {
            $query->andWhere('p.date LIKE :date:',array(
                'date' => '%'.(new DateTime($from))->format('Y-m-d').'%',
            ));
        } else {

            if($from) {
                $query->andWhere('p.date >= :from:',array(
                    'from' => (new DateTime($from))->getUnixFormat()
                ));
            }

            if($to) {
                $query->andWhere('p.date <= :to:',array(
                    'to' => (new DateTime($to))->getUnixFormat()
                ));
            }
        }



        if (!empty($filters) && $search == 'true') {
            $filters = json_decode($filters);
            foreach ($filters->rules as $filter) {
                if ($filter->data && array_key_exists($filter->field, $orgAr)) {

                    $property = $orgAr[$filter->field];
                    $key = $filter->field;
                    $value = trim($filter->data);

                    switch ($filter->op) {
                        case 'eq' :
                            $query->andWhere("$property = :$key:", array($key => $value));
                            break;
                        case 'ne' :
                            $query->andWhere("$property != :$key:", array($key => $value));
                            break;
                        case 'cn' :
                            $query->andWhere("$property LIKE :$key:", array($key => "%$value%"));
                            break;
                        case 'lt' :
                            $query->andWhere("$property < :$key:", array($key => $value));
                            break;
                        case 'gt' :
                            $query->andWhere("$property > :$key:", array($key => $value));
                            break;
                    }
                }
            }
        }

        if($orgAr[$order_by] == 'multisort') {
            $query->orderBy("p.date desc, p.id desc");
        } else {
            $query->orderBy("{$orgAr[$order_by]} $order");
        }

        $purchases = $query->getQuery()->execute();

        $i = 0;
        $sum = 0;
        $total = 0;
        $purchasesArray = [];

        /** @var \Purchase $purchase */
        foreach($purchases as $purchase) {
            $pass = true;
            if($statusPayment) {
                switch($statusPayment) {
                    case 'PAID':
                        if($purchase->getPrice() > $purchase->getTotalPaid()) {
                            $pass = false;
                        }
                        break;
                    case 'TODAY':
                        if(!$purchase->isTodayPaymentDate()) {
                            $pass = false;
                        }
                        break;
                    case 'DUE':
                        if(!$purchase->isThereMissingPayments()) {
                            $pass = false;
                        }
                        break;
                    case 'OLD_DUE':
                        if(!$purchase->isThereMissingPayments(true)) {
                            $pass = false;
                        }
                        break;
                }
            }

            if($pass) {
                $total++;
                $purchasesArray[] = $purchase;
            }
        }

        $purchasesArray2 = array_slice($purchasesArray, $offset, $limit);

        $json = array(
            'currpage' => $page,
            'totalpages' => $total > 0 ? ceil($total / $limit) : 1,
            'totalrecords' => $total,
        );

        foreach ($purchasesArray2 as $purchase) {

            $client = $purchase->getClient();

            $registeredUntil = null;
            if($purchase->getRegisteredUntil()) {
                $registeredUntil = (new DateTime($purchase->getRegisteredUntil()))->format('d.m.Y');
            }

            $date = null;
            if($purchase->getCreatedDatetime()) {
                $date = (new DateTime($purchase->getCreatedDatetime()))->format('d.m.Y');
            }


            $color = '';
            if($purchase->getPrice() == $purchase->getTotalPaid()) {
                $color = "#62FF90";
            } elseif($purchase->isTodayPaymentDate()) {
                $color = '#FFD967';
            } elseif($purchase->isThereMissingPayments()) {
                $color = '#FF432E';
            }

            $sum += $purchase->getPrice();

            $json ['purchase'] [$i] [\Purchase::PROPERTY_ID] = $purchase->getId();
            $json ['purchase'] [$i] ['name'] = $client ? $client->getDisplayName() : '';
            $json ['purchase'] [$i] [\Purchase::PROPERTY_GUARANTOR] = $purchase ? $purchase->getGuarantor() : '';
            $json ['purchase'] [$i] [\Client::PROPERTY_PHONE_NUMBER] = $purchase ? '<a href="tel:'.$purchase->getPhoneNumber() .'">' .$purchase->getPhoneNumber() .'</a>' : '';
            $json ['purchase'] [$i] [\Purchase::PROPERTY_EXTRA] = $purchase->getExtra();
            $json ['purchase'] [$i] [\Client::PROPERTY_JMBG ]= $client->getJmbg();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_PLATES] = $purchase->getPlates();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_LEVEL] = $purchase->getLevel();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_MODEL] = $purchase->getModel();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_INVOICE] = $purchase->getInvoice();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_REGISTERED_UNTIL] = $registeredUntil;
            $json ['purchase'] [$i] [\Purchase::PROPERTY_QUANTITY] = $purchase->getQuantity();
            $json ['purchase'] [$i] [\Purchase::PROPERTY_DATE] = $date;
            $json ['purchase'] [$i] [\Purchase::PROPERTY_DATE] =  (new DateTime($purchase->getDate()))->format('d.m.Y');
            $json ['purchase'] [$i] [\Purchase::PROPERTY_PRICE] = $purchase->getPrice();
            $json ['purchase'] [$i] ['color'] = $color;
            $json ['purchase'] [$i] ['action']['id'] = $purchase->getId();

            $i++;
        }

        $json['sum'] = $sum;

        return $json;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteAction($id)
    {
        $purchase = \Purchase::findFirst(array(
            \Purchase::PROPERTY_ID . ' = :id:',
            'bind' => array(
                'id' => $id
            )
        ));

        if (!$purchase) {
            return $this->_returnNotFound();
        }

        $purchase->delete();
    }

    public function copyAction($id)
    {
        $purchase = \Purchase::findFirst(array(
            \Purchase::PROPERTY_ID . ' = :id:',
            'bind' => array(
                'id' => $id
            )
        ));

        if (!$purchase) {
            return $this->_returnNotFound();
        }

        $form = $purchase->getClient()->getForm();

        if($purchase->getPurchaseTypeId() == 1) {
            $formRegistration = $purchase->getForm(array(
                \Purchase::PROPERTY_PURCHASE_TYPE_ID,
                \Purchase::PROPERTY_EXTRA,
                \Purchase::PROPERTY_CLIENT_ID,
                \Purchase::PROPERTY_QUANTITY,
                \Purchase::PROPERTY_DATE,
            ));

            $contributor = new Check('contributorCheck');
            $contributor->setLabel('Dodaj Saradnika');

            $form->add($contributor,'contributor', true);

        } else {
            $formRegistration = $purchase->getForm(array(
                \Purchase::PROPERTY_PURCHASE_TYPE_ID,
                \Purchase::PROPERTY_CLIENT_ID,
                \Purchase::PROPERTY_LEVEL,
                \Purchase::PROPERTY_PLATES,
                \Purchase::PROPERTY_REGISTERED_UNTIL,
                //\Purchase::PROPERTY_NOTE,
                \Purchase::PROPERTY_CONTRIBUTOR,
                \Purchase::PROPERTY_CONTRIBUTOR_AMOUNT
            ));
        }


        foreach ($formRegistration->getElements() as $element) {

            if($element->getLabel() == 'Model') {
                $element->setLabel('Vrsta');
            }

            $form->add($element);
        }

        $form->get('price')->setDefault('');

        $clientId = new Hidden('clientId');
        $clientId->setDefault($purchase->getClientId());
        $form->add($clientId);


        $renderer = new Json();
        return $renderer->render($form);

    }

    public function downloadAction($purchaseId)
    {
        $purchase = \Purchase::findFirst(
            array(
                \Purchase::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $purchaseId
                )
            )
        );

        if(!$purchase) {
            $this->_returnNotFound();
        }

        if(!$client = $purchase->getClient()) {

        }

        $file = $purchase->getFileUrl();

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
}
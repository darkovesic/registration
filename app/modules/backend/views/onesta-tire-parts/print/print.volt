<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.0 Transitional//EN">
<html>
<head>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE>UGOVOR</TITLE>
    <META NAME="GENERATOR" CONTENT="OpenOffice 4.1.1  (FreeBSD/amd64)">
    <META NAME="CREATED" CONTENT="20160312;14130300">
    <META NAME="CHANGED" CONTENT="0;0">
    <META NAME="AppVersion" CONTENT="1.0">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { size: 9.27in 12.69in; margin-left: 0; margin-right: 0; margin-top: 0; margin-bottom: 0 }
        p {margin-bottom: -8px!important;direction: ltr; color: #000000; line-height: 125%; widows: 2; orphans: 2;font-size: 10pt }
        p.western { font-family: "Calibri", serif; font-size: 10pt }
        p.cjk { font-family: "Calibri"; font-size: 10pt;  }
        p.ctl { font-family: "Calibri"; font-size: 10pt }
        -->
    </STYLE>
</head>
{% set client = purchase.getClient() %}
<p class="western" ALIGN=CENTER STYLE="line-height: 0.17in">
    <font size=4><span LANG="pt-BR"><b>U  G  O  V  O  R</b></span></font>
</p>
<p class="western" ALIGN=CENTER STYLE="line-height: 0.17in">
    <font size=4><span LANG="pt-BR"><b>O PRODAJI DELOVA NA RATE</b></span></font>
</p>
<br>
<p class="western" ALIGN=JUSTIFY STYLE="line-height: 0.17in">
    <font size=2><span LANG="pl-PL">Zaključen u </span></font><font size=2>Beogradu, dana: {{ purchase.getCreateDatatimeShort() }} između :</font>
</p>
<p class="western" STYLE="margin-bottom: 0.04in; line-height: 0.17in">
    <font size=2><span LANG="pl-PL"><b>1.ONESTA, sa sedistem u Beogradu, Mirijevski Bulebar 81, mat. broj: 53875017, PIB: 102485112, br. tel: 064/668-79-83  i </b></span></font><font size=2><span LANG="pl-PL">(u
daljem tekstu Agencija) i</span></font></p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.04in; line-height: 0.17in">
    <b>2.</b><b>{{ client }}, </b><b>matični broj:</b>{{ client.getJmbg() }}<b>,</b> (u daljem tekstu Klijent)</span>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="line-height: 0.17in">
    <font size=2><span LANG="pl-PL">Ugovorne strane su se sporazumele o sledećem:</span></font></p>
<p class="western" ALIGN=CENTER STYLE="line-height: 0.17in">
    <font size=2><span LANG="pl-PL"><b>Čl.1</b></span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="pl-PL">Ovim Ugovorom Agencija se obavezuje da prodaje auto deo klijentu i to: </span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><b>Marka i tip auto dela:  {{ purchase.model }};</b></font><br>
    <font size=2><b>Oznaka auto dela:  {{ purchase.extra }};</b></font><br>
    <font size=2><b>Kolicina:  {{ purchase.quantity }};</b></font><br>
</p>
<br>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">a Klijent se obavezuje da Agenciji obezbedi i dostavi svu potrebnu dokumentaciju za registraciju predmetnog vozila, kao i da plati naknadu za izvršenu uslugu registracije u iznosu i na način utvrđen ovim Ugovorom.</span></font>
</p>
<p class="western" ALIGN=CENTER STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE"><b>Čl. 2</b></span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">Naknada za prodaju akumulatora/gume iz čl. 1 ovog Ugovora sa svim troškovima iznosi:: <b>{{ purchase.price }} </b></span></font><font size=2><span LANG="sv-SE"> <b>(dinara)</b></span></font><font size=2><span LANG="sv-SE">.</span></font>
</p>
<p class="western" ALIGN=CENTER STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE"><b>Čl.3</b></span></font>
</p>

<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">Klijent se obavezuje da Agenciji plati naknadu utvrđenu u Čl. 2. ovog Ugovora u {{ purchase.paymentInstallments|length }} ({{ purchase.getSerbianNumerationOfProRata() }}) mesečne rate i to na sledeći način:</span></font>
</p>

{% set counter = 0 %}
{% set word = '' %}
{% for paymnetInstallment in purchase.paymentInstallments %}
    {% set counter = counter + 1 %}
    {% if(counter == 1) %}

        <p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
            <font size=2><span LANG="sv-SE"><b>Prva rata: {{ paymnetInstallment.amount }}</b></span></font>
                    <font size=2><span LANG="sv-SE">(<b>dinara</b>) daje se prilikom potpisivanja ovog Ugovora;</span></font>
        </p>

    {% else %}

        {% if counter == 2 %}
            {% set word = 'Druga' %}
        {% elseif counter == 3 %}
            {% set word = 'Treća' %}
        {% elseif counter == 4 %}
            {% set word = 'Četvrta' %}
        {% elseif counter == 5 %}
            {% set word = 'Peta' %}
        {% elseif counter == 6 %}
            {% set word = 'Šesta' %}
        {% endif %}

        <p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
            <font size=2><span LANG="sv-SE"><b>{{ word }} rata: {{ paymnetInstallment.amount }} (<b>dinara</b>)</b></span></font><font size=2><span LANG="sv-SE"> dospeva za naplatu dana: <b>{{ paymnetInstallment.date.format('d.m.Y') }}.</b></span></font>
        </p>

    {% endif %}

{% endfor %}
<p class="western" ALIGN=CENTER STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE"><b>Čl.4</b></span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">Svojim potpisom na ovom Ugovoru Agencija potvrdjuje da je iznos za rvu ratu primio na dan potpisivanja ovog Ugovora.</span></font>
</p>
<p class="western" ALIGN=CENTER STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE"><b>Čl.5</b></span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">Klijent se obavezuje da će ostale rate plaćati najkasnije onog dana koji je naveden za svaku konkretnu ratu u Članu 3. ovog Ugovora. Ako taj dan pada u nedelju ili neradni dan, Klijent se obavezuje da uplatu izvrši prvog narednog radnog dana posle tog dana. </span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">Ukoliko Klijent zakasni sa plaćanjem bilo koje od narednih rata, sve
ostale rate dospevaju za naplatu odmah i Agencija je ovlašćena da od Klijenta zahteva ceo iznos naknade iz Člana 2. umanjen za iznose koje je Klijent platio na ime do tada dospelih rata. </span></font>
</p>
<p class="western" ALIGN=CENTER STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE"><b>Čl.6</b></span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">Za sve što nije izričito regulisano ovim Ugovorom, primenjivaće se odredbe Zakona o obligacionim odnosima.</span></font></p>
<p class="western" ALIGN=CENTER STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE"><b>Čl.7</b></span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">Ugovorne strane su saglasne da će sve nesporazume koji proisteknu iz realizacije ovog Ugovora pokušati da reše mirnim putem, u suprotnom spor će rešavati stvarno nadležni sud u Beogradu.</span></font>
</p>
<p class="western" ALIGN=CENTER STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE"><b>Čl.8</b></span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><span LANG="sv-SE">Ovaj Ugovor je sačinjen u 2 (dva) istovetna primerka, po jedan za svaku Ugovornu stranu.</span></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><b style="text-align:left">Agencija ONESTA<span style="float: right; margin-right: 75px">Klijent</span></b></font>
</p>
<p class="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.06in; line-height: 0.17in">
    <font size=2><b style="text-align:left">________________________<span style="float: right">__________________________</span></b></font>
</p>
</body>
</html>
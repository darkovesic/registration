<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-3">
        <a href="#" onclick="getCreateForm('admin/onesta-tire-parts', event)" class="btn btn-primary">
            <i class="fa fa-ambulance"></i>
            {{ t('Nova Prodaja') }}
        </a>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">
        <input type="text" id="from" name="from" class="form-control" placeholder="Od Datuma"/>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">
        <input type="text" id="to" name="to" class="form-control" placeholder="Do Datuma"/>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">
        <div class="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown ">
                    <a href="#" id="drop1" data-toggle="dropdown" class="dropdown-toggle" role="button">
                        <span>Filter</span> <b class="caret"></b>
                    </a>
                    <ul role="menu" class="dropdown-menu" aria-labelledby="drop1">
                        <li role="presentation"><a href="#" role="menuitem">Sve</a></li>
                        <li data-status="" data-filter="PAID" class="list-success" role="presentation"><a href="#" role="menuitem">Plaćeni</a></li>
                        <li data-status="" data-filter="TODAY" class="list-warning" role="presentation"><a href="#" role="menuitem">Današnji</a></li>
                        <li data-status="" data-filter="DUE" class="list-danger" role="presentation"><a href="#" role="menuitem">Zakasneli</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="space-15 clear"></div>
<div class="row">
    <div class="col-xs-12">
        {{ partial('onesta-tire-parts/partials/scripts') }}
    </div>
</div>
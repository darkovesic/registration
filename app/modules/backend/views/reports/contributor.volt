<div class="row">
    <div class="col-xs-12">
        <h2>Izvestaj Prometa Saradnika</h2>
    </div>
</div>
<div class="space-15 clear"></div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-3">
        <input type="text" id="from" name="from" class="form-control" value = "{{ firstDayOfMonth }}" placeholder="Od Datuma"/>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">
        <input type="text" id="to" name="to" class="form-control" value = "{{ lastDayOfMonth }}" placeholder="Do Datuma"/>
    </div>
</div>
<div class="space-15 clear"></div>
<div class="row">
    <div class="col-xs-12">
        {{ partial('reports/partials/contributor-scripts') }}
    </div>
</div>
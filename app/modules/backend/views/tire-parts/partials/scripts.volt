<div class="row">
    <div class="col-xs-12">
        <table id="grid-table"></table>
        <div id="grid-pager"></div>
    </div>
</div>
<script type="text/javascript">
    (function ($) {

        $(document).ready(function () {

            $('#from, #to').bind("enterKey",function(e){
                var from = $('#from').val();
                var to = $('#to').val();
                var status = $('.dropdown-menu li[data-status="active"]').attr("data-filter");

                var postData = {
                    from: from,
                    to: to,
                    statusPayment: status
                };

                $("#grid-table").jqGrid('setGridParam',{
                    postData:postData,
                    search:true
                }).trigger('reloadGrid');

            });
            $('#from,#to').keyup(function(e){
                if(e.keyCode == 13){
                    $(this).trigger("enterKey");
                }
            });

            $(document).on("click", ".dropdown-menu li", function() {
                var from = $('#from').val();
                var to = $('#to').val();
                var status = $(this).attr("data-filter");

                if(typeof status === 'undefined') {
                    status = '';
                }

                var postData = {
                    from: from,
                    to: to,
                    statusPayment: status
                };

                $("#grid-table").jqGrid('setGridParam',{
                    postData:postData,
                    search:true
                }).trigger('reloadGrid');
            });

            getRegistrations();

            $(window).bind('resize', function () {
                var width = $('.jqgrid_wrapper').width();
                $('#grid-table').setGridWidth(width);
            });
        });
    })(jQuery);


    function getRegistrations(myPostData) {

        var grid_selector = "#grid-table";
        var pager_selector = "#grid-pager";

        $(grid_selector).jqGrid({
            url: "/admin/purchases/list/2",
            datatype: "json",
            mtype: "post",
            height: '100%',
            jsonReader: {
                root: "purchase",
                page: "currpage",
                total: "totalpages",
                records: "totalrecords",
                id: "id"
            },
            colNames: [
                'Ime i Prezime',
                'Datum',
                'Garant',
                'Telefon',
                'Model',
                'Razno',
                'Kolicina',
                'Faktura',
                'Cena',
                'Uredi',
                'Boja'
            ],
            colModel: [
                {
                    name: 'name',
                    index: 'name',
                    width: 100,
                    sorttype: 'name',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'date',
                    index: 'date',
                    width: 80,
                    sorttype: 'date',
                    align: 'center',
                    search: false
                },
                {
                    name: 'guarantor',
                    index: 'guarantor',
                    width: 70,
                    sorttype: 'guarantor',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'phoneNumber',
                    index: 'phoneNumber',
                    width: 70,
                    sorttype: 'phoneNumber',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'model',
                    index: 'model',
                    width: 70,
                    sorttype: 'model',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'extra',
                    index: 'extra',
                    width: 70,
                    sorttype: 'extra',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'quantity',
                    index: 'quantity',
                    width: 30,
                    sorttype: 'quantity',
                    align: 'center',
                    search: false
                },                {
                    name: 'invoice',
                    index: 'invoice',
                    width: 30,
                    sorttype: 'invoice',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'price',
                    index: 'price',
                    width: 50,
                    sorttype: 'price',
                    align: 'center',
                    search: false
                },
                {
                    name: 'action',
                    index: 'action',
                    width: 120,
                    sorttype: 'medical',
                    align: 'center',
                    search: false,
                    formatter: addOptionButtons
                },
                {
                    name: 'color',
                    index: 'color',
                    width: 80,
                    sorttype: 'color',
                    hidden: true,
                    align: 'center',
                    search: false
                }


            ],
            viewrecords: true,
            rowNum: 25,
            rowList: [25, 50, 75, 100],
            pager: pager_selector,
            page: 1,
            sortname: 'date',
            sortorder: 'desc',
            altRows: true,
            multiselect: false,
            multiboxonly: true,
            search: true,
            footerrow: true,
            beforeSubmitCell : function(rowid,celname,value,iRow,iCol) {
                var color = $(grid_selector).getCell(rowid, "rowColor");
                return {
                    color:color
                };
            },
            beforeRequest: function () {
                $(grid_selector).jqGrid('filterToolbar', {
                    defaultSearch: true,
                    stringResult: true,
                    beforeSearch: function () {
                        if ($(this).data('firstSearchAbortedFlag') != '1') {
                            $(this).data('firstSearchAbortedFlag', '1');
                            return true;
                        }
                        return false;
                    }
                });

                if ($(this).data('defaultValuesSetFlag') != '1') {
                    $(this).data('defaultValuesSetFlag', '1');
                    $(grid_selector)[0].triggerToolbar();
                }

                if ("" != myPostData) {
                    $(grid_selector).jqGrid('setGridParam', {'postData': myPostData});
                }
            },
            gridComplete: function () {
                var rows = $(grid_selector).getDataIDs();
                for (var i = 0; i < rows.length; i++) {

                    var color = $(grid_selector).getCell(rows[i], "color");
                    var id = $(grid_selector).getCell(rows[i], "plates");

                    if(color) {
                        $(grid_selector).jqGrid('setRowData', rows[i], false, {background: color});
                    }

                }
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                    updateTableIcons(table);
                }, 0);

                $(table).jqGrid('footerData','set',
                        {
                            name:'<span style="font-size:130%"><b>TOTAL</b></span>',
                            price:'<span style="font-size:130%; color:red"> ' + data.sum + '</span>'
                        }
                );

                myPostData = "";
            },
            autowidth: true

        });

        //navButtons
    }

    var addOptionButtons = function (obj) {
        return '<div class="center action-buttons">' +
                    '<a href="#" onclick="getUpdateForm(\'admin/tire-parts\', ' + obj.id + ', event)" class="bigger-140 text-success">' +
                        '<button class="btn btn-info btn-circle" type="button" title="Edit">' +
                            '<i class="fa fa-pencil"></i>' +
                        '</button>' +
                    '</a>&nbsp;' +
                    '<a href="/admin/tire-parts/overview/'+ obj.id +'" class="bigger-140">' +
                        '<button class="btn btn-primary btn-circle" type="button" title="Vise">' +
                            '<i class="fa fa-list"></i>' +
                        '</button>' +
                    '</a>&nbsp;' +
                    '<a href="#" onclick="getDeleteForm(\'admin/purchases\', ' + obj.id + ', event)" class="bigger-140 text-danger">' +
                        '<button class="btn btn-danger btn-circle" type="button" title="Brisanje">' +
                            '<i class="fa fa-times"></i>' +
                        '</button>' +
                    '</a>' +
                    '<a href="#" onclick="getCustomForm(\'/admin/purchases/copy/' +obj.id +'\',\'/admin/tire-parts/create\', event)" class="bigger-140">' +
                        '<button class="btn btn-warning btn-circle" type="button" title="Kopiranje klijenta">' +
                            '<i class="fa fa-copy"></i>' +
                        '</button>' +
                    '</a>' +
                    '<a href="/admin/tire-parts/print-file/' +obj.id +'" class="bigger-140" target="_blank">' +
                        '<button class="btn btn-success btn-circle bigger-120" type="button" title="Stampa Fakture">' +
                          '<i class="fa fa-print bigger-120"></i>' +
                        '</button>' +
                    '</a>' +
                '</div>';
    };

</script>
<div class="row">
    <div class="col-xs-12">
        <a href="/admin/tire-parts" class="btn btn-primary">
            <i class="fa fa-ambulance"></i>
            {{ t('Nazad') }}
        </a>
    </div>
</div>
<div class="space-15 clear"></div>
<div class="row">
    <div class="col-xs-12">
        <table class="table table-bordered">
            <tbody>
            <tr>
                <td>Tip Klijenta</td>
                <td>{{ purchase.getDisplayTypeOfClient() }}</td>
            </tr>
            <tr>
                <td>Faktura</td>
                <td>{{ purchase.getInvoice() }}</td>
            </tr>
            <tr>
                <td>Klijent</td>
                <td>{{ purchase.getClient().getDisplayName() }}</td>
            </tr>
            <tr>
                <td>Telefon Klijenta</td>
                <td>{{ purchase.getPhoneNumber() }}</td>
            </tr>
            <tr>
                <td>Garant</td>
                <td>{{ purchase.getGuarantor() }}</td>
            </tr>
            <tr>
                <td>Telefon Garanta</td>
                <td>{{ purchase.getGuarantorPhone() }}</td>
            </tr>
            <tr>
                <td>Vrsta</td>
                <td>{{ purchase.getModel() }}</td>
            </tr>
            <tr>
                <td>Detalji</td>
                <td>{{ purchase.getExtra() }}</td>
            </tr>
            <tr>
                <td>Datum</td>
                <td>{{ purchase.getDate() ? purchase.getDate().format('d.m.Y') : '' }}</td>
            </tr>
            <tr>
                <td>Vrednost Kupovine</td>
                <td>{{ purchase.getPrice() }}</td>
            </tr>
            <tr>
                <td>Dokument</td>
                <td>
                    {% if purchase.getFile() %}
                        <a href="/admin/purchases/download/{{ purchase.getId() }}" class="btn btn-primary">
                            <i class="fa fa-download"> {{ t('Skini dokument') }}</i>
                        </a>
                    {% endif %}
                </td>
            </tr>
            <tr>
                <td>Saradnik</td>
                <td>{{ purchase.getContributor() }}</td>
            </tr>
            <tr>
                <td>Vrednost ucinka saradnika</td>
                <td>{{ purchase.getContributorAmount() }}</td>
            </tr>
            <tr>
                <td>Napomena</td>
                <td>{{ purchase.getNote() }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="space-15 clear"></div>

<div class="row">
    <div class="col-xs-12">
        <h2><i class="fa fa-money"></i> Uplate</h2>
        <hr/>
    </div>
</div>

{% set paymentInstallmentsTotalAmount = 0 %}
{% for paymentInstallment in purchase.getPaymentInstallments() %}
    {% set paymentInstallmentsTotalAmount = paymentInstallmentsTotalAmount + paymentInstallment.getAmount() %}
{% endfor %}
{% if paymentInstallmentsTotalAmount < purchase.getPrice() %}
    {% set difference = purchase.getPrice() - paymentInstallmentsTotalAmount%}
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-danger">{{ t('Vrednost rata se razlikuje od ukupne vrednosti! Rasporediti preostali iznos od: '~ difference) }}</div>
        </div>
    </div>
{% elseif paymentInstallmentsTotalAmount > purchase.getPrice() %}
    {% set difference =  purchase.getPrice() - paymentInstallmentsTotalAmount %}
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-info">{{ t('Vrednost rata se razlikuje od ukupne vrednosti! Rasporediti preostali iznos od: '~ difference) }}</div>
        </div>
    </div>
{% endif %}


<div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Datum naplate</th>
                    <th>Iznos</th>
                    <th>Datum Uplate</th>
                    <th>Akcija</th>
                </tr>
                </thead>
                <tbody>

                {% set totalRows = 6 - purchase.getPaymentInstallments()|length %}

                {% set paymentInstallmentsTotalAmount = 0 %}

                {% for paymentInstallment in purchase.getPaymentInstallments(['order': 'date ASC']) %}
                    {% set paymentInstallmentsTotalAmount = paymentInstallmentsTotalAmount + paymentInstallment.getAmount() %}
                    <tr>
                        <td>{{ paymentInstallment.getDate().format('d.m.Y') }}</td>
                        <td>{{ paymentInstallment.getAmount() }}</td>
                        <td>{{ paymentInstallment.getPaymentDate() ? paymentInstallment.getPaymentDate().format('d.m.Y') : '' }}</td>
                        <td>

                            {% if not paymentInstallment.getPaymentDate() %}
                                <a href="#" onclick="getAjaxForm('admin/payment-installments/add-payment/{{ paymentInstallment.getId() }}', event)" class="bigger-140 text-success">
                                    <button class="btn btn-primary btn-circle" type="button" title="Dodaj Uplatu">
                                        <i class="fa fa-check-square-o"></i>
                                    </button>
                                </a>


                            {% endif %}
                            <a href="#" onclick="getUpdateForm('admin/payment-installments', {{ paymentInstallment.getId() }}, event)" class="bigger-140 text-success">
                                <button class="btn btn-info btn-circle" type="button" title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </button>
                            </a>
                            {% if currentUser.role == 'ACCOUNT' or currentUser.role == 'ADMIN' %}

                                <a href="#" onclick="getDeleteForm('admin/payment-installments', {{ paymentInstallment.getId() }}, event)" class="bigger-140 text-danger">
                                    <button class="btn btn-danger btn-circle" type="button" title="Brisanje">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </a>

                            {% if paymentInstallment.getPaymentDate() %}
                                <a href="#" onclick="getAjaxForm('admin/payment-installments/delete-payment/{{ paymentInstallment.getId() }}', event)" class="bigger-140 text-success">
                                    <button class="btn btn-danger btn-circle" type="button" title="Rascekiranje">
                                        <i class="fa fa-check-square-o"></i>
                                    </button>
                                </a>
                            {% endif %}

                            {% endif %}
                        </td>
                    </tr>
                {% endfor %}
                {% if totalRows and paymentInstallmentsTotalAmount < purchase.getPrice() %}
                    {% for i in 1..totalRows %}
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <a href="#" onclick="getCustomCreateForm('admin/payment-installments', 'purchase-id', {{ purchase.getId() }}, event)" class="bigger-140 text-success">
                                    <button class="btn btn-warning btn-circle" type="button">
                                        <i class="fa fa-money"></i>
                                    </button>
                                </a>

                            </td>
                        </tr>
                    {% endfor %}
                {% endif %}
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="row">

    <div class="col-xs-12">
        <div class="text-center">
            <h1 class="logo-name">VEMID</h1>
        </div>
    </div>
</div>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>

        <h3>{{ t('Dobro dosli') }}</h3>
        <p>Login in!</p>
        <form class="m-t" role="form" method="post" action="">
            <div class="form-group">
                <input type="text" class="form-control" name="username" id="username" placeholder="Username" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" id="password" placeholder="Password" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

        </form>
        <p class="m-t"> <small>Vemid software development  &copy; 2016</small> </p>
    </div>
</div>


    <div class="jqgrid_wrapper">
    <table id="grid-table"></table>
    <div id="grid-pager"></div>
</div>
<script type="text/javascript">
    (function ($) {

        $(document).ready(function () {

            $('#from, #to').bind("enterKey",function(e){
                var from = $('#from').val();
                var to = $('#to').val();
                var status = $('.dropdown-menu li[data-status="active"]').attr("data-filter");

                var postData = {
                    from: from,
                    to: to,
                    statusPayment: status
                };

                $("#grid-table").jqGrid('setGridParam',{
                            postData:postData,
                            search:true
                        }).trigger('reloadGrid');

            });
            $('#from,#to').keyup(function(e){
                if(e.keyCode == 13){
                    $(this).trigger("enterKey");
                }
            });

            $(document).on("click", ".dropdown-menu li", function() {
                var from = $('#from').val();
                var to = $('#to').val();
                var status = $(this).attr("data-filter");

                if(typeof status === 'undefined') {
                    status = '';
                }

                var postData = {
                    from: from,
                    to: to,
                    statusPayment: status
                };

                $("#grid-table").jqGrid('setGridParam',{
                    postData:postData,
                    search:true
                }).trigger('reloadGrid');
            });

            getRegistrations();

            $(window).bind('resize', function () {
                var width = $('.jqgrid_wrapper').width();
                $('#grid-table').setGridWidth(width);
            });
        });
    })(jQuery);


    function getRegistrations(myPostData) {

        var grid_selector = "#grid-table";
        var pager_selector = "#grid-pager";

        $(grid_selector).jqGrid({
            url: "/admin/payment-installments/get-by-period/2",
            datatype: "json",
            mtype: "post",
            height: '100%',
            jsonReader: {
                root: "purchase",
                page: "currpage",
                total: "totalpages",
                records: "totalrecords",
                id: "id"
            },
            colNames: [
                'Ime i Prezime',
                'Garant',
                'Telefon',
                'JMBG',
                'Reg. Broj',
                'Model',
                'Vrednost',
                'Uredi',
                'Boja'
            ],
            colModel: [
                {
                    name: 'name',
                    index: 'name',
                    width: 120,
                    sorttype: 'name',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'guarantor',
                    index: 'guarantor',
                    width: 70,
                    sorttype: 'guarantor',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'phoneNumber',
                    index: 'phoneNumber',
                    width: 70,
                    sorttype: 'phoneNumber',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'jmbg',
                    index: 'jmbg',
                    width: 90,
                    sorttype: 'jmbg',
                    align: 'center',
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'plates',
                    index: 'plates',
                    width: 70,
                    sorttype: 'plates',
                    align: 'center',
                    search: true,
                    searchoptions: {
                        sopt: ['cn', 'eq'],
                        clearSearch: true
                    }
                },
                {
                    name: 'model',
                    index: 'model',
                    width: 80,
                    sorttype: 'model',
                    align: 'center',
                    search: false
                },
                {
                    name: 'amount',
                    index: 'amount',
                    width: 70,
                    sorttype: 'amount',
                    align: 'center',
                    search: false
                },
                {
                    name: 'action',
                    index: '',
                    width: 60,
                    align: 'center',
                    search: false,
                    formatter: addOptionButtons
                },
                {
                    name: 'color',
                    index: 'color',
                    width: 80,
                    sorttype: 'color',
                    hidden: true,
                    align: 'center',
                    search: false
                }

            ],
            viewrecords: true,
            rowNum: 25,
            rowList: [25, 50, 75, 100],
            pager: pager_selector,
            page: 1,
            sortname: 'date',
            sortorder: 'desc',
            nexticon: 'fa fa-refresh green',
            altRows: true,
            multiselect: false,
            multiboxonly: true,
            search: true,
            footerrow: true,
            beforeSubmitCell : function(rowid,celname,value,iRow,iCol) {
                var color = $(grid_selector).getCell(rowid, "rowColor");
                return {
                    color:color
                };
            },
            beforeRequest: function () {
                $(grid_selector).jqGrid('filterToolbar', {
                    defaultSearch: true,
                    stringResult: true,
                    beforeSearch: function () {
                        if ($(this).data('firstSearchAbortedFlag') != '1') {
                            $(this).data('firstSearchAbortedFlag', '1');
                            return true;
                        }
                        return false;
                    }
                });

                if ($(this).data('defaultValuesSetFlag') != '1') {
                    $(this).data('defaultValuesSetFlag', '1');
                    $(grid_selector)[0].triggerToolbar();
                }

                if ("" != myPostData) {
                    $(grid_selector).jqGrid('setGridParam', {'postData': myPostData});
                }
            },
            gridComplete: function () {
            },
            loadComplete: function (data) {
                var table = this;
                setTimeout(function () {
                    updatePagerIcons(table);
                    enableTooltips(table);
                    updateTableIcons(table);
                }, 0);


                $(table).jqGrid('footerData','set',
                    {
                        name:'<span style="font-size:130%"><b>TOTAL</b></span>',
                        amount:'<span style="font-size:130%; color:red"> ' + data.sum + '</span>'
                    }
                );

                myPostData = "";
            },
            autowidth: true

        });

        //navButtons
    }

    var addOptionButtons = function (obj) {
        return '<div class="center action-buttons">' +
                    '<a href="/admin/registrations/overview/'+ obj.id +'" class="bigger-140">' +
                        '<button class="btn btn-primary btn-circle" type="button">' +
                            '<i class="fa fa-list"></i>' +
                        '</button>' +
                    '</a>&nbsp;' +
                '</div>';
    };

</script>
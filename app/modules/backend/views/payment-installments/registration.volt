<div class="row">
    <div class="col-xs-12">
        <h2>Izvestaj Dnevnog Prometa</h2>
    </div>
</div>
<div class="space-15 clear"></div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-3">
        <input type="text" id="from" name="from" class="form-control" value =" {{ today }}" placeholder="Od Datuma"/>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">
        <input type="text" id="to" name="to" class="form-control" value = "{{ today }}" placeholder="Do Datuma"/>
    </div>
</div>
<div class="space-15 clear"></div>
<div class="row">
    <div class="col-xs-12">
        {{ partial('payment-installments/partials/scripts-registration') }}
    </div>
</div>
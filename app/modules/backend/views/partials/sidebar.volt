<!--<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li>
                {{ link_to('/', '<i class="fa fa-home"></i><span class="nav-label">' ~ t('Home') ~ '</span>') }}
            </li>
            <li class="{{ router.getControllerName() == 'dashboard' ? 'active' : '' }}">
                {{ link_to('admin/dashboard', '<i class="fa fa-cogs"></i><span class="nav-label">' ~ t('Dashboard') ~ '</span>') }}
            </li>
            <li class="{{ router.getControllerName() == 'sport-categories' ? 'active' : '' }}">
                {{ link_to('admin/sport-categories', '<i class="fa fa-sitemap"></i><span class="nav-label">' ~ t('Sport categories') ~ '</span>') }}
            </li>
            <li class="{{ router.getControllerName() == 'pages' ? 'active' : '' }}">
                {{ link_to('admin/pages', '<i class="fa fa-paste"></i><span class="nav-label">' ~ t('Pages') ~ '</span>') }}
            </li>
            <li class="{{ router.getControllerName() == 'post-categories' ? 'active' : '' }}">
                {{ link_to('admin/post-categories', '<i class="fa fa-sitemap"></i><span class="nav-label">' ~ t('Post categories') ~ '</span>') }}
            </li>
            <li class="{{ router.getControllerName() == 'posts' ? 'active' : '' }}">
                {{ link_to('admin/posts', '<i class="fa fa-copy"></i><span class="nav-label">' ~ t('Posts') ~ '</span>') }}
            </li>
        </ul>
    </div>
</nav>-->

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul id="side-menu" class="nav metismenu">
            <li class="nav-header">
                <div class="dropdown profile-element text-center">
                    <span>
                        <img class="img-circle" width="60px" src="{{ currentUser.getAvatarUrl() }}" alt="image">
                    </span>
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                        <span class="clear">
                            <span class="text-muted text-xs block">
                                {{ currentUser.displayName }}
                            </span>
                        </span>
                    </a>
                </div>
            </li>
            <li class="{{ request.getUri() == '/admin/registrations' ? 'active' : '' }}">
                <a href="/admin/registrations">
                    <i class="fa fa-car"></i>
                    <span class="nav-label">{{ t('Registracija') }}</span>
                </a>
            </li>
            <li class="{{ request.getUri() == '/admin/tire-parts' ? 'active' : '' }}">
                <a href="/admin/tire-parts">
                    <i class="fa fa-gear"></i>
                    <span class="nav-label">{{ t('Gume i Delovi') }}</span>
                </a>
            </li>
                        <li class="{{ request.getUri() == '/admin/onesta-tire-parts' ? 'active' : '' }}">
                            <a href="/admin/onesta-tire-parts">
                                <i class="fa fa-gear"></i>
                                <span class="nav-label">{{ t('Onesta Delovi') }}</span>
                            </a>
                        </li>
            <li class="">
                <a href="#">
                    <i class="fa fa-signal"></i>
                    <span class="nav-label">{{ t('Izvestaji') }}</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li>
                        <a href="/admin/payment-installments/registration">
                            <i class="fa fa-money"></i>
                            <span class="nav-label">{{ t('Promet Reg.') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/payment-installments/parts">
                            <i class="fa fa-money"></i>
                            <span class="nav-label">{{ t('Promet Delova') }}</span>
                        </a>
                    </li>
                    <li>
                                            <a href="/admin/payment-installments/onesta-parts">
                                                <i class="fa fa-money"></i>
                                                <span class="nav-label">{{ t('Onesta Promet Delova') }}</span>
                                            </a>
                                        </li>
                    <li>
                        <a href="/admin/reports/contributor">
                            <i class="fa fa-key"></i>
                            <span class="nav-label">{{ t('Izvestaj Saradnika') }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/reports/technical">
                            <i class="fa fa-cogs"></i>
                            <span class="nav-label">{{ t('Izvestaj Tehnickog') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        </ul>
    </div>
</nav>

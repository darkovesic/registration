{% if formUrl is defined and object is defined %}

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="button" class="btn btn-white"
                        onclick="getUpdateForm('{{ formUrl }}', {{ object.id }}, event)"
                        title="{{ t('Edit') }}">
                    <i class="fa fa-edit"></i>
                </button>
                <button type="button" class="btn btn-white"
                        onclick="getDeleteForm('{{ formUrl }}', {{ object.id }}, event)"
                        title="{{ t('Delete') }}">
                    <i class="fa fa-eraser"></i>
                </button>
            </div>
        </div>
    </div>

{% endif %}
{% if pagination is defined and pagination.items|length %}

    {% set limit = pagination.items|length %}
    {% set min = (pagination.current == pagination.last) ? pagination.total_items - limit + 1 : 1 + (pagination.current - 1) * limit %}
    {% set max = (pagination.current == pagination.last) ? pagination.total_items : pagination.current * limit %}
    {% set total = pagination.total_items %}

    <?php
    $currentUrl = $this->request->getURI();
    $currentUrl = preg_replace('/[\?|&]*page=\d+/', '', $currentUrl);
    $currentUrl .= (false === strpos($currentUrl, '?') ? '?page=' : '&page=');
    ?>

    <div class="row">
        <div class="col-sm-6">
            {{ t('Showing %d to %d of %d entries', [min, max, total]) }}
        </div>
        <div class="col-sm-6">
            <div class="btn-group pull-right">
                <a class="btn btn-white" href="{{ currentUrl ~ '1' }}" title="{{ t('First') }}">
                    <i class="fa fa-step-backward"></i>
                </a>
                <a class="btn btn-white" href="{{ currentUrl ~  pagination.before }}" title="{{ t('Previous') }}">
                    <i class="fa fa-backward"></i>
                </a>
                <a class="btn btn-white active" href="javascript:void(0)">{{ pagination.current }}</a>
                <a class="btn btn-white"
                   href="{{ currentUrl ~ (pagination.next > pagination.current ? pagination.next : pagination.last) }}"
                   title="{{ t('Next') }}">
                    <i class="fa fa-forward"></i>
                </a>
                <a class="btn btn-white" href="{{ currentUrl ~ pagination.last }}" title="{{ t('Last') }}">
                    <i class="fa fa-step-forward"></i>
                </a>
            </div>
        </div>
    </div>
{% endif %}
<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" style="margin-bottom: 0" role="navigation">
        <div class="navbar-header col-xs-6 no-padding">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
            <form class="navbar-form-custom" action="" role="search">
                <div class="form-group">
                    <input id="top-search border" class="form-control bordered" type="text" name="top-search" placeholder="{{ t('Pretraga') }}">
                </div>
            </form>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a href="/admin/auth/logout">
                    <i class="fa fa-sign-out"></i>
                    Log out
                </a>
            </li>
        </ul>
    </nav>
</div>
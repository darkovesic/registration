<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{ get_title() }}
    {{ assets.outputCss('headCss') }}
    {{ assets.outputJs('headJs') }}
</head>

<body class="gray-bg">

    <div id="flash-session">
        {{ flashSession.output() }}
    </div>
    {{ content() }}

{{ assets.outputCss('footCss') }}
{{ assets.outputJs('footJs') }}
<!-- Mainly scripts -->
</body>

</html>

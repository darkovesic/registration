<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {{ get_title() }}
    {{ assets.outputCss('headCss') }}
    {{ assets.outputJs('headJs') }}
    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
</head>
<body class="pace-done">
<div id="wrapper">
    {{ partial('partials/sidebar') }}

    <div id="page-wrapper" class="gray-bg dashbard-1">

        {{ partial('partials/header') }}

            <div id="flash-session">
                {{ flashSession.output() }}
            </div>

            <div id="content" class="wrapper wrapper-content">
                {{ content() }}
            </div>

            <div class="footer">
                {{ partial('partials/footer') }}
            </div>
        </div>

         {{ partial('partials/modal') }}

    </div>
</div>

{{ assets.outputCss('footCss') }}
{{ assets.outputJs('footJs') }}
</body>
</html>
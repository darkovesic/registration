SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users` ;

CREATE TABLE IF NOT EXISTS `users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  `password_hash` VARCHAR(255) NOT NULL,
  `avatar` VARCHAR(20) NULL,
  `gender` ENUM('MALE','FEMALE') NULL,
  `role` ENUM('MEMBER','TRAINER', 'SPORTSMEN','ADMIN') NULL DEFAULT NULL,
  `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `last_ip` VARCHAR(20) NULL,
  `registered_datetime` DATETIME NULL,
  `last_visit_datetime` DATETIME NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username` (`username` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `languages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `languages` ;

CREATE TABLE IF NOT EXISTS `languages` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(5) NOT NULL,
  `locale` VARCHAR(5) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `is_active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `is_default` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `is_translated` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `countries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `countries` ;

CREATE TABLE IF NOT EXISTS `countries` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `iso_alpha_2` VARCHAR(2) NOT NULL,
  `iso_alpha_3` VARCHAR(3) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `spoken_languages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `spoken_languages` ;

CREATE TABLE IF NOT EXISTS `spoken_languages` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `language_id` INT(11) UNSIGNED NOT NULL,
  `level` ENUM('NATIVE','FLUENT','OTHER') NULL DEFAULT 'OTHER',
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  INDEX `language_id` (`language_id` ASC),
  CONSTRAINT `spoken_languages__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `spoken_languages__languages`
  FOREIGN KEY (`language_id`)
  REFERENCES `languages` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `currencies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `currencies` ;

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(3) NOT NULL,
  `symbol` VARCHAR(3) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `is_default` TINYINT(1) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `orders` ;

CREATE TABLE IF NOT EXISTS `orders` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `paid_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `paid_entity_id` INT(11) UNSIGNED NOT NULL,
  `net_amount` FLOAT(9,2) UNSIGNED NOT NULL,
  `vat_amount` FLOAT(9,2) UNSIGNED NOT NULL,
  `total_amount` FLOAT(9,2) UNSIGNED NOT NULL,
  `currency_id` INT(11) UNSIGNED NOT NULL,
  `payment_status` ENUM('PENDING','FAILED','VOIDED','CANCELED','SUCCEEDED') NULL DEFAULT 'PENDING',
  `created_datetime` DATETIME NULL,
  `updated_datetime` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `currency_id` (`currency_id` ASC),
  INDEX `user_id` (`user_id` ASC),
  CONSTRAINT `orders__currencies`
  FOREIGN KEY (`currency_id`)
  REFERENCES `currencies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `orders__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `services`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `services` ;

CREATE TABLE IF NOT EXISTS `services` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `net_amount` FLOAT(9,2) UNSIGNED NOT NULL,
  `vat_amount` FLOAT(9,2) UNSIGNED NOT NULL,
  `currency_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(100) NOT NULL COMMENT 'This should be in translations',
  `description` MEDIUMTEXT NULL COMMENT 'This should be in translations',
  PRIMARY KEY (`id`),
  INDEX `currency_id` (`currency_id` ASC),
  CONSTRAINT `services__currencies`
  FOREIGN KEY (`currency_id`)
  REFERENCES `currencies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `subscriptions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `subscriptions` ;

CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `service_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `api_key` VARCHAR(100) NOT NULL,
  `valid_from` DATE NOT NULL,
  `valid_to` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  INDEX `service_id` (`service_id` ASC),
  UNIQUE INDEX `api_key` (`api_key` ASC),
  CONSTRAINT `subscriptions__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `subscriptions__services`
  FOREIGN KEY (`service_id`)
  REFERENCES `services` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `translations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `translations` ;

CREATE TABLE IF NOT EXISTS `translations` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `translated_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `translated_entity_id` INT(11) UNSIGNED NOT NULL,
  `language_id` INT(11) UNSIGNED NOT NULL,
  `translated_property` VARCHAR(100) NULL DEFAULT 'name',
  `translated_value` MEDIUMTEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `translated_entity` (`translated_entity_type_id` ASC, `translated_entity_id` ASC),
  INDEX `language_id` (`language_id` ASC),
  CONSTRAINT `translations__languages`
  FOREIGN KEY (`language_id`)
  REFERENCES `languages` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `members`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `members` ;

CREATE TABLE IF NOT EXISTS `members` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  CONSTRAINT `members__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `trainers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `trainers` ;

CREATE TABLE IF NOT EXISTS `trainers` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `biography` MEDIUMTEXT NULL DEFAULT NULL,
  `sportsman_from` DATE NULL DEFAULT NULL,
  `teaching_from` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `users_id` (`user_id` ASC),
  CONSTRAINT `trainers__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sportsmen`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sportsmen` ;

CREATE TABLE IF NOT EXISTS `sportsmen` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `sportsman_from` DATE NULL DEFAULT NULL,
  `biography` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  CONSTRAINT `sportsmen__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `certifications`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `certifications` ;

CREATE TABLE IF NOT EXISTS `certifications` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `trainer_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `trainer_id` (`trainer_id` ASC),
  CONSTRAINT `certifications__trainers`
  FOREIGN KEY (`trainer_id`)
  REFERENCES `trainers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sport_categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sport_categories` ;

CREATE TABLE IF NOT EXISTS `sport_categories` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `programmes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `programmes` ;

CREATE TABLE IF NOT EXISTS `programmes` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `trainer_id` INT(11) UNSIGNED NOT NULL,
  `sport_category_id` INT(11) UNSIGNED NOT NULL,
  `number_of_trainings` INT(11) UNSIGNED NOT NULL,
  `training_type` ENUM('INDIVIDUAL','GROUP') NOT NULL DEFAULT 'INDIVIDUAL',
  `total_amount` DECIMAL(9,2) UNSIGNED NOT NULL,
  `currency_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `currency_id` (`currency_id` ASC),
  INDEX `trainer_id` (`trainer_id` ASC),
  INDEX `sport_category_id` (`sport_category_id` ASC),
  CONSTRAINT `programmes__currencies`
  FOREIGN KEY (`currency_id`)
  REFERENCES `currencies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `programmes__trainers`
  FOREIGN KEY (`trainer_id`)
  REFERENCES `trainers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `programmes__sport_categories`
  FOREIGN KEY (`sport_category_id`)
  REFERENCES `sport_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teaching_locations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `teaching_locations` ;

CREATE TABLE IF NOT EXISTS `teaching_locations` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `teacher_entity_id` INT(11) UNSIGNED NOT NULL,
  `sport_category_id` INT(11) UNSIGNED NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `country_id` INT(11) UNSIGNED NOT NULL,
  `latitude` DECIMAL(9,6) NOT NULL,
  `longitude` DECIMAL(9,6) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `sport_category_id` (`sport_category_id` ASC),
  INDEX `country_id` (`country_id` ASC),
  INDEX `teacher_entity` (`teacher_entity_type_id` ASC, `teacher_entity_id` ASC),
  CONSTRAINT `teaching_locations__sport_categories`
  FOREIGN KEY (`sport_category_id`)
  REFERENCES `sport_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `teaching_locations__countries`
  FOREIGN KEY (`country_id`)
  REFERENCES `countries` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bid_requests`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bid_requests` ;

CREATE TABLE IF NOT EXISTS `bid_requests` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sportsman_id` INT(11) UNSIGNED NOT NULL,
  `min_amount` DECIMAL(9,2) NOT NULL,
  `currency_id` INT(11) UNSIGNED NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `country_id` INT(11) UNSIGNED NOT NULL,
  `start_datetime` DATETIME NOT NULL,
  `end_datetime` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `sportsman_id` (`sportsman_id` ASC),
  INDEX `currency_id` (`currency_id` ASC),
  CONSTRAINT `bid_requests__sportsmen`
  FOREIGN KEY (`sportsman_id`)
  REFERENCES `sportsmen` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bid_requests__currencies`
  FOREIGN KEY (`currency_id`)
  REFERENCES `currencies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bids`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bids` ;

CREATE TABLE IF NOT EXISTS `bids` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `member_id` INT(11) UNSIGNED NOT NULL,
  `sportsman_id` INT(11) UNSIGNED NOT NULL,
  `bid_request_id` INT(11) UNSIGNED NOT NULL,
  `status` ENUM('PENDING','ACCEPTED','REJECTED') NOT NULL DEFAULT 'PENDING',
  `total_amount` DECIMAL(9,2) NOT NULL,
  `currency_id` INT(11) UNSIGNED NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `city` VARCHAR(255) NOT NULL,
  `country_id` INT(11) UNSIGNED NOT NULL,
  `latitude` DECIMAL(9,6) NOT NULL,
  `longitude` DECIMAL(9,6) NOT NULL,
  `created_datetime` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `member_id` (`member_id` ASC),
  INDEX `sportsman_id` (`sportsman_id` ASC),
  INDEX `currency_id` (`currency_id` ASC),
  INDEX `bid_request_id` (`bid_request_id` ASC),
  INDEX `country_id` (`country_id` ASC),
  CONSTRAINT `bids__bid_requests`
  FOREIGN KEY (`bid_request_id`)
  REFERENCES `bid_requests` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bids__members`
  FOREIGN KEY (`member_id`)
  REFERENCES `members` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bids__sportsmen`
  FOREIGN KEY (`sportsman_id`)
  REFERENCES `sportsmen` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bids__currencies`
  FOREIGN KEY (`currency_id`)
  REFERENCES `currencies` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bids__countries`
  FOREIGN KEY (`country_id`)
  REFERENCES `countries` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `trainings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `trainings` ;

CREATE TABLE IF NOT EXISTS `trainings` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `teacher_entity_id` INT(11) UNSIGNED NOT NULL,
  `member_id` INT(11) UNSIGNED NOT NULL,
  `sport_category_id` INT(11) UNSIGNED NOT NULL,
  `programme_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `bid_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `teaching_location_id` INT(11) UNSIGNED NOT NULL,
  `level` ENUM('BEGINNER','INTERMEDIATE','ADVANCED') NULL DEFAULT NULL,
  `group` ENUM('KIDS','TEENAGERS','ADULTS') NULL DEFAULT NULL,
  `status` ENUM('PENDING','COMPLETED','CANCELED') NOT NULL DEFAULT 'PENDING',
  `start_datetime` DATETIME NOT NULL,
  `end_datetime` DATETIME NOT NULL,
  `description` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `member_id` (`member_id` ASC),
  INDEX `programme_id` (`programme_id` ASC),
  INDEX `teacher_entity` (`teacher_entity_type_id` ASC, `teacher_entity_id` ASC),
  INDEX `teaching_location_id` (`teaching_location_id` ASC),
  INDEX `sport_category_id` (`sport_category_id` ASC),
  INDEX `bid_id` (`bid_id` ASC),
  CONSTRAINT `trainings__members`
  FOREIGN KEY (`member_id`)
  REFERENCES `members` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `trainings__programmes`
  FOREIGN KEY (`programme_id`)
  REFERENCES `programmes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `trainings__teaching_locations`
  FOREIGN KEY (`teaching_location_id`)
  REFERENCES `teaching_locations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `trainings__sport_categories`
  FOREIGN KEY (`sport_category_id`)
  REFERENCES `sport_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `trainings__bids`
  FOREIGN KEY (`bid_id`)
  REFERENCES `bids` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `timetable_slots`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `timetable_slots` ;

CREATE TABLE IF NOT EXISTS `timetable_slots` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `trainer_id` INT(11) UNSIGNED NOT NULL,
  `day_of_week` SMALLINT(1) UNSIGNED NULL DEFAULT NULL,
  `start_time` TIME(2) NOT NULL,
  `end_time` TIME(2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `trainer_id` (`trainer_id` ASC),
  CONSTRAINT `timetable_slots__trainers`
  FOREIGN KEY (`trainer_id`)
  REFERENCES `trainers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teaching_levels`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `teaching_levels` ;

CREATE TABLE IF NOT EXISTS `teaching_levels` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `teacher_entity_id` INT(11) UNSIGNED NOT NULL,
  `sport_category_id` INT(11) UNSIGNED NOT NULL,
  `level` ENUM('BEGINNER','INTERMEDIATE','ADVANCED') NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `sport_category_id` (`sport_category_id` ASC),
  INDEX `teacher_entity` (`teacher_entity_type_id` ASC, `teacher_entity_id` ASC),
  CONSTRAINT `teaching_levels__sport_categories`
  FOREIGN KEY (`sport_category_id`)
  REFERENCES `sport_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teaching_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `teaching_groups` ;

CREATE TABLE IF NOT EXISTS `teaching_groups` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `teacher_entity_id` INT(11) UNSIGNED NOT NULL,
  `sport_category_id` INT(11) UNSIGNED NOT NULL,
  `group` ENUM('KIDS','TEENAGERS','ADULTS') NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `sport_category_id` (`sport_category_id` ASC),
  INDEX `teacher_entity` (`teacher_entity_type_id` ASC, `teacher_entity_id` ASC),
  CONSTRAINT `teaching_groups__sport_categories`
  FOREIGN KEY (`sport_category_id`)
  REFERENCES `sport_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `rates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rates` ;

CREATE TABLE IF NOT EXISTS `rates` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `rated_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `rated_entity_id` INT(11) UNSIGNED NOT NULL,
  `rate` SMALLINT(3) NULL DEFAULT NULL,
  `created_datetime` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  INDEX `rated_entity` (`rated_entity_type_id` ASC, `rated_entity_id` ASC),
  CONSTRAINT `rates__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `training_requests`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `training_requests` ;

CREATE TABLE IF NOT EXISTS `training_requests` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `member_id` INT(11) UNSIGNED NOT NULL,
  `programme_id` INT(11) UNSIGNED NOT NULL,
  `sport_category_id` INT(11) UNSIGNED NOT NULL,
  `level` ENUM('BEGINNER','INTERMEDIATE','ADVANCED') NOT NULL,
  `group` ENUM('KIDS','TEENAGERS','ADULTS') NOT NULL,
  `status` ENUM('PENDING','ACCEPTED','REJECTED') NOT NULL DEFAULT 'PENDING',
  `start_datetime` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `member_id` (`member_id` ASC),
  INDEX `sport_category_id` (`sport_category_id` ASC),
  INDEX `programme_id` (`programme_id` ASC),
  CONSTRAINT `training_requests__programmes`
  FOREIGN KEY (`programme_id`)
  REFERENCES `programmes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `training_requests__members`
  FOREIGN KEY (`member_id`)
  REFERENCES `members` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `training_requests__sport_categories`
  FOREIGN KEY (`sport_category_id`)
  REFERENCES `sport_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `social_networks`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `social_networks` ;

CREATE TABLE IF NOT EXISTS `social_networks` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `url` VARCHAR(255) NOT NULL,
  `type` ENUM('FACEBOOK','TWITTER','YOUTUBE','LINKEDIN','INSTAGRAM','PINTEREST') NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  CONSTRAINT `social_networks__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pages` ;

CREATE TABLE IF NOT EXISTS `pages` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` VARCHAR(255) BINARY NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` MEDIUMTEXT NULL,
  `featured_image` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `slug` (`slug` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `galleries`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `galleries` ;

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `training_id` INT(11) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `description` MEDIUMTEXT NULL DEFAULT NULL,
  `created_datetime` DATETIME NULL DEFAULT NULL,
  `modified_datetime` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  INDEX `training_id` (`training_id` ASC),
  CONSTRAINT `galleries__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `galleries__trainings`
  FOREIGN KEY (`training_id`)
  REFERENCES `trainings` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `uploads`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `uploads` ;

CREATE TABLE IF NOT EXISTS `uploads` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gallery_id` INT(11) UNSIGNED NOT NULL,
  `file_name` VARCHAR(100) NOT NULL,
  `file_type` VARCHAR(100) NULL DEFAULT NULL,
  `file_size` DECIMAL(9,2) NULL DEFAULT NULL,
  `created_datetime` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `gallery_id` (`gallery_id` ASC),
  CONSTRAINT `uploads__galleries`
  FOREIGN KEY (`gallery_id`)
  REFERENCES `galleries` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `comments` ;

CREATE TABLE IF NOT EXISTS `comments` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `commented_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `commented_entity_id` INT(11) UNSIGNED NOT NULL,
  `parent_comment_id` INT(11) UNSIGNED NULL DEFAULT NULL,
  `comment` MEDIUMTEXT NOT NULL,
  `created_datetime` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  INDEX `commented_entity` (`commented_entity_type_id` ASC, `commented_entity_id` ASC),
  CONSTRAINT `comments__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `follows`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `follows` ;

CREATE TABLE IF NOT EXISTS `follows` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `member_id` INT(11) UNSIGNED NOT NULL,
  `followed_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `followed_entity_id` INT(11) UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `member_id` (`member_id` ASC),
  INDEX `followed_entity` (`followed_entity_type_id` ASC, `followed_entity_id` ASC),
  CONSTRAINT `follows__members`
  FOREIGN KEY (`member_id`)
  REFERENCES `members` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `searches`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `searches` ;

CREATE TABLE IF NOT EXISTS `searches` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `search_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `search_entity_id` INT(11) UNSIGNED NOT NULL,
  `search_value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `search_entity` (`search_entity_type_id` ASC, `search_entity_id` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teaching_categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `teaching_categories` ;

CREATE TABLE IF NOT EXISTS `teaching_categories` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `teacher_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `teacher_entity_id` INT(11) UNSIGNED NOT NULL,
  `sport_category_id` INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `teacher_entity` (`teacher_entity_type_id` ASC, `teacher_entity_id` ASC),
  INDEX `sport_category_id` (`sport_category_id` ASC),
  CONSTRAINT `teaching_categories__sport_categories`
  FOREIGN KEY (`sport_category_id`)
  REFERENCES `sport_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bank_accounts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bank_accounts` ;

CREATE TABLE IF NOT EXISTS `bank_accounts` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `account_number` VARCHAR(255) NOT NULL,
  `swift_code` VARCHAR(255) NULL DEFAULT NULL,
  `iban` VARCHAR(255) NULL DEFAULT NULL,
  `bank_name` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  CONSTRAINT `bank_accounts__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seo_metadata`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `seo_metadata` ;

CREATE TABLE IF NOT EXISTS `seo_metadata` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `seo_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `seo_entity_id` INT(11) UNSIGNED NOT NULL,
  `seo_keywords` MEDIUMTEXT NULL DEFAULT NULL,
  `seo_description` MEDIUMTEXT NULL DEFAULT NULL,
  `seo_robots` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `seo_entity` (`seo_entity_type_id` ASC, `seo_entity_id` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `likes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `likes` ;

CREATE TABLE IF NOT EXISTS `likes` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) UNSIGNED NOT NULL,
  `liked_entity_type_id` INT(11) UNSIGNED NOT NULL,
  `liked_entity_id` INT(11) UNSIGNED NOT NULL,
  `created_datetime` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC),
  CONSTRAINT `likes__users`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `post_categories`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `post_categories` ;

CREATE TABLE IF NOT EXISTS `post_categories` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` MEDIUMTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `slug` (`slug` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `posts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `posts` ;

CREATE TABLE IF NOT EXISTS `posts` (
  `id` INT(11) UNSIGNED NULL AUTO_INCREMENT,
  `post_category_id` INT(11) UNSIGNED NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `description` MEDIUMTEXT NULL,
  `featured_image` VARCHAR(255) NULL DEFAULT NULL,
  `created_datetime` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `slug` (`slug` ASC),
  INDEX `post_category_id` (`post_category_id` ASC),
  CONSTRAINT `posts__post_categories`
  FOREIGN KEY (`post_category_id`)
  REFERENCES `post_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

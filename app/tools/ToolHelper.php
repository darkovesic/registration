<?php

ini_set('memory_limit', '1024M');

define('APP_PATH', realpath(__DIR__ . '/../..') . '/');

require_once(APP_PATH . 'vendor/autoload.php');
require_once(APP_PATH . 'app/library/Comit/Application.php');

$application = new \Comit\Application();
$application->init();

<?php

require_once __DIR__ . '/../ToolHelper.php';

$di = $application->getDI();
$adapter = new Comit\Database\Adapter($di->get('db'));
$config = $di->get('config');

$objectNames = array();

foreach ($adapter->listTables() as $table) {

    $generator = new \Comit\CodeGenerator\Model($table);
    $writer = new \Comit\CodeGenerator\File($generator);

    if ($writer->write($config->application->modelsDir)) {
        echo "Successfully saved Model: '" . $writer->className . "'" . PHP_EOL;
    } else {
        echo "Not saved Model: '" . $writer->className . "'" . PHP_EOL;
    }

    $objectNames[strtoupper($table->getEntityName(true))] = $table->getEntityName();
}

/**
 * Generate Entity Type constants
 */
$i = 0;
foreach ($objectNames as $classConstant => $className) {
    $i++;
    echo "const $classConstant = $i;" . PHP_EOL;
}

foreach ($objectNames as $classConstant => $className) {
    echo "self::$classConstant => '$className'," . PHP_EOL;
}
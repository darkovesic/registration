<?php

require_once __DIR__ . '/../ToolHelper.php';

/** @var \User $user */
foreach (\User::find() as $user) {
    if ($user->isTrainer() && ($trainer = $user->getTrainer())) {
        \Search::updateExistingSearch($trainer, $user->getDisplayName());
    } else if ($user->isSportsman() && ($sportsman = $user->getSportsman())) {
        \Search::updateExistingSearch($sportsman, $user->getDisplayName());
    }
}

/** @var \SportCategory $sportCategory */
foreach (\SportCategory::find() as $sportCategory) {
    \Search::updateExistingSearch($sportCategory, $sportCategory->getName());
}

/** @var \TeachingLocation $teachingLocation */
foreach (\TeachingLocation::find() as $teachingLocation) {

    if ($teachingLocation->getObjectTypeId() === \Comit\Model\Type::TRAINER) {
        \Search::updateExistingSearch($teachingLocation, $teachingLocation->getDisplayName());
    }
}

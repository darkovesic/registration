<?php
/**
 * SMS Report Email Script
 * Created by PhpStorm
 * Modified to use SSL and HTML formatted email
 * Fixed RFC 5322 compliance issue with Message-ID
 */

include_once __DIR__ . '/../ToolHelper.php';

use Comit\Helper\DateTime;
use infobip\api\client\GetSentSmsLogs;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\sms\mt\logs\GetSentSmsLogsExecuteContext;

// Configuration
$username = 'onesta';
$password = 'adminabc123';
$date = new DateTime();

// Database Query
$modelsManager = $application->getDI()->get('modelsManager');
$query = $modelsManager->createBuilder();
$query->addFrom("\\SmsLog", "s")
    ->where("date(s.sent) = :date:", [
        'date' => (new DateTime())->format('Y-m-d')
    ]);
$logs = $query->getQuery()->execute();

// Generate HTML Content
$htmlContent = '
<!DOCTYPE html>
<html>
<head>
    <style>
        .sms-table {
            width: 100%;
            border-collapse: collapse;
            margin: 25px 0;
            font-family: Arial, sans-serif;
        }
        .sms-table th, .sms-table td {
            padding: 12px 15px;
            border: 1px solid #ddd;
        }
        .sms-table th {
            background-color: #f8f9fa;
            color: #333;
            font-weight: bold;
        }
        .sms-table tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        .sms-table tr:hover {
            background-color: #ddd;
        }
        @media screen and (max-width: 600px) {
            .sms-table {
                display: block;
                overflow-x: auto;
            }
        }
    </style>
</head>
<body>
    <table class="sms-table">
        <thead>
            <tr>
                <th>RBR.</th>
                <th>Sent at</th>
                <th>Sender</th>
                <th>Receiver</th>
                <th>Message text</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>';

foreach ($logs as $i => $log) {
    $htmlContent .= sprintf('
        <tr>
            <td>%d</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>DELIVERED | ID: %s</td>
        </tr>',
        $i + 1,
        htmlspecialchars($log->getSent()),
        'Onesta',
        htmlspecialchars($log->getPhone()),
        htmlspecialchars($log->getText()),
        htmlspecialchars($log->getSmsId())
    );
}

$htmlContent .= '
        </tbody>
    </table>
</body>
</html>';

// Configure SwiftMailer with SSL
$transport = new \Swift_SmtpTransport('smtp.titan.email', 465, 'ssl');
$transport->setUsername('info@vemid.net')
    ->setPassword('Admin710412!');

$mailer = new \Swift_Mailer($transport);

// Create Email Message
$mailMessage = new \Swift_Message();

// Let SwiftMailer handle the Message-ID automatically
// Removed the manual Message-ID header addition
$headers = $mailMessage->getHeaders();
$headers->addTextHeader('X-Mailer', 'Bebakids');

// Set Email Properties
$serbianDate = $date->format('d.m.Y');
$mailMessage->setSubject('Izvestaj poslatih poruka VEMID ' . $serbianDate)
    ->setFrom(['info@vemid.net' => 'Server Vemid'])
    ->setTo(['agencija.onesta@gmail.com' => 'Agencija Onesta'])
    ->setCc(['admin@bebakids.com' => 'Administrator BebaKids'])
    ->setBody($htmlContent, 'text/html');

// Send Email
$mailer->send($mailMessage);
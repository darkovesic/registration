<?php
/**
 * Created by PhpStorm.
 * User: darkovesic
 * Date: 3.3.16.
 * Time: 22:45
 */


use OnlineCity\Transport\SocketTransport;
use OnlineCity\SMPP\SMPP;
use OnlineCity\SMPP\SmppClient;
use OnlineCity\SMPP\SmppAddress;
use OnlineCity\Encoder\GsmEncoder;

include_once __DIR__ . '/../ToolHelper.php';

/** @var \Phalcon\Mvc\Model\Query\Builder $query */
$modelsManager = $application->getDI()->get('modelsManager');
$query = $modelsManager->createBuilder();
$query->addFrom("\\Purchase", "p")
    ->leftJoin('\Client', 'c.id = p.clientId', 'c')
    ->where("c.smsNotification = :smsNotification:", array(
        'smsNotification' => 1
    ))
    ->andWhere("p.registeredUntil = :date:", array(
        'date' => (new \Comit\Helper\DateTime('+30 days'))->format('Y-m-d')
    ));

$purchases = $query->getQuery()->execute();

$text = "Izvestaj poslatih poruka podsetnik registracije";

print_r($purchases->count());

/** @var \Purchase $purchase */
foreach($purchases as $purchase) {

    $phoneNumber = $purchase->getPhoneNumber();
    $phoneNumber = "00381".str_replace('+','', substr($phoneNumber,1));
    $carModel = $purchase->getModel();
    $plate = $purchase->getPlates();

    $displayName = $purchase->getClient()->getDisplayName();
    $registered = $purchase->getRegisteredUntil()->format('d-m-Y');

    $sender = new \Comit\Helper\SendSms();
    $message ="Agencija ONESTA Vas obavestava da uskoro istice registracija vaseg vozila $carModel , reg. oznake:$plate. Da obnovimo saradnju, pozovite nas na tel:064-1390-417, 011-4074-584";
//    $message2="Kancelarija Onesta je preseljena i nalazi se u servisu Onesta u ulici Bana Ivanisa 27 (okretnica trolejbusa 40)";
    $sender->execute($phoneNumber,$message);

//
//    $requestBody = new infobip\api\model\sms\mt\send\textual\SMSTextualRequest();
//    $requestBody->setFrom('ONESTA');
//    $requestBody->setTo($phoneNumber);
//    $requestBody->setText("Agencija ONESTA Vas obavestava da uskoro istice registracija vaseg vozila $carModel , reg. oznake:$plate. Da obnovimo saradnju, pozovite nas na tel:064-668-7983, 011-4074-584");
//
//    $response = $client->execute($requestBody);
    echo 'Poslao sms na broj: ' .$phoneNumber. ' id clijenta: '.$purchase->getClientId().', u vreme : '.new \Comit\Helper\DateTime();

    $text .="\n Poslao sms na broj: ".$phoneNumber." klijent: ".$purchase->getClient()->getDisplayName(). ", u vreme : ".new \Comit\Helper\DateTime();

    $i=0;
    $i=$i+1;
}

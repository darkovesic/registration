<?php
/**dasd
 * Created by PhpStorm.
 * User: darkovesic
 * Date: 3.3.16.
 * Time: 22:45
 */

use OnlineCity\Transport\SocketTransport;
use OnlineCity\SMPP\SMPP;
use OnlineCity\SMPP\SmppClient;
use OnlineCity\SMPP\SmppAddress;
use OnlineCity\Encoder\GsmEncoder;
include_once __DIR__ . '/../ToolHelper.php';

$client = new infobip\api\client\SendSingleTextualSms(new infobip\api\configuration\BasicAuthConfiguration('onesta', 'adminabc123'));

$endDate = (new \Comit\Helper\DateTime('+21 days'))->format('Y-m-d');


/** @var \Phalcon\Mvc\Model\Query\Builder $query */
$modelsManager = $application->getDI()->get('modelsManager');
$query = $modelsManager->createBuilder();
$query->addFrom("\\PaymentInstallment", "pi")
    ->leftJoin('\Purchase', 'p.id = pi.purchaseId', 'p')
    ->leftJoin('\Client', 'c.id = p.clientId', 'c')
    ->where("c.smsNotification = :smsNotification: AND pi.paymentDate IS NULL", array(
        'smsNotification' => 1
    ))
    ->andWhere("pi.date = :date:", array(
        'date' => (new \Comit\Helper\DateTime('-7 days'))->format('Y-m-d')
    ));

$paymentInstallments = $query->getQuery()->execute();

$text = "Izvestaj poslatih poruka kojima uplata kasni 7 dana";
/** @var \PaymentInstallment $paymentInstallment */
foreach($paymentInstallments as $paymentInstallment) {

    $purchase = $paymentInstallment->getPurchase();

    $phoneNumber = $purchase->getPhoneNumber();
    $phoneNumber = "00381".str_replace('+','', substr($phoneNumber,1));
//    $phoneNumber = "00381648382015";
//
    $displayName = $paymentInstallment->getPurchase()->getClient()->getDisplayName();

    $paymentInstallmentAmount = $paymentInstallment->getAmount();
    $paymentDate = $paymentInstallment->getDate();

    $sender = new \Comit\Helper\SendSms();
    $message="Agencija ONESTA Vas podseca da niste uplatili ratu u vrednosti od $paymentInstallmentAmount din, koja je na naplatu dospela $paymentDate";
//    $message2=". Kancelarija Onesta je preseljena i nalazi se u servisu Onesta u ulici Bana Ivanisa 27 (okretnica trolejbusa 40)";
//
    $sender->execute($phoneNumber,$message);

    //$test = $sender->execute($phoneNumber,$message);

//    $requestBody = new infobip\api\model\sms\mt\send\textual\SMSTextualRequest();
//    $requestBody->setFrom('ONESTA');
//    $requestBody->setTo($phoneNumber);
//    $requestBody->setText("Agencija ONESTA Vas podseca da niste uplatili ratu u vrednosti od $paymentInstallmentAmount din, koja je na naplatu dospela $paymentDate ");
//
//    $response = $client->execute($requestBody);
    echo 'Poslao sms na broj: ' .$phoneNumber. ' id clijenta: '.$purchase->getClientId().', u vreme : '.new \Comit\Helper\DateTime().'<br>';

    $text .="\n Poslao sms na broj: ".$phoneNumber." klijent: ".$purchase->getClient()->getDisplayName(). ", u vreme : ".new \Comit\Helper\DateTime();

    $i=0;
    $i=$i+1;

}



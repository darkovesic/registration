<?php

namespace Comit\Mailer;

use Phalcon\Config;
use Phalcon\Mvc\View\Simple;
use Phalcon\Mvc\User\Component;

/**
 * Class Manager
 *
 * @package Comit\Mailer
 * @author Igor Vuckovic
 */
class Manager extends Component
{

    /**
     * @var array
     */
    protected $config = array();

    /**
     * @var \Swift_Transport
     */
    protected $transport;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * Create a new MailerManager component using $config for configuring
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        $this->configure($config);
    }

    /**
     * Create a new Message instance.
     *
     * Events:
     * - mailer:beforeCreateMessage
     * - mailer:afterCreateMessage
     *
     * @return Message
     */
    public function createMessage()
    {
        $eventsManager = $this->getEventsManager();

        if ($eventsManager) {
            $eventsManager->fire('mailer:beforeCreateMessage', $this);
        }

        /** @var Message $message */
        $message = new Message($this);

        if (($from = $this->getConfig('from'))) {
            $message->from($from['email'], isset($from['name']) ? $from['name'] : null);
        }

        if ($eventsManager) {
            $eventsManager->fire('mailer:afterCreateMessage', $this, [$message]);
        }

        return $message;
    }

    /**
     * Create a new Message instance.
     * For the body of the message uses the result of render of view
     *
     * Events:
     * - mailer:beforeCreateMessage
     * - mailer:afterCreateMessage
     *
     * @param string $view
     * @param array $params optional
     * @param null|string $viewsDir optional
     * @return Message
     * @see \Comit\Mailer\Manager::createMessage()
     */
    public function createMessageFromView($view, $params = [], $viewsDir = null)
    {
        $message = $this->createMessage();
        $message->content($this->renderView($view, $params, $viewsDir), $message::CONTENT_TYPE_HTML);

        return $message;
    }

    /**
     * Return a {@link \Swift_Mailer} instance
     *
     * @return \Swift_Mailer
     */
    public function getMailer()
    {
        if (!$this->isInitSwiftMailer()) {
            $this->registerSwiftMailer();
        }

        return $this->mailer;
    }

    /**
     * Normalize IDN domains.
     *
     * @param $email
     * @return string
     * @see \Comit\Mailer\Manager::punyCode()
     */
    public function normalizeEmail($email)
    {
        if (preg_match('#[^(\x20-\x7F)]+#', $email)) {

            list($user, $domain) = explode('@', $email);

            return $user . '@' . $this->punyCode($domain);

        } else {
            return $email;
        }
    }

    /**
     * Configure MailerManager class
     *
     * @param array $config
     */
    protected function configure($config)
    {
        $this->config = (array)$config;
    }

    /**
     * Create a new Driver-mail of SwiftTransport instance.
     *
     * Supported driver-mail:
     * - smtp
     * - sendmail
     * - mail
     */
    protected function registerTransport()
    {
        switch ($driver = $this->getConfig('driver')) {
            case 'smtp':
                $this->transport = $this->registerSmtpTransport();
                break;

            case 'mail':
                $this->transport = $this->registerMailTransport();
                break;

            case 'sendmail':
                $this->transport = $this->registerSendmailTransport();
                break;

            default:
                throw new \InvalidArgumentException(sprintf('Driver-mail "%s" is not supported', $driver));
        }
    }

    /**
     * Create a new SmtpTransport instance.
     *
     * @return \Swift_SmtpTransport
     * @see \Swift_SmtpTransport
     */
    protected function registerSmtpTransport()
    {
        $config = $this->getConfig();

        $transport = \Swift_SmtpTransport::newInstance();

        if (isset($config['host'])) {
            $transport->setHost($config['host']);
        }

        if (isset($config['port'])) {
            $transport->setPort($config['port']);
        }

        if (isset($config['encryption'])) {
            $transport->setEncryption($config['encryption']);
        }

        if (isset($config['username'])) {
            $transport->setUsername($this->normalizeEmail($config['username']));
            $transport->setPassword($config['password']);
        }

        return $transport;
    }

    /**
     * Create a new MailTransport instance.
     *
     * @return \Swift_MailTransport
     * @see \Swift_MailTransport
     */
    protected function registerMailTransport()
    {
        $transport = \Swift_MailTransport::newInstance();
        return $transport;
    }

    /**
     * Create a new SendmailTransport instance.
     *
     * @return \Swift_SendmailTransport
     * @see \Swift_SendmailTransport
     */
    protected function registerSendmailTransport()
    {
        $transport = \Swift_SendmailTransport::newInstance();
        $transport->setCommand($this->getConfig('sendmail', '/usr/sbin/sendmail -bs'));

        return $transport;
    }

    /**
     * Get option config or the entire array of config, if the parameter $key is not specified.
     *
     * @param null $key
     * @param null $default
     * @return string|array
     */
    protected function getConfig($key = null, $default = null)
    {
        if ($key !== null) {
            if (isset($this->config[$key])) {
                return $this->config[$key];
            } else {
                return $default;
            }

        } else {
            return $this->config;
        }
    }

    /**
     * Convert UTF-8 encoded domain name to ASCII
     *
     * @param $str
     * @return string
     */
    protected function punyCode($str)
    {
        if (function_exists('idn_to_ascii')) {
            return idn_to_ascii($str);
        } else {
            return $str;
        }
    }

    /**
     * Register SwiftMailer
     *
     * @see \Swift_Mailer
     */
    protected function registerSwiftMailer()
    {
        $this->registerTransport();
        $this->mailer = \Swift_Mailer::newInstance($this->transport);
    }

    /**
     * Renders a view
     *
     * @param $viewPath
     * @param $params
     * @param null $viewsDir
     * @return string
     */
    protected function renderView($viewPath, $params, $viewsDir = null)
    {
        /** @var Simple $view */
        $view = $this->getDI()->get('simpleView');

        if ($viewsDir !== null) {
            $viewsDirOld = $view->getViewsDir();
            $view->setViewsDir($viewsDir);

            $content = $view->render($viewPath, $params);
            $view->setViewsDir($viewsDirOld);

            return $content;
        } else {
            return $view->render($viewPath, $params);
        }
    }

    /**
     * Check init SwiftMailer
     *
     * @return bool
     */
    protected function isInitSwiftMailer()
    {
        return $this->mailer && $this->transport;
    }

}

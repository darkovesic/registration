<?php

namespace Comit\Ajax;

use Comit\Messenger\Manager as MessengerManager;
use Comit\Messenger\Message as MessengerMessage;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Http\Request;
use Phalcon\Http\Response;

/**
 * Class Handler
 *
 * @package Comit\Ajax
 * @author Igor Vuckovic
 */
class Handler extends Plugin
{

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return Response
     */
    public function afterDispatchLoop(Event $event, Dispatcher $dispatcher)
    {
        $di = $this->getDI();

        /** @var Request $request */
        $request = $di->getShared('request');

        /** @var Response $response */
        $response = $di->getShared('response');

        if ($request->isAjax()) {

            if ($dispatcher->getReturnedValue()) {
                $data = $dispatcher->getReturnedValue();
            } else {

                $data = array();
                foreach ($dispatcher->getParams() as $param => $value) {
                    if (!is_numeric($param)) {
                        $data[$param] = $value;
                    }
                }

                /** @var MessengerManager $messengerManager */
                $messengerManager = $this->getDI()->getShared('messengerManager');

                if ($messengerManager->hasMessages(MessengerMessage::DANGER)) {
                    $data['error'] = true;
                    $data['messages'] = $messengerManager->toArray(MessengerMessage::DANGER);
                } else if ($messengerManager->hasMessages(MessengerMessage::WARNING)) {
                    $data['error'] = true;
                    $data['messages'] = $messengerManager->toArray(MessengerMessage::WARNING);
                } else if ($messengerManager->hasMessages(MessengerMessage::INFO)) {
                    $data['error'] = false;
                    $data['messages'] = $messengerManager->toArray(MessengerMessage::INFO);
                } else if ($messengerManager->hasMessages(MessengerMessage::SUCCESS)) {
                    $data['error'] = false;
                    $data['messages'] = $messengerManager->toArray(MessengerMessage::SUCCESS);
                }
            }

            if (isset($data) && count($data)) {
                $di->getShared('view')->disable();
                if (is_array($data)) {
                    $data = json_encode($data);
                }

                $response->setContentType('application/json', 'UTF-8');
                $response->setContent($data);

                return $response->send();
            }
        }
    }

}

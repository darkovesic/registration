<?php

namespace Comit\Helper;

/**
 * Class Hash
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class Hash
{

    private static $_salt = '1#2Fds1CR.&b7CfQz-9BwX*R&xC3R+Mn';

    /**
     * @param string $input
     * @return string
     */
    public static function password($input)
    {
        if (function_exists('password_hash')) {
            $hash = password_hash($input, PASSWORD_BCRYPT, array('salt' => self::$_salt));
        } else {
            $hash = sha1(self::$_salt . $input);
        }

        return $hash;
    }

    /**
     * @param $input
     * @param $algorithm
     * @return string
     */
    public static function crypt($input, $algorithm)
    {
        if (function_exists('hash') && in_array($algorithm, hash_algos())) {
            return hash($algorithm, self::$_salt . $input);
        } else {
            return sha1(self::$_salt . $input);
        }
    }

}

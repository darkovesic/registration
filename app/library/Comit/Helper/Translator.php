<?php

namespace Comit\Helper;

use Phalcon\Config\Adapter\Php as ConfigPhp;

/**
 * Class Translator
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class Translator
{

    private static $_instance;
    private static $_translations = array();

    /**
     *
     */
    private function __construct()
    {

    }

    /**
     * @param string $string
     * @param mixed $args
     * @param string $locale
     * @return string
     */
    public static function translate($string, $args = null, $locale = 'en_US')
    {
        self::_getTranslations($locale);
        if (array_key_exists($string, self::$_translations) && strlen(self::$_translations[$string])) {
            $string = self::$_translations[$string];
        }

        if (!is_null($args)) {
            if (!is_array($args)) {
                $args = array($args);
            }
            $string = self::translate($string);
            array_unshift($args, $string);
            return call_user_func_array('sprintf', $args);
        }

        return $string;
    }

    /**
     * @return Translator
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new static;
        }

        return self::$_instance;
    }

    /**
     * @param string $locale
     * @return array
     */
    private static function _getTranslations($locale)
    {
        if (count(self::$_translations) < 1) {
            if (file_exists($filePath = APP_PATH . 'app/locale/' . $locale . '/translations.php')) {
                $config = new ConfigPhp($filePath);
                self::$_translations = $config->toArray();
            }
        }

        return self::$_translations;
    }

}

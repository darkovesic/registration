<?php

namespace Comit\Helper;

/**
 * Class Money
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class Money
{

    /** @var float */
    protected $_amount;

    /** @var \Currency */
    protected $_currency;

    /**
     * @param float $amount
     * @param \Currency $currency
     */
    public function __construct($amount, \Currency $currency)
    {
        $this->setAmount($amount);
        $this->setCurrency($currency);
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->_amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->_amount = floatval($amount);

        return $this;
    }

    /**
     * @return \Currency
     */
    public function getCurrency()
    {
        return $this->_currency;
    }

    /**
     * @param \Currency $currency
     * @return $this
     */
    public function setCurrency(\Currency $currency)
    {
        $this->_currency = $currency;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->format();
    }

    /**
     * @return string
     */
    public function format()
    {
        return sprintf('%s%s',
            $this->getCurrency()->getSymbol(),
            $this->_amount == round($this->_amount) ? number_format($this->_amount) : number_format($this->_amount, 2)
        );
    }

    /**
     * Add a numeric amount in the same currency
     *
     * @param Money $money
     * @return $this
     * @throws \Exception
     */
    public function addAmount(Money $money)
    {
        if ($this->getCurrency()->isEqualToEntity($money)) {
            $this->_amount += $money->getAmount();
        } else {
            throw new \Exception('Currencies are not equal.');
        }

        return $this;
    }


    /**
     * Subtract a numeric amount in the same currency
     *
     * @param Money $amount
     * @return $this
     * @throws \Exception
     */
    public function subAmount(Money $amount)
    {
        if ($this->getCurrency()->isEqualToEntity($amount)) {
            $this->_amount -= $amount->getAmount();
        } else {
            throw new \Exception('Currencies are not equal.');
        }

        return $this;
    }

    /**
     * @param float|int $quantity
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function multiply($quantity)
    {
        if (!is_numeric($quantity)) {
            throw new \InvalidArgumentException('You must specify a numeric argument $quantity.');
        }

        $this->setAmount($quantity * $this->_amount);

        return $this;
    }

    /**
     * @param float|int $quantity
     * @return Money
     * @throws \InvalidArgumentException
     */
    public function divide($quantity)
    {
        if (!is_numeric($quantity)) {
            throw new \InvalidArgumentException('You must specify a numeric argument $quantity');
        }

        $this->setAmount($this->_amount / $quantity);

        return $this;
    }

}

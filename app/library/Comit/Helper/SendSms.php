<?php
namespace Comit\Helper;

use GuzzleHttp\Client;
/**
 * Class SendSms
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class SendSms
{

    /**
     * @param string $number
     * @param string $message
     * @return string
     */
    public static function execute($number, $message)
    {
        $length = 10;
        $random = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'),1,$length);


        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic YmViYWthcGk6dG0zdmRlYVokeDJy'
        ];
        $body = '{
  "channels": [
    "SMS"
  ],
  "destinations": [
    {
      "phoneNumber": "'.$number.'"
    }
  ],
  "requestId": "'.$random.'",
  "transactionId": "'.$random.'",
  "dlr": true,
  "dlrUrl": "",
  "sms": {
    "priority": 2,
    "sender": "Onesta",
    "text": "'.$message.'"
  }
}';
        $response = $client->post('https://msg.mobile-gw.com:9000/v1/omni-channel/message',['headers' => $headers, 'body' => $body]);
        $body = $response->getBody();
        $nthResponse =json_decode((string) $body);

        $log = new \SmsLog();

        $sent = (new \Comit\Helper\DateTime())->getUnixFormat();

        $log->setPhone($number);
        $log->setText($message);
        $log->setSent($sent);
        $log->setSmsId($nthResponse->messages[0]->messageId);
        $log->save();
    }
//
//    /**
//     * @param string $number
//     * @param string $message
//     * @return string
//     */
//    public static function execute($number, $message)
//    {
//        $sent = (new \Comit\Helper\DateTime())->getUnixFormat();
//
//        $client = new Client();
//        $data = [
//            'username' => 'bebaapi',
//            'password' => 'dKkvWMoH',
//            'origin' => 'Onesta',
//            'call-number' => $number,
//            'text' => $message,
//            'status_url' => 'http://webservis.bebakids.com',
//            'messageid' => '1000',
//        ];
//        $res = $client->get('http://bulk.mobile-gw.com:9000?' . http_build_query($data));
//
//        $log = new \SmsLog();
//
//        $log->setPhone($number);
//        $log->setText($message);
//        $log->setSent($sent);
//        $log->setSmsId($res->getHeader('X-Nth-SmsId')[0]);
//        $log->save();
//
//    }

}
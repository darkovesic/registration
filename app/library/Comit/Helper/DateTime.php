<?php

namespace Comit\Helper;

/**
 * Class DateTime
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class DateTime extends \DateTime
{

    const ISO_8601_NO_TIMEZONE = 'Y-m-d H:i:s';
    const ISO_8601_DATE_ONLY = 'Y-m-d';

    const FORMAT_LONG = 'd F Y';
    const FORMAT_LONG_DAY_OF_WEEK = 'l, d F Y';
    const FORMAT_SHORT = 'd.m.Y';
    const FORMAT_SHORT_DAY_OF_WEEK = 'D, d M Y';
    const FORMAT_TIME = 'H:i';

    const TYPE_DATETIME = 'datetime';
    const TYPE_DATE = 'date';

    protected $_dateType;

    /**
     * @param string $time
     * @param \DateTimeZone $timezone
     * @param string $dataType
     */
    public function __construct($time = 'now', \DateTimeZone $timezone = null, $dataType = self::TYPE_DATETIME)
    {
        $this->setDateType($dataType);

        parent::__construct($time, $timezone);
    }

    /**
     * @return string
     */
    public function getDateType()
    {
        return $this->_dateType;
    }

    /**
     * @param $dateType
     */
    public function setDateType($dateType)
    {
        $this->_dateType = $dateType;
    }

    /**
     * @return string
     */
    public function getShort()
    {
        return $this->format(self::FORMAT_SHORT);
    }

    /**
     * @return string
     */
    public function getLong()
    {
        $format = self::FORMAT_SHORT;
        if ($this->_dateType === self::TYPE_DATETIME) {
            $format .= ', ' . self::FORMAT_TIME;
        }

        return $this->format($format);
    }

    /**
     * @return string
     */
    public function getUnixFormat()
    {
        if ($this->_dateType === self::TYPE_DATE) {
            return $this->format(self::ISO_8601_DATE_ONLY);
        } else {
            return $this->format(self::ISO_8601_NO_TIMEZONE);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLong();
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (is_callable(array($this, $name))) {
            return $this->$name;
        }

        if (method_exists($this, $name)) {
            return call_user_func(array($this, $name));
        }

        $getName = 'get' . ucfirst($name);
        if (method_exists($this, $getName)) {
            return call_user_func(array($this, $getName));
        }

        throw new \Exception('Calling undefined property or method ' . __CLASS__ . '::$' . $name);
    }

}

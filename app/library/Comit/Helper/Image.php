<?php

namespace Comit\Helper;

use Phalcon\Image\Adapter\Imagick;

/**
 * Class Image
 *
 * @author Igor Vuckovic
 * @package Comit\Helper
 */
class Image
{

    /**
     * @param $imagePath
     * @param $width
     * @param $height
     * @return Imagick
     */
    public static function resizeAndCrop($imagePath, $width, $height)
    {
        $image = new Imagick($imagePath);

        $offsetX = null;
        $offsetY = null;

        if ($image->getWidth() > $image->getHeight()) {
            $master = \Phalcon\Image::HEIGHT;
            $offsetX = ($height * $image->getWidth() / $image->getHeight() - $width) / 2;
        } else {
            $master = \Phalcon\Image::WIDTH;
            $offsetY = ($width * $image->getHeight() / $image->getWidth() - $height) / 2;
        }

        return $image->resize($width, $height, $master)
            ->crop($width, $height, $offsetX, $offsetY)
            ->save();
    }

}

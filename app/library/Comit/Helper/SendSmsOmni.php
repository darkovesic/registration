<?php

namespace Comit\Helper;

use GuzzleHttp\Client;
use http\Client\Request;

/**
 * Class SendSmsOmni
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class SendSmsOmni
{
    /**
     * @param string $number
     * @param string $message
     * @return string
     */
    public static function execute($number, $message)
    {
        $length = 10;
        $random = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'), 1, $length);


        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic YmViYWthcGk6dG0zdmRlYVokeDJy'
        ];
        $body = '{
  "channels": [
    "SMS"
  ],
  "destinations": [
{ "phoneNumber": "00381613169339"},
{ "phoneNumber": "00381616915945"},
{ "phoneNumber": "00381692917733"},
{ "phoneNumber": "00381646514237"},
{ "phoneNumber": "00381659803576"},
{ "phoneNumber": "00381641106810"},
{ "phoneNumber": "00381612978547"},
{ "phoneNumber": "00381600500343"},
{ "phoneNumber": "00381658327521"},
{ "phoneNumber": "00381604442554"},
{ "phoneNumber": "00381641238394"},
{ "phoneNumber": "00381640222822"},
{ "phoneNumber": "00381613427669"},
{ "phoneNumber": "00381641294545"},
{ "phoneNumber": "00381603353503"},
{ "phoneNumber": "00381601668616"},
{ "phoneNumber": "00381691002505"},
{ "phoneNumber": "0038163240728"},
{ "phoneNumber": "00381677575765"},
{ "phoneNumber": "00381611759597"},
{ "phoneNumber": "0038163222760"},
{ "phoneNumber": "00381638525885"},
{ "phoneNumber": "00381621112112"},
{ "phoneNumber": "00381693377198"},
{ "phoneNumber": "00381692283005"},
{ "phoneNumber": "00381692283005"},
{ "phoneNumber": "00381621062414"},
{ "phoneNumber": "00381646412083"},
{ "phoneNumber": "00381605536373"},
{ "phoneNumber": "00381642865213"},
{ "phoneNumber": "00381644870437"},
{ "phoneNumber": "00381603715657"},
{ "phoneNumber": "0038166264024"},
{ "phoneNumber": "0038163409180"},
{ "phoneNumber": "00381652941111"},
{ "phoneNumber": "0038163259927"},
{ "phoneNumber": "00381658444408"},
{ "phoneNumber": "00381621973018"},
{ "phoneNumber": "00381648983995"},
{ "phoneNumber": "0038163309821"},
{ "phoneNumber": "00381658788696"},
{ "phoneNumber": "00381606090923"},
{ "phoneNumber": "00381611751670"},
{ "phoneNumber": "00381604660606"},
{ "phoneNumber": "00381652779305"},
{ "phoneNumber": "0038163498009"},
{ "phoneNumber": "0038163206240"},
{ "phoneNumber": "00381640890890"},
{ "phoneNumber": "00381641400924"},
{ "phoneNumber": "00381643450775"},
{ "phoneNumber": "00381692211490"},
{ "phoneNumber": "00381641576752"},
{ "phoneNumber": "00381631224134"},
{ "phoneNumber": "00381641106810"},
{ "phoneNumber": "00381669744074"},
{ "phoneNumber": "00381612272501"},
{ "phoneNumber": "00381616413617"},
{ "phoneNumber": "00381668791943"},
{ "phoneNumber": "00381698168135"},
{ "phoneNumber": "00381645005008"},
{ "phoneNumber": "00381645295233"},
{ "phoneNumber": "00381616273577"},
{ "phoneNumber": "00381603392570"},
{ "phoneNumber": "0038162623652"},
{ "phoneNumber": "00381603403407"},
{ "phoneNumber": "00381631111120"},
{ "phoneNumber": "00381653027037"},
{ "phoneNumber": "00381653476234"},
{ "phoneNumber": "00381604677640"},
{ "phoneNumber": "00381601668616"},
{ "phoneNumber": "0038163409180"},
{ "phoneNumber": "0038163289612"},
{ "phoneNumber": "0038163289612"},
{ "phoneNumber": "00381643939393"},
{ "phoneNumber": "00381628556922"},
{ "phoneNumber": "00381611995203"},
{ "phoneNumber": "00381658525545"},
{ "phoneNumber": "00381612888888"},
{ "phoneNumber": "00381607683008"},
{ "phoneNumber": "0038162623652"},
{ "phoneNumber": "00381638270251"},
{ "phoneNumber": "00381616189710"},
{ "phoneNumber": "00381616909995"},
{ "phoneNumber": "00381652631962"},
{ "phoneNumber": "00381658444408"},
{ "phoneNumber": "00381648606089"},
{ "phoneNumber": "00381653425876"},
{ "phoneNumber": "0038163322110"},
{ "phoneNumber": "00381611889704"},
{ "phoneNumber": "00381658471862"},
{ "phoneNumber": "00381612797991"},
{ "phoneNumber": "00381601628788"},
{ "phoneNumber": "00381644074902"},
{ "phoneNumber": "00381692390446"},
{ "phoneNumber": "00381621986610"},
{ "phoneNumber": "00381658354420"},
{ "phoneNumber": "0038166300099"},
{ "phoneNumber": "00381606725670"},
{ "phoneNumber": "0038163247704"},
{ "phoneNumber": "00381649571720"},
{ "phoneNumber": "00381628068614"},
{ "phoneNumber": "00381652000218"},
{ "phoneNumber": "00381612106210"},
{ "phoneNumber": "00381612004455"},
{ "phoneNumber": "00381612004455"},
{ "phoneNumber": "00381605794266"},
{ "phoneNumber": "00381601333112"},
{ "phoneNumber": "00381604101444"},
{ "phoneNumber": "0038169687579"},
{ "phoneNumber": "00381641294545"},
{ "phoneNumber": "00381652680734"},
{ "phoneNumber": "0038163499511"},
{ "phoneNumber": "0038163222760"},
{ "phoneNumber": "00381611862846"},
{ "phoneNumber": "00381621031946"},
{ "phoneNumber": "0038163632383"},
{ "phoneNumber": "00381628923781"},
{ "phoneNumber": "0038162578508"},
{ "phoneNumber": "00381603344637"},
{ "phoneNumber": "00381693407650"},
{ "phoneNumber": "00381631082410"},
{ "phoneNumber": "00381600588929"},
{ "phoneNumber": "00381605499949"},
{ "phoneNumber": "00381640890890"},
{ "phoneNumber": "0038163498009"},
{ "phoneNumber": "0038163208249"},
{ "phoneNumber": "00381694631511"},
{ "phoneNumber": "00381644773717"},
{ "phoneNumber": "00381643212706"},
{ "phoneNumber": "00381658874878"},
{ "phoneNumber": "00381643212706"},
{ "phoneNumber": "00381638731903"},
{ "phoneNumber": "00381642949431"},
{ "phoneNumber": "00381601617781"},
{ "phoneNumber": "00381603716120"},
{ "phoneNumber": "0038166271991"},
{ "phoneNumber": "00381604055563"},
{ "phoneNumber": "00381603047885"},
{ "phoneNumber": "00381600241387"},
{ "phoneNumber": "00381641429086"},
{ "phoneNumber": "00381601951122"},
{ "phoneNumber": "00381613159952"},
{ "phoneNumber": "0038163322357"},
{ "phoneNumber": "00381642612882"},
{ "phoneNumber": "0038163375570"},
{ "phoneNumber": "00381607225281"},
{ "phoneNumber": "00381631491775"},
{ "phoneNumber": "00381611810969"},
{ "phoneNumber": "00381641761672"},
{ "phoneNumber": "00381616284854"},
{ "phoneNumber": "00381603612679"},
{ "phoneNumber": "0038163210522"},
{ "phoneNumber": "00381628546224"},
{ "phoneNumber": "00381603392570"},
{ "phoneNumber": "0038162587070"},
{ "phoneNumber": "0038166235099"},
{ "phoneNumber": "00381628139140"},
{ "phoneNumber": "00381606916465"},
{ "phoneNumber": "00381603975479"},
{ "phoneNumber": "00381643093009"},
{ "phoneNumber": "00381654135008"},
{ "phoneNumber": "00381613221700"},
{ "phoneNumber": "00381640890890"},
{ "phoneNumber": "00381607265986"},
{ "phoneNumber": "0038162623652"},
{ "phoneNumber": "00381631293033"},
{ "phoneNumber": "00381605262144"},
{ "phoneNumber": "00381658558066"},
{ "phoneNumber": "00381612273675"},
{ "phoneNumber": "00381646514234"},
{ "phoneNumber": "00381691989800"},
{ "phoneNumber": "00381641149220"},
{ "phoneNumber": "00381611327460"},
{ "phoneNumber": "00381653184478"},
{ "phoneNumber": "00381642006261"},
{ "phoneNumber": "00381616875793"},
{ "phoneNumber": "0038169750052"}
  ],
  "requestId": "' . $random . '",
  "transactionId": "' . $random . '",
  "dlr": true,
  "dlrUrl": "",
  "sms": {
    "priority": 2,
    "sender": "Onesta",
    "text": "' . $message . '"
  }
}';
        $response = $client->post('https://msg.mobile-gw.com:9000/v1/omni-channel/message', ['headers' => $headers, 'body' => $body]);
        $body = $response->getBody();
        $nthResponse = json_decode((string)$body);

        $log = new \SmsLog();

        $sent = (new \Comit\Helper\DateTime())->getUnixFormat();

        $log->setPhone($number);
        $log->setText($message);
        $log->setSent($sent);
        $log->setSmsId($nthResponse->messages[0]->messageId);
        $log->save();
    }
}
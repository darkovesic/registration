<?php

namespace Comit\Helper;

/**
 * Class Model
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class Model
{


    /**
     * @param array|string $arguments
     * @param array $defaultArguments
     * @return array
     */
    public static function mergeArguments($arguments, $defaultArguments)
    {

        if (is_array($arguments)) {

            if (isset($arguments['conditions'])) {
                $conditions = $arguments['conditions'];
                unset($arguments['conditions']);
            } else if (isset($arguments[0])) {
                $conditions = $arguments[0];
                unset($arguments[0]);
            }

            if (isset($conditions)) {
                if (is_array($conditions)) {
                    $defaultArguments['conditions'] .= ' AND ' . implode(' AND ', $conditions);
                } else {
                    $defaultArguments['conditions'] .= ' AND ' . $conditions;
                }
            }

            foreach ($arguments as $index => $value) {
                if (array_key_exists($index, $defaultArguments)) {
                    $defaultArguments[$index] = array_merge($arguments[$index], $defaultArguments[$index]);
                    unset($arguments[$index]);
                }
            }

            $arguments = array_merge($arguments, $defaultArguments);

        } else if (is_string($arguments)) {

            $defaultArguments['conditions'] .= ' AND ' . $arguments;
            $arguments = $defaultArguments;

        } else {
            $arguments = $defaultArguments;
        }

        return $arguments;
    }

}

<?php

namespace Comit\Helper;

use Phalcon\DI;

/**
 * Class Common
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class Common
{

    protected static $configs = array();

    /**
     * @return string
     */
    public static function getSiteName()
    {
        return self::getConfig('siteName');
    }

    /**
     * @return string
     */
    public static function getBaseUrl()
    {
        return rtrim(self::getConfig('baseUri'), '/') . '/';
    }

    /**
     * @param string $url
     * @param string $link
     * @return string
     */
    public static function linkTo($url, $link = null)
    {
        $url = self::prepUrl($url);

        if (is_null($link)) {
            $link = $url;
        }

        return '<a href="' . $url . '">' . $link . '</a>';
    }

    /**
     * @param string $url
     * @return string
     */
    public static function prepUrl($url)
    {
        if (0 !== strpos($url, 'http')) {
            $url = self::getBaseUrl() . ltrim($url, '/');
        }

        return $url;
    }

    /**
     * @return array
     */
    public static function toArray()
    {
        return self::refreshConfig();
    }

    /**
     * @return \stdClass
     */
    public function toSimpleObject()
    {
        return $this->arrayToObject(self::refreshConfig());
    }

    /**
     * @param array $input
     * @return \stdClass
     */
    protected function arrayToObject($input)
    {
        $obj = new \stdClass();
        foreach ($input as $k => $v) {
            if (is_array($v)) {
                $obj->{$k} = $this->arrayToObject($v);
            } else {
                $obj->{$k} = $v;
            }
        }

        return $obj;
    }

    /**
     * @param string $key
     * @return string|array|null
     */
    protected static function getConfig($key)
    {
        self::refreshConfig();

        if (array_key_exists($key, self::$configs)) {
            return self::$configs[$key];
        }


        return null;
    }

    /**
     * @return array
     */
    protected static function refreshConfig()
    {
        if (0 === count(self::$configs)) {
            $di = DI::getDefault();

            /** @var \Phalcon\Config $config */
            $config = $di->get('config');
            $config = $config->toArray();
            self::$configs = $config['application'];
        }

        return self::$configs;
    }

}

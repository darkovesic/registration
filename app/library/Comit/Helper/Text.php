<?php

namespace Comit\Helper;

/**
 * Class Text
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class Text
{

    /**
     * @param string $string
     * @param string $separator
     * @return string $string
     */
    public static function prepareUrl($string, $separator = '-')
    {
        $qSeparator = preg_quote($separator);

        $trans = array(
            '&.+?;' => '',
            '[^a-z0-9 _-]' => '',
            '\s+' => $separator,
            '(' . $qSeparator . ')+' => $separator
        );

        $string = self::serbianToEnglish(strip_tags($string));

        foreach ($trans as $key => $val) {
            $string = preg_replace("#" . $key . "#i", $val, $string);
        }

        return trim(strtolower($string), $separator);
    }

    /**
     * @param string $serbian
     * @return string
     */
    public static function serbianToEnglish($serbian)
    {
        $serbian = self::cyrillicToLatin($serbian);

        $replaceSerbian = array(
            'š' => 's', 'đ' => 'dj', 'č' => 'c', 'ć' => 'c', 'ž' => 'z',
            'Š' => 'S', 'Đ' => 'Dj', 'Č' => 'C', 'Ć' => 'C', 'Ž' => 'Z',
        );

        return strtr($serbian, $replaceSerbian);
    }

    /**
     *
     * @param string $latin
     * @return string
     */
    public static function latinToCyrillic($latin)
    {
        $replaceLat = array(
            "A" => "А", "B" => "Б", "V" => "В", "G" => "Г", "D" => "Д", "Đ" => "Ђ", "DJ" => "Ђ", "Dj" => "Ђ",
            "E" => "Е", "Ž" => "Ж", "Z" => "З", "I" => "И", "J" => "Ј", "K" => "К", "L" => "Л", "LJ" => "Љ",
            "M" => "М", "N" => "Н", "NJ" => "Њ", "O" => "О", "P" => "П", "R" => "Р", "S" => "С", "T" => "Т",
            "Ć" => "Ћ", "U" => "У", "F" => "Ф", "H" => "Х", "C" => "Ц", "Č" => "Ч", "DŽ" => "Џ", "Š" => "Ш",
            "a" => "а", "b" => "б", "v" => "в", "g" => "г", "d" => "д", "đ" => "ђ", "dj" => "ђ",
            "e" => "е", "ž" => "ж", "z" => "з", "i" => "и", "j" => "ј", "k" => "к", "l" => "л", "lj" => "љ",
            "m" => "м", "n" => "н", "nj" => "њ", "o" => "о", "p" => "п", "r" => "р", "s" => "с", "t" => "т",
            "ć" => "ћ", "u" => "у", "f" => "ф", "h" => "х", "c" => "ц", "č" => "ч", "dž" => "џ", "š" => "ш",
            "Nj" => "Њ", "Lj" => "Љ", "Dž" => "Џ"
        );

        return strtr($latin, $replaceLat);
    }

    /**
     *
     * @param string $cyrillic
     * @return string
     */
    public static function cyrillicToLatin($cyrillic)
    {
        $replaceCyr = array(
            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D", "Ђ" => "Đ",
            "Е" => "E", "Ж" => "Ž", "З" => "Z", "И" => "I", "Ј" => "J", "К" => "K", "Л" => "L", "Љ" => "Lj",
            "М" => "M", "Н" => "N", "Њ" => "Nj", "О" => "O", "П" => "P", "Р" => "R", "С" => "S", "Т" => "T",
            "Ћ" => "Ć", "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "C", "Ч" => "Č", "Џ" => "Dž", "Ш" => "Š",
            "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "ђ" => "đ",
            "е" => "e", "ж" => "ž", "з" => "z", "и" => "i", "ј" => "j", "к" => "k", "л" => "l", "љ" => "lj",
            "м" => "m", "н" => "n", "њ" => "nj", "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t",
            "ћ" => "ć", "у" => "u", "ф" => "f", "х" => "h", "ц" => "c", "ч" => "č", "џ" => "dž", "ш" => "š",
        );

        return strtr($cyrillic, $replaceCyr);
    }

}

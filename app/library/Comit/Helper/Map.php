<?php

namespace Comit\Helper;

use Geocoder\HttpAdapter\CurlHttpAdapter;
use Geocoder\HttpAdapter\GeoIP2Adapter;
use Geocoder\Provider\ArcGISOnlineProvider;
use Geocoder\Provider\BaiduProvider;
use Geocoder\Provider\BingMapsProvider;
use Geocoder\Provider\ChainProvider;
use Geocoder\Provider\DataScienceToolkitProvider;
use Geocoder\Provider\FreeGeoIpProvider;
use Geocoder\Provider\GeocoderCaProvider;
use Geocoder\Provider\GeocoderUsProvider;
use Geocoder\Provider\GeoIP2Provider;
use Geocoder\Provider\GeoipProvider;
use Geocoder\Provider\GeoIPsProvider;
use Geocoder\Provider\GeoPluginProvider;
use Geocoder\Provider\GoogleMapsProvider;
use Geocoder\Provider\HostIpProvider;
use Geocoder\Provider\IGNOpenLSProvider;
use Geocoder\Provider\IpGeoBaseProvider;
use Geocoder\Provider\IpInfoDbProvider;
use Geocoder\Provider\MaxMindProvider;
use Geocoder\Provider\NominatimProvider;
use Geocoder\Provider\OpenCageProvider;
use Geocoder\Provider\OpenStreetMapProvider;
use Geocoder\Provider\TomTomProvider;
use Ivory\GoogleMap\Events\Event;
use Ivory\GoogleMap\Events\MouseEvent;
use Ivory\GoogleMap\Helper\MapHelper;
use Ivory\GoogleMap\MapTypeId;
use Ivory\GoogleMap\Overlays\Animation;
use Ivory\GoogleMap\Overlays\Marker;
use Ivory\GoogleMap\Overlays\MarkerImage;
use Ivory\GoogleMap\Services\Geocoding\GeocoderProvider;

/**
 * Class Model
 *
 * @package Comit\Helper
 * @author Darko Vesic
 */
class Map
{

    /** @var array|string */
    protected $request;

    /** @var  string */
    protected $mapType;

    /** @var  array */
    protected $mapOptions;

    /** @var array */
    protected $coordinates = array();

    /**
     * @param $request
     * @param array $options
     * @param null|string $mapType
     */
    public function __construct($request, $options = array(), $mapType = null)
    {
        if (is_string($request)) {
            $request = array($request);
        }

        if (!array_key_exists('mapTypeId', $options)) {
            $options['options'] = MapTypeId::ROADMAP;
        }

        $this->request = $request;
        $this->mapType = $mapType;
        $this->mapOptions = $options;
        $this->coordinates = $this->getCoordinates();

    }

    /**
     * @return array
     */
    public function getDefaultOptions()
    {
        return $this->mapOptions;
    }

    /**
     * @param $image
     * @return MarkerImage
     * @throws \Ivory\GoogleMap\Exception\AssetException
     * @throws \Ivory\GoogleMap\Exception\OverlayException
     */
    public function setMarkerImage($image)
    {
        $markerImage = new MarkerImage();

        $markerImage->setPrefixJavascriptVariable('marker_image_');
        $markerImage->setUrl($image);
        $markerImage->setAnchor(20, 34);
        $markerImage->setOrigin(0, 0);
        $markerImage->setSize(20, 34, "px", "px");
        $markerImage->setScaledSize(20, 34, "px", "px");

        return $markerImage;
    }

    /**
     * @param $latitude
     * @param $longitude
     * @param MarkerImage $markerImage
     * @return Marker
     * @throws \Ivory\GoogleMap\Exception\AssetException
     * @throws \Ivory\GoogleMap\Exception\OverlayException
     */
    private function setMarker($latitude, $longitude, MarkerImage $markerImage = null)
    {
        $marker = new Marker();

        $marker->setPrefixJavascriptVariable('marker_');
        $marker->setPosition($latitude, $longitude, true);
        $marker->setAnimation(Animation::DROP);

        $marker->setOptions(array(
            'clickable' => true,
            'flat' => true,
        ));

        if ($markerImage) {
            $marker->setIcon($markerImage);
        }

        return $marker;
    }

    /**
     * @param $address
     * @return array
     */
    public function getGeoCoderAddresses($address)
    {
        $curl = new CurlHttpAdapter();
        $geoCoder = new GeoPluginProvider($curl);
        $geoCoderResponse = $geoCoder->getGeocodedData($address);

        return $geoCoderResponse;

    }

    /**
     * @return \Ivory\GoogleMap\Map
     * @throws \Ivory\GoogleMap\Exception\AssetException
     * @throws \Ivory\GoogleMap\Exception\EventException
     * @throws \Ivory\GoogleMap\Exception\MapException
     */
    public function getRenderedMap()
    {
        $map = new \Ivory\GoogleMap\Map();

        $options = $this->getDefaultOptions();

        $map->setPrefixJavascriptVariable('map_');
        $map->setHtmlContainerId('map_canvas');
        $map->setAsync(false);
        $map->setAutoZoom(false);
        $map->setMapOptions($options);
        $map->setStylesheetOption('width', '100%');

        if (isset($options['height'])) {
            $map->setStylesheetOption('height', $options['height']);
        } else {
            $map->setStylesheetOption('height', '100%');
        }

        if (isset($options['width'])) {
            $map->setStylesheetOption('width', '100%');
        } else {
            $map->setStylesheetOption('width', '100%');
        }

        if (isset($options['showMarkers'])) {

            foreach ($this->coordinates as $key => $coordinate) {

                $marker = $this->setMarker($coordinate['latitude'], $coordinate['longitude']);
                $map->addMarker($marker);
                $map->getBound()->extend($marker);

            }
        }
        if (count($this->coordinates)) {
            $map->setCenter($this->coordinates[0]['latitude'], $this->coordinates[0]['longitude']);
        }

        $map->getEventManager()->addDomEvent($this->getMovableMarker($map));

        return $map;
    }

    /**
     * @return array
     */
    public function getCoordinates()
    {
        $coordinates = array();
        foreach ($this->request as $key => $address) {

            if($address != '127.0.0.1' && $address != '::1') {

                $addresses = $this->getGeoCoderAddresses($address);

                if (count($addresses)) {

                    /** @var \Ivory\GoogleMap\Services\Geocoding\Result\GeocoderResult $result */
                    $result = $addresses[0];

                    $coordinates[$key] = array('latitude' => $result['latitude'], 'longitude' => $result['longitude']);

                }

            } else {
                $coordinates[$key] = array('latitude' => 0, 'longitude' => 0);
            }

        }

        return $coordinates;
    }

    /**
     * @param \Ivory\GoogleMap\Map $map
     * @return Event
     * @throws \Ivory\GoogleMap\Exception\EventException
     */
    private function getMovableMarker(\Ivory\GoogleMap\Map $map)
    {
        $event = new Event();
        $event->setInstance($map->getJavascriptVariable());
        $event->setEventName(MouseEvent::CLICK);
        $event->setHandle(sprintf('function(event){

            var map = %s;
            marker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                draggable: true
            });

            if (typeof oldMarker !== "undefined"){
                oldMarker.setMap(null);
            }

            var newLat = event.latLng.lat();
            var newLng = event.latLng.lng();

            document.getElementById("latitude").value = newLat;
            document.getElementById("longitude").value = newLng;

            oldMarker = marker;
            map.setCenter(event.latLng);

            google.maps.event.addListener(marker, "dragend", function (event) {
                document.getElementById("latitude").value = this.getPosition().lat();
                document.getElementById("longitude").value = this.getPosition().lng();
            });


        }',
            $map->getJavascriptVariable()
        ));

        $event->setCapture(true);

        return $event;
    }

    /**
     * @param \Ivory\GoogleMap\Map $map
     * @return Event
     * @throws \Ivory\GoogleMap\Exception\EventException
     */
    private function getScrollMapOnClick(\Ivory\GoogleMap\Map $map)
    {
        $event = new Event();
        $event->setInstance($map->getJavascriptVariable());
        $event->setEventName(MouseEvent::CLICK);
        $event->setHandle(sprintf('function(){%s.setOptions({ scrollwheel: true });}',
            $map->getJavascriptVariable()
        ));

        $event->setCapture(true);

        return $event;
    }

    /**
     * @param \Ivory\GoogleMap\Map $map
     * @return Event
     * @throws \Ivory\GoogleMap\Exception\EventException
     */
    private function getDisableScrollOnFocusOut(\Ivory\GoogleMap\Map $map)
    {
        $event = new Event();
        $event->setInstance($map->getJavascriptVariable());
        $event->setEventName(MouseEvent::MOUSEOUT);
        $event->setHandle(sprintf('function(){%s.setOptions({ scrollwheel: false });}',
            $map->getJavascriptVariable()
        ));

        $event->setCapture(true);

        return $event;
    }

    /**
     * @return MapHelper
     */
    public function getMapHelper()
    {
        return new MapHelper();
    }

}
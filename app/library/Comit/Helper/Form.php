<?php

namespace Comit\Helper;

use Comit\Model\Base;
use Comit\Model\EntityInterface;
use Phalcon\Forms\Element;
use Phalcon\Mvc\Model\Resultset;

/**
 * Class Form
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class Form
{

    /** @var Base|EntityInterface */
    protected $_entity;

    protected $_parameters;

    /**
     * @param EntityInterface $entity
     * @param array $parameters
     */
    public function __construct(EntityInterface $entity, array $parameters)
    {
        $this->_entity = $entity;
        $this->_parameters = $parameters;
    }

    /**
     * @return \Comit\Form\Form
     */
    public function getSearchForm()
    {
        $searchForm = $this->_entity->getForm(
            array_diff(
                array_keys($this->_entity->toArray()),
                array_keys($this->_parameters)
            )
        );

        foreach ($searchForm->getElements() as $element) {

            /** @var Element $element */

            if (array_key_exists($element->getName(), $this->_parameters)) {
                $element->setDefault($this->_parameters[$element->getName()]);
            }

            $element->setAttribute('class', 'form-control');
            if ($element instanceof Element\Select) {
                $element->setAttribute('multiple', 'multiple');
                $element->setAttribute('class', 'chosen-select form-control');
                $element->setAttribute('name', $element->getName() . '[]');
            }
        }

        return $searchForm;
    }

    /**
     * @param null|string $order
     * @return Resultset
     */
    public function getCollection($order = null)
    {
        $conditions = '1 = 1';
        $bind = array();

        foreach ($this->_parameters as $property => $value) {
            if (is_array($value) && count($value)) {
                array_walk($value, function (&$val) {
                    $val = preg_quote($val, '"');
                    return $val;
                });
                $conditions .= " AND {$property} IN (\"" . implode('","', $value) . "\")";
            } else if ($value) {
                $conditions .= " AND {$property} LIKE :{$property}:";
                $bind[$property] = '%' . preg_quote($value, "'") . '%';
            }
        }

        return call_user_func(
            array(
                get_class($this->_entity),
                'find'
            ),
            array(
                $conditions,
                'bind' => $bind,
                'order' => $order
            )
        );
    }

}

<?php

namespace Comit\Helper;

/**
 * Class Directory
 *
 * @package Comit\Helper
 * @author Igor Vuckovic
 */
class Directory
{

    /**
     * @param string $dir
     */
    public static function removeDir($dir)
    {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file)) {
                self::removeDir($file);
            } else {
                @unlink($file);
            }
        }

        @rmdir($dir);
    }

}

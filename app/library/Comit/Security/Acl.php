<?php

namespace Comit\Security;

use Comit\Filter\Underscore;
use Phalcon\Acl as PhalconAcl;
use Phalcon\Acl\Resource;
use Phalcon\Acl\Adapter\Memory as AclAdapter;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;

/**
 * Security
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 *
 * @package Comit\Security
 * @author Igor Vuckovic
 */
class Acl extends Plugin
{

    const ROLE_GUEST = 'GUEST';
    const ROLE_MEMBER = 'MEMBER';
    const ROLE_ACCOUNT = 'ACCOUNT';
    const ROLE_ADMIN = 'ADMIN';

    /**
     * @return AclAdapter
     */
    public function getAcl()
    {
        $acl = new AclAdapter;
        $acl->setDefaultAction(PhalconAcl::DENY);

        $acl->addRole(self::ROLE_GUEST);
        $acl->addRole(self::ROLE_MEMBER, self::ROLE_GUEST);
        $acl->addRole(self::ROLE_ACCOUNT, self::ROLE_MEMBER);
        $acl->addRole(self::ROLE_ADMIN);

        $rules = array(
            self::ROLE_GUEST => array(
                PhalconAcl::ALLOW =>  array(
                    'backend/errors' => array('*'),
                    'backend/auth' => array('login', 'logout'),
                )
            ),
            self::ROLE_MEMBER => array(
                PhalconAcl::ALLOW => array(
                    'backend/clients' => array('*'),
                    'backend/purchases' => array('*'),
                    'backend/tire-parts' => array('*'),
                    'backend/onesta-tire-parts' => array('*'),
                    'backend/registrations' => array('*'),
                    'backend/payment-installments' => array('index', 'get-by-period','by-period', 'add-payment','create','update','get-create-form','get-update-form'),
                    'backend/dashboard' => array('*'),
                    'backend/reports' => array('*'),
                )
            ),
            self::ROLE_ACCOUNT => array(
                PhalconAcl::ALLOW => array(
                    'backend/payment-installments' => array('*'),
                )
            ),
            self::ROLE_ADMIN => array(
                PhalconAcl::ALLOW => '*'
            ),
        );

        foreach ($rules as $role => $roleRules) {
            foreach ($roleRules as $accessRule => $roleResources) {
                if (is_array($roleResources)) {
                    foreach ($roleResources as $resource => $accessList) {
                        $acl->addResource(
                            new Resource($resource),
                            $accessList
                        );

                        if ($accessRule === PhalconAcl::ALLOW) {
                            $acl->allow($role, $resource, $accessList);
                        } else {
                            $acl->deny($role, $resource, $accessList);
                        }
                    }
                } else {
                    if ($accessRule === PhalconAcl::ALLOW) {
                        $acl->allow($role, $roleResources, $roleResources);
                    } else {
                        $acl->deny($role, $roleResources, $roleResources);
                    }
                }
            }
        }

        return $acl;
    }

    /**
     * @param Dispatcher $dispatcher
     * @param string $role
     * @return bool
     */
    public function checkAccess(Dispatcher $dispatcher, $role = self::ROLE_GUEST)
    {
        $underscore = new Underscore();
        $resource = '';
        if ($module = $dispatcher->getModuleName()) {
            $resource .= $underscore->filter($module) . '/';
        }
        $resource .= $underscore->filter($dispatcher->getControllerName());
        $resource = str_replace('_', '-', $resource);
        $access = str_replace('_', '-', $underscore->filter($dispatcher->getActionName()));

        return $this->getAcl()->isAllowed($role, $resource, $access);
    }

}

<?php

namespace Comit\Task;


/**
 * Class Worker
 *
 * @package Comit\Task
 * @author Igor Vuckovic
 */
class Worker
{

    /** @var Queue */
    protected $queue;

    /**
     * @param Queue $queue
     */
    public function __construct(Queue $queue)
    {
        $this->queue = $queue;
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        $this->registerSignalHandler();
        $this->queue->watch($this->queue->getTube());

        do {

            $this->reserveAndExecuteJob();

            if (\memory_get_usage(true) > (200 * 1024 * 1024)) { // 200 Mb
                echo "Shutting down worker with high memory usage: " . \memory_get_usage(true);
                exit(1);
            }

            // If you press CTRL+C (SIGINT) or give SIGTERM this will terminate the worker.
            pcntl_signal_dispatch();

        } while (true);
    }

    /**
     * @throws \Exception
     */
    private function reserveAndExecuteJob()
    {
        // timeout needed here for pcntl_signal_dispatch
        $job = $this->queue->reserve(5);

        if ($job) {

            // Instantiate the task
            try {
                $task = TaskAbstract::factoryFromJson($job->getBody());
            } catch (\Exception $e) {
                $job->bury();
                throw $e;
            }

            // Execute task
            $task->run();

            // On complete, delete job from queue.
            $job->delete();
        }
    }

    /**
     * Registers TERMINATE and INTERRUPT signals handler
     */
    public function registerSignalHandler()
    {
        if (!function_exists('pcntl_signal')) {
            die("Requires pcntl PHP extension. http://php.net/manual/en/intro.pcntl.php");
        }

        \pcntl_signal(\SIGTERM, array($this, 'signalHandler'));
        \pcntl_signal(\SIGINT, array($this, 'signalHandler'));
    }

    /**
     * @param int $signal
     */
    public function signalHandler($signal)
    {
        switch ($signal) {

            case \SIGTERM :
                echo "Terminating with SIGTERM";
                exit(0);

            case \SIGINT :
                echo "Terminating with SIGINT";
                exit(0);

            default :
                echo "Terminating with another signal that was caught.";
                exit(1);
        }
    }

}

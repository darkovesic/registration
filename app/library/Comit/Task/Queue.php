<?php

namespace Comit\Task;

use Phalcon\Queue\Beanstalk;

/**
 * Class Queue
 *
 * @package Comit\Task
 * @author Igor Vuckovic
 */
class Queue extends Beanstalk
{

    protected $_tube = 'myTube';

    /**
     * @param TaskAbstract $task
     * @param int $priority
     * @param int $delay
     * @param int $ttr
     * @return null
     */
    public function enqueue(TaskAbstract $task, $priority = 1024, $delay = 0, $ttr = 1800)
    {
        $jobId = null;
        $maxAttempts = 3;

        for ($attempt = 1; $attempt <= $maxAttempts; $attempt++) {
            try {
                $data = json_encode($task->toArray());
                $options = array('priority' => $priority, 'delay' => $delay, 'ttr' => $ttr);
                $this->choose($this->_tube);
                $jobId = $this->put($data, $options);
                break;
            } catch (\Exception $e) {
                if ($attempt < $maxAttempts) {
                    usleep(200000);
                    continue;
                }
            }
        }

        return $jobId;
    }

    /**
     * @return string
     */
    public function getTube()
    {
        return $this->_tube;
    }

    /**
     * @param string $tube
     */
    public function setTube($tube)
    {
        $this->_tube = $tube;
    }

}

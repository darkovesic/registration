<?php

namespace Comit\Task;

use Comit\Model\Serializer;
use Phalcon\Di;

/**
 * Class TaskAbstract
 *
 * @package Comit\Task
 * @author Igor Vuckovic
 */
abstract class TaskAbstract
{

    const PRIORITY_VERY_LOW = 4096;
    const PRIORITY_LOW = 2048;
    const PRIORITY_NORMAL = 1024;
    const PRIORITY_HIGH = 512;
    const PRIORITY_VERY_HIGH = 256;
    const PRIORITY_URGENT = 128;

    protected $_backgroundJobId;
    protected $_priority = self::PRIORITY_NORMAL;
    protected $_ttr = 1800;

    /**
     * @return mixed
     */
    abstract public function execute();

    /**
     * @return int
     */
    public function getBackgroundJobId()
    {
        return $this->_backgroundJobId;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->_priority;
    }

    /**
     * @return int
     */
    public function getTtr()
    {
        return $this->_ttr;
    }

    /**
     * Creates the task from a JSON string
     *
     * @param string $json
     * @return $this
     */
    public static function factoryFromJson($json)
    {
        $payload = json_decode($json, true);

        /** @var self $task */
        $task = new $payload['task'];
        $task->setParamsFromArray($payload['params']);

        return $task;
    }

    /**
     * Creates the task from explicit params
     *
     * @param string $taskClass
     * @param array $params
     * @return $this
     */
    public static function factoryFromParams($taskClass, array $params)
    {
        /** @var self $task */
        $task = new $taskClass;
        $task->setParamsFromArray($params);

        return $task;
    }

    /**
     * Set parameters based on the JSON decoded array. Instantiates models etc.
     *
     * @param array $params
     */
    public function setParamsFromArray(array $params)
    {
        $serializer = new Serializer();
        $params = $serializer->unserializeFromJson($params);
        $serializer->setObjectPropertiesFromParams($this, $params);
    }

    /**
     * Encodes all parameters as simple primitive objects ready to be encoded to JSON
     *
     * @return array
     */
    public function getParamsAsArray()
    {
        $serializer = new Serializer();

        return $serializer->getParamsFromObject($this);
    }

    /**
     * @return string
     */
    public function getParamsAsJson()
    {
        return json_encode($this->getParamsAsArray());
    }

    /**
     * Returns a payload array which could be serialized to json
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'task' => get_called_class(),
            'params' => $this->getParamsAsArray()
        );
    }

    /**
     * Runs the task in process now.
     */
    public function run()
    {
        return $this->execute();
    }

    /**
     * @param int $delay
     * @return self
     */
    public function runInBackground($delay = 0)
    {
        try {
            $di = Di::getDefault();
            /** @var Queue $queue */
            $queue = $di->getShared('beanstalkQueue');
            $this->_backgroundJobId = $queue->enqueue($this, $this->getPriority(), $delay, $this->getTtr());
        } catch (\Exception $e) {
            $this->run();
        }

        return $this;
    }

}

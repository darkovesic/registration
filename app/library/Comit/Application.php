<?php

namespace Comit;

use Comit\Error\Client;
use Comit\Error\LocalHandler;
use Comit\Error\RemoteHandler;
use Comit\Mailer\Manager as MailManager;
use Comit\Messenger\Manager as MessengerManager;
use Comit\Security\Acl as AclPlugin;
use Phalcon\Config\Adapter\Php as ConfigPhp;
use Phalcon\Db\Profiler as ProfilerDb;
use Phalcon\DI\FactoryDefault;
use Phalcon\Events\Event;
use Phalcon\Flash\Session;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Annotations\Adapter\Memory as AnnotationsAdapter;
use Phalcon\Mvc\Model\MetaData\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Loader;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Text;


/**
 * Class Application
 *
 * @package Comit\Application
 * @author Igor Vuckovic
 */
class Application extends \Phalcon\Mvc\Application
{

    /**
     *
     */
    public function init()
    {
        // Set default timezone
        date_default_timezone_set('Europe/London');

        /** @var object|ConfigPhp $config */
        $config = new ConfigPhp(APP_PATH . 'app/config/global.php');

        $loader = new Loader();
        $loader->registerNamespaces(
            array(
                'Comit' => APP_PATH . 'app/library/Comit/',
            )
        );

        $loader->registerDirs(
            array(
                $config->application->modelsDir,
                $config->application->toolsDir,
            )
        );

        $loader->register();

        // The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
        $di = new FactoryDefault();

        // Specify config
        $di->set('config', function () use ($config) {

            $localConfig = new ConfigPhp(APP_PATH . 'app/config/local.php');
            $config->merge($localConfig);

            return $config;
        }, true);

        /** @var object|ConfigPhp $config */
        $config = $di->getShared('config');
        define('ENVIRONMENT', $config->application->environment);

        // Init
        $this->_initServices($di);
        $this->_initRoutes($di);
        $this->_initModules($di);
        $this->_initErrorHandler($di);

        $this->setDI($di);
    }

    /**
     * @param FactoryDefault $di
     */
    protected function _initServices(FactoryDefault $di)
    {
        /** @var object|ConfigPhp $config */
        $config = $di->getShared('config');

        /**
         * Setting up dispatcher
         */
        $di->set('dispatcher', function () use ($di) {

            /** @var EventsManager $eventsManager */
            $eventsManager = $di->getShared('eventsManager');
            $eventsManager->attach('dispatch:beforeDispatchLoop', function (Event $event, Dispatcher $dispatcher) {
                $actionName = Text::camelize($dispatcher->getActionName());
                $dispatcher->setActionName(lcfirst($actionName));
            });

            $dispatcher = new Dispatcher();
            $dispatcher->setEventsManager($eventsManager);

            return $dispatcher;
        }, true);

        /**
         * Setting up ACL
         */
        $di->set('acl', function () use ($di) {

            /** @var EventsManager $eventsManager */
            $eventsManager = $di->getShared('eventsManager');

            $aclPlugin = new AclPlugin();
            $acl = $aclPlugin->getAcl();
            $acl->setEventsManager($eventsManager);

            return $acl;
        }, true);

        /**
         * The URL component is used to generate all kind of urls in the application
         */
        $di->set('url', function () {
            $url = new UrlResolver();
            $url->setBaseUri('/');

            return $url;
        }, true);


        /**
         * Setting up the view component
         */
        $di->set('view', function () use ($di) {
            $view = new View();
            $view->registerEngines(array(
                '.volt' => 'volt',
                '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
            ));

            return $view;
        }, true);

        /**
         * Setting up simple view component
         */
        $di->set('simpleView', function () use ($di) {
            $simpleView = new View\Simple();
            $simpleView->registerEngines(array(
                '.volt' => 'volt',
                '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
            ));

            return $simpleView;
        }, true);

        $di->set('volt', function ($view, $di) use ($config) {
            $volt = new VoltEngine($view, $di);
            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir . 'volt/',
                'compiledSeparator' => '_'
            ));

            $compiler = $volt->getCompiler();
            $compiler->addFunction(
                't',
                function ($resolvedArgs, $exprArgs) use ($compiler) {

                    $firstArgument = $compiler->expression($exprArgs[0]['expr']);
                    $secondArgument = 'null';
                    $thirdArgument = 'isset($locale) ? $locale : \'en_US\'';

                    if (isset($exprArgs[1])) {
                        $secondArgument = $compiler->expression($exprArgs[1]['expr']);
                    }

                    return "\\Comit\\Helper\\Translator::translate($firstArgument, $secondArgument, $thirdArgument)";
                }
            );

            return $volt;
        }, true);


        /**
         * Database connection is created based in the parameters defined in the configuration file
         */
        $di->set('db', function () use ($config, $di) {

            $config = $config->toArray();
            $connection = new DbAdapter($config['database']);

            /** @var EventsManager $eventsManager */
            $eventsManager = $di->getShared('eventsManager');
            $connection->setEventsManager($eventsManager);

            return $connection;
        }, true);


        /**
         * Set a models manager
         */
        $di->set('modelsManager', function () use ($di) {

            /** @var EventsManager $eventsManager */
            $eventsManager = $di->getShared('eventsManager');
            $eventsManager->attach('modelsManager', new Annotations\Initializer());

            $modelsManager = new ModelsManager();
            $modelsManager->setEventsManager($eventsManager);

            return $modelsManager;
        }, true);


        /**
         * If the configuration specify the use of metadata adapter use it or use memory otherwise
         */
        $di->set('modelsMetadata', function () use ($config) {

            $metaData = new MetaDataAdapter(array(
                'metaDataDir' => $config->application->cacheDir . 'metadata/'
            ));

            //Set a custom meta-data database introspection
            $metaData->setStrategy(new Annotations\MetaDataInitializer());

            return $metaData;
        }, true);


        /**
         * Set annotations
         */
        $di->set('annotations', function () use ($config) {
            return new AnnotationsAdapter(array(
                'annotationsDir' => $config->application->cacheDir . 'annotations/'
            ));
        }, true);


        /**
         * Start the session the first time some component request the session service
         */
        $di->set('session', function () {
            $session = new SessionAdapter();
            $session->start();

            return $session;
        }, true);

        /**
         * Register the flash session service with custom CSS classes
         */
        $di->set('flashSession', function () {
            return new Session(array(
                'notice' => 'alert alert-info',
                'success' => 'alert alert-success',
                'error' => 'alert alert-danger',
                'warning' => 'alert alert-warning',
            ));
        }, true);


        /**
         * Register Swift Mailer
         */
        $di->set('mailManager', function () use ($di, $config) {
            $config = $config->toArray();
            $mailManager = new MailManager($config['mailer']);

            /** @var EventsManager $eventsManager */
            $eventsManager = $di->getShared('eventsManager');
            $mailManager->setEventsManager($eventsManager);

            return $mailManager;
        }, true);

        $di->set('mailer', function () use ($di) {
            /** @var MailManager $manager */
            $manager = $di->get('mailManager');
            return $manager->getMailer();
        }, true);


        /**
         * Register messenger manager
         */
        $di->set('messengerManager', function () {
            $manager = new MessengerManager();

            return $manager;
        }, true);


        /**
         * Register beanstalk queue
         */
        $di->set('beanstalkQueue', function () use ($config) {

            /** @var object $config */

            $queue = new Task\Queue(array(
                'host' => $config->beanstalk->host,
                'port' => $config->beanstalk->port
            ));

            $queue->setTube($config->beanstalk->tube);

            return $queue;
        }, true);

    }

    /**
     * @param FactoryDefault $di
     */
    protected function _initRoutes(FactoryDefault $di)
    {
        $di->set('router', function () {

            $router = new Router();
            $router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);
            $router->removeExtraSlashes(true);
            $router->setDefaultModule('backend');
            $router->setDefaultController('index');
            $router->setDefaultAction('index');


            $router->add('/admin/:controller/:action/:params', array(
                'module' => 'backend',
                'controller' => 1,
                'action' => 2,
                'params' => 3,
            ));

            $router->add('/admin/:controller', array(
                'module' => 'backend',
                'controller' => 1,
                'action' => 'index',
            ));

            $router->add('/', array(
                'module' => 'backend',
                'controller' => 'Dashboard',
                'action' => 'index',
            ));

            $router->add('/admin', array(
                'module' => 'backend',
                'controller' => 'Dashboard',
                'action' => 'index',
            ));

            return $router;
        }, true);
    }

    /**
     * @param FactoryDefault $di
     */
    protected function _initModules(FactoryDefault $di)
    {
        $this->registerModules(array(
            'backend' => array(
                'className' => 'Backend\Module',
                'path' => APP_PATH . 'app/modules/backend/Module.php'
            ),
        ));

        $this->setDefaultModule('backend');
    }

    /**
     * @param FactoryDefault $di
     */
    protected function _initErrorHandler(FactoryDefault $di)
    {

        $di->set('errorHandler', function () use ($di) {

            /** @var object|ConfigPhp $config */
            $config = $di->getShared('config');

            if ($config->error->server) {
                $errorHandler = new RemoteHandler(
                    new Client($config->error->server, $config->error->options)
                );
            } else {
                $errorHandler = new LocalHandler(
                    new FileLogger(APP_PATH . 'var/logs/error.log')
                );
            }

            $errorHandler->registerErrorHandler();
            $errorHandler->registerExceptionHandler();
            $errorHandler->registerShutdownFunction();

            return $errorHandler;
        }, true);

        /** @var RemoteHandler|LocalHandler $errorHandler */
        $errorHandler = $di->getShared('errorHandler');
        $errorHandler->initEnvironment(ENVIRONMENT);
    }

}

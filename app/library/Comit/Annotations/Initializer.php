<?php

namespace Comit\Annotations;

use Phalcon\Annotations\Adapter;
use Phalcon\Events\Event;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use Phalcon\Mvc\User\Plugin;

/**
 * Class Initializer
 *
 * @package Annotations
 * @author Igor Vuckovic
 */
class Initializer extends Plugin
{

    /**
     * This is called after initialize the model
     *
     * @param Event $event
     * @param ModelsManager $manager
     * @param $model
     */
    public function afterInitialize(Event $event, ModelsManager $manager, $model)
    {
        /** @var Adapter $annotationsAdapter */
        $annotationsAdapter = $this->getDI()->get('annotations');

        // Reflector
        $reflector = $annotationsAdapter->get($model);

        /**
         * Read the annotations in the class' docblock
         */
        if ($annotations = $reflector->getClassAnnotations()) {

            /**
             * Traverse the annotations
             */
            foreach ($annotations as $annotation) {
                switch ($annotation->getName()) {

                    /**
                     * Initializes the model's source
                     */
                    case 'Source':
                        $arguments = $annotation->getArguments();
                        $manager->setModelSource($model, $arguments[0]);
                        break;

                    /**
                     * Initializes Has-One relations
                     */
                    case 'HasOne':
                        $arguments = $annotation->getArguments();
                        if (isset($arguments[3])) {
                            $manager->addHasOne($model, $arguments[0], $arguments[1], $arguments[2], $arguments[3]);
                        } else {
                            $manager->addHasOne($model, $arguments[0], $arguments[1], $arguments[2]);
                        }
                        break;

                    /**
                     * Initializes Has-Many relations
                     */
                    case 'HasMany':
                        $arguments = $annotation->getArguments();
                        if (isset($arguments[3])) {
                            $manager->addHasMany($model, $arguments[0], $arguments[1], $arguments[2], $arguments[3]);
                        } else {
                            $manager->addHasMany($model, $arguments[0], $arguments[1], $arguments[2]);
                        }
                        break;

                    /**
                     * Initializes Belongs-To relations
                     */
                    case 'BelongsTo':
                        $arguments = $annotation->getArguments();
                        if (isset($arguments[3])) {
                            $manager->addBelongsTo($model, $arguments[0], $arguments[1], $arguments[2], $arguments[3]);
                        } else {
                            $manager->addBelongsTo($model, $arguments[0], $arguments[1], $arguments[2]);
                        }
                        break;

                    /**
                     * Initializes Has-Many-To-Many relations
                     */
                    case 'HasManyToMany':
                        $arguments = $annotation->getArguments();
                        if (isset($arguments[6])) {
                            $manager->addHasManyToMany($model, $arguments[0], $arguments[1], $arguments[2], $arguments[3], $arguments[4], $arguments[5], $arguments[6]);
                        } else {
                            $manager->addHasManyToMany($model, $arguments[0], $arguments[1], $arguments[2], $arguments[3], $arguments[4], $arguments[5]);
                        }
                        break;

                }
            }
        }
    }

}

<?php

namespace Comit\Model;

use Comit\Helper\DateTime;
use Phalcon\Db\Column;
use Phalcon\Mvc\ModelInterface;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;

/**
 * Class DateCastBehavior
 *
 * @package Comit\Model
 * @author Igor Vuckovic
 */
class DateCastBehavior extends Behavior implements BehaviorInterface
{

    /**
     * This method receives the notifications from the EventsManager
     *
     * @param string $type
     * @param ModelInterface $model
     */
    public function notify($type, ModelInterface $model)
    {
        switch ($type) {

            case 'beforeSave':
                $this->beforeSave($model);
                break;

            case 'afterSave':
                $this->afterSave($model);
                break;

            case 'postFetch':
                $this->castAllMySQLDatesToPhpDateTimes($model);
                break;
        }
    }

    /**
     * @param Base|EntityInterface|ModelInterface $model
     */
    private function beforeSave($model)
    {
        if ($modelMetaData = $model->getModelsMetaData()) {
            $columnMap = $modelMetaData->readColumnMap($model);

            foreach ($modelMetaData->getDataTypes($model) as $modelProperty => $dataType) {
                if ($dataType == Column::TYPE_DATETIME || $dataType == Column::TYPE_DATE) {
                    $property = $columnMap[0][$modelProperty];
                    if ($valueBeforeSave = $model->getProperty($property)) {
                        if ($valueBeforeSave instanceof DateTime) {
                            $model->setProperty($property, $valueBeforeSave->getUnixFormat());
                        } else {
                            if (!is_object($valueBeforeSave) && strtotime($valueBeforeSave) && 1 === preg_match('/[0-9]/', $valueBeforeSave)) {
                                $date = new DateTime($valueBeforeSave);
                                $model->setProperty($property, $date->getUnixFormat());
                            } else {
                                $model->setProperty($property, null);
                            }
                        }
                    } else {
                        $model->setProperty($property, null);
                    }
                }
            }
        }
    }

    /**
     * @param Base|EntityInterface|ModelInterface $model
     */
    private function afterSave($model)
    {
        if ($modelMetaData = $model->getModelsMetaData()) {
            $columnMap = $modelMetaData->readColumnMap($model);

            foreach ($modelMetaData->getDataTypes($model) as $modelProperty => $dataType) {

                if ($dataType == Column::TYPE_DATETIME || $dataType == Column::TYPE_DATE) {
                    $property = $columnMap[0][$modelProperty];
                    if ($date = $model->getProperty($property)) {
                        if ($dataType == Column::TYPE_DATE) {
                            $date = new DateTime($date, null, DateTime::TYPE_DATE);
                        } else {
                            $date = new DateTime($date, null, DateTime::TYPE_DATETIME);
                        }
                        $model->setProperty($property, $date);
                    } else {
                        $model->setProperty($property, null);
                    }
                }
            }
        }
    }

    /**
     * @param Base|EntityInterface|ModelInterface $model
     */
    private function castAllMySQLDatesToPhpDateTimes($model)
    {
        if ($modelMetaData = $model->getModelsMetaData()) {
            $columnMap = $modelMetaData->readColumnMap($model);

            foreach ($modelMetaData->getDataTypes($model) as $modelProperty => $dataType) {

                if ($dataType == Column::TYPE_DATETIME || $dataType == Column::TYPE_DATE) {
                    $property = $columnMap[0][$modelProperty];
                    if ($date = $model->getProperty($property)) {
                        if ($dataType == Column::TYPE_DATE) {
                            $date = new DateTime($date, null, DateTime::TYPE_DATE);
                        } else {
                            $date = new DateTime($date, null, DateTime::TYPE_DATETIME);
                        }
                        $model->setProperty($property, $date);
                    } else {
                        $model->setProperty($property, null);
                    }
                }
            }
        }
    }

}

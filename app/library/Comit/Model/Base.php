<?php

namespace Comit\Model;

use Comit\Filter\CamelCase;
use Comit\Form\Builder\Annotations as FormBuilder;
use Comit\Form\Form;
use Comit\Helper\DateTime;
use Comit\Helper\Translator;
use Comit\Messenger\Manager as MessengerManager;
use Comit\Messenger\Message as MessengerMessage;
use Phalcon\Db\Column;
use Phalcon\Flash;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Resultset as ResultSet;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Model;

/**
 * Class Base
 *
 * @package Comit\Model
 * @author Igor Vuckovic
 */
class Base extends Model implements EntityInterface
{

    protected $_objectId = 'id';

    protected $_restRepresentationDefinition = array();

    /**
     * @param null|string|array $parameters
     * @return $this
     */
    public static function findFirst($parameters = null)
    {
        if (is_null($parameters)) {
            return null;
        }

        return parent::findFirst($parameters);
    }

    /**
     * @param null|string|array $parameters
     * @return ResultSet|EntityInterface[]|$this[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }


    /**
     * Gets property value
     *
     * @param $property
     * @return mixed
     */
    public function getProperty($property)
    {
        $method = 'get' . ucfirst($property);
        if (method_exists($this, $method)) {
            return call_user_func(array($this, $method));
        }

        return null;
    }

    /**
     * @param string $property
     * @param mixed $value
     * @return $this
     */
    public function setProperty($property, $value)
    {
        $method = 'set' . ucfirst($property);
        if (method_exists($this, $method)) {
            if (0 === strlen($value)) {
                $value = null;
            }
            return call_user_func_array(array($this, $method), array($value));
        }

        return false;
    }

    /**
     * @return int
     */
    public function getObjectTypeId()
    {
        return Type::getEntityType(get_called_class());
    }

    /**
     * @return int
     */
    public function getObjectId()
    {
        return $this->getProperty($this->_objectId);
    }

    /**
     * Generates unique index for this Entity
     *
     * @return string
     */
    public function getObjectIndex()
    {
        return $this->getObjectTypeId() . '__' . $this->getObjectId();
    }

    /**
     * Tests if the object is equal (object type id and object id are the same)
     *
     * @param EntityInterface $object
     * @return bool
     */
    public function isEqualToEntity(EntityInterface $object = null)
    {
        if (is_object($object)) {
            return $this->getObjectIndex() === $object->getObjectIndex();
        }

        return false;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->getObjectIndex();
    }

    /**
     * @param array $exclude
     * @return Form
     */
    public function getForm($exclude = array())
    {
        $builder = new FormBuilder();

        return $builder->build($this, $exclude);
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
    }

    /**
     * Generates basic REST representation
     *
     * @return \ArrayObject
     */
    public function getBasicRestRepresentation()
    {
        $camelCase = new CamelCase();
        $basicRepresentation = new \ArrayObject();
        foreach ($this->getModelsMetaData()->getAttributes($this) as $fieldName) {
            $propertyName = $camelCase->filter($fieldName);
            $getMethod = 'get' . ucfirst($propertyName);
            if (method_exists($this, $getMethod)
                && array_key_exists($propertyName, $this->_restRepresentationDefinition)
                && $this->_restRepresentationDefinition[$propertyName]
            ) {
                $value = call_user_func(array($this, $getMethod));
                if ($value instanceof DateTime) {
                    $value = $value->getUnixFormat();
                }

                $basicRepresentation->offsetSet($fieldName, $value);
            }
        }

        return $basicRepresentation;
    }

    /**
     * Generates basic REST definition
     *
     * @return \ArrayObject
     */
    public function getBasicRestDefinition()
    {
        $camelCase = new CamelCase();
        $basicDefinition = new \ArrayObject();

        foreach ($this->getModelsMetaData()->getDataTypes($this) as $fieldName => $dataType) {
            $propertyName = $camelCase->filter($fieldName);
            $getMethod = 'get' . ucfirst($propertyName);
            if (method_exists($this, $getMethod)
                && array_key_exists($propertyName, $this->_restRepresentationDefinition)
                && $this->_restRepresentationDefinition[$propertyName]
            ) {

                switch ($dataType) {

                    case Column::TYPE_INTEGER :
                        $type = 'int';
                        break;

                    case Column::TYPE_BOOLEAN :
                        $type = 'bool';
                        break;

                    case Column::TYPE_DECIMAL :
                        $type = 'decimal';
                        break;

                    case Column::TYPE_DATE :
                        $type = 'date';
                        break;

                    case Column::TYPE_DATETIME :
                        $type = 'datetime';
                        break;

                    case Column::TYPE_CHAR :
                    case Column::TYPE_VARCHAR :
                    case Column::TYPE_TEXT :
                    default :
                        $type = 'string';
                        break;
                }

                $basicDefinition->offsetSet($fieldName, $type);
            }
        }

        return $basicDefinition;
    }


    /**
     * Gets REST definition
     *
     * @return array
     */
    public function getRestRepresentationDefinition()
    {
        return $this->_restRepresentationDefinition;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDisplayName();
    }

    /**
     * Initializer
     */
    public function initialize()
    {
        $this->addBehavior(new DateCastBehavior());
        $this->keepSnapshots(true);
    }

    /**
     *
     */
    public function afterFetch()
    {
        $this->fireEvent('postFetch');
    }

    /**
     *
     */
    public function onValidationFails()
    {
        /** @var MessengerManager $messengerManager */
        $messengerManager = $this->getDI()->getShared('messengerManager');

        foreach ($this->getMessages() as $message) {
            if (is_array($message->getField())) {
                $field = implode('__', $message->getField());
            } else {
                $field = $message->getField();
            }

            $messengerManager->appendMessage(new MessengerMessage(
                $this->_translate($message->getMessage()),
                $field,
                MessengerMessage::WARNING
            ));
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (!$this->isDeletable()) {

            /** @var MessengerManager $messengerManager */
            $messengerManager = $this->getDI()->getShared('messengerManager');
            $messengerManager->appendMessage(new MessengerMessage(
                $this->_translate('It is not allowed to delete this record.'),
                null,
                MessengerMessage::WARNING
            ));

            return false;
        }

        return true;
    }

    /**
     *
     */
    public function afterDelete()
    {
        /** @var MessengerManager $messengerManager */
        $messengerManager = $this->getDI()->getShared('messengerManager');
        $messengerManager->appendMessage(new MessengerMessage(
            $this->_translate('Data was deleted successfully.'),
            null,
            MessengerMessage::SUCCESS
        ));
    }

    /**
     * @param string $string
     * @param mixed $args
     * @return string string
     */
    protected function _translate($string, $args = null)
    {
        $translator = Translator::getInstance();

        if ($session = $this->getDI()->getShared('session')) {
            if ($currentUser = $session->get('currentUser')) {
                $locale = isset($currentUser['locale']) ? $currentUser['locale'] : 'en_US';
                return $translator::translate($string, $args, $locale);
            }
        }

        return $translator::translate($string, $args);
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        $identity = $this->getModelsMetaData()->getIdentityField($this);
        $fields = $this->getModelsMetaData()->getColumnMap($this);
        unset($fields[$identity]);
        return array_values($fields);

    }

    /**
     * @param $property
     * @return string
     */
    public function getFieldName($property)
    {
        $form = $this->getForm();

        $label = '';
        if($form->has($property)) {
            $label = $form->get($property)->getLabel();
        }

        return $label;
    }

    /**
     * @param $value
     * @return mixed
     */
    public static function getConstantDisplayValue($value)
    {
        if($value) {
            $class = new \ReflectionClass(get_called_class());
            $constants = $class->getConstants();

            return $constants[$value];
        }

    }

}

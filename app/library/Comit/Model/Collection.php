<?php

namespace Comit\Model;

/**
 * Class Collection
 *
 * @package Comit\Model
 * @author Igor Vuckovic
 */
class Collection extends \ArrayObject
{

    /**
     * @param null $input
     * @param int $flags
     * @param string $iteratorClass
     */
    public function __construct($input = null, $flags = 0, $iteratorClass = 'ArrayIterator')
    {

        $this->setFlags($flags);
        $this->setIteratorClass($iteratorClass);

        if ($input) {
            foreach ($input as $item) {
                $this->append($item);
            }
        }
    }

    /**
     * @param array $criteriaQuantifiers
     * Associative array mapping EntityTypeIDs to callables. the callable needs
     * to accept a single collection element and return a value which will be
     * used to sort this element.
     *
     * @throws \RuntimeException
     * @return void
     * @see MIS-2028
     */
    public function usort($criteriaQuantifiers)
    {
        $stagingArray = array();

        /** @var EntityInterface $element */
        foreach ($this as $element) {
            if (!array_key_exists($element->getObjectTypeId(), $criteriaQuantifiers) && !isset($criteriaQuantifiers['*'])) {
                throw new \RuntimeException('Invalid Sort criteria quantifier. ' .
                    'The quantifier is missing a definition for Entity Type: '
                    . $element->getObjectTypeId());
            }

            $callback = (isset($criteriaQuantifiers[$element->getObjectTypeId()])
                ? $criteriaQuantifiers[$element->getObjectTypeId()]
                : $criteriaQuantifiers['*']
            );

            $quantifierVal = call_user_func($callback, $element);
            $stagingArray[$quantifierVal][] = $element;
        }

        // Sort by $quantifierVal
        ksort($stagingArray, SORT_NATURAL | SORT_FLAG_CASE);

        // Clear data and import from staging array
        $this->exchangeArray(array());
        foreach ($stagingArray as $quantifierVal => $entities) {
            foreach ($entities as $entity) {
                $this->append($entity);
            }
        }
    }

    /**
     * @return array An array of object type id's which are present in this collection
     */
    public function getEntityTypes()
    {
        $entityTypes = array();
        /** @var $element EntityInterface */
        foreach ($this as $element) {
            $entityTypes[$element->getObjectTypeId()] = $element->getObjectTypeId();
        }

        return array_keys($entityTypes);
    }

    /**
     * @param mixed $index
     * @param EntityInterface $newVal
     * @throws \InvalidArgumentException
     */
    public function offsetSet($index, $newVal)
    {
        if (!$newVal instanceof EntityInterface) {
            throw new \InvalidArgumentException(
                'Only objects implementing the EntityInterface or objects of class can be added to an Collection'
            );
        }

        if (is_null($index)) {
            if ($newVal instanceof EntityInterface) {
                $index = $newVal->getObjectIndex();
            } else {
                $index = null;
            }
        }

        parent::offsetSet($index, $newVal);
    }


    /**
     * This is equivalent to current($collection) EXCEPT that it returns NULL not FALSE.
     * Do not use current() with Collections.
     *
     * @return Base|null
     */
    public function getFirstEntity()
    {
        foreach ($this as $element) {
            return $element;
        }

        return null;
    }

    /**
     * Gets the last entity in the collection
     * @return Base|null
     */
    public function getLastEntity()
    {
        if ($this->count() > 0) {
            $arrayCopy = $this->getArrayCopy();

            return end($arrayCopy);
        }

        return null;

    }

    /**
     * Gets a random member of the collection. Useful for data generation.
     *
     * @return Base
     */
    public function getRandomEntity()
    {
        if ($this->count() > 0) {
            $arrayCopy = $this->getArrayCopy();
            return $arrayCopy[array_rand($arrayCopy)];
        }

        return null;
    }

    /**
     * @param EntityInterface $value
     */
    public function append($value)
    {
        if (is_object($value)) {
            if ($value instanceof EntityInterface) {
                $this->offsetSet($value->getObjectIndex(), $value);
            } else {
                $this->offsetSet(null, $value);
            }
        }
    }

    /**
     * Checks if the Collection contains the object.
     *
     * @param EntityInterface $object
     * @return bool
     */
    public function contains(EntityInterface $object)
    {
        if (is_null($object)) {
            return false;
        } else {
            return $this->offsetExists($object->getObjectIndex());
        }
    }

    /**
     * Merge $collection into this collection.
     *
     * @param Collection $collection
     * @return Collection
     */
    public function mergeCollection(Collection $collection)
    {
        foreach ($collection as $entity) {
            $this->append($entity);
        }

        return $this;
    }

    /**
     * The method excludes (removes) items from $excludeCollection from this collection.
     * The collection returned is NOT a copy of the original collection with the items from $excludeCollection removed,
     * but the original collection itself.
     *
     * @param Collection $excludeCollection
     * @return Collection
     *
     * @todo Optimize to use ArrayObject methods
     */
    public function excludeCollection(Collection $excludeCollection)
    {
        foreach ($excludeCollection as $item) {
            $this->remove($item);
        }

        return $this;
    }

    /**
     * @param EntityInterface $object
     */
    public function remove(EntityInterface $object)
    {
        if (!is_null($object) && $this->offsetExists($object->getObjectIndex())) {
            $this->offsetUnset($object->getObjectIndex());
        }
    }

    /**
     * Intersect this collection with the collection of the argument
     * Returns a new entity collection so the called entity collection remains intact
     *
     * @param Collection $collectionToIntersectWith
     * @return Collection
     */
    public function intersectWithCollection(Collection $collectionToIntersectWith)
    {
        $intersectedCollection = new Collection();

        foreach ($this as $thisItem) {
            if ($collectionToIntersectWith->contains($thisItem)) {
                $intersectedCollection->append($thisItem);
            }
        }

        return $intersectedCollection;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return implode(', ', $this->toArray());
    }


    /**
     * Reverses this collection.
     */
    public function reverse()
    {
        $arrayCopy = $this->getArrayCopy();

        $this->exchangeArray(array_reverse($arrayCopy, true));

        return $this;
    }

    /**
     * Returns TRUE if $collection2 contains identical elements to this collection
     *
     * @param Collection $collection2
     *
     * @return bool
     */
    public function equals(Collection $collection2)
    {

        $collection1 = $this->getArrayCopy();
        $collection2 = $collection2->getArrayCopy();

        if (sizeof(array_diff_key($collection1, $collection2)) == 0 && sizeof($collection1) == sizeof($collection2)) {
            return true;
        }

        return false;
    }

    /**
     * Returns the object for the entity type and ID if it exists in the collection
     *
     * @param string $entityType
     * @param int $id
     *
     * @return EntityInterface
     */
    public function getEntity($entityType, $id)
    {
        $key = $entityType . '__' . $id;

        if ($this->offsetExists($key)) {
            return $this->offsetGet($key);
        }

        return null;
    }

    /**
     * @return Collection
     */
    public function sortByDisplayName()
    {

        $sortQuantifier = array(
            '*' => function (Base $model) {
                return $model->getDisplayName();
            },
        );

        $this->usort($sortQuantifier);

        return $this;
    }

    /**
     * @param string $entityType
     * @return array
     */
    public function getObjectIds($entityType = null)
    {
        $objectIds = array();
        foreach ($this as $thisItem) {
            /** @var EntityInterface $thisItem */
            if (!$entityType || $thisItem->getObjectTypeId() === $entityType) {
                $objectIds[$thisItem->getObjectId()] = $thisItem->getObjectId();
            }
        }

        return $objectIds;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $array = array();
        foreach ($this as $thisItem) {
            /** @var Base $thisItem */
            $array[$thisItem->getObjectId()] = $thisItem->getDisplayName();
        }

        return $array;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDisplayName();
    }

}

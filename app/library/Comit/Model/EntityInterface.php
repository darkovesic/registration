<?php

namespace Comit\Model;

/**
 * Interface EntityInterface
 *
 * @package Comit\Model
 * @author Igor Vuckovic
 */
interface EntityInterface
{

    /**
     * Gets object type identifier (equivalent to value of Type::ENTITY_NAME constant)
     *
     * @return int
     */
    public function getObjectTypeId();


    /**
     * Gets Object ID (i.e. primary key)
     * @return int
     */
    public function getObjectId();


    /**
     * Generates unique index for this Entity
     *
     * @return string
     */
    public function getObjectIndex();

    /**
     * Tests if the object is equal (object type id and object id are the same)
     *
     * @param EntityInterface $object
     * @return bool
     */
    public function isEqualToEntity(EntityInterface $object = null);

    /**
     * Generates basic REST representation
     *
     * @return \ArrayObject
     */
    public function getBasicRestRepresentation();

    /**
     * Generates basic REST definition
     *
     * @return \ArrayObject
     */
    public function getBasicRestDefinition();

    /**
     * @return array
     */
    public function getRestRepresentationDefinition();

}

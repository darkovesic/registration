<?php

namespace Comit\Model;

/**
 * Class Type
 *
 * @package Comit\Model
 * @author Igor Vuckovic
 */
class Type
{

    const BANK_ACCOUNT = 1;
    const BID_REQUEST = 2;
    const BID = 3;
    const CERTIFICATION = 4;
    const COMMENT = 5;
    const COUNTRY = 6;
    const CURRENCY = 7;
    const FOLLOW = 8;
    const GALLERY = 9;
    const LANGUAGE = 10;
    const LIKE = 35;
    const MEMBER = 11;
    const ORDER = 12;
    const PAGE = 13;
    const PROGRAMME = 14;
    const POST = 36;
    const POST_CATEGORY = 37;
    const RATE = 15;
    const SEARCH = 16;
    const SEO_METADATA = 17;
    const SERVICE = 18;
    const SOCIAL_NETWORK = 19;
    const SPOKEN_LANGUAGE = 20;
    const SPORT_CATEGORY = 21;
    const SPORTSMAN = 22;
    const SUBSCRIPTION = 23;
    const TEACHING_CATEGORY = 24;
    const TEACHING_GROUP = 25;
    const TEACHING_LEVEL = 26;
    const TEACHING_LOCATION = 27;
    const TIMETABLE_SLOT = 28;
    const TRAINER = 29;
    const TRAINING_REQUEST = 30;
    const TRAINING = 31;
    const TRANSLATION = 32;
    const UPLOAD = 33;
    const USER = 34;



    /*
     * Each Model should have its own unique 'Entity number'
     * When adding new Model, it should have defined 'Entity number'
     * Use first available value
     * Current max 'Entity number' is 37
     */

    private static $objectNames = array(
        self::BANK_ACCOUNT => 'BankAccount',
        self::BID_REQUEST => 'BidRequest',
        self::BID => 'Bid',
        self::CERTIFICATION => 'Certification',
        self::COMMENT => 'Comment',
        self::COUNTRY => 'Country',
        self::CURRENCY => 'Currency',
        self::FOLLOW => 'Follow',
        self::GALLERY => 'Gallery',
        self::LANGUAGE => 'Language',
        self::LIKE => 'Like',
        self::MEMBER => 'Member',
        self::ORDER => 'Order',
        self::PAGE => 'Page',
        self::POST => 'Post',
        self::POST_CATEGORY => 'PostCategory',
        self::PROGRAMME => 'Programme',
        self::RATE => 'Rate',
        self::SEARCH => 'Search',
        self::SEO_METADATA => 'SeoMetadata',
        self::SERVICE => 'Service',
        self::SOCIAL_NETWORK => 'SocialNetwork',
        self::SPOKEN_LANGUAGE => 'SpokenLanguage',
        self::SPORT_CATEGORY => 'SportCategory',
        self::SPORTSMAN => 'Sportsman',
        self::SUBSCRIPTION => 'Subscription',
        self::TEACHING_CATEGORY => 'TeachingCategory',
        self::TEACHING_GROUP => 'TeachingGroup',
        self::TEACHING_LEVEL => 'TeachingLevel',
        self::TEACHING_LOCATION => 'TeachingLocation',
        self::TIMETABLE_SLOT => 'TimetableSlot',
        self::TRAINER => 'Trainer',
        self::TRAINING_REQUEST => 'TrainingRequest',
        self::TRAINING => 'Training',
        self::TRANSLATION => 'Translation',
        self::UPLOAD => 'Upload',
        self::USER => 'User',
    );

    /**
     * @return array
     */
    public static function getObjectNames()
    {
        return self::$objectNames;
    }

    /**
     * Get Model convenience method
     * Returns a model instance
     *
     * @param int $objectTypeId
     * @param int $objectId
     * @return null|Base
     */
    public static function getEntity($objectTypeId, $objectId)
    {
        if (array_key_exists($objectTypeId, self::$objectNames)) {
            $entityName = '\\' . self::$objectNames[$objectTypeId];

            if (class_exists($entityName)) {

                if (is_callable(array($entityName, 'findFirst'))) {
                    $conditions = array('id = :id:', 'bind' => array('id' => $objectId));
                    return call_user_func(array($entityName, 'findFirst'), $conditions);
                } else {
                    $entity = new $entityName;
                    return $entity;
                }
            }
        }

        return null;
    }

    /**
     * Checks whether the class contains a certain entity type, which may
     * be passed in as an object type id or a string.
     *
     * @param int|string $entityType
     * @return bool
     */
    public static function hasEntityType($entityType)
    {
        if (is_int($entityType) && array_key_exists($entityType, self::$objectNames)) {
            $entityType = self::getObjectName($entityType);
        }

        if (strtoupper($entityType) == $entityType) {
            $entityType = ucfirst(strtolower($entityType));
        }

        return (in_array($entityType, self::$objectNames));
    }

    /**
     * @param int $entityType
     * @return string|null
     */
    public static function getObjectName($entityType)
    {
        if (array_key_exists($entityType, self::$objectNames)) {
            return self::$objectNames[$entityType];
        }

        return null;
    }

    /**
     * @param string $objectName
     * @return int|null
     */
    public static function getEntityType($objectName)
    {
        if (in_array($objectName, self::$objectNames)) {
            $entities = array_keys(self::$objectNames, $objectName);

            return $entities[0];
        }

        return null;
    }

}
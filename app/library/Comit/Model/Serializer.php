<?php

namespace Comit\Model;

use Comit\Helper\DateTime;
use Phalcon\Mvc\Model\Resultset as ResultSet;

/**
 * Class Serializer
 *
 * @package Comit\Task
 * @author Igor Vuckovic
 */
class Serializer
{

    /**
     * @param array $phpArray
     * @return array
     */
    public function serializeToSimpleArray($phpArray)
    {
        if (is_array($phpArray)) {
            foreach ($phpArray as $paramName => $param) {
                try {
                    $phpArray[$paramName] = $this->serializeVariableToJson($param);
                } catch (\InvalidArgumentException $e) {
                    throw new \InvalidArgumentException('Do not know how to serialize property $'
                        . $paramName . ' = ' . get_class($param));
                }
            }
        } elseif (is_object($phpArray)) {
            throw new \InvalidArgumentException('You must provide an array of key/value pairs'
                . ' to encode objects (' . get_class($phpArray) . ' passed).');
        } else {
            $phpArray = $this->serializeVariableToJson($phpArray);
        }

        return $phpArray;
    }

    /**
     * @param array $phpArray
     * @return string
     */
    public function serializeToJson($phpArray)
    {
        if (is_null($phpArray)) {
            return null;
        }

        return json_encode($this->serializeToSimpleArray($phpArray));
    }


    /**
     * @param array $jsonArray
     * @return array
     */
    public function unserializeFromJson($jsonArray)
    {
        if (is_null($jsonArray)) {
            return [];
        }
        if (is_array($jsonArray)) {
            foreach ($jsonArray as $paramName => $param) {
                $jsonArray[$paramName] = $this->unSerializeVariableFromJson($param);
            }
        } else {
            $jsonArray = $this->unSerializeVariableFromJson($jsonArray);
        }

        return $jsonArray;
    }


    /**
     * @param Collection|ResultSet|Base[] $variable
     * @return array
     * @throws \InvalidArgumentException
     */
    public function serializeVariableToJson($variable)
    {
        if (is_object($variable)) {

            if ($variable instanceof Base) {
                $variable = ['objectTypeId' => $variable->getObjectTypeId(), 'objectId' => $variable->getObjectId()];
            } elseif ($variable instanceof Collection) {
                $serializedCollection = ['collection' => []];
                foreach ($variable as $model) {
                    $serializedCollection['collection'][] = [
                        'objectTypeId' => $model->getObjectTypeId(),
                        'objectId' => $model->getObjectId()
                    ];
                }
                $variable = $serializedCollection;
            } elseif ($variable instanceof DateTime) {
                $variable = ['date' => $variable->getUnixFormat()];
            } else {
                throw new \InvalidArgumentException('Do not know how to serialize property ' . get_class($variable));
            }
        }

        return $variable;
    }

    /**
     * @param $variable
     * @return DateTime|Base|Collection|null
     */
    public function unSerializeVariableFromJson($variable)
    {
        if (is_array($variable)) {
            if (array_key_exists('date', $variable)) {
                $variable = new DateTime($variable['date']);
            } elseif (array_key_exists('objectTypeId', $variable) && array_key_exists('objectId', $variable)) {
                $variable = Type::getEntity($variable['objectTypeId'], $variable['objectId']);
            } elseif (array_key_exists('collection', $variable)) {
                $collection = new Collection();
                foreach ($variable['collection'] as $modelReference) {
                    $variable = Type::getEntity($modelReference['objectTypeId'], $modelReference['objectId']);
                    $collection->append($variable);
                }
                $variable = $collection;
            }
        }

        return $variable;
    }

    /**
     * @param $object
     * @return array
     */
    public function getParamsFromObject($object)
    {
        $currentParams = call_user_func('get_object_vars', $object);
        $currentParams = $this->serializeToSimpleArray($currentParams);
        $defaultParams = get_class_vars(get_class($object));
        $defaultParams = $this->serializeToSimpleArray($defaultParams);

        $params = [];
        foreach ($currentParams as $paramName => $param) {
            if ($param !== $defaultParams[$paramName]) {
                $params[$paramName] = $param;
            }
        }

        return $params;
    }

    /**
     * @param object $object
     * @param array $params
     * @return object
     */
    public function setObjectPropertiesFromParams($object, array $params)
    {
        foreach ($params as $paramName => $param) {
            if (!property_exists($object, $paramName)) {
                throw new \InvalidArgumentException('Task ' . get_class($object)
                    . ' does not have public property ' . $paramName . ' set in params.');
            }
            $object->$paramName = $param;
        }

        return $object;
    }

}

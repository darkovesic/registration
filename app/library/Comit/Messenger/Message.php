<?php

namespace Comit\Messenger;

/**
 * Class Message
 *
 * @package Comit\Messenger
 * @author Igor Vuckovic
 */
class Message
{

    const INFO = 'INFO';
    const SUCCESS = 'SUCCESS';
    const WARNING = 'WARNING';
    const DANGER = 'DANGER';

    protected $_message;
    protected $_type = self::INFO;
    protected $_field;

    /**
     * @param string $message
     * @param string $field
     * @param string $type
     */
    public function __construct($message, $field = null, $type = self::INFO)
    {
        $this->setMessage($message);
        $this->setField($field);
        $this->setType($type);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->_field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->_field = $field;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->_message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->_message = $message;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array(
            'message' => $this->getMessage(),
            'field' => $this->getField(),
            'type' => strtolower($this->getType()),
        );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getMessage();
    }

}

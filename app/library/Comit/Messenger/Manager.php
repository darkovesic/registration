<?php

namespace Comit\Messenger;

/**
 * Class Manager
 *
 * @package Comit\Messenger
 * @author Igor Vuckovic
 */
class Manager
{

    protected $_messages;

    /**
     *
     */
    public function __construct()
    {
        $this->clear();
    }

    /**
     * @param Message $message
     * @return $this
     */
    public function appendMessage(Message $message)
    {
        $this->_messages[$message->getType()][] = $message;

        return $this;
    }

    /**
     * @param null|string $type
     * @return array|null|Message[]
     */
    public function getMessages($type = null)
    {
        if (is_null($type)) {
            return array_values($this->_messages);
        }

        if (array_key_exists($type, $this->_messages)) {
            return $this->_messages[$type];
        }

        return null;
    }

    /**
     * @param $type
     * @return bool
     */
    public function hasMessages($type)
    {
        if (array_key_exists($type, $this->_messages)) {
            return count($this->_messages[$type]) > 0;
        }

        return false;
    }

    /**
     * @param null|string $type
     * @return array
     */
    public function toArray($type = null)
    {
        $array = array();
        foreach ($this->getMessages($type) as $message) {
            $array[] = $message->toArray();
        }

        return $array;
    }

    /**
     * @return $this
     */
    public function clear()
    {
        $this->_messages = array(
            Message::INFO => array(),
            Message::SUCCESS => array(),
            Message::WARNING => array(),
            Message::DANGER => array(),
        );

        return $this;
    }

}

<?php

namespace Comit\Error;

/**
 * Class RemoteHandler
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
class RemoteHandler extends AbstractHandler implements HandlerInterface
{

    /** @var Client */
    private $client;


    /**
     * @param Client $client
     * @param bool $sendErrorsLast
     * @param null|array $defaultErrorTypes
     */
    public function __construct(Client $client, $sendErrorsLast = false, $defaultErrorTypes = null)
    {
        $this->client = $client;

        if (!is_null($defaultErrorTypes)) {
            $this->_defaultErrorTypes = $defaultErrorTypes;
        }

        register_shutdown_function(function () {
            if (!defined('ERROR_CLIENT_END_REACHED')) {
                define('ERROR_CLIENT_END_REACHED', true);
            }
        });

        if ($sendErrorsLast) {
            $this->client->storeErrorsForBulkSend = true;
            register_shutdown_function(array($this->client, 'sendUnsentErrors'));
        }
    }

    /**
     * @param \Exception $e
     * @param bool $isError
     * @param null $vars
     */
    public function handleException(\Exception $e, $isError = false, $vars = null)
    {
        $this->client->captureException($e, null, null, $vars);
    }

    /**
     *
     */
    public function registerExceptionHandler()
    {
        set_exception_handler(array($this, 'handleException'));
    }

    /**
     *
     */
    public function registerErrorHandler()
    {
        set_error_handler(array($this, 'handleError'), error_reporting());
    }

    /**
     *
     */
    public function registerShutdownFunction()
    {
        register_shutdown_function(array($this, 'handleFatalError'));
    }

}

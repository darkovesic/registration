<?php

namespace Comit\Error;

/**
 * Class AbstractHandler
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
abstract class AbstractHandler implements HandlerInterface
{

    const DEBUG = 'DEBUG';
    const INFO = 'INFO';
    const WARNING = 'WARNING';
    const ERROR = 'ERROR';
    const FATAL = 'FATAL';

    /**
     * Error types that can be processed by the handler
     *
     * @var array
     */
    protected $_validErrorTypes = array(
        E_ERROR,
        E_WARNING,
        E_PARSE,
        E_NOTICE,
        E_CORE_ERROR,
        E_CORE_WARNING,
        E_COMPILE_ERROR,
        E_COMPILE_WARNING,
        E_USER_ERROR,
        E_USER_WARNING,
        E_USER_NOTICE,
        E_STRICT,
        E_RECOVERABLE_ERROR,
        E_DEPRECATED,
        E_USER_DEPRECATED,
    );

    /**
     * The default Error types that are always processed by the handler. Can be set during construction.
     *
     * @var array
     */
    protected $_defaultErrorTypes = array(
        E_ERROR,
        E_PARSE,
        E_CORE_ERROR,
        E_CORE_WARNING,
        E_COMPILE_ERROR,
        E_COMPILE_WARNING,
        E_STRICT,
    );

    /**
     * @return array
     */
    protected function getErrorTypesToProcess()
    {
        return array_unique(array_merge($this->_defaultErrorTypes, $this->_validErrorTypes));
    }

    /**
     * @param int $code
     * @param string $message
     * @param string $file
     * @param int $line
     * @param array $context
     * @return bool
     */
    public function handleError($code, $message, $file = '', $line = 0, $context = array())
    {
        if ($code && error_reporting()) {
            $e = new \ErrorException($message, 0, $code, $file, $line);
            $this->handleException($e, true, $context);
        }
    }

    /**
     *
     */
    public function handleFatalError()
    {
        if (null === $lastError = error_get_last()) {
            return;
        }

        $errors = 0;
        foreach ($this->getErrorTypesToProcess() as $errorType) {
            $errors |= $errorType;
        }

        if ($lastError['type'] && $errors) {
            $e = new \ErrorException(
                @$lastError['message'], @$lastError['type'], @$lastError['type'],
                @$lastError['file'], @$lastError['line']
            );
            $this->handleException($e, true);
        }
    }

    /**
     * @param int $severity
     * @return string
     */
    public static function mapSeverity($severity)
    {
        switch ($severity) {
            case E_ERROR:
                return self::ERROR;

            case E_WARNING:
                return self::WARNING;

            case E_PARSE:
                return self::ERROR;

            case E_NOTICE:
                return self::INFO;

            case E_CORE_ERROR:
                return self::ERROR;

            case E_CORE_WARNING:
                return self::WARNING;

            case E_COMPILE_ERROR:
                return self::ERROR;

            case E_COMPILE_WARNING:
                return self::WARNING;

            case E_USER_ERROR:
                return self::ERROR;

            case E_USER_WARNING:
                return self::WARNING;

            case E_USER_NOTICE:
                return self::INFO;

            case E_STRICT:
                return self::INFO;

            case E_RECOVERABLE_ERROR:
                return self::ERROR;
        }

        if (version_compare(PHP_VERSION, '5.3.0', '>=')) {
            switch ($severity) {

                case E_DEPRECATED:
                    return self::WARNING;

                case E_USER_DEPRECATED:
                    return self::WARNING;
            }
        }

        return self::ERROR;
    }

    /**
     * @param string $environment
     */
    public function initEnvironment($environment)
    {
        switch ($environment) {

            case 'production' :
            case 'staging' :
                ini_set('display_errors', 0);
                ini_set('display_startup_errors', 0);
                error_reporting(E_ALL & ~E_NOTICE);
                break;

            case 'development':
            case 'testing':
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
                break;

            default:
                ini_set('display_errors', 0);
                ini_set('display_startup_errors', 0);
                error_reporting(E_ALL);
                break;
        }

    }

}

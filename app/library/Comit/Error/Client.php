<?php

namespace Comit\Error;

/**
 * Class Client
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
class Client
{

    const MESSAGE_LIMIT = 1024;

    public $storeErrorsForBulkSend = false;

    /** @var Context */
    private $context;
    /** @var Asynchronous */
    private $asynchronous;
    /** @var Processor */
    private $processor;

    private $_options = array();
    private $_server;
    private $_publicKey;
    private $_secretKey;
    private $_autoLogStacks;
    private $_name;
    private $_site;
    private $_tags;
    private $_release;
    private $_trace;
    private $_timeout;
    private $_messageLimit;
    private $_exclude;
    private $_shiftVars;
    private $_curlMethod;
    private $_curlPath;
    private $_errorData;

    /**
     * @param string $server
     * @param array $options
     */
    public function __construct($server, $options = array())
    {
        $this->processor = new Processor($this);
        $this->context = new Context();

        $this->_options = $options;

        $this->_server = $server;
        $this->_autoLogStacks = (bool)$this->getOption('autoLogStacks', false);
        $this->_name = $this->getOption('name', gethostname());
        $this->_site = $this->getOption('site', $this->getServerVariable('SERVER_NAME'));
        $this->_tags = $this->getOption('tags', array());
        $this->_release = $this->getOption('release', null);
        $this->_trace = (bool)$this->getOption('trace', true);
        $this->_timeout = $this->getOption('timeout', 2);
        $this->_messageLimit = $this->getOption('messageLimit', self::MESSAGE_LIMIT);
        $this->_exclude = $this->getOption('exclude', array());
        $this->_shiftVars = (bool)$this->getOption('shiftVars', true);
        $this->_curlMethod = $this->getOption('curlMethod', 'sync');
        $this->_curlPath = $this->getOption('curlPath', 'curl');

        if ($this->_curlMethod == 'async') {
            $this->asynchronous = new Asynchronous($this->getCurlOptions());
        }
    }

    /**
     * Wrapper to handle encoding and sending data to the API server.
     *
     * @param array $data
     */
    public function send($data)
    {
        if (!$this->_server) {
            return;
        }

        $parts = parse_url($this->_server);
        if (isset($parts['user'])) {
            $this->_publicKey = $parts['user'];
        }
        if (isset($parts['pass'])) {
            $this->_secretKey = $parts['pass'];
        }

        $headers = array(
            'User-Agent' => 'Error Handler',
            'Authorization' => $this->getAuthHeader($this->_publicKey, $this->_secretKey),
            'Content-Type' => 'application/json'
        );

        $this->sendHttp($this->_server, json_encode($data), $headers);
    }

    /**
     *
     */
    public function sendUnsentErrors()
    {
        if (!empty($this->_errorData)) {
            foreach ($this->_errorData as $data) {
                $this->send($data);
            }
            unset($this->_errorData);
        }

        if ($this->storeErrorsForBulkSend) {
            $this->storeErrorsForBulkSend = !defined('ERROR_CLIENT_END_REACHED');
        }
    }

    /**
     * Sets user context.
     *
     * @param array $data
     */
    public function setUserContext($data)
    {
        $this->context->user = $data;
    }

    /**
     * Appends tags context.
     *
     * @param array $data
     */
    public function setTagsContext($data)
    {
        $this->context->tags = array_merge($this->context->tags, $data);
    }

    /**
     * Appends additional context.
     *
     * @param array $data
     */
    public function setExtraContext($data)
    {
        $this->context->extra = array_merge($this->context->extra, $data);
    }

    /**
     * @param array $data
     * @param $stack
     * @param null $vars
     * @return string
     */
    protected function capture($data, $stack, $vars = null)
    {
        if (!isset($data['timestamp'])) {
            $data['timestamp'] = gmdate('Y-m-d\TH:i:s\Z');
        }

        if (!isset($data['level'])) {
            $data['level'] = RemoteHandler::ERROR;
        }

        if (!isset($data['tags'])) {
            $data['tags'] = array();
        }

        if (!isset($data['extra'])) {
            $data['extra'] = array();
        }

        if (!isset($data['event_id'])) {
            $data['event_id'] = $this->uuid4();
        }

        if (isset($data['message'])) {
            $data['message'] = substr($data['message'], 0, $this->_messageLimit);
        }

        $data = array_merge($this->getDefaultData(), $data);

        if ($this->isHttpRequest()) {
            $data = array_merge($this->getHttpData(), $data);
        }

        $data = array_merge($this->getUserData(), $data);

        if ($this->_release) {
            $data['release'] = $this->_release;
        }

        $data['tags'] = array_merge(
            $this->_tags,
            $this->context->tags,
            $data['tags']);

        $data['extra'] = array_merge(
            $this->context->extra,
            $data['extra']);

        if (0 == count($data['extra'])) {
            unset($data['extra']);
        }

        if (0 == count($data['tags'])) {
            unset($data['tags']);
        }

        if ((!$stack && $this->_autoLogStacks) || $stack === true) {
            $stack = debug_backtrace();

            // Drop last stack
            array_shift($stack);
        }

        if (count($stack) > 0) {
            if (!isset($data['stacktrace'])) {
                $data['stacktrace'] = array(
                    'frames' => Stacktrace::getStackInfo(
                        $stack, $this->_trace, $this->_shiftVars, $vars, $this->_messageLimit
                    ),
                );
            }
        }

        $this->sanitize($data);
        $this->process($data);

        if (!$this->storeErrorsForBulkSend) {
            $this->send($data);
        } else {
            if (empty($this->_errorData)) {
                $this->_errorData = array();
            }
            $this->_errorData[] = $data;
        }

        return $data['event_id'];
    }

    /**
     * @param string $message
     * @param array $params
     * @param array $options
     * @param bool $stack
     * @param null $vars
     * @return string
     */
    public function captureMessage($message, $params = array(), $options = array(), $stack = false, $vars = null)
    {
        if (count($params) > 0) {
            $formattedMessage = vsprintf($message, $params);
        } else {
            $formattedMessage = $message;
        }

        if (is_null($options)) {
            $data = array();
        } elseif (!is_array($options)) {
            $data = array(
                'level' => $options,
            );
        } else {
            $data = $options;
        }

        $data['message'] = $formattedMessage;
        $data['original_message'] = array(
            'message' => $message,
            'params' => $params,
        );

        return $this->capture($data, $stack, $vars);
    }

    /**
     * @param \Exception $exception
     * @param null $options
     * @param null $logger
     * @param null $vars
     * @return null
     */
    public function captureException(\Exception $exception, $options = null, $logger = null, $vars = null)
    {
        $hasChainedExceptions = version_compare(PHP_VERSION, '5.3.0', '>=');

        if (in_array(get_class($exception), $this->_exclude)) {
            return null;
        }

        if (!is_array($options)) {
            $data = array();
            if ($options !== null) {
                $data['culprit'] = $options;
            }
        } else {
            $data = $options;
        }

        $message = $exception->getMessage();
        if (0 == strlen($message)) {
            $message = get_class($exception);
        }

        $exc = $exception;
        do {
            $excData = array(
                'value' => $exc->getMessage(),
                'type' => get_class($exc),
                'module' => $exc->getFile() . ':' . $exc->getLine(),
            );


            $trace = $exc->getTrace();
            $frameWhereExceptionThrown = array(
                'file' => $exc->getFile(),
                'line' => $exc->getLine(),
            );

            array_unshift($trace, $frameWhereExceptionThrown);

            $excData['stacktrace'] = array(
                'frames' => Stacktrace::getStackInfo(
                    $trace, $this->_trace, $this->_shiftVars, $vars, $this->_messageLimit
                ),
            );

            $exceptions[] = $excData;
        } while ($hasChainedExceptions && $exc = $exc->getPrevious());

        $data['message'] = $message;
        $data['exception'] = array(
            'values' => array_reverse($exceptions),
        );

        if (!is_null($logger)) {
            $data['logger'] = $logger;
        }

        if (empty($data['level'])) {
            if (method_exists($exception, 'getSeverity')) {
                $data['level'] = RemoteHandler::mapSeverity($exception->getSeverity());
            } else {
                $data['level'] = RemoteHandler::ERROR;
            }
        }

        return $this->capture($data, $trace, $vars);
    }

    /**
     * Get the value of a key from $_SERVER
     *
     * @param string $key
     * @return string
     */
    protected function getServerVariable($key)
    {
        if (isset($_SERVER[$key])) {
            return $_SERVER[$key];
        }

        return '';
    }

    /**
     * @return array
     */
    protected function getCurlOptions()
    {
        $options = array(
            CURLOPT_VERBOSE => false,
        );

        if (defined('CURLOPT_TIMEOUT_MS')) {
            $timeout = max(1, ceil(1000 * $this->_timeout));

            if (!defined('CURLOPT_CONNECTTIMEOUT_MS')) {
                define('CURLOPT_CONNECTTIMEOUT_MS', 156);
            }

            $options[CURLOPT_CONNECTTIMEOUT_MS] = $timeout;
            $options[CURLOPT_TIMEOUT_MS] = $timeout;
        } else {
            $timeout = max(1, ceil($this->_timeout));
            $options[CURLOPT_CONNECTTIMEOUT] = $timeout;
            $options[CURLOPT_TIMEOUT] = $timeout;
        }

        return $options;
    }

    /**
     * @return string
     */
    protected function uuid4()
    {
        $uuid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );

        return str_replace('-', '', $uuid);
    }

    /**
     * @return array
     */
    protected function getDefaultData()
    {
        return array(
            'server_name' => $this->_name,
            'site' => $this->_site,
            'tags' => $this->_tags,
            'platform' => 'php',
        );
    }

    /**
     * @return bool
     */
    protected function isHttpRequest()
    {
        return isset($_SERVER['REQUEST_METHOD']) && PHP_SAPI !== 'cli';
    }

    /**
     * @return array
     */
    protected function getHttpData()
    {
        $env = $headers = array();

        foreach ($_SERVER as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                if (in_array($key, array('HTTP_CONTENT_TYPE', 'HTTP_CONTENT_LENGTH'))) {
                    continue;
                }
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($key, 5)))))] = $value;
            } elseif (in_array($key, array('CONTENT_TYPE', 'CONTENT_LENGTH'))) {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', $key))))] = $value;
            } else {
                $env[$key] = $value;
            }
        }

        $result = array(
            'method' => $this->getServerVariable('REQUEST_METHOD'),
            'url' => $this->getCurrentUrl(),
            'query_string' => $this->getServerVariable('QUERY_STRING'),
        );

        if (!empty($_POST)) {
            $result['data'] = $_POST;
        }
        if (!empty($_COOKIE)) {
            $result['cookies'] = $_COOKIE;
        }
        if (!empty($headers)) {
            $result['headers'] = $headers;
        }
        if (!empty($env)) {
            $result['env'] = $env;
        }

        return array(
            'request' => $result,
        );
    }

    /**
     * @return string|null
     */
    protected function getCurrentUrl()
    {
        if (!isset($_SERVER['REQUEST_URI'])) {
            return null;
        }

        $schema = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'
            || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

        $host = (!empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST']
            : (!empty($_SERVER['LOCAL_ADDR']) ? $_SERVER['LOCAL_ADDR']
                : (!empty($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '')));

        return $schema . $host . $_SERVER['REQUEST_URI'];
    }

    /**
     * @return array
     */
    protected function getUserData()
    {
        $user = $this->context->user;

        if ($user === null) {
            if (!session_id()) {
                return array();
            }
            $user = array(
                'id' => session_id(),
            );
            if (!empty($_SESSION)) {
                $user['data'] = $_SESSION;
            }
        }

        return array(
            'user' => $user,
        );
    }

    /**
     * @param $data
     */
    protected function sanitize(&$data)
    {
        $data = Serializer::serialize($data);
    }

    /**
     * Process data through defined Processor
     *
     * @param array $data
     */
    protected function process(&$data)
    {
        $this->processor->process($data);
    }

    /**
     * @param string $publicKey
     * @param string $secretKey
     * @return string
     */
    protected function getAuthHeader($publicKey, $secretKey)
    {
        return 'Basic ' . base64_encode($publicKey . ':' . $secretKey);
    }

    /**
     * Send the message over http to the url given
     *
     * @param string $url
     * @param array $data
     * @param array $headers
     */
    protected function sendHttp($url, $data, $headers = array())
    {
        if ($this->_curlMethod == 'async') {
            $this->asynchronous->enqueue($url, $data, $headers);
        } elseif ($this->_curlMethod == 'exec') {
            $this->sendHttpAsynchronouslyCurlExec($url, $data, $headers);
        } else {
            $this->sendHttpSynchronous($url, $data, $headers);
        }
    }

    /**
     * @param string $url
     * @param array $data
     * @param array $headers
     * @return bool
     */
    protected function sendHttpAsynchronouslyCurlExec($url, $data, $headers)
    {
        $cmd = $this->_curlPath . ' -X POST ';
        foreach ($headers as $key => $value) {
            $cmd .= '-H \'' . $key . ': ' . $value . '\' ';
        }

        $cmd .= '-d \'' . $data . '\' ';
        $cmd .= '\'' . $url . '\' ';
        $cmd .= '-m 5 ';  // 5 second timeout for the whole process (connect + send)
        $cmd .= '> /dev/null 2>&1 &'; // ensure exec returns immediately while curl runs in the background

        exec($cmd);

        return true;
    }

    /**
     * @param string $url
     * @param array $data
     * @param array $headers
     * @return bool
     */
    protected function sendHttpSynchronous($url, $data, $headers)
    {
        $newHeaders = array();
        foreach ($headers as $key => $value) {
            array_push($newHeaders, $key . ': ' . $value);
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $newHeaders);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $options = $this->getCurlOptions();
        curl_setopt_array($ch, $options);

        curl_exec($ch);

        $errNo = curl_errno($ch);
        if ($errNo == CURLE_SSL_CACERT) {
            curl_exec($ch);
        }

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return $code == 200;
    }

    /**
     * @param string $var
     * @param mixed $default
     * @return mixed
     */
    protected function getOption($var, $default = null)
    {
        if (isset($this->_options[$var])) {
            return $this->_options[$var];
        }

        return $default;
    }

}

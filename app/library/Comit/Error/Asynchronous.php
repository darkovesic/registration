<?php

namespace Comit\Error;

/**
 * Class Asynchronous
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
class Asynchronous
{

    private $_joinTimeout;
    private $_multiHandle;
    private $_options;
    private $_requests;

    /**
     * @param $options
     * @param int $joinTimeout
     */
    public function __construct($options, $joinTimeout = 5)
    {
        $this->_options = $options;
        $this->_multiHandle = curl_multi_init();
        $this->_requests = array();
        $this->_joinTimeout = $joinTimeout;

        register_shutdown_function(array($this, 'join'));
    }

    /**
     *
     */
    public function __destruct()
    {
        $this->join();
    }

    /**
     * @param $url
     * @param string|array $data
     * @param array $headers
     * @return int
     */
    public function enqueue($url, $data = null, $headers = array())
    {
        $ch = curl_init();

        $newHeaders = array();
        foreach ($headers as $key => $value) {
            array_push($newHeaders, $key . ': ' . $value);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $newHeaders);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt_array($ch, $this->_options);

        if (!is_null($data)) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        curl_multi_add_handle($this->_multiHandle, $ch);

        $fd = (int)$ch;
        $this->_requests[$fd] = 1;

        $this->select();

        return $fd;
    }

    /**
     * @param int $timeout
     */
    public function join($timeout = null)
    {
        if (!isset($timeout)) {
            $timeout = $this->_joinTimeout;
        }

        $start = time();
        do {
            $this->select();
            if (count($this->_requests) === 0) {
                break;
            }
            usleep(10000);
        } while ($timeout !== 0 && time() - $start > $timeout);
    }

    /**
     *
     */
    protected function select()
    {
        do {
            $mrc = curl_multi_exec($this->_multiHandle, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($this->_multiHandle) !== -1) {
                do {
                    $mrc = curl_multi_exec($this->_multiHandle, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            } else {
                return;
            }
        }

        while ($info = curl_multi_info_read($this->_multiHandle)) {
            $ch = $info['handle'];
            $fd = (int)$ch;

            curl_multi_remove_handle($this->_multiHandle, $ch);

            if (!isset($this->_requests[$fd])) {
                return;
            }

            unset($this->_requests[$fd]);
        }
    }

}

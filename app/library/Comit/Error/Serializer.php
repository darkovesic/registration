<?php

namespace Comit\Error;

/**
 * Class Serializer
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
class Serializer
{

    /**
     * @param object|resource|array $value
     * @param int $maxDepth
     * @param int $depth
     * @return array|int|string
     */
    public static function serialize($value, $maxDepth = 9, $depth = 0)
    {
        if (is_object($value) || is_resource($value)) {
            return self::serializeValue($value);
        } elseif ($depth < $maxDepth && is_array($value)) {
            $new = array();
            foreach ($value as $k => $v) {
                $new[self::serializeValue($k)] = self::serialize($v, $maxDepth, $depth + 1);
            }

            return $new;
        } else {
            return self::serializeValue($value);
        }
    }

    /**
     * @param $value
     * @return int|string
     */
    public static function serializeValue($value)
    {
        if ($value === null) {
            return 'null';
        } elseif ($value === false) {
            return 'false';
        } elseif ($value === true) {
            return 'true';
        } elseif (is_float($value) && (int)$value == $value) {
            return $value . '.0';
        } elseif (is_object($value) || gettype($value) == 'object') {
            return 'Object ' . get_class($value);
        } elseif (is_resource($value)) {
            return 'Resource ' . get_resource_type($value);
        } elseif (is_array($value)) {
            return 'Array of length ' . count($value);
        } elseif (is_integer($value)) {
            return (integer)$value;
        } else {
            $value = (string)$value;

            if (function_exists('mb_convert_encoding')) {
                $value = mb_convert_encoding($value, 'UTF-8', 'auto');
            }

            return $value;
        }
    }

}

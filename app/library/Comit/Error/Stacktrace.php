<?php

namespace Comit\Error;

/**
 * Class Stacktrace
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
class Stacktrace
{

    public static $statements = array(
        'include',
        'include_once',
        'require',
        'require_once',
    );

    /**
     * @param array $frames
     * @param bool|false $trace
     * @param bool|true $shiftVars
     * @param null $errContext
     * @param int $frameVarLimit
     * @return array
     */
    public static function getStackInfo($frames, $trace = false, $shiftVars = true, $errContext = null, $frameVarLimit = Client::MESSAGE_LIMIT)
    {
        $result = array();

        for ($i = 0; $i < count($frames); $i++) {

            $frame = $frames[$i];
            $nextFrame = isset($frames[$i + 1]) ? $frames[$i + 1] : null;

            if (!array_key_exists('file', $frame)) {
                // Disable capturing of anonymous functions
                continue;
            } else {
                $context = self::readSourceFile($frame['file'], $frame['line']);
                $absPath = $frame['file'];
                $fileName = basename($frame['file']);
            }

            $module = $fileName;
            if (isset($nextFrame['class'])) {
                $module .= ':' . $nextFrame['class'];
            }

            if (empty($result) && isset($errContext)) {
                // If we've been given an error context that can be used as the vars for the first frame.
                $vars = $errContext;
            } else {
                if ($trace) {
                    if ($shiftVars) {
                        $vars = self::getFrameContext($nextFrame, $frameVarLimit);
                    } else {
                        $vars = self::getCallerFrameContext($frame);
                    }
                } else {
                    $vars = array();
                }
            }

            $data = array(
                'abs_path' => $absPath,
                'file_name' => $context['file_name'],
                'line_number' => (int)$context['line_number'],
                'module' => $module,
                'function' => $nextFrame['function'],
                'pre_context' => $context['prefix'],
                'context_line' => $context['line'],
                'post_context' => $context['suffix'],
            );

            if (!empty($vars)) {
                $cleanVars = array();
                foreach ($vars as $key => $value) {
                    if (is_string($value) || is_numeric($value)) {
                        $cleanVars[$key] = substr($value, 0, $frameVarLimit);
                    } else {
                        $cleanVars[$key] = $value;
                    }
                }
                $data['vars'] = $cleanVars;
            }

            $result[] = $data;
        }

        return array_reverse($result);
    }

    /**
     * @param array $frame
     * @return array
     */
    public static function getCallerFrameContext($frame)
    {
        if (!isset($frame['args'])) {
            return array();
        }

        $i = 1;
        $args = array();
        foreach ($frame['args'] as $arg) {
            $args['param' . $i] = $arg;
            $i++;
        }

        return $args;
    }

    /**
     * @param array $frame
     * @param int $frameArgLimit
     * @return array
     */
    public static function getFrameContext($frame, $frameArgLimit = Client::MESSAGE_LIMIT)
    {
        if (!isset($frame['function'])) {
            return array();
        }

        if (!isset($frame['args'])) {
            return array();
        }

        if (strpos($frame['function'], '__lambda_func') !== false) {
            return array();
        }

        if (isset($frame['class']) && $frame['class'] == 'Closure') {
            return array();
        }

        if (strpos($frame['function'], '{closure}') !== false) {
            return array();
        }

        if (in_array($frame['function'], self::$statements)) {
            if (empty($frame['args'])) {
                return array();
            } else {
                return array($frame['args'][0]);
            }
        }

        try {
            if (isset($frame['class'])) {
                if (method_exists($frame['class'], $frame['function'])) {
                    $reflection = new \ReflectionMethod($frame['class'], $frame['function']);
                } elseif ($frame['type'] === '::') {
                    $reflection = new \ReflectionMethod($frame['class'], '__callStatic');
                } else {
                    $reflection = new \ReflectionMethod($frame['class'], '__call');
                }
            } else {
                $reflection = new \ReflectionFunction($frame['function']);
            }
        } catch (\ReflectionException $e) {
            return array();
        }

        $params = $reflection->getParameters();
        $args = array();
        foreach ($frame['args'] as $i => $arg) {
            if (isset($params[$i])) {
                if (is_array($arg)) {
                    foreach ($arg as $key => $value) {
                        if (is_string($value) || is_numeric($value)) {
                            $arg[$key] = substr($value, 0, $frameArgLimit);
                        }
                    }
                }
                $args[$params[$i]->name] = $arg;
            }
        }

        return $args;
    }

    /**
     * @param string $fileName
     * @param int $lineNumber
     * @param int $contextLines
     * @return array
     */
    private static function readSourceFile($fileName, $lineNumber, $contextLines = 5)
    {
        $frame = array(
            'prefix' => array(),
            'line' => '',
            'suffix' => array(),
            'file_name' => $fileName,
            'line_number' => $lineNumber,
        );

        if ($fileName === null || $lineNumber === null) {
            return $frame;
        }

        $matches = array();
        $matched = preg_match("/^(.*?)\((\d+)\) : eval\(\)'d code$/", $fileName, $matches);
        if ($matched) {
            $frame['file_name'] = $fileName = $matches[1];
            $frame['line_number'] = $lineNumber = $matches[2];
        }

        $matches = array();
        $matched = preg_match("/^(.*?)\((\d+)\) : runtime-created function$/", $fileName, $matches);
        if ($matched) {
            $frame['file_name'] = $fileName = $matches[1];
            $frame['line_number'] = $lineNumber = $matches[2];
        }

        try {
            $file = new \SplFileObject($fileName);
            $target = max(0, ($lineNumber - ($contextLines + 1)));
            $file->seek($target);
            $currentLineNumber = $target + 1;
            while (!$file->eof()) {
                $line = rtrim($file->current(), "\r\n");
                if ($currentLineNumber == $lineNumber) {
                    $frame['line'] = $line;
                } elseif ($currentLineNumber < $lineNumber) {
                    $frame['prefix'][] = $line;
                } elseif ($currentLineNumber > $lineNumber) {
                    $frame['suffix'][] = $line;
                }
                $currentLineNumber++;
                if ($currentLineNumber > $lineNumber + $contextLines) {
                    break;
                }
                $file->next();
            }
        } catch (\RuntimeException $exc) {
            return $frame;
        }

        return $frame;
    }

}

<?php

namespace Comit\Error;

use Phalcon\Logger\Adapter\File as Logger;

/**
 * Class Handler
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
class LocalHandler extends AbstractHandler implements HandlerInterface
{

    /** @var Logger */
    private $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function registerExceptionHandler()
    {
        set_exception_handler(function (\Exception $e) {
            $this->logException($e);
        });
    }

    /**
     * @inheritDoc
     */
    public function registerErrorHandler()
    {
        set_error_handler(array($this, 'handleError'), error_reporting());
    }

    /**
     * @inheritDoc
     */
    public function registerShutdownFunction()
    {
        register_shutdown_function(array($this, 'handleFatalError'));
    }

    /**
     * @inheritDoc
     */
    public function handleException(\Exception $e, $isError = false, $vars = null)
    {
        $this->logException($e, $isError, $vars);
    }

    /**
     * @param \Exception $exception
     * @param bool $isError
     * @param array $vars
     */
    protected function logException(\Exception $exception, $isError = false, $vars = null)
    {
        if (method_exists($exception, 'getSeverity')) {
            $type = self::mapSeverity($exception->getSeverity());
        } else {
            $type = self::ERROR;
        }

        $message = "$type: {$exception->getMessage()} in {$exception->getFile()} on line {$exception->getLine()}";

        $this->logger->log($type, $message);
    }

}

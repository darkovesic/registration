<?php

namespace Comit\Error;

/**
 * Interface HandlerInterface
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
interface HandlerInterface
{

    /**
     *
     */
    public function registerExceptionHandler();


    /**
     *
     */
    public function registerErrorHandler();


    /**
     *
     */
    public function registerShutdownFunction();


    /**
     * @param \Exception $e
     * @param bool $isError
     * @param null $vars
     */
    public function handleException(\Exception $e, $isError = false, $vars = null);


    /**
     * @param string $code
     * @param string $message
     * @param string $file
     * @param int $line
     * @param array $context
     */
    public function handleError($code, $message, $file = '', $line = 0, $context = array());


    /**
     *
     */
    public function handleFatalError();

}

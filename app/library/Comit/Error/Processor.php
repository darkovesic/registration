<?php

namespace Comit\Error;

/**
 * Class Processor
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
class Processor
{

    const MASK = '********';
    const FIELDS_REPLACEMENTS = '/(authorization|password|passwd|secret|password_confirmation|card_number|auth_pw)/i';
    const VALUES_REPLACEMENTS = '/^(?:\d[ -]*?){13,16}$/';

    /** @var Client */
    private $client;

    private $_fieldsReplacements;
    private $_valuesReplacements;
    private $_sessionCookieName;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->_fieldsReplacements = self::FIELDS_REPLACEMENTS;
        $this->_valuesReplacements = self::VALUES_REPLACEMENTS;
        $this->_sessionCookieName = ini_get('session.name');
    }

    /**
     * Replace any array values with our mask if the field name or the value matches a respective regex
     *
     * @param mixed $item Associative array value
     * @param string $key Associative array key
     */
    public function sanitize(&$item, $key)
    {
        if (empty($item)) {
            return;
        }

        if (preg_match($this->_valuesReplacements, $item)) {
            $item = self::MASK;
        }

        if (empty($key)) {
            return;
        }

        if (preg_match($this->_fieldsReplacements, $key)) {
            $item = self::MASK;
        }
    }

    /**
     * @param array $data
     */
    public function sanitizeHttp(&$data)
    {
        if (empty($data['request'])) {
            return;
        }

        $http = &$data['request'];
        if (empty($http['cookies'])) {
            return;
        }

        $cookies = &$http['cookies'];
        if (!empty($cookies[$this->_sessionCookieName])) {
            $cookies[$this->_sessionCookieName] = self::MASK;
        }
    }

    /**
     * @param array $data
     */
    public function process(&$data)
    {
        array_walk_recursive($data, array($this, 'sanitize'));
        $this->sanitizeHttp($data);
    }

    /**
     * @return string
     */
    public function getFieldsReplacements()
    {
        return $this->_fieldsReplacements;
    }

    /**
     * @param string $fieldsReplacements
     */
    public function setFieldsReplacements($fieldsReplacements)
    {
        $this->_fieldsReplacements = $fieldsReplacements;
    }

    /**
     * @return string
     */
    public function getValuesReplacements()
    {
        return $this->_valuesReplacements;
    }

    /**
     * @param string $valuesReplacements
     */
    public function setValuesReplacements($valuesReplacements)
    {
        $this->_valuesReplacements = $valuesReplacements;
    }

}

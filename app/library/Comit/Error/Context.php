<?php

namespace Comit\Error;

/**
 * Class Context
 *
 * @package Comit\Error
 * @author Igor Vuckovic
 */
class Context
{

    public $tags;
    public $extra;
    public $user;

    /**
     *
     */
    public function __construct()
    {
        $this->clear();
    }

    /**
     * Clean up existing context.
     */
    public function clear()
    {
        $this->tags = array();
        $this->extra = array();
        $this->user = null;
    }

}

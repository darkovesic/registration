<?php

namespace Comit\Form;

use Comit\Messenger\Manager as MessengerManager;
use Comit\Messenger\Message as MessengerMessage;
use Phalcon\Http\Request;
use Phalcon\Mvc\Dispatcher;

/**
 * Class Form
 *
 * @package Comit\Form
 * @author Igor Vuckovic
 */
class Form extends \Phalcon\Forms\Form
{

    /**
     * Validates the form
     *
     * @param array $data
     * @param object $entity
     * @return boolean
     */
    public function isValid($data = null, $entity = null)
    {
        $isValid = parent::isValid($data, $entity);

        if (!$isValid) {

            /** @var MessengerManager $messengerManager */
            $messengerManager = $this->getDI()->getShared('messengerManager');
            foreach ($this->getMessages() as $message) {

                $messengerManager->appendMessage(new MessengerMessage(
                    $message->getMessage(),
                    $message->getField(),
                    MessengerMessage::WARNING
                ));
            }
        }

        return $isValid;
    }

}

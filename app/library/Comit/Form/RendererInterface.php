<?php

namespace Comit\Form;

/**
 * Interface RendererInterface
 *
 * @package Form
 * @author Igor Vuckovic
 */
interface RendererInterface
{

    /**
     * @param Form $form
     * @return mixed
     */
    public function render($form);

}

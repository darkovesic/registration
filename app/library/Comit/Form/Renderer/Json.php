<?php

namespace Comit\Form\Renderer;

use Comit\Form\Form;
use Comit\Form\RendererInterface;
use Phalcon\Forms\Element;

/**
 * Class Json
 *
 * @package Comit\Form\Renderer
 * @author Igor Vuckovic
 */
class Json implements RendererInterface
{

    /**
     * @param Form $form
     * @return string
     */
    public function render($form)
    {
        $formArray = array();

        foreach ($form->getElements() as $key => $formElement) {

            /** @var Element $formElement */

            // Get element properties
            $defaultValue = $formElement->getDefault();
            $formArray[$key]['name'] = $formElement->getName() ? $formElement->getName() : '';
            $formArray[$key]['label'] = $formElement->getLabel() ? $formElement->getLabel() : '';
            $formArray[$key]['default'] = !is_null($defaultValue) ? $defaultValue : '';

            // Get element type based on its CLASS
            $formArray[$key]['type'] = '';
            $classString = get_class($formElement);
            if (!empty($classString)) {
                $classNameArray = explode('\\', $classString);
                if (!empty($classNameArray)) {
                    $className = end($classNameArray);
                    $formArray[$key]['type'] = strtolower($className);

                    //Populate choices’ options
                    if ($className == 'Select') {
                        $elementOptions = $formElement->getOptions();
                        if (is_array($elementOptions)) {
                            $formArray[$key]['options'] = $elementOptions;
                        }
                    }
                }
            }

            $formArray[$key]['attributes'] = $formElement->getAttributes();

            if (array_key_exists('name', $formArray[$key]['attributes'])) {
                $formArray[$key]['name'] = $formArray[$key]['attributes']['name'];
            }

            // Check if element is required
            $formArray[$key]['required'] = false;
            if ($elementClass = $formElement->getAttribute('class')) {
                if (preg_match('/required/', $elementClass)) {
                    $formArray[$key]['required'] = true;
                }
            }

            // Get element messages
            $formArray[$key]['messages'] = array();
            if (count($formElement->getMessages())) {
                foreach ($formElement->getMessages() as $message) {
                    $formArray[$key]['messages'] [] = $message;
                }
            }
        }

        return json_encode($formArray);
    }

}
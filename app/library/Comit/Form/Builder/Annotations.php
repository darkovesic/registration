<?php

namespace Comit\Form\Builder;

use Comit\Form\BuilderInterface;
use Comit\Form\Form;
use Comit\Filter\Humanize;
use Comit\Helper\DateTime;
use Comit\Helper\Translator;
use Comit\Model\Base as Model;
use Comit\Model\Base;
use Phalcon\Annotations\Adapter\Memory as Reader;
use Phalcon\Di\Injectable;
use Phalcon\Forms\Element;

/**
 * Class Annotations
 *
 * @package Comit\Form\Builder
 * @author Igor Vuckovic
 */
class Annotations extends Injectable implements BuilderInterface
{

    /**
     * @param Model $model
     * @param array $exclude
     * @return Form
     */
    public function build($model, $exclude = array())
    {
        $reader = new Reader();
        $form = new Form($model);
        $humanize = new Humanize();

        $locale = 'en_US';
        if ($session = $this->getDI()->getShared('session')) {
            if ($currentUser = $session->get('currentUser')) {
                $locale = $currentUser['locale'];
            }
        }

        $reflection = $reader->get(get_class($model));

        $properties = $reflection->getPropertiesAnnotations();
        foreach ($properties as $propertyName => $annotations) {

            if (!in_array($propertyName, $exclude)) {

                if ($annotations->has('Column') && $annotations->has('FormElement')) {

                    $columnAnnotation = $annotations->get('Column');
                    $columnArguments = $columnAnnotation->getArguments();

                    $formElementAnnotation = $annotations->get('FormElement');
                    $formElementArguments = $formElementAnnotation->getArguments();

                    if (isset($formElementArguments['type'])) {

                        if (!strstr($formElementArguments['type'], '\\')) {
                            $namespace = '\\Phalcon\\Forms\\Element\\';
                        } else {
                            $namespace = '';
                        }

                        $className = $namespace . $formElementArguments['type'];

                        if ($formElementArguments['type'] == 'Select') {
                            $formElementNull = array();
                            $formElementOptions = array();

                            if (isset($formElementArguments['relation'])) {

                                /** @var Model $elementRelationClass */
                                $elementRelationClass = $formElementArguments['relation'];
                                if (class_exists($elementRelationClass)) {

                                    foreach ($elementRelationClass::find() as $elementObj) {
                                        /** @var Base $elementObj */
                                        $key = $elementObj->getObjectId();
                                        $val = $elementObj->getDisplayName();
                                        if ($key && $val) {
                                            $formElementOptions[$key] = $val;
                                        }
                                    }
                                }

                            } else if (isset($formElementArguments['options']) && is_array($formElementArguments['options'])) {

                                if (array_values($formElementArguments['options']) === $formElementArguments['options']) {
                                    $formElementOptions = array_combine($formElementArguments['options'], $formElementArguments['options']);
                                } else {
                                    $formElementOptions = $formElementArguments['options'];
                                }

                                array_walk($formElementOptions, function (&$value) use ($locale) {
                                    $value = Translator::translate($value, null, $locale);
                                    return $value;
                                });
                            }

                            $formElementOptions = $formElementNull + $formElementOptions;
                            $element = new $className($propertyName, $formElementOptions);
                        } else {
                            $element = new $className($propertyName);
                        }

                        /**@var Element $element */
                        $element->clear();

                        // Set element's label
                        if (isset($formElementArguments['label'])) {
                            $element->setLabel(
                                Translator::translate($formElementArguments['label'], null, $locale)
                            );
                            $label = $formElementArguments['label'];
                        } else {
                            $label = $propertyName;
                        }

                        // Set element's default value
                        if ($defaultValue = $model->getProperty($propertyName)) {
                            if ($defaultValue instanceof DateTime) {
                                $element->setDefault($defaultValue->getLong());
                            } else {
                                $element->setDefault($defaultValue);
                            }
                        }

                        // Set some class for date element
                        if ($formElementArguments['type'] === 'Date') {
                            if ($columnArguments['type'] === 'dateTime') {
                                $element->setAttribute('class', 'datepicker timepicker');
                            } else {
                                $element->setAttribute('class', 'datepicker');
                            }
                        }

                        // Create default validators based on FormElement properties
                        if (isset($formElementArguments['required']) && $formElementArguments['required']) {
                            $element->addValidator(new \Phalcon\Validation\Validator\PresenceOf(array(
                                'message' => Translator::translate(
                                    'The %s is required.',
                                    Translator::translate($humanize->filter($label), null, $locale),
                                    $locale
                                )
                            )));

                            if ($attributeClass = $element->getAttribute('class')) {
                                $element->setAttribute('class', "$attributeClass required");
                            } else {
                                $element->setAttribute('class', 'required');
                            }
                        }

                        if ('Email' === $formElementArguments['type']) {
                            $element->addValidator(new \Phalcon\Validation\Validator\Email(array(
                                'message' => Translator::translate('Please provide valid Email address.', null, $locale)
                            )));
                        }

                        // Create custom validators based on FormElementValidator properties
                        if ($annotations->has('FormElementValidator')) {
                            $formElementValidatorAnnotation = $annotations->get('FormElementValidator');
                            $formElementValidatorArguments = $formElementValidatorAnnotation->getArguments();
                            if (count($formElementValidatorArguments)) {
                                if (isset($formElementValidatorArguments['class'])) {

                                    if (!strstr($formElementValidatorArguments['class'], "\\")) {
                                        $namespace = "\\Phalcon\\Validation\\Validator\\";
                                    } else {
                                        $namespace = "";
                                    }
                                    if (isset($formElementValidatorArguments['arguments'])) {
                                        $classArguments = $formElementValidatorArguments['arguments'];
                                    } else {
                                        $classArguments = array();
                                    }
                                    $className = $namespace . $formElementValidatorArguments['class'];
                                    $element->addValidator(new $className($classArguments));
                                }
                            }
                        }

                        // Create custom FormElementAttributes
                        if ($annotations->has('FormElementAttributes')) {
                            $formElementAttributesAnnotation = $annotations->get('FormElementAttributes');
                            $formElementAttributesArguments = $formElementAttributesAnnotation->getArguments();
                            if (count($formElementAttributesArguments)) {
                                $element->setAttributes($formElementAttributesArguments);
                            }
                        }

                        $form->add($element);

                    }
                }

            }
        }

        return $form;
    }

}

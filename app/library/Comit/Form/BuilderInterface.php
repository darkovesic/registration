<?php

namespace Comit\Form;

use Comit\Model\Base as Model;

/**
 * Interface BuilderInterface
 *
 * @package Comit\Form
 */
interface BuilderInterface
{

    /**
     * @param Model $model
     * @param array $exclude
     * @return Form
     */
    public function build($model, $exclude = array());

}

<?php

namespace Comit\Database;

use Comit\Filter\Singular;

/**
 * Class Table
 *
 * @package Database
 * @author Igor Vuckovic
 */
class Table
{
    /** @var Adapter */
    private $adapter;

    /** @var array|Field[] */
    private $fields = array();

    /** @var array|Field[] */
    private $primaryFields = array();

    /** @var array|Field[] */
    private $foreignFields = array();

    /** @var string */
    private $name;

    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return Adapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @param Field $field
     * @return $this
     */
    public function addField(Field $field)
    {
        $this->fields[] = $field;

        if ($field->isPrimaryKey()) {
            $this->primaryFields[] = $field;
        }

        if ($field->isReferenceKey()) {
            $this->foreignFields[] = $field;
        }

        return $this;
    }

    /**
     * @return array|Field[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return array|Field[]
     */
    public function getPrimaryFields()
    {
        return $this->primaryFields;
    }

    /**
     * @return array|Field[]
     */
    public function getForeignFields()
    {
        return $this->foreignFields;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param bool $underscored
     * @return string
     */
    public function getEntityName($underscored = false)
    {
        $singular = new Singular();
        $nameArray = explode('_', strtolower($this->name));
        foreach ($nameArray as &$value) {
            $value = ucfirst($singular->filter($value));
        }

        return implode(($underscored ? '_' : ''), $nameArray);
    }

    /**
     * @param bool $showSingular
     * @return string
     */
    public function getHumanTableName($showSingular = true)
    {
        if ($showSingular) {
            $singular = new Singular();
            $nameArray = explode('_', strtolower($this->name));
            foreach ($nameArray as &$value) {
                $value = ucfirst($singular->filter($value));
            }
            return implode(' ', $nameArray);
        }

        return ucwords(str_replace('_', ' ', $this->name));
    }

}

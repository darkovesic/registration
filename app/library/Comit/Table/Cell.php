<?php

namespace Comit\Table;

/**
 * Class Cell
 *
 * @package Table
 * @author Igor Vuckovic
 */
class Cell
{
    /** @var string */
    private $backgroundColor;

    /** @var string */
    private $color;

    /** @var string */
    private $tooltipHtml;

    /** @var mixed */
    private $value;

    /**
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    /**
     * @param string $backgroundColor
     * @return $this
     */
    public function setBackgroundColor($backgroundColor)
    {
        $this->backgroundColor = $backgroundColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;
        $this->backgroundColor = $color;
        return $this;
    }

    /**
     * @return string
     */
    public function getTooltipHtml()
    {
        return $this->tooltipHtml;
    }

    /**
     * @param string $tooltipHtml
     * @return $this
     */
    public function setTooltipHtml($tooltipHtml)
    {
        if (strpos($tooltipHtml, 'simple-tooltip')) {
            $this->tooltipHtml = $tooltipHtml;
        } else {
            $this->tooltipHtml = '<div class="simple-tooltip">' . $tooltipHtml . '</div>';
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

}

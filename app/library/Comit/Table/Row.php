<?php

namespace Comit\Table;

/**
 * Class Row
 *
 * @package Table
 * @author Igor Vuckovic
 */
class Row
{

    /** @var  array|Cell[] */
    private $cells = array();

    /**
     * @param $fieldName
     * @param $value
     * @return Cell
     */
    public function addCell($fieldName, $value)
    {
        $cell = new Cell();
        $cell->setValue($value);
        $this->cells[$fieldName] = $cell;
        return $cell;
    }

    /**
     * @param string $fieldName
     * @return Cell|null
     */
    public function getCell($fieldName)
    {
        if (array_key_exists($fieldName, $this->cells)) {
            return $this->cells[$fieldName];
        }

        return null;
    }

    /**
     * @return array|Cell[]
     */
    public function getCells()
    {
        return $this->cells;
    }

}

<?php

namespace Comit\Table;

/**
 * Class Table
 *
 * @package Table
 * @author Igor Vuckovic
 */
class Table
{

    /** @var Definition|null */
    private $definition;

    /** @var array|Row[] */
    private $rows = array();

    /** @var int */
    private $totalRows;

    /** @var array */
    private $visibleColumns = array();

    /** @var  string */
    private $title;

    /**
     * @param Definition $definition
     */
    public function __construct(Definition $definition)
    {
        $this->setDefinition($definition);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param Row|null $row
     * @return Row
     */
    public function addRow($row = null)
    {
        if (is_null($row)) {
            $row = new Row();
        }
        $this->rows[] = $row;

        return $row;
    }

    /**
     * @param Row $removeRow
     */
    public function removeRow(Row $removeRow)
    {
        foreach ($this->rows as $index => $row) {
            if ($row == $removeRow) {
                unset($this->rows[$index]);
            }
        }
    }

    /**
     * @return array|Row[]
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @return $this
     */
    public function showAllColumns()
    {
        foreach ($this->visibleColumns as $columnName => $value) {
            $this->showColumn($columnName);
        }

        return $this;
    }

    /**
     * @param $columnName
     * @return $this
     */
    public function showColumn($columnName)
    {
        $this->visibleColumns[$columnName] = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function hideAllColumns()
    {
        foreach ($this->visibleColumns as $columnName => $value) {
            $this->hideColumn($columnName);
        }

        return $this;
    }

    /**
     * @param $columnName
     * @return $this
     */
    public function hideColumn($columnName)
    {
        if (array_key_exists($columnName, $this->visibleColumns)) {
            $this->visibleColumns[$columnName] = false;
        }

        return $this;
    }

    /**
     * @return array|Column[]
     */
    public function getVisibleColumns()
    {
        $visibleColumns = array();

        foreach ($this->getDefinition()->getColumns() as $columnName => $column) {
            if ($this->isColumnVisible($columnName)) {
                $visibleColumns[$columnName] = $column;
            }
        }

        return $visibleColumns;
    }

    /**
     * @return Definition
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * @param Definition $definition
     * @return $this
     */
    public function setDefinition(Definition $definition)
    {
        $this->definition = $definition;

        foreach ($definition->getColumns() as $columnName => $column) {
            if ($column->isDefaultVisible()) {
                $this->showColumn($columnName);
            }
        }

        return $this;
    }

    /**
     * @param string $columnName
     * @return bool
     */
    public function isColumnVisible($columnName)
    {
        return isset($this->visibleColumns[$columnName]) && $this->visibleColumns[$columnName];
    }

    /**
     * @return int
     */
    public function getTotalRows()
    {
        if (is_int($this->totalRows)) {
            return $this->totalRows;
        } else {
            return sizeof($this->rows);
        }
    }

    /**
     * @param $intValue
     * @return $this
     */
    public function setTotalRows($intValue = null)
    {
        if (is_null($intValue)) {
            $intValue = sizeof($this->rows);
        }
        $this->totalRows = $intValue;

        return $this;
    }

}

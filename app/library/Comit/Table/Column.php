<?php

namespace Comit\Table;

/**
 * Class Column
 *
 * @package Table
 * @author Igor Vuckovic
 */
class Column
{

    const ALIGN_LEFT = 'left';
    const ALIGN_CENTER = 'center';
    const ALIGN_RIGHT = 'right';

    const DATA_TYPE_BOOLEAN = 'boolean';
    const DATA_TYPE_DATE = 'date';
    const DATA_TYPE_STRING = 'string';
    const DATA_TYPE_FLOAT = 'float';
    const DATA_TYPE_INT = 'int';

    /** @var string */
    private $columnLabel;

    /** @var string */
    private $tooltipHtml;

    /** @var int */
    private $flex;

    /** @var string */
    private $textAlign;

    /** @var bool */
    private $defaultVisible = true;

    /** @var Definition */
    private $dataTableDefinition;

    /** @var string */
    private $groupedColumnText;

    /** @var bool */
    private $primaryKey = false;

    /** @var string */
    private $dataType = self::DATA_TYPE_STRING;

    /**
     * @return string
     */
    public function getColumnLabel()
    {
        return $this->columnLabel;
    }

    /**
     * @param string $columnLabel
     * @return $this
     */
    public function setColumnLabel($columnLabel)
    {
        $this->columnLabel = $columnLabel;
        return $this;
    }

    /**
     * @return string
     */
    public function getTooltipHtml()
    {
        return $this->tooltipHtml;
    }

    /**
     * @param string $tooltipHtml
     * @return $this
     */
    public function setTooltipHtml($tooltipHtml)
    {
        if (strpos($tooltipHtml, 'simple-tooltip')) {
            $this->tooltipHtml = $tooltipHtml;
        } else {
            $this->tooltipHtml = '<div class="simple-tooltip">' . $tooltipHtml . '</div>';
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getFlex()
    {
        return $this->flex;
    }

    /**
     * @param int $flex
     * @return $this
     */
    public function setFlex($flex)
    {
        $this->flex = $flex;
        return $this;
    }

    /**
     * @return string
     */
    public function getTextAlign()
    {
        return $this->textAlign;
    }

    /**
     * @param string $textAlign
     * @return $this
     */
    public function setTextAlign($textAlign)
    {
        $this->textAlign = $textAlign;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDefaultVisible()
    {
        return $this->defaultVisible;
    }

    /**
     * @param bool $boolValue
     * @return $this
     */
    public function setDefaultVisible($boolValue)
    {
        $this->defaultVisible = $boolValue;
        return $this;
    }

    /**
     * @return Definition
     */
    public function getDataTableDefinition()
    {
        return $this->dataTableDefinition;
    }

    /**
     * @param Definition $dataTableDefinition
     */
    public function setDataTableDefinition(Definition $dataTableDefinition)
    {
        $this->dataTableDefinition = $dataTableDefinition;
    }

    /**
     * @return string
     */
    public function getGroupedColumnText()
    {
        return $this->groupedColumnText;
    }

    /**
     * @param string $groupedColumnText
     * @return $this
     */
    public function setGroupedColumnText($groupedColumnText)
    {
        $this->groupedColumnText = $groupedColumnText;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * @param mixed $dataType
     * @return $this
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * @param bool $primaryKey
     * @return $this
     */
    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = (bool)$primaryKey;
        return $this;
    }

}

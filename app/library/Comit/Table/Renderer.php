<?php

namespace Comit\Table;

/**
 * Class Renderer
 *
 * @package Table
 * @subpackage Table\Renderer
 * @author Igor Vuckovic
 */
abstract class Renderer
{
    /** @var Table */
    protected $_table;

    /**
     * @param Table $table
     */
    public function __construct(Table $table)
    {
        $this->_table = $table;
    }

    /**
     * Sets the table
     *
     * @param Table $table
     * @return RendererInterface
     */
    public function setTable(Table $table)
    {
        $this->_table = $table;
        return $this;
    }

    /**
     * Returns the table
     *
     * @return Table
     */
    public function getTable()
    {
        return $this->_table;
    }

}

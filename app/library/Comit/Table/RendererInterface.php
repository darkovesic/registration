<?php

namespace Comit\Table;

/**
 * Interface RendererInterface
 *
 * @package Table
 * @subpackage Table\Renderer
 * @author Igor Vuckovic
 */
interface RendererInterface
{

    /**
     * Sets the table
     *
     * @param Table $table
     * @return RendererInterface
     */
    public function setTable(Table $table);

    /**
     * Returns the table
     *
     * @return Table
     */
    public function getTable();

    /**
     * @return mixed
     */
    public function render();

}

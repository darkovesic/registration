<?php

namespace Comit\Table;

/**
 * Class Definition
 *
 * @package Table
 * @author Igor Vuckovic
 */
class Definition
{

    /** @var array|Column[] */
    private $columns = array();

    /**
     * @return Column|null
     */
    public function getPrimaryKeyColumn()
    {
        foreach ($this->getColumns() as $column) {
            if ($column->isPrimaryKey()) {
                return $column;
            }
        }

        return null;
    }

    /**
     * @return array|Column[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param string $columnName
     * @return Column
     */
    public function addColumn($columnName)
    {
        $column = new Column();
        $column->setDataTableDefinition($this);
        $this->columns[$columnName] = $column;

        return $column;
    }

    /**
     * @param string $columnName
     * @param string $insertAfterColumnName
     * @return Column
     */
    public function addColumnAfter($columnName, $insertAfterColumnName)
    {
        $column = new Column();
        $column->setDataTableDefinition($this);
        $position = array_search($insertAfterColumnName, array_keys($this->columns)) + 1;

        $this->columns = array_merge(
            array_slice($this->columns, 0, $position),
            [$columnName => $column],
            array_slice($this->columns, $position)
        );

        return $column;
    }

    /**
     * @param string $columnName
     * @param string $insertBeforeColumnName
     * @return Column
     */
    public function addColumnBefore($columnName, $insertBeforeColumnName)
    {
        $column = new Column();
        $column->setDataTableDefinition($this);
        $position = array_search($insertBeforeColumnName, array_keys($this->columns));

        $this->columns = array_merge(
            array_slice($this->columns, 0, $position),
            [$columnName => $column],
            array_slice($this->columns, $position)
        );

        return $column;
    }

    /**
     * @param string $columnName
     * @return Column
     * @throws \Exception
     */
    public function getColumn($columnName)
    {
        if (!$this->hasColumn($columnName)) {
            throw new \Exception("Column name '$columnName' not specified in table definition " . get_called_class());
        }

        return $this->columns[$columnName];
    }

    /**
     * @param string $columnName
     * @return bool
     */
    public function hasColumn($columnName)
    {
        return array_key_exists($columnName, $this->columns);
    }

}

<?php

namespace Comit\Filter;

/**
 * Class CamelCase
 *
 * @package Filter
 * @author Igor Vuckovic
 */
class CamelCase implements FilterInterface
{

    /**
     * Takes multiple words separated by spaces or underscores and camelizes them
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        $value = trim($value);

        if (!preg_match('/[\s_-]+/', $value)) {
            return $value;
        }

        $value = 'x' . strtolower($value);
        $value = ucwords(preg_replace('/[\s_-]+/', ' ', $value));

        return substr(str_replace(' ', '', $value), 1);
    }

}

<?php

namespace Comit\Filter;

/**
 * Interface FilterInterface
 *
 * @package Filter
 * @author Igor Vuckovic
 */
interface FilterInterface
{
    /**
     * Filter input value
     *
     * @param mixed $value
     * @return mixed
     */
    public function filter($value);

}

<?php

namespace Comit\Controller;

use Comit\Security\Acl as AclPlugin;
use Comit\Helper\Common;
use Comit\Helper\Translator;
use Comit\Messenger\Manager as MessengerManager;
use Comit\Messenger\Message as MessengerMessage;
use Phalcon\Assets\Filters\Cssmin;
use Phalcon\Assets\Filters\Jsmin;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Tag;

/**
 * Class Base
 *
 * @package Controller
 * @author Igor Vuckovic
 */
class Base extends Controller
{

    /** @var  \User */
    protected $user;

    /**
     *
     */
    public function initialize()
    {
        $this->_initializeEnvironment();

        $this->assets->collection('headCss')
            ->addCss(APP_PATH . 'assets/css/bootstrap.css')
            ->addCss(APP_PATH . 'assets/css/font-awesome.css')
            ->addCss(APP_PATH . 'assets/css/jquery-ui.css')
            ->addCss(APP_PATH . 'assets/css/chosen.css')
            ->addCss(APP_PATH . 'assets/css/date-picker.css')
            ->addCss(APP_PATH . 'assets/css/summer-note.css')
            ->addCss(APP_PATH . 'assets/css/summer-note-bs3.css')
            ->addCss(APP_PATH . 'assets/css/switchery.css')
            ->addCss(APP_PATH . 'assets/css/jq-grid.css')
            ->addCss(APP_PATH . 'assets/css/toastr.min.css')
            ->addCss(APP_PATH . 'assets/css/sweetalert.css')
            ->addCss(APP_PATH . 'assets/css/animate.css')
            ->setTargetPath(APP_PATH . 'public/css/head.css')
            ->setTargetUri('css/head.css')
            ->join(true)
            ->addFilter(new Cssmin());

        $this->assets->collection('headJs')
            ->addJs(APP_PATH . 'assets/js/jquery.min.js')
            ->addJs(APP_PATH . 'assets/js/bootstrap.min.js')
            ->addJs(APP_PATH . 'assets/js/jquery.ui.min.js')
            ->addJs(APP_PATH . 'assets/js/date-picker.js')
            ->addJs(APP_PATH . 'assets/js/chosen.js')
            ->addJs(APP_PATH . 'assets/js/toastr.min.js')
            ->addJs(APP_PATH . 'assets/js/switchery.js')
            ->addJs(APP_PATH . 'assets/js/pace.min.js')
            ->addJs(APP_PATH . 'assets/js/metis-menu.js')
            ->addJs(APP_PATH . 'assets/js/slim-scroll.min.js')
            ->addJs(APP_PATH . 'assets/js/drop-zone.js')
            ->addJs(APP_PATH . 'assets/js/plugins/jq-grid.min.js')
            ->addJs(APP_PATH . 'assets/js/plugins/jqgrid.locale-en.js')
            ->addJs(APP_PATH . 'assets/js/comit/jquery.jqgrid.functions.js')
            ->addJs(APP_PATH . 'assets/js/summer-note.min.js')
            ->addJs(APP_PATH . 'assets/js/sweet-alert.min.js')
            ->setTargetPath(APP_PATH . 'public/js/head.js')
            ->setTargetUri('js/head.js')
            ->join(true)
            ->addFilter(new Jsmin());

        $this->assets->collection('footCss')
            ->setTargetPath(APP_PATH . 'public/css/foot.css')
            ->setTargetUri('css/foot.css')
            ->join(true)
            ->addFilter(new Cssmin());

        $this->assets->collection('footJs')
            ->addJs(APP_PATH . 'assets/js/comit/bootstrap.js')
            ->setTargetPath(APP_PATH . 'public/js/foot.js')
            ->setTargetUri('js/foot.js')
            ->join(true)
            ->addFilter(new Jsmin());

    }

    /**
     * @param Dispatcher $dispatcher
     * @return \Phalcon\Http\ResponseInterface|void
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {

        /**
         * Set currently logged User
         */
        $role = AclPlugin::ROLE_GUEST;
        if ($currentUser = $this->session->get('currentUser')) {
            $this->user = \User::findFirst(array(
                \User::PROPERTY_ID . ' = :id:',
                'bind' => array(
                    'id' => $currentUser['id']
                )
            ));

            if ($this->user) {
                $role = $this->user->getRole() ? $this->user->getRole() : AclPlugin::ROLE_MEMBER;
            }
        }


        /**
         * Check ACL
         */
        $aclPlugin = new AclPlugin();
        if (!$aclPlugin->checkAccess($dispatcher, $role)) {

            if ($this->request->isAjax()) {

                /** @var MessengerManager $messengerManager */
                $messengerManager = $this->getDI()->getShared('messengerManager');
                $messengerManager->appendMessage(new MessengerMessage(
                    $this->_translate('You are not allowed to access this resource.'),
                    null,
                    MessengerMessage::DANGER
                ));

                return false;

            } else {

                if ($role === AclPlugin::ROLE_GUEST) {
                    $this->session->set('requestedUrl', $this->request->getURI());
                    $this->response->redirect('auth/login');
                    return false;
                } else {
                    $dispatcher->forward(
                        array(
                            'module' => 'backend',
                            'controller' => 'errors',
                            'action' => 'show401'
                        )
                    );

                    return false;
                }

            }
        }
    }

    /**
     * @param Dispatcher $dispatcher
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        /** Ensure that style.css is added to the bottom of collection
         *  so it can override other styles
         */
        $this->assets->collection('headCss')
            ->addCss(APP_PATH . 'assets/css/style.css');
        $this->_setTitle();
    }

    /**
     * @return bool
     */
    protected function _returnNotAuthorized()
    {
        $this->response->setStatusCode(401, 'Not authorized');

        $this->dispatcher->forward(array(
            'module' => 'backend',
            'controller' => 'errors',
            'action' => 'show401',
        ));

        return false;
    }

    /**
     * @return bool
     */
    protected function _returnNotFound()
    {
        $this->response->setStatusCode(404, 'Not found');

        $this->dispatcher->forward(array(
            'module' => 'backend',
            'controller' => 'errors',
            'action' => 'show404',
        ));

        return false;
    }

    /**
     * @param string $string
     * @param mixed $args
     * @return string string
     */
    protected function _translate($string, $args = null)
    {
        $translator = Translator::getInstance();

        return $translator::translate($string, $args);
    }

    /**
     * @param string $title
     */
    protected function _setTitle()
    {
        Tag::setTitle(Common::getSiteName());
    }

    /**
     * Initialize languages
     */
    private function _initializeEnvironment()
    {
        $this->view->setVar('isRoleSet', intval($this->_isRoleSet()));
    }

    /**
     * @return null|string
     */
    protected function _getRedirectUrl()
    {
        return $this->dispatcher->getParam('url', 'string', null);
    }

    /**
     * @param string $url
     */
    protected function _setRedirectUrl($url)
    {
        $this->dispatcher->setParam('url', ltrim($url, '/'));
    }

    /**
     * @return bool
     */
    protected function _isRoleSet()
    {
        if ($this->user && $this->user->getRole()) {
            return true;
        }

        return false;
    }

}

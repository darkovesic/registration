<?php

namespace Comit\CodeGenerator;

use Comit\Filter\CamelCase;
use Comit\Filter\Humanize;
use Comit\Filter\Singular;

/**
 * Class Model
 *
 * @package ClassGenerator
 * @author Igor Vuckovic
 */
class Model extends CodeGenerator implements CodeGeneratorInterface
{

    /**
     * @return string
     */
    public function generate()
    {
        $output = '<?php' . self::EOL . self::EOL;
        $output .= 'use Comit\Helper\DateTime;' . self::EOL;
        $output .= 'use Comit\Model\Base as BaseModel;' . self::EOL;
        $output .= 'use Phalcon\Mvc\Model\Resultset as ResultSet;' . self::EOL;
        $output .= self::EOL;

        /**
         * Generate Class Doc Blocks
         */
        $this->_generateDocBlock($output);


        $output .= 'class ' . $this->getTable()->getEntityName() . ' extends BaseModel' . self::EOL;
        $output .= '{' . self::EOL . self::EOL;


        /**
         * Set Class constants
         */
        $this->_generateConstants($output);


        /**
         * Set Class properties
         */
        $this->_generateProperties($output);


        /**
         * Set Class REST definition
         */
        $this->_generateRestRepresentationDefinition($output);


        /**
         * Set Class methods (getters and setters)
         */
        $this->_generateGettersAndSetters($output);


        /**
         * Set get/set related methods
         */
        $this->_generateRelatedEntityGettersAndSetters($output);


        $output .= '}' . self::EOL;

        return $output;
    }

    /**
     * @param string $output
     */
    private function _generateDocBlock(&$output)
    {
        $output .= '/**' . self::EOL;
        $output .= ' * ' . $this->getTable()->getEntityName() . ' Model Class' . self::EOL;
        $output .= ' *' . self::EOL;
        $output .= ' * @Source(\'' . $this->getTable()->getName() . '\')' . self::EOL;

        // Generates BELONGS_TO relationships
        $foreignFields = $this->getTable()->getForeignFields();
        if (!empty($foreignFields)) {
            $output .= ' *' . self::EOL;
            foreach ($foreignFields as $foreignField) {
                $output .= ' * @BelongsTo(\'' . $foreignField->getPropertyName() . '\', \''
                    . $foreignField->getReferenceTable()->getEntityName() . '\', \'id\', {\'alias\':\''
                    . ucfirst($foreignField->getReferenceTable()->getEntityName()) . '\'})' . self::EOL;
            }
        }

        // Generate HAS_MANY relationships
        $singular = new Singular();
        $camelCase = new CamelCase();

        $referencedFieldName = $singular->filter($this->getTable()->getName()) . '_id';
        foreach ($this->getTable()->getAdapter()->listTables() as $table) {
            foreach ($table->getFields() as $field) {
                if ($field->getName() === $referencedFieldName) {
                    $output .= ' * @HasMany(\'id\', \'' . $table->getEntityName() . '\', \''
                        . $camelCase->filter($referencedFieldName) . '\', {\'alias\':\''
                        . ucfirst($camelCase->filter($table->getName())) . '\'})' . self::EOL;
                }
            }
        }

        $output .= ' */' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateProperties(&$output)
    {
        $humanize = new Humanize();

        foreach ($this->getTable()->getFields() as $field) {
            if ($field->isPrimaryKey()) {
                $output .= self::INDENT . 'protected $_objectId = self::PROPERTY_' . strtoupper($field->getName()) . ';' . self::EOL;
            }
            break;
        }

        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::EOL . self::INDENT . '/**' . self::EOL;

            if ($field->isPrimaryKey()) {
                $output .= self::INDENT . ' * @Primary' . self::EOL;
                $output .= self::INDENT . ' * @Identity' . self::EOL;
            }

            $output .= self::INDENT . ' * @Column(column="' . $field->getName() . '", type="' . $field->getTypeDisplayName()
                . '", nullable=' . ($field->isNotNull() ? 'false' : 'true') . ')' . self::EOL;

            if (!$field->isPrimaryKey()) {
                $output .= self::INDENT . ' * @FormElement(label="' . ucfirst($humanize->filter(str_replace('_id', '', $field->getName())))
                    . '", type="' . $field->getFormElementType()
                    . '", required=' . ($field->isNotNull() ? 'true' : 'false')
                    . ($field->isReferenceKey() ? ', relation="' . $field->getReferenceTable()->getEntityName() . '"' : '')
                    . ')' . self::EOL;
            }

            $output .= self::INDENT . ' */' . self::EOL;

            $output .= self::INDENT . 'protected $' . $field->getPropertyName() . ';' . self::EOL;
        }

        $output .= self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateRestRepresentationDefinition(&$output)
    {
        $output .= self::INDENT . 'protected $_restRepresentationDefinition = array(' . self::EOL;

        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . self::INDENT . 'self::PROPERTY_' . strtoupper($field->getName()) . ' => true,' . self::EOL;
        }

        foreach ($this->getTable()->getForeignFields() as $field) {
            $output .= self::INDENT . self::INDENT . 'self::ALIAS_' . strtoupper($field->getReferenceTable()->getEntityName(true))
                . ' => true,' . self::EOL;
        }

        $output .= self::INDENT . ');' . self::EOL;
        $output .= self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateConstants(&$output)
    {
        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . 'const PROPERTY_' . strtoupper($field->getName())
                . ' = \'' . $field->getPropertyName() . '\';' . self::EOL;
        }

        $output .= self::EOL;

        /**
         * Get BELONGS TO relationships
         */
        foreach ($this->getTable()->getForeignFields() as $field) {
            $output .= self::INDENT . 'const ALIAS_' . strtoupper($field->getReferenceTable()->getEntityName(true))
                . ' = \'' . ucfirst($field->getReferenceTable()->getEntityName()) . '\';' . self::EOL;
        }


        /**
         * Get HAS MANY relationships
         */
        $singular = new Singular();
        $camelCase = new CamelCase();
        $referencedFieldName = $singular->filter($this->getTable()->getName()) . '_id';
        foreach ($this->getTable()->getAdapter()->listTables() as $table) {
            foreach ($table->getFields() as $field) {
                if ($field->getName() === $referencedFieldName) {
                    $output .= self::INDENT . 'const ALIAS_' . strtoupper($table->getName())
                        . ' = \'' . ucfirst($camelCase->filter($table->getName())) . '\';' . self::EOL;
                }
            }

        }

        $output .= self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateGettersAndSetters(&$output)
    {
        foreach ($this->getTable()->getFields() as $field) {

            if ($field->isBoolean()) {
                $return = 'bool';
                $param = 'int|bool';
                $getterBody = 'return (bool)$this->' . $field->getPropertyName() . ';';
                $setterBody = '$this->' . $field->getPropertyName() . ' = $' . $field->getPropertyName() . ' ? 1 : 0;';
            } else if ($field->isInteger()) {
                $return = 'int';
                $param = 'int';
                $getterBody = 'return !is_null($this->' . $field->getPropertyName() . ') ? intval($this->' . $field->getPropertyName() . ') : null;';
                $setterBody = '$this->' . $field->getPropertyName() . ' = !is_null($' . $field->getPropertyName() . ') ? intval($' . $field->getPropertyName() . ') : null;';
            } else if ($field->isDate()) {
                $return = 'null|DateTime';
                $param = 'string|DateTime';
                $getterBody = 'return $this->' . $field->getPropertyName() . ';';
                $setterBody = '$this->' . $field->getPropertyName() . ' = $' . $field->getPropertyName() . ';';
            } else if ($field->isFloat()) {
                $return = 'float';
                $param = 'float';
                $getterBody = 'return !is_null($this->' . $field->getPropertyName() . ') ? floatval($this->' . $field->getPropertyName() . ') : null;';
                $setterBody = '$this->' . $field->getPropertyName() . ' = !is_null($' . $field->getPropertyName() . ') ? floatval($' . $field->getPropertyName() . ') : null;';
            } else {
                $return = 'string';
                $param = 'string';
                $getterBody = 'return $this->' . $field->getPropertyName() . ';';
                $setterBody = '$this->' . $field->getPropertyName() . ' = $' . $field->getPropertyName() . ';';
            }

            // Generate get method
            $output .= self::INDENT . '/**' . self::EOL;
            $output .= self::INDENT . ' * @return ' . $return . self::EOL;
            $output .= self::INDENT . ' */' . self::EOL;
            $output .= self::INDENT . 'public function get' . ucfirst($field->getPropertyName()) . '()' . self::EOL;
            $output .= self::INDENT . '{' . self::EOL;
            $output .= self::INDENT . self::INDENT . $getterBody . self::EOL;
            $output .= self::INDENT . '}' . self::EOL . self::EOL;

            // Generate set method
            $output .= self::INDENT . '/**' . self::EOL;
            $output .= self::INDENT . ' * @param ' . $param . ' $' . $field->getPropertyName() . self::EOL;
            $output .= self::INDENT . ' * @return $this' . self::EOL;
            $output .= self::INDENT . ' */' . self::EOL;
            $output .= self::INDENT . 'public function set' . ucfirst($field->getPropertyName()) . '($' . $field->getPropertyName() . ')' . self::EOL;
            $output .= self::INDENT . '{' . self::EOL;
            $output .= self::INDENT . self::INDENT . $setterBody . self::EOL . self::EOL;
            $output .= self::INDENT . self::INDENT . 'return $this;' . self::EOL;
            $output .= self::INDENT . '}' . self::EOL . self::EOL;
        }

    }

    /**
     * @param string $output
     */
    private function _generateRelatedEntityGettersAndSetters(&$output)
    {
        foreach ($this->getTable()->getForeignFields() as $field) {
            if ($referenceTable = $field->getReferenceTable()) {
                $relatedEntityName = $referenceTable->getEntityName();

                // Generate get method
                $output .= self::INDENT . '/**' . self::EOL;
                $output .= self::INDENT . ' * @return null|' . $relatedEntityName . '' . self::EOL;
                $output .= self::INDENT . ' */' . self::EOL;
                $output .= self::INDENT . 'public function get' . $relatedEntityName . '()' . self::EOL;
                $output .= self::INDENT . '{' . self::EOL;
                $output .= self::INDENT . self::INDENT . 'return $this->getRelated(self::ALIAS_' . strtoupper($referenceTable->getEntityName(true)) . ');' . self::EOL;
                $output .= self::INDENT . '}' . self::EOL . self::EOL;

                // Generate set method
                $output .= self::INDENT . '/**' . self::EOL;
                $output .= self::INDENT . ' * @param ' . $relatedEntityName . ' $' . lcfirst($relatedEntityName) . self::EOL;
                $output .= self::INDENT . ' * @return $this' . self::EOL;
                $output .= self::INDENT . ' */' . self::EOL;
                $output .= self::INDENT . 'public function set' . $relatedEntityName . '(' . $relatedEntityName . ' $' . lcfirst($relatedEntityName) . ')' . self::EOL;
                $output .= self::INDENT . '{' . self::EOL;
                $output .= self::INDENT . self::INDENT . '$this->' . $field->getPropertyName() . ' = $' . lcfirst($relatedEntityName) . '->getId();' . self::EOL . self::EOL;
                $output .= self::INDENT . self::INDENT . 'return $this;' . self::EOL;
                $output .= self::INDENT . '}' . self::EOL . self::EOL;
            }
        }

        $singular = new Singular();
        $referencedFieldName = $singular->filter($this->getTable()->getName()) . '_id';
        $camelCase = new CamelCase();

        foreach ($this->getTable()->getAdapter()->listTables() as $table) {
            foreach ($table->getFields() as $field) {
                if ($field->getName() === $referencedFieldName) {

                    // Generate get method
                    $output .= self::INDENT . '/**' . self::EOL;
                    $output .= self::INDENT . ' * @param null|string|array $arguments' . self::EOL;
                    $output .= self::INDENT . ' * @return ResultSet|' . $table->getEntityName() . '[]' . self::EOL;
                    $output .= self::INDENT . ' */' . self::EOL;
                    $output .= self::INDENT . 'public function get' . ucfirst($camelCase->filter($table->getName())) . '($arguments = null)' . self::EOL;
                    $output .= self::INDENT . '{' . self::EOL;
                    $output .= self::INDENT . self::INDENT . 'return $this->getRelated(self::ALIAS_' . strtoupper($table->getName()) . ', $arguments);' . self::EOL;
                    $output .= self::INDENT . '}' . self::EOL . self::EOL;
                }
            }

        }

    }

}

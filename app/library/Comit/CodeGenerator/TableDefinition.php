<?php

namespace Comit\CodeGenerator;

/**
 * Class TableDefinition
 *
 * @package CodeGenerator
 * @author Igor Vuckovic
 */
class TableDefinition extends CodeGenerator implements CodeGeneratorInterface
{

    /**
     * @return string
     */
    public function generate()
    {
        $className = $this->getTable()->getEntityName() . 'TableDefinition';

        $output = '<?php' . self::EOL . self::EOL;
        $output .= 'use \\Table\\Column;' . self::EOL . self::EOL;
        $output .= '/**' . self::EOL;
        $output .= ' * ' . $className . ' Class' . self::EOL;
        $output .= ' *' . self::EOL;
        $output .= ' * @package Table' . self::EOL;
        $output .= ' * @subpackage Definitions' . self::EOL;
        $output .= ' * @author Igor Vuckovic' . self::EOL;
        $output .= ' */' . self::EOL;

        $output .= 'class ' . $this->getTable()->getEntityName() . 'TableDefinition extends \\Table\\Definition' . self::EOL;
        $output .= '{' . self::EOL . self::EOL;


        /**
         * Set Class constants
         */
        $this->_generateConstants($output);
        $output .= self::EOL;

        /**
         * Set Class construct method
         */
        $this->_generateConstructor($output);
        $output .= self::EOL;

        $output .= '}';

        return $output;

    }

    /**
     * @param string $output
     */
    private function _generateConstants(&$output)
    {
        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . 'const ' . strtoupper($field->getName())
                . ' = \'' . $field->getName() . '\';' . self::EOL;
        }
    }

    /**
     * @param string $output
     */
    private function _generateConstructor(&$output)
    {
        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Constructor' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function __construct()' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . self::INDENT . '$this->addColumn(self::' . strtoupper($field->getName()) . ')' . self::EOL;

            if ($field->isPrimaryKey()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setPrimaryKey(true)' . self::EOL;
            }

            if ($field->isInteger()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_INT)' . self::EOL;
            } elseif ($field->isFloat()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_FLOAT)' . self::EOL;
            } elseif ($field->isDate()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_DATETIME)' . self::EOL;
            } elseif ($field->isBoolean()) {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_BOOLEAN)' . self::EOL;
            } else {
                $output .= self::INDENT . self::INDENT . self::INDENT . '->setDataType(Column::DATA_TYPE_STRING)' . self::EOL;
            }

            $output .= self::INDENT . self::INDENT . self::INDENT . '->setColumnLabel(_(\'' . $field->getHumanName() . '\'))' . self::EOL;
            $output .= self::EOL;
        }

        $output .= self::INDENT . '}' . self::EOL;
    }

}

<?php

namespace Comit\CodeGenerator;

use Comit\Filter\CamelCase;

/**
 * Class Controller
 *
 * @package CodeGenerator
 * @author Igor Vuckovic
 */
class Controller extends CodeGenerator implements CodeGeneratorInterface
{

    /**
     * @return string
     */
    public function generate()
    {
        $camelCase = new CamelCase();
        $className = ucfirst($camelCase->filter($this->getTable()->getName())) . 'Controller';

        $output = '<?php' . self::EOL . self::EOL;

        $output .= '/**' . self::EOL;
        $output .= ' * ' . $className . ' Controller Class' . self::EOL;
        $output .= ' *' . self::EOL;
        $output .= ' * @package Controllers' . self::EOL;
        $output .= ' * @author Igor Vuckovic' . self::EOL;
        $output .= ' */' . self::EOL;

        $output .= 'class ' . $className . ' extends \\Comit\\Controller\\Base' . self::EOL;
        $output .= '{' . self::EOL . self::EOL;


        /**
         * Set Class construct method
         */
        $this->_generateConstructor($output);
        $output .= self::EOL;


        /**
         * Set Index Action
         */
        $this->_generateIndexAction($output);
        $output .= self::EOL;


        /**
         * Set Retrieve Action
         */
        $this->_generateRetrieveAction($output);
        $output .= self::EOL;


        /**
         * Set Create Action
         */
        $this->_generateCreateAction($output);
        $output .= self::EOL;


        /**
         * Set Update Action
         */
        $this->_generateUpdateAction($output);
        $output .= self::EOL;


        /**
         * Set Delete Action
         */
        $this->_generateDeleteAction($output);
        $output .= self::EOL;

        $output .= '}';

        return $output;
    }

    /**
     * @param string $output
     */
    private function _generateConstructor(&$output)
    {
        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Constructor' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function __construct()' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;
        $output .= self::INDENT . self::INDENT . 'parent::__construct();' . self::EOL . self::EOL;
        $output .= self::INDENT . '}' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateIndexAction(&$output)
    {
        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Displays all ' . $this->getTable()->getHumanTableName(false) . self::EOL;
        $output .= self::INDENT . ' * ' . self::EOL;
        $output .= self::INDENT . ' * @access public' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function indexAction()' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->ocular->setView(\'retrieve\');' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$this->retrieveAction();' . self::EOL;

        $output .= self::INDENT . '}' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateRetrieveAction(&$output)
    {
        $camelCase = new CamelCase();
        $tableName = $this->getTable()->getName();
        $humanTableName = $this->getTable()->getHumanTableName(false);
        $entityName = $this->getTable()->getEntityName();

        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Displays all ' . $humanTableName . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function retrieveAction()' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$this->load->library(\'form_validation\');' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$this->load->helper(\'form\');' . self::EOL . self::EOL;
        $output .= self::INDENT . self::INDENT . '$query = new \Query();' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$query->setWhere(array(' . self::EOL;

        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . self::INDENT . self::INDENT . '\\' . $entityName . '::FIELD_' . strtoupper($field->getName())
                . ' => $this->input->post(\'' . $field->getName() . '\'),' . self::EOL;
        }

        $output .= self::INDENT . self::INDENT . '));' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$queryTotal = $query->cloneQuery();' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$queryTotal->setCountAllResults(true);' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$query->setLimit($this->resultsPerPage)->setOffset($this->resultsOffset);' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$' . $camelCase->filter($tableName) . ' = \\' . $entityName . '::find($query);' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$total = \\' . $entityName . '::find($queryTotal);' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->ocular->setTitle(_(\'' . $humanTableName . '\'));' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$this->ocular->set(\'' . $camelCase->filter($tableName) . '\', $' . $camelCase->filter($tableName) . ');' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$this->ocular->set(\'total\', $total);' . self::EOL;

        $camelCase = new CamelCase();
        foreach ($this->getTable()->getForeignFields() as $referencedField) {
            $output .= self::INDENT . self::INDENT . '$this->ocular->set(\'' . $camelCase->filter($referencedField->getReferenceTable()->getName()) . '\', \\' . $referencedField->getReferenceTable()->getEntityName() . '::find());' . self::EOL;
        }

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->_render();' . self::EOL;

        $output .= self::INDENT . '}' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateCreateAction(&$output)
    {
        $tableName = $this->getTable()->getName();
        $humanTableName = $this->getTable()->getHumanTableName(true);
        $entityName = $this->getTable()->getEntityName();

        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Creates new ' . $humanTableName . self::EOL;
        $output .= self::INDENT . ' * ' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function createAction()' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->load->library(\'form_validation\');' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$this->load->helper(\'form\');' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'if (is_post_request() && $this->form_validation->run(\'' . str_replace('_', '-', $tableName) . '/create\')) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$' . lcfirst($entityName) . ' = new \\' . $entityName . '();' . self::EOL;

        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . self::INDENT . self::INDENT . '$' . lcfirst($entityName) . '->set' . ucfirst($field->getPropertyName())
                . '($this->input->post(\'' . $field->getName() . '\'));' . self::EOL;
        }

        $output .= self::INDENT . self::INDENT . self::INDENT . 'if($' . lcfirst($entityName) . '->save()) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '$this->session->set_flashdata(\'ocular_message\', array(' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . self::INDENT . '_(\'Data was created successfully!\'),' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'success\'' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '));' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '}' . self::EOL;

        $output .= self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'redirect(\'' . str_replace('_', '-', $tableName) . '\');' . self::EOL;

        $output .= self::INDENT . self::INDENT . '}' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->ocular->setTitle(_(\'Add ' . $humanTableName . '\'));' . self::EOL;

        $camelCase = new CamelCase();
        foreach ($this->getTable()->getForeignFields() as $referencedField) {
            $output .= self::INDENT . self::INDENT . '$this->ocular->set(\'' . $camelCase->filter($referencedField->getReferenceTable()->getName()) . '\', \\' . $referencedField->getReferenceTable()->getEntityName() . '::find());' . self::EOL;
        }

        $output .= self::INDENT . self::INDENT . '$this->_render();' . self::EOL;

        $output .= self::INDENT . '}' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateUpdateAction(&$output)
    {
        $tableName = $this->getTable()->getName();
        $humanTableName = $this->getTable()->getHumanTableName(true);
        $entityName = $this->getTable()->getEntityName();

        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Updates current ' . $humanTableName . self::EOL;
        $output .= self::INDENT . ' * ' . self::EOL;
        $output .= self::INDENT . ' * @param int $id' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function updateAction($id)' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        $output .= self::INDENT . self::INDENT . 'if(!$' . lcfirst($entityName) . ' = \\' . $entityName . '::findFirst($id))' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . ' return $this->_show404();' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->load->library(\'form_validation\');' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$this->load->helper(\'form\');' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'if (is_post_request() && $this->form_validation->run(\'' . str_replace('_', '-', $tableName) . '/update\')) {' . self::EOL;

        foreach ($this->getTable()->getFields() as $field) {
            $output .= self::INDENT . self::INDENT . self::INDENT . '$' . lcfirst($entityName) . '->set' . ucfirst($field->getPropertyName())
                . '($this->input->post(\'' . $field->getName() . '\'));' . self::EOL;
        }

        $output .= self::INDENT . self::INDENT . self::INDENT . 'if($' . lcfirst($entityName) . '->save()) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '$this->session->set_flashdata(\'ocular_message\', array(' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . self::INDENT . '_(\'Data was created successfully!\'),' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'success\'' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '));' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '}' . self::EOL;

        $output .= self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'redirect(\'' . str_replace('_', '-', $tableName) . '\');' . self::EOL;

        $output .= self::INDENT . self::INDENT . '}' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->ocular->setTitle(_(\'Update ' . $humanTableName . '\'));' . self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->ocular->set(\'' . lcfirst($entityName) . '\', ' . '$' . lcfirst($entityName) . ');' . self::EOL;

        $camelCase = new CamelCase();
        foreach ($this->getTable()->getForeignFields() as $referencedField) {
            $output .= self::INDENT . self::INDENT . '$this->ocular->set(\'' . $camelCase->filter($referencedField->getReferenceTable()->getName()) . '\', \\' . $referencedField->getReferenceTable()->getEntityName() . '::find());' . self::EOL;
        }

        $output .= self::INDENT . self::INDENT . '$this->_render();' . self::EOL;

        $output .= self::INDENT . '}' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateDeleteAction(&$output)
    {
        $tableName = $this->getTable()->getName();
        $humanTableName = $this->getTable()->getHumanTableName(true);
        $entityName = $this->getTable()->getEntityName();

        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Deletes existing ' . $humanTableName . self::EOL;
        $output .= self::INDENT . ' * ' . self::EOL;
        $output .= self::INDENT . ' * @param int $id' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function deleteAction($id)' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        $output .= self::INDENT . self::INDENT . 'if(!$' . lcfirst($entityName) . ' = \\' . $entityName . '::findFirst($id))' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . ' return $this->_show404();' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'if($' . lcfirst($entityName) . '->delete()) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$this->session->set_flashdata(\'ocular_message\', array(' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '_(\'Data was deleted successfully!\'),' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'success\'' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '));' . self::EOL;
        $output .= self::INDENT . self::INDENT . '}' . self::EOL;

        $output .= self::EOL;
        $output .= self::INDENT . self::INDENT . 'redirect(\'' . str_replace('_', '-', $tableName) . '\');' . self::EOL;

        $output .= self::INDENT . '}' . self::EOL;
    }

}

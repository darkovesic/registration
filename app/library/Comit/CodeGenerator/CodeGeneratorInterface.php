<?php

namespace Comit\CodeGenerator;

/**
 * Interface CodeGeneratorInterface
 *
 * @package CodeGenerator
 * @author Igor Vuckovic
 */
interface CodeGeneratorInterface
{

    /**
     * @return string
     */
    public function generate();

}

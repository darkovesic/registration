<?php

namespace Comit\CodeGenerator;

use Comit\Database\Table;

/**
 * Class CodeGenerator
 *
 * @package CodeGenerator
 * @author Igor Vuckovic
 */
abstract class CodeGenerator
{
    const EOL = PHP_EOL;
    const INDENT = '    ';

    /** @var Table */
    private $table;

    /**
     * @param Table $table
     */
    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    /**
     * @return Table
     */
    public function getTable()
    {
        return $this->table;
    }

}

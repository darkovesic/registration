<?php

namespace Comit\CodeGenerator;

use Comit\Filter\CamelCase;

/**
 * Class File
 *
 * @package ClassGenerator
 * @author Igor Vuckovic
 */
class File
{
    /** @var Model|Controller */
    private $codeGenerator;

    public $className;

    /**
     * @param CodeGeneratorInterface $codeGenerator
     */
    public function __construct(CodeGeneratorInterface $codeGenerator)
    {
        $this->codeGenerator = $codeGenerator;

        if ($this->codeGenerator instanceof Model) {
            $this->className = $this->codeGenerator->getTable()->getEntityName();
        } else if ($this->codeGenerator instanceof Controller || $this->codeGenerator instanceof RestCrud) {
            $camelCase = new CamelCase();
            $this->className = ucfirst($camelCase->filter($this->codeGenerator->getTable()->getName())) . 'Controller';
        }

    }

    /**
     * @param string $path
     * @param bool $force
     * @return bool
     */
    public function write($path, $force = false)
    {
        if (is_dir($path)) {
            $file = $path . $this->className . '.php';

            if ($force || !file_exists($file)) {
                $fp = fopen($file, 'w');
                $return = fwrite($fp, $this->codeGenerator->generate());
                fclose($fp);

                return (bool)$return;
            }
        }

        return false;
    }

}

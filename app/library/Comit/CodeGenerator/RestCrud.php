<?php

namespace Comit\CodeGenerator;

use Comit\Filter\CamelCase;

/**
 * Class RestCrud
 *
 * @package CodeGenerator
 * @author Igor Vuckovic
 */
class RestCrud extends CodeGenerator implements CodeGeneratorInterface
{

    /**
     * @return string
     */
    public function generate()
    {
        $camelCase = new CamelCase();
        $className = ucfirst($camelCase->filter($this->getTable()->getName())) . 'Controller';

        $output = '<?php' . self::EOL . self::EOL;

        $output .= 'namespace Rest\\Controllers;' . self::EOL . self::EOL;
        $output .= 'use Comit\\Controller\\Rest;' . self::EOL;
        $output .= 'use Comit\\Model\\Type;' . self::EOL;
        $output .= 'use Comit\\Rest\\Representation;' . self::EOL;
        $output .= 'use Phalcon\\Http\\Response;' . self::EOL;

        $output .= self::EOL;

        $output .= '/**' . self::EOL;
        $output .= ' * ' . $className . ' Class' . self::EOL;
        $output .= ' *' . self::EOL;
        $output .= ' * @package Rest\\Controllers' . self::EOL;
        $output .= ' * @author Igor Vuckovic' . self::EOL;
        $output .= ' */' . self::EOL;

        $output .= 'class ' . $className . ' extends Rest' . self::EOL;
        $output .= '{' . self::EOL . self::EOL;


        /**
         * Set Retrieve Action
         */
        $this->_generateRetrieveAction($output);
        $output .= self::EOL;


        /**
         * Set Create Action
         */
        $this->_generateCreateAction($output);
        $output .= self::EOL;


        /**
         * Set Update Action
         */
        $this->_generateUpdateAction($output);
        $output .= self::EOL;


        /**
         * Set Delete Action
         */
        $this->_generateDeleteAction($output);
        $output .= self::EOL;

        $output .= '}' . self::EOL;

        return $output;
    }

    /**
     * @param string $output
     */
    private function _generateRetrieveAction(&$output)
    {
        $camelCase = new CamelCase();
        $tableName = $this->getTable()->getName();
        $humanTableName = $this->getTable()->getHumanTableName(false);
        $entityName = $this->getTable()->getEntityName();

        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Displays all ' . $humanTableName . self::EOL;
        $output .= self::INDENT . ' * ' . self::EOL;
        $output .= self::INDENT . ' * @return Response' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function retrieveAction()' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        $output .= self::INDENT . self::INDENT . '$representation = new Representation(Type::'
            . strtoupper($this->getTable()->getEntityName(true)) . ', $this->user);' . self::EOL;

        $output .= self::INDENT . self::INDENT . '$parameters = $representation->generateConditions(func_get_args());' . self::EOL;

        $getRelated = '$' . $camelCase->filter($tableName) . ' = \\' . $entityName . '::find($parameters);';
        foreach ($this->getTable()->getFields() as $field) {
            if ($field->getName() === 'user_id') {
                $getRelated = '$' . $camelCase->filter($tableName)
                    . ' = $this->user->get' . ucfirst($camelCase->filter($tableName)) . '($parameters);';
                break;
            }
        }
        $output .= self::INDENT . self::INDENT . $getRelated . self::EOL;

        $output .= self::INDENT . self::INDENT . '$this->setPayload($representation->getAdvancedRepresentations($'
            . $camelCase->filter($tableName) . '));' . self::EOL;

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'return $this->getResponse();' . self::EOL;
        $output .= self::INDENT . '}' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateCreateAction(&$output)
    {
        $humanTableName = $this->getTable()->getHumanTableName(true);
        $entityName = $this->getTable()->getEntityName();

        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Creates new ' . $humanTableName . self::EOL;
        $output .= self::INDENT . ' * ' . self::EOL;
        $output .= self::INDENT . ' * @return Response' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function createAction()' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        $output .= self::INDENT . self::INDENT . '$payload = (array)$this->getPostData();' . self::EOL;

        foreach ($this->getTable()->getFields() as $field) {
            if ($field->getName() === 'user_id') {
                $output .= self::INDENT . self::INDENT . '$payload[\\' . $entityName . '::PROPERTY_USER_ID] = $this->user->getId();' . self::EOL;
            }
        }

        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$' . lcfirst($entityName) . ' = new \\' . $entityName . '();' . self::EOL;
        $output .= self::INDENT . self::INDENT . '$form = $' . lcfirst($entityName) . '->getForm();' . self::EOL;
        $output .= self::INDENT . self::INDENT . 'if ($form->isValid($payload)) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'foreach ($payload as $propertyName => $value) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '$' . lcfirst($entityName) . '->setProperty($propertyName, $value);' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '}' . self::EOL;
        $output .= self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'if ($' . lcfirst($entityName) . '->save()) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT
            . '$representation = new Representation(Type::' . strtoupper($this->getTable()->getEntityName(true)) . ', $this->user);' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT
            . '$this->setPayload($representation->getBasicRepresentation($' . lcfirst($entityName) . '));' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '}' . self::EOL;
        $output .= self::INDENT . self::INDENT . '}' . self::EOL;
        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'return $this->getResponse();' . self::EOL;

        $output .= self::INDENT . '}' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateUpdateAction(&$output)
    {
        $humanTableName = $this->getTable()->getHumanTableName(true);
        $entityName = $this->getTable()->getEntityName();

        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Updates existing ' . $humanTableName . self::EOL;
        $output .= self::INDENT . ' * ' . self::EOL;
        $output .= self::INDENT . ' * @param int $id' . self::EOL;
        $output .= self::INDENT . ' * @return Response' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function updateAction($id)' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        $isUserResource = false;
        $condition = '\\' . $entityName . '::PROPERTY_ID . \' = :id:\',';
        $bind = self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'id\' => $id,' . self::EOL;
        foreach ($this->getTable()->getFields() as $field) {
            if ($field->getName() === 'user_id') {
                $condition = '\\' . $entityName . '::PROPERTY_ID . \' = :id: AND \' . ' . '\\' . $entityName . '::PROPERTY_USER_ID . \' = :userId:\',';
                $bind = self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'id\' => $id,' . self::EOL;
                $bind .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'userId\' => $this->user->getId(),' . self::EOL;
                $isUserResource = true;
                break;
            }
        }
        $output .= self::INDENT . self::INDENT . '$' . lcfirst($entityName) . ' = \\' . $entityName . '::findFirst(array(' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . $condition . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '\'bind\' => array(' . self::EOL;
        $output .= $bind;
        $output .= self::INDENT . self::INDENT . self::INDENT . ')' . self::EOL;
        $output .= self::INDENT . self::INDENT . '));' . self::EOL;
        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'if (!$' . lcfirst($entityName) . ') {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$this->dispatcher->forward(array(' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'controller\' => \'errors\',' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'action\' => \'show404\',' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '));' . self::EOL;
        $output .= self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'return false;' . self::EOL;
        $output .= self::INDENT . self::INDENT . '}' . self::EOL;
        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . '$payload = (array)$this->getPostData();' . self::EOL;
        $output .= self::INDENT . self::INDENT . 'if (array_key_exists(\\' . $entityName . '::PROPERTY_ID, $payload)) unset($payload[\\' . $entityName . '::PROPERTY_ID]);' . self::EOL;
        if ($isUserResource) {
            $output .= self::INDENT . self::INDENT . 'if (array_key_exists(\\' . $entityName . '::PROPERTY_USER_ID, $payload)) unset($payload[\\' . $entityName . '::PROPERTY_USER_ID]);' . self::EOL;
        }
        $output .= self::EOL;
        $output .= self::INDENT . self::INDENT . '$form = $' . lcfirst($entityName)
            . '->getForm(array_diff(array_keys($' . lcfirst($entityName)
            . '->toArray()), array_keys($payload)));' . self::EOL;
        $output .= self::INDENT . self::INDENT . 'if ($form->isValid($payload)) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'foreach ($payload as $propertyName => $value) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '$' . lcfirst($entityName) . '->setProperty($propertyName, $value);' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '}' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$' . lcfirst($entityName) . '->save();' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$' . lcfirst($entityName) . '->refresh();' . self::EOL;
        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . self::INDENT
            . '$representation = new Representation(Type::' . strtoupper($this->getTable()->getEntityName(true)) . ', $this->user);' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT
            . '$this->setPayload($representation->getBasicRepresentation($' . lcfirst($entityName) . '));' . self::EOL;
        $output .= self::INDENT . self::INDENT . '}' . self::EOL;
        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'return $this->getResponse();' . self::EOL;

        $output .= self::INDENT . '}' . self::EOL;
    }

    /**
     * @param string $output
     */
    private function _generateDeleteAction(&$output)
    {
        $humanTableName = $this->getTable()->getHumanTableName(true);
        $entityName = $this->getTable()->getEntityName();

        $output .= self::INDENT . '/**' . self::EOL;
        $output .= self::INDENT . ' * Deletes existing ' . $humanTableName . self::EOL;
        $output .= self::INDENT . ' * ' . self::EOL;
        $output .= self::INDENT . ' * @param int $id' . self::EOL;
        $output .= self::INDENT . ' * @return Response' . self::EOL;
        $output .= self::INDENT . ' */' . self::EOL;
        $output .= self::INDENT . 'public function deleteAction($id)' . self::EOL;
        $output .= self::INDENT . '{' . self::EOL;

        $isUserResource = false;
        $condition = '\\' . $entityName . '::PROPERTY_ID . \' = :id:\',';
        $bind = self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'id\' => $id,' . self::EOL;
        foreach ($this->getTable()->getFields() as $field) {
            if ($field->getName() === 'user_id') {
                $condition = '\\' . $entityName . '::PROPERTY_ID . \' = :id: AND \' . ' . '\\' . $entityName . '::PROPERTY_USER_ID . \' = :userId:\',';
                $bind = self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'id\' => $id,' . self::EOL;
                $bind .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'userId\' => $this->user->getId(),' . self::EOL;
                $isUserResource = true;
                break;
            }
        }
        $output .= self::INDENT . self::INDENT . '$' . lcfirst($entityName) . ' = \\' . $entityName . '::findFirst(array(' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . $condition . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '\'bind\' => array(' . self::EOL;
        $output .= $bind;
        $output .= self::INDENT . self::INDENT . self::INDENT . ')' . self::EOL;
        $output .= self::INDENT . self::INDENT . '));' . self::EOL;
        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'if (!$' . lcfirst($entityName) . ') {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$this->dispatcher->forward(array(' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'controller\' => \'errors\',' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'action\' => \'show404\',' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '));' . self::EOL;
        $output .= self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'return false;' . self::EOL;
        $output .= self::INDENT . self::INDENT . '}' . self::EOL;
        $output .= self::EOL;

        $output .= self::INDENT . self::INDENT . 'if ($' . lcfirst($entityName) . '->isDeletable()) {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$' . lcfirst($entityName) . '->delete();' . self::EOL;
        $output .= self::INDENT . self::INDENT . '} else {' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '$this->dispatcher->forward(array(' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'controller\' => \'errors\',' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . self::INDENT . '\'action\' => \'show401\',' . self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . '));' . self::EOL;
        $output .= self::EOL;
        $output .= self::INDENT . self::INDENT . self::INDENT . 'return false;' . self::EOL;
        $output .= self::INDENT . self::INDENT . '}' . self::EOL;
        $output .= self::EOL;


        $output .= self::INDENT . self::INDENT . 'return $this->getResponse();' . self::EOL;

        $output .= self::INDENT . '}' . self::EOL;
    }

}

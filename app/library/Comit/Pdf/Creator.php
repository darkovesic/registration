<?php

namespace Comit\Pdf;

use Comit\Pdf\Renderer\WkHtmlToPdf;

/**
 * Class Creator
 *
 * @package Comit\Pdf
 * @author Igor Vuckovic
 */
class Creator
{

    /** @var RendererInterface */
    private $_renderer = null;

    /**
     * @return RendererInterface
     */
    public function getRenderer()
    {
        if (is_null($this->_renderer)) {
            $this->_renderer = new WkHtmlToPdf();
        }

        return $this->_renderer;
    }

    public function setRenderer(RendererInterface $renderer)
    {
        $this->_renderer = $renderer;
    }

    /**
     * @param string $html
     * @param string $orientation
     * @return mixed
     */
    public function createPdf($html, $orientation = 'portrait')
    {
        return $this->getRenderer()->create($html, $orientation);
    }

    /**
     * @param mixed $width
     * @param mixed $height
     */
    public function setPageSize($width, $height)
    {
        $this->getRenderer()->setPageSize($width, $height);
    }

    /**
     * @param string $marginTop
     * @param string $marginRight
     * @param string $marginBottom
     * @param string $marginLeft
     */
    public function setMargins($marginTop, $marginRight, $marginBottom, $marginLeft)
    {
        $this->getRenderer()->setMargins($marginTop, $marginRight, $marginBottom, $marginLeft);
    }

}

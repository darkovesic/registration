<?php

namespace Comit\Pdf;

/**
 * Class RendererTrait
 *
 * @package Comit\Pdf
 * @author Igor Vuckovic
 */
trait RendererTrait
{

    protected $_pageHeight = '297mm';
    protected $_pageWidth = '210mm';
    protected $_marginTop = '15mm';
    protected $_marginRight = '15mm';
    protected $_marginBottom = '15mm';
    protected $_marginLeft = '15mm';

    /**
     * @return string
     */
    public function getMarginBottom()
    {
        return $this->_marginBottom;
    }

    /**
     * @param string $marginBottom
     */
    public function setMarginBottom($marginBottom)
    {
        $this->_marginBottom = $marginBottom;
    }

    /**
     * @return string
     */
    public function getMarginLeft()
    {
        return $this->_marginLeft;
    }

    /**
     * @param string $marginLeft
     */
    public function setMarginLeft($marginLeft)
    {
        $this->_marginLeft = $marginLeft;
    }

    /**
     * @return string
     */
    public function getMarginRight()
    {
        return $this->_marginRight;
    }

    /**
     * @param string $marginRight
     */
    public function setMarginRight($marginRight)
    {
        $this->_marginRight = $marginRight;
    }

    /**
     * @return string
     */
    public function getMarginTop()
    {
        return $this->_marginTop;
    }

    /**
     * @param string $marginTop
     */
    public function setMarginTop($marginTop)
    {
        $this->_marginTop = $marginTop;
    }

    /**
     * @return mixed
     */
    public function getPageHeight()
    {
        return $this->_pageHeight;
    }

    /**
     * @param mixed $pageHeight
     */
    public function setPageHeight($pageHeight)
    {
        $this->_pageHeight = $pageHeight;
    }

    /**
     * @return mixed
     */
    public function getPageWidth()
    {
        return $this->_pageWidth;
    }

    /**
     * @param mixed $pageWidth
     */
    public function setPageWidth($pageWidth)
    {
        $this->_pageWidth = $pageWidth;
    }

}

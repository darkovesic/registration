<?php

namespace Comit\Pdf\Renderer;

use Comit\Pdf\RendererInterface;
use Comit\Pdf\RendererTrait;
use Knp\Snappy\Pdf as Snappy;

/**
 * Class WkHtmlToPdf
 *
 * @package Comit\Pdf\Renderer
 * @author Igor Vuckovic
 */
class WkHtmlToPdf implements RendererInterface
{

    use RendererTrait;

    /**
     * @param $content
     * @param string $orientation
     * @return string
     */
    public function create($content, $orientation = 'portrait')
    {
        $snappy = new Snappy();
        if (stripos(php_uname(), 'linux') !== false) {
            $snappy->setBinary(APP_PATH . 'vendor/bin/wkhtmltopdf-amd64');
        } else {
            $snappy->setBinary('/usr/local/bin/wkhtmltopdf');
        }

        $snappy->setOption('page-width', $this->_pageWidth);
        $snappy->setOption('page-height', $this->_pageHeight);
        $snappy->setOption('margin-top', $this->_marginTop);
        $snappy->setOption('margin-bottom', $this->_marginBottom);
        $snappy->setOption('margin-right', $this->_marginRight);
        $snappy->setOption('margin-left', $this->_marginLeft);
        $snappy->setOption('print-media-type', true);
        $snappy->setOption('orientation', $orientation);
        $snappy->setTimeout(120);

        return $snappy->getOutputFromHtml($content);
    }

    /**
     * Sets page size
     *
     * @param mixed $width
     * @param mixed $height
     */
    public function setPageSize($width, $height)
    {
        $this->setPageWidth($width);
        $this->setPageHeight($height);
    }

    /**
     * Sets documents margins
     *
     * @param mixed $marginTop
     * @param mixed $marginRight
     * @param mixed $marginBottom
     * @param mixed $marginLeft
     */
    public function setMargins($marginTop, $marginRight, $marginBottom, $marginLeft)
    {
        $this->setMarginTop($marginTop);
        $this->setMarginRight($marginRight);
        $this->setMarginBottom($marginBottom);
        $this->setMarginLeft($marginLeft);
    }

}

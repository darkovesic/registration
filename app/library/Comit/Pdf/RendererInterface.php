<?php

namespace Comit\Pdf;

/**
 * Interface RendererInterface
 *
 * @package Comit\Pdf
 * @author Igor Vuckovic
 */
interface RendererInterface
{


    /**
     * Creates PDF document from the content
     *
     * @param $content
     * @return mixed
     */
    public function create($content);


    /**
     * Sets page size
     *
     * @param mixed $width
     * @param mixed $height
     */
    public function setPageSize($width, $height);


    /**
     * Sets documents margins
     *
     * @param mixed $marginTop
     * @param mixed $marginRight
     * @param mixed $marginBottom
     * @param mixed $marginLeft
     */
    public function setMargins($marginTop, $marginRight, $marginBottom, $marginLeft);

}

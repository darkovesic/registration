<?php

return array(
    'application' => array(
        'modelsDir' => APP_PATH . 'app/models/',
        'pluginsDir' => APP_PATH . 'app/plugins/',
        'libraryDir' => APP_PATH . 'app/library/',
        'toolsDir' => APP_PATH . 'app/tools/',
        'cacheDir' => APP_PATH . 'var/cache/',
    ),
);
<?php

use Comit\Helper\DateTime;
use Comit\Model\Base as BaseModel;
use Phalcon\Mvc\Model\Resultset as ResultSet;

/**
 * PurchaseType Model Class
 *
 * @Source('purchase_type')
 *
 * @HasMany('id', 'Purchase', 'purchaseTypeId', {'alias':'Purchases'})
 */
class PurchaseType extends BaseModel
{

    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';
    const PROPERTY_DESCRIPTION = 'description';

    const ALIAS_PURCHASES = 'Purchases';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="name", type="string", nullable=false)
     * @FormElement(label="Name", type="Text", required=true)
     */
    protected $name;

    /**
     * @Column(column="description", type="string", nullable=true)
     * @FormElement(label="Description", type="TextArea", required=false)
     */
    protected $description;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_NAME => true,
        self::PROPERTY_DESCRIPTION => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return !is_null($this->id) ? intval($this->id) : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = !is_null($id) ? intval($id) : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->isDeletable()) {

        }

        return parent::beforeDelete();
    }

    /**
     * @param null|string|array $arguments
     * @return ResultSet|Purchase[]
     */
    public function getPurchases($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PURCHASES, $arguments);
    }

}

<?php

use Comit\Model\Base;
use \Phalcon\Mvc\Model\Behavior\Timestampable;

/**
 * CampaignType
 *
 * @Source('campaign_types')
 *
 * @HasMany('campaignTypeId', 'Client', 'campaignTypeId')
 *
 */

class CampaignType extends Base
{

    const PROPERTY_ID = 'id';
    const PROPERTY_NAME = 'name';

    const ALIAS_CLIENT = 'Client';

    const TYPE_REGULAR = 1;
    const TYPE_COUPON = 2;
    const TYPE_EMAIL = 3;

    /**
     * @Primary
     * @Identity
     *
     * @Column(type="integer", nullable=false, column="id")
     *
     */
    protected $id;

    /**
     * @Column(type="string", nullable=false, column="name")
     *
     * @FormElement(type="Text", required=true, label="Name")
     *
     */
    protected $name;


    /**
     * Returns the value of field campaignTypeId
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Method to set the value of field campaignTypeId
     *
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool|Client
     */
    public function getClient()
    {
        return $this->getRelated(self::ALIAS_CLIENT);
    }

    /**
     * @param null $arguments
     * @return \Phalcon\Mvc\Model\ResultsetInterface|Campaign[]
     */
    public function getCampaigns($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CLIENT, $arguments);
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}
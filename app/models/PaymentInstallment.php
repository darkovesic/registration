<?php

use Comit\Helper\DateTime;
use Comit\Model\Base as BaseModel;
use Phalcon\Mvc\Model\Resultset as ResultSet;

/**
 * PaymentInstallment Model Class
 *
 * @Source('payment_installments')
 * @BelongsTo('purchaseId', 'Purchase', 'id', {'alias':'Purchase'})
 */
class PaymentInstallment extends BaseModel
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PURCHASE_ID = 'purchaseId';
    const PROPERTY_DATE = 'date';
    const PROPERTY_AMOUNT = 'amount';
    const PROPERTY_PAYMENT_DATE = 'paymentDate';

    const ALIAS_PURCHASE = 'Purchase';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="purchase_id", type="integer", nullable=false)
     * @FormElement(label="Purchase", type="Select", required=true)
     */
    protected $purchaseId;

    /**
     * @Column(column="date", type="date", nullable=false)
     * @FormElement(label="Datum Rate", type="Text", required=true)
     */
    protected $date;

    /**
     * @Column(column="payment_date", type="date", nullable=true)
     * @FormElement(label="Datum Uplate", type="Date", required=false)
     */
    protected $paymentDate;

    /**
     * @Column(column="amount", type="decimal", nullable=true)
     * @FormElement(label="Iznos", type="Text", required=false)
     */
    protected $amount;

    /**
     * @var array
     */
    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PURCHASE_ID => true,
        self::PROPERTY_DATE => true,
        self::PROPERTY_AMOUNT => true,
        self::PROPERTY_PAYMENT_DATE => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return !is_null($this->id) ? intval($this->id) : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = !is_null($id) ? intval($id) : null;

        return $this;
    }

    /**
     * @return int
     */
    public function getPurchaseId()
    {
        return $this->purchaseId;
    }

    /**
     * @param int $purchaseId
     * @return $this
     */
    public function setPurchaseId($purchaseId)
    {
        $this->purchaseId = $purchaseId;

        return $this;
    }

    /**
     * @return Datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param string $paymentDAte
     * @return $this
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * @return double
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param double $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = !is_null($amount) ? doubleval($amount) : null;;

        return $this;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->isDeletable()) {

        }

        return parent::beforeDelete();
    }

    /**
     * @return null|Purchase
     */
    public function getPurchase()
    {
        return $this->getRelated(self::ALIAS_PURCHASE);
    }

    /**
     * @param Purchase $purchase
     * @return $this
     */
    public function setPurchaseType(Purchase $purchase)
    {
        $this->purchaseId = $purchase->getId();

        return $this;
    }

}

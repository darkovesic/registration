<?php

use Comit\Model\Base;
use \Phalcon\Mvc\Model\Behavior\Timestampable;

/**
 * Company
 *
 * @Source('companies')
 *
 * @HasMany('companyId', 'Group', 'companyId')
 * @HasMany('companyId', 'Campaign', 'companyId')
 *
 */

class Company extends Base
{
    const Vemid = 1;

    const PROPERTY_ID = 'id';
    const PROPERTY_COMPANY_NAME = 'companyName';
    const PROPERTY_SENDER_ID = 'senderId';
    const PROPERTY_SENDING_BIRTHDAY_CARD = 'sendingBirthdayCard';
    const PROPERTY_BIRTHDAY_MESSAGE = 'birthdayMessage';
    const PROPERTY_START_DATE = 'startDate';

    const ALIAS_CAMPAIGN = 'Campaign';

    /**
     * @Primary
     * @Identity
     *
     * @Column(type="integer", nullable=false, column="id")
     *
     */
    protected $id;

    /**
     * @Column(type="string", nullable=false, column="company_name")
     *
     * @FormElement(type="Text", required=true, label="Company Name")
     *
     */
    protected $companyName;

    /**
     * @Column(type="string", nullable=false, column="sender_id")
     *
     * @FormElement(type="Text", required=true, label="Sender ID")
     *
     */
    protected $senderId;

    /**
     * @Column(type="integer", nullable=true, column="sending_birthday_card")
     *
     * @FormElement(type="Check", required=false, label="Birthday Card")
     *
     */
    protected $sendingBirthdayCard;

    /**
     * @Column(type="string", nullable=true, column="birthday_message")
     *
     * @FormElement(type="TextArea", required=false, label="Birthday Card")
     *
     */
    protected $birthdayMessage;

    /**
     * @Column(type="dateTime", nullable=true, column="start_date")
     *
     * @FormElement(type="Date", required=false, label="Start Date")
     *
     */
    protected $startDate;

    /**
     * Returns the value of field companyId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Method to set the value of field companyId
     *
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the value of field companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Method to set the value of field companyName
     *
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * Returns the value of field senderId
     *
     * @return integer
     */
    public function getSenderId()
    {
        return $this->senderId;
    }

    /**
     * Method to set the value of field senderId
     *
     * @param integer $senderId
     */
    public function setSenderId($senderId)
    {
        $this->senderId = $senderId;
    }

    /**
     * Returns the value of field sendingBirthdayCard
     *
     * @return integer
     */
    public function getSendingBirthdayCard()
    {
        return $this->sendingBirthdayCard;
    }

    /**
     * Method to set the value of field sendingBirthdayCard
     *
     * @param integer $sendingBirthdayCard
     */
    public function setSendingBirthdayCard($sendingBirthdayCard)
    {
        $this->sendingBirthdayCard = $sendingBirthdayCard;
    }

    /**
     * Returns the value of field birthdayMessage
     *
     * @return string
     */
    public function getBirthdayMessage()
    {
        return $this->birthdayMessage;
    }

    /**
     * Method to set the value of field birthdayMessage
     *
     * @param string $birthdayMessage
     */
    public function setBirthdayMessage($birthdayMessage)
    {
        $this->birthdayMessage = $birthdayMessage;
    }

    /**
     * Returns the value of field startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Method to set the value of field startDate
     *
     * @param \DateTime $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @param null $arguments
     * @return \Phalcon\Mvc\Model\ResultsetInterface|Campaign[]
     */
    public function getCampaigns($arguments = null)
    {
        return $this->getRelated(self::ALIAS_CAMPAIGN, $arguments);
    }

    /**
     * @param null $arguments
     * @return \Phalcon\Mvc\Model\ResultsetInterface|Group[]
     */
    public function getGroups($arguments = null)
    {
        return $this->getRelated("Group", $arguments);
    }

    /**
     * Timestap on date column
     */
    public function initialize()
    {
        parent::initialize();
        $this->addBehavior(
            new Timestampable(
                array(
                    'beforeValidationOnCreate' => array(
                        'field' => 'startDate',
                        'format' => 'Y-m-d H:i:s'
                    )
                )
            )
        );

    }
}
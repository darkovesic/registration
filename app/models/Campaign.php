<?php

use Comit\Model\Base;
use \Phalcon\Mvc\Model\Behavior\Timestampable;

/**
 * Campaign
 *
 * @Source('campaigns')
 *
 * @HasMany('campaignId', 'SmsNumber', 'campaignId')
 *
 * @BelongsTo('companyId', 'Company', 'companyId')
 * @BelongsTo('groupId', 'Group', 'groupId')
 * @BelongsTo('campaignTypeId', 'CampaignType', 'campaignTypeId')
 *
 * @HasManyToMany('campaignId', 'CampaignGroup', 'campaignId', 'groupId', 'Group', 'groupId', {'alias':'NumberCampaign'})
 *
 */

class Campaign extends Base
{


    const PROPERTY_ID = 'id';
    const PROPERTY_CAMPAIGN_TYPE_ID = 'campaignTypeId';
    const PROPERTY_COMPANY_ID = 'companyId';
    const PROPERTY_NAME = 'name';
    const PROPERTY_MESSAGE = 'message';
    const PROPERTY_DATE = 'date';
    const PROPERTY_FINISHED = 'finished';

    const ALIAS_CAMPAIGN_TYPE = 'CampaignType';
    const ALIAS_COMPANY = 'Company';

    /**
     * @Primary
     * @Identity
     *
     * @Column(type="integer", nullable=false, column="id")
     *
     */
    protected $id;

    /**
     * @Column(type="integer", nullable=false, column="campaign_type_id")
     *
     * @FormElement(type="Select", required=true, label="CampaignType", relation="CampaignType", key="campaignTypeId", value="name")
     *
     */
    protected $campaignTypeId;

    /**
     * @Column(type="integer", nullable=false, column="company_id")
     *
     * @FormElement(type="Select", required=true, label="Company", relation="Company", key="companyId", value="companyName")
     *
     */
    protected $companyId;

    /**
     * @Column(type="string", nullable=false, column="name")
     *
     * @FormElement(type="Text", required=true, label="Name")
     *
     */
    protected $name;

    /**
     * @Column(type="string", nullable=false, column="message")
     *
     * @FormElement(type="TextArea", required=true, label="Message")
     *
     */
    protected $message;

    /**
     * @Column(type="dateTime", nullable=true, column="date")
     *
     * @FormElement(type="Date", required=false, label="Date")
     */
    protected $date;

    /**
     * @Column(type="string", nullable=true, column="finished")
     *
     * @FormElement(type="Check", required=false, label="Finished")
     *
     */
    protected $finished;

    /**
     * Returns the value of field campaignId
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Method to set the value of field campaignId
     *
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the value of field smsTypeId
     *
     * @return integer
     */
    public function getCampaignTypeId()
    {
        return $this->campaignTypeId;
    }

    /**
     * Method to set the value of field campaignTypeId
     *
     * @param integer $campaignTypeId
     */
    public function setCampaignTypeId($campaignTypeId)
    {
        $this->campaignTypeId = $campaignTypeId;
    }

    /**
     * Returns the value of field companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Method to set the value of field companyId
     *
     * @param integer $companyId
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the value of field message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Method to set the value of field message
     *
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * Returns the value of field date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Method to set the value of field date
     *
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Returns the value of field finished
     *
     * @return string
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * Method to set the value of field finished
     *
     * @param string $finished
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;
    }

    /**
     * @return bool|CampaignType
     */
    public function getCampaignType()
    {
        return $this->getRelated(self::ALIAS_CAMPAIGN_TYPE);
    }

    /**
     * @param CampaignType $campaignType
     */
    public function setCampaignType(CampaignType $campaignType)
    {
        $this->campaignTypeId = $campaignType->getId();
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->getRelated(self::ALIAS_COMPANY);
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->companyId = $company->getCompanyId();
    }

    /**
     * @param null $arguments
     * @return \Phalcon\Mvc\Model\ResultsetInterface|SmsNumber[]
     */
    public function getSmsNumbers($arguments = null)
    {
        return $this->getRelated("SmsNumber", $arguments);
    }

    /**
     * @param null $arguments
     * @return \Phalcon\Mvc\Model\ResultsetInterface|CampaignGroup[]
     */
    public function getCampaignGroups($arguments = null)
    {
        return $this->getRelated("CampaignGroup", $arguments);
    }

    /**
     * Timestap on date column
     */
    public function initialize()
    {
        parent::initialize();
        $this->addBehavior(
            new Timestampable(
                array(
                    'beforeValidationOnCreate' => array(
                        'field' => 'date',
                        'format' => 'Y-m-d H:i:s'
                    )
                )
            )
        );
    }
}
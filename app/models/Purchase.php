<?php

use Comit\Helper\DateTime;
use Comit\Model\Base as BaseModel;
use Phalcon\Mvc\Model\Resultset as ResultSet;

/**
 * Purchase Model Class
 *
 * @Source('purchases')
 * @BelongsTo('purchaseTypeId', 'PurchaseType', 'id', {'alias':'PurchaseType'})
 * @BelongsTo('clientId', 'Client', 'id', {'alias':'Client'})
 *
 * @HasMany('id', 'PaymentInstallment', 'purchaseId', {'alias':'PaymentInstallments'})
 */
class Purchase extends BaseModel
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PURCHASE_TYPE_ID = 'purchaseTypeId';
    const PROPERTY_CLIENT_ID = 'clientId';
    const PROPERTY_GUARANTOR = 'guarantor';
    const PROPERTY_AUTHORIZATION = 'authorization';
    const PROPERTY_QUANTITY = 'quantity';
    const PROPERTY_PLATES = 'plates';
    const PROPERTY_LEVEL = 'level';
    const PROPERTY_MODEL = 'model';
    const PROPERTY_REGISTERED_UNTIL = 'registeredUntil';
    const PROPERTY_PRICE = 'price';
    const PROPERTY_DATE = 'date';
    const PROPERTY_EXTRA = 'extra';
    const PROPERTY_NOTE = 'note';
    const PROPERTY_CONTRIBUTOR = 'contributor';
    const PROPERTY_CONTRIBUTOR_AMOUNT = 'contributorAmount';
    const PROPERTY_TECHNICAL = 'technical';
    const PROPERTY_TECHNICAL_AMOUNT = 'technicalAmount';
    const PROPERTY_INVOICE = 'invoice';
    const PROPERTY_CHASSIS = 'chassis';
    const PROPERTY_FILE = 'file';
    const PROPERTY_PHONE_NUMBER = 'phoneNumber';


    const INDIVIDUAL = 'INDIVIDUAL';
    const LEGAL = 'LEGAL';

    const ALIAS_PURCHASE_TYPE = 'PurchaseType';
    const ALIAS_CLIENT = 'Client';
    const ALIAS_PAYMENT_INSTALLMENTS = 'PaymentInstallments';

    /** @var string  */
    protected $_objectId = self::PROPERTY_ID;

    const TEH1 = 'TEH1';
    const TEH2 = 'TEH2';
    const TEH3 = 'TEH3';
    const TEH4 = 'TEH4';
    const TEH5 = 'TEH5';
    const TEH6 = 'TEH6';

    /** @var array  */
    public static $constants = array(
        self::TEH1 => 'Matrix',
        self::TEH2 => 'GM',
        self::TEH3 => 'TiM',
        self::TEH4 => 'Natasa',
        self::TEH5 => 'MONS',
        self::TEH6 => 'GM FAK',
    );

    protected static $_constants = array(
        self::INDIVIDUAL => 'Fizicko Lice',
        self::LEGAL => 'Pravno Lice',
    );

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="purchase_type_id", type="string", nullable=false)
     * @FormElement(label="Vrsta klijenta", type="Select", required=true)
     */
    protected $purchaseTypeId;

    /**
     * @Column(column="client_id", type="string", nullable=false)
     * @FormElement(label="Klijent", type="Text", required=true)
     */
    protected $clientId;

    /**
     * @Column(column="phone_number", type="string", nullable=true)
     * @FormElement(label="Broj telefona", type="Text", required=false)
     */
    protected $phoneNumber;

    /**
     * @Column(column="guarantor", type="string", nullable=true)
     * @FormElement(label="Garantor", type="Text", required=false)
     */
    protected $guarantor;

    /**
     * @Column(column="guarantor_phone", type="string", nullable=true)
     * @FormElement(label="Telefon Garantora", type="Text", required=false)
     */
    protected $guarantorPhone;

    /**
     * @Column(column="type_of_client", type="string", nullable=false)
     * @FormElement(label="Tip Klijenta", type="Select", required=true, options={'INDIVIDUAL':'Fizicko Lice','LEGAL':'Pravno Lice'})
     */
    protected $typeOfClient;

    /**
     * @Column(column="authorization", type="bool", nullable=true)
     * @FormElement(label="Ovlašćenje", type="Check",required=false)
     */
    protected $authorization;

    /**
     * @Column(column="quantity", type="integer", nullable=false)
     * @FormElement(label="Kolicina", type="Text", required=true)
     */
    protected $quantity;

    /**
     * @Column(column="plates", type="string", nullable=false)
     * @FormElement(label="Tablice", type="Text", required=true)
     */
    protected $plates;

    /**
     * @Column(column="level", type="string", nullable=true)
     * @FormElement(label="Prem. Stepen", type="Text", required=false)
     */
    protected $level;

    /**
     * @Column(column="model", type="string", nullable=false)
     * @FormElement(label="Model", type="Text", required=true)
     */
    protected $model;

    /**
     * @Column(column="chassis", type="string", nullable=true)
     * @FormElement(label="Broj Sasije", type="Text", required=false)
     */
    protected $chassis;

    /**
     * @Column(column="extra", type="string", nullable=true)
     * @FormElement(label="Oznaka", type="Text", required=false)
     */
    protected $extra;

    /**
     * @Column(column="created_datetime", type="date", nullable=true)
     */
    protected $createdDatetime;

    /**
     * @Column(column="date", type="date", nullable=true)
     * @FormElement(label="Datum Unosa", type="Text", required=false)
     */
    protected $date;

    /**
     * @Column(column="registered_until", type="date", nullable=true)
     * @FormElement(label="Registrovan Do", type="Text", required=false)
     */
    protected $registeredUntil;

    /**
     * @Column(column="price", type="decimal", nullable=false)
     * @FormElement(label="Cena", type="Text", required=true)
     */
    protected $price;

    /**
     * @Column(column="note", type="string", nullable=true)
     * @FormElement(label="Beleska", type="TextArea", required=false)
     */
    protected $note;


    /**
     * @Column(column="technical", type="string", nullable=true)
     * @FormElement(label="Tehnicki", type="Select", required=false, options={'0':'Izaberite','TEH1':'Matrix','TEH2':'GM','TEH3':'TiM','TEH4':'Natasa','TEH5':'MONS','TEH6':'GM FAK'})
     */
    protected $technical;


    /**
     * @Column(column="technical_amount", type="DateTime", nullable=true)
     * @FormElement(label="Vrednost tehnickog", type="Text", required=false)
     */
    protected $technicalAmount;


    /**
     * @Column(column="contributor", type="string", nullable=true)
     * @FormElement(label="Saradnik", type="Text", required=false)
     */
    protected $contributor;

    /**
     * @Column(column="contributor_amount", type="DateTime", nullable=true)
     * @FormElement(label="Vrednost ucinka", type="Text", required=false)
     */
    protected $contributorAmount;

    /**
     * @Column(column="invoice", type="Text", nullable=true)
     * @FormElement(label="Broj Fakture", type="Text", required=false)
     */
    protected $invoice;

    /**
     * @Column(column="file", type="string", nullable=true)
     * @FormElement(label="Fajl", type="File",required=false)
     */
    protected $file;

    /** @var array */
    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_CLIENT_ID => true,
        self::PROPERTY_QUANTITY => true,
        self::PROPERTY_PLATES => true,
        self::PROPERTY_LEVEL => false,
        self::PROPERTY_MODEL => false,
        self::PROPERTY_REGISTERED_UNTIL => false,
        self::PROPERTY_PRICE => false,
        self::PROPERTY_DATE => false,
        self::PROPERTY_EXTRA => false,
        self::PROPERTY_NOTE => false,
        self::PROPERTY_CONTRIBUTOR => false,
        self::PROPERTY_CONTRIBUTOR_AMOUNT => false,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return !is_null($this->id) ? intval($this->id) : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = !is_null($id) ? intval($id) : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getPurchaseTypeId()
    {
        return $this->purchaseTypeId;
    }

    /**
     * @param string $purchaseTypeId
     * @return $this
     */
    public function setPurchaseTypeId($purchaseTypeId)
    {
        $this->purchaseTypeId = $purchaseTypeId;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getGuarantor()
    {
        return $this->guarantor;
    }

    /**
     * @param string $guarantor
     * @return $this
     */
    public function setGuarantor($guarantor)
    {
        $this->guarantor = $guarantor;

        return $this;
    }

    /**
     * @return string
     */
    public function getGuarantorPhone()
    {
        return $this->guarantorPhone;
    }

    /**
     * @param string $guarantorPhone
     * @return $this
     */
    public function setGuarantorPhone($guarantorPhone)
    {
        $this->guarantorPhone = $guarantorPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getTypeOfClient()
    {
        return $this->typeOfClient;
    }

    /**
     * @param string $typeOfClient
     * @return $this
     */
    public function setTypeOfClient($typeOfClient)
    {
        $this->typeOfClient = $typeOfClient;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorization()
    {
        return $this->authorization;
    }

    /**
     * @param string $authorization
     * @return $this
     */
    public function setAuthorization($authorization)
    {
        $this->authorization = $authorization;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlates()
    {
        return $this->plates;
    }

    /**
     * @param string $plates
     * @return $this
     */
    public function setPlates($plates)
    {
        $this->plates = $plates;

        return $this;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param string $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return string
     */
    public function getChassis()
    {
        return $this->chassis;
    }

    /**
     * @param string $model
     * @return $this
     */
    public function setChassis($chassis)
    {
        $this->chassis = $chassis;

        return $this;
    }

    /**
     * @return \Phalcon\Forms\Element\Date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime|string $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getRegisteredUntil()
    {
        return $this->registeredUntil;
    }

    /**
     * @param DateTime|string $registeredUntil
     * @return $this
     */
    public function setRegisteredUntil($registeredUntil)
    {
        $this->registeredUntil = $registeredUntil;

        return $this;
    }

    /**
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param double $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = !is_null($price) ? doubleval($price) : null;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }



    /**
     * @return string
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * @param string $extra
     * @return $this
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return $this
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string
     */
    public function getTechnical()
    {
        return $this->technical;
    }

    /**
     * @param string $technical
     * @return $this
     */
    public function setTechnical($technical)
    {
        $this->technical = $technical;

        return $this;
    }



    /**
     * @return double
     */
    public function getTechnicalAmount()
    {
        return $this->technicalAmount;
    }

    /**
     * @param string $technicalAmount
     * @return $this
     */
    public function setTechnicalAmount($technicalAmount)
    {
        $this->technicalAmount = $technicalAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getContributor()
    {
        return $this->contributor;
    }



    /**
     * @param string $contributor
     * @return $this
     */
    public function setContributr($contributor)
    {
        $this->contributor = $contributor;

        return $this;
    }

    /**
     * @return double
     */
    public function getContributorAmount()
    {
        return $this->contributorAmount;
    }

    /**
     * @param double $contributorAmount
     * @return $this
     */
    public function setContributorAmount($contributorAmount)
    {
        $this->contributorAmount = $contributorAmount;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param string $contributorAmount
     * @return $this
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisplayTypeOfClient()
    {
        return self::$_constants[$this->getTypeOfClient()];
    }

    public function initialize()
    {
        $this->addBehavior(
            new \Phalcon\Mvc\Model\Behavior\Timestampable(
                array(
                    'beforeValidationOnCreate' => array(
                        'field' => 'createdDatetime',
                        'format' => 'Y-m-d H:i:s'
                    )
                )
            )
        );

        parent::initialize();

    }

    /**
     * @return null|Client
     */
    public function getClient()
    {
        return $this->getRelated(self::ALIAS_CLIENT);
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/files/';
    }

    /**
     * @return string
     */
    public function getFileUrl()
    {
        return $this->getUploadPath() . $this->getFile();
    }

    /**
     * @param \Phalcon\Http\Request\File $file
     * @param string $name
     */
    public function moveFile(\Phalcon\Http\Request\File $file = null, $name = '')
    {
        if (!is_null($file)) {

            if($name) {
                $filePath = $this->getUploadPath() . $name. '.' . $file->getExtension();
            } else {
                $filePath = $this->getUploadPath() . substr(sha1(rand()), 0, 8) . '.' . $file->getExtension();
            }

            if ($file->moveTo($filePath)) {

                if (is_file($currentAvatar = $this->getUploadPath() . $this->getFile())) {
                    @unlink($currentAvatar);
                }

                $this->setFile(basename($filePath));
                $this->save();
            }
        }
    }

    /**
     * @param Client $client
     * @return $this
     */
    public function setClient(Client $client)
    {
        $this->clientId = $client->getId();

        return $this;
    }

    /**
     * @return null|PurchaseType
     */
    public function getPurchaseType()
    {
        return $this->getRelated(self::ALIAS_PURCHASE_TYPE);
    }

    /**
     * @param PurchaseType $purchaseType
     * @return $this
     */
    public function setPurchaseType(PurchaseType $purchaseType)
    {
        $this->purchaseTypeId = $purchaseType->getId();

        return $this;
    }

    /**
     * @param null|string|array $arguments
     * @return ResultSet|PaymentInstallment[]
     */
    public function getPaymentInstallments($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PAYMENT_INSTALLMENTS, $arguments);
    }

    /**
     *
     */
    public function beforeDelete()
    {
        $this->getPaymentInstallments()->delete();
    }

    /**
     * @return float|int
     */
    public function getTotalPaid()
    {
        $paid = 0;
        foreach($this->getPaymentInstallments() as $paymentInstallment) {

            if($paymentInstallment->getPaymentDate()) {
                $paid += $paymentInstallment->getAmount();
            }
        }

        return $paid;
    }

    /**
     * @return bool|\Phalcon\Mvc\Model|\Phalcon\Mvc\ModelInterface|void|\PaymentInstallment
     */
    public function isTodayPaymentDate()
    {
        $isTodayPaymentDate = false;

        if($this->getPrice() > $this->getTotalPaid()) {
            $isTodayPaymentDate = $this->getPaymentInstallments(
                array(
                    PaymentInstallment::PROPERTY_DATE . ' = :date: AND ' . PaymentInstallment::PROPERTY_PAYMENT_DATE . ' IS NULL',
                    'bind' => array(
                        'date' => (new DateTime())->format('Y-m-d')
                    )
                )
            )->getLast();
        }

        return $isTodayPaymentDate;
    }

    /**
     * @param bool $withoutThisMonth
     * @return bool
     */
    public function isThereMissingPayments($withoutThisMonth = false)
    {
        $date = (new DateTime())->format('Y-m-d');
        if ($withoutThisMonth) {
            $date = (new DateTime('-1 month'))->format('Y-m-d');
        }

        $isThereMissingPayments = false;
        if($this->getPrice() > $this->getTotalPaid()) {
            $paymentInstallments = $this->getPaymentInstallments(
                array(
                    PaymentInstallment::PROPERTY_DATE . ' < :date: AND ' . PaymentInstallment::PROPERTY_PAYMENT_DATE . ' IS NULL',
                    'bind' => array(
                        'date' => $date
                    )
                )
            );

            if($paymentInstallments->count()) {
                $isThereMissingPayments = true;
            }
        }

        return $isThereMissingPayments;
    }

    public function getSerbianNumerationOfProRata()
    {
        switch($this->getPaymentInstallments()->count()) {
            case 1:
                $text = 'jednu';
                break;
            case 2:
                $text = 'dve';
                break;
            case 3:
                $text = 'tri';
                break;
            case 4:
                $text = 'četiri';
                break;
            case 5:
                $text = 'pet';
                break;
            case 6:
                $text = 'šest';
                break;
            default:
                $text = 'ni jednu';
                break;
        }

        return $text;
    }

    /**
     * @return string
     */
    public function getCreateDatatimeShort()
    {
        return (new DateTime($this->createdDatetime))->getShort();
    }

    /**
     * @return string|null
     */
    public function getDisplayNameOfTechnical() {

        if ($this->getTechnical()) {
            return self::$constants[$this->getTechnical()];
        }

        return null;
    }

}

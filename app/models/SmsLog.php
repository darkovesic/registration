<?php

use Comit\Helper\DateTime;
use Comit\Model\Base as BaseModel;
use Phalcon\Mvc\Model\Resultset as ResultSet;

/**
 * SmsLog Model Class
 *
 * @Source('sms_logs')
 *
 */
class SmsLog extends BaseModel
{

    const PROPERTY_ID = 'id';
    const PROPERTY_PHONE = 'phone';
    const PROPERTY_TEXT = 'text';
    const PROPERTY_SENT = 'sent';


    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="phone", type="string", nullable=false)
     */
    protected $phone;

    /**
     * @Column(column="text", type="string", nullable=false)
     */
    protected $text;

    /**
     * @Column(column="sms_id", type="string", nullable=false)
     */
    protected $smsId;

    /**
     * @Column(column="sent", type="dateTime", nullable=true)
     */
    protected $sent;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_PHONE => true,
        self::PROPERTY_TEXT => true,
        self::PROPERTY_SENT => true,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return !is_null($this->id) ? intval($this->id) : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = !is_null($id) ? intval($id) : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getSmsId()
    {
        return $this->smsId;
    }

    /**
     * @param string $smsId
     * @return $this
     */
    public function setSmsId($smsId)
    {
        $this->smsId = $smsId;

        return $this;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->isDeletable()) {

        }

        return parent::beforeDelete();
    }

    /**
     * @return null|DateTime
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * @param string|DateTime $sent
     * @return $this
     */
    public function setSent($sent)
    {
        $this->sent = $sent;

        return $this;
    }


}

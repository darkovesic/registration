<?php

use Comit\Helper\DateTime;
use Comit\Helper\Hash;
use Comit\Helper\Image;
use Comit\Model\Base as BaseModel;
use Phalcon\Mvc\Model\Resultset as ResultSet;
use \Comit\Security\Acl;

/**
 * User Model Class
 *
 * @Source('users')
 */
class User extends BaseModel
{

    const PROPERTY_ID = 'id';
    const PROPERTY_FIRST_NAME = 'firstName';
    const PROPERTY_LAST_NAME = 'lastName';
    const PROPERTY_USERNAME = 'username';
    const PROPERTY_PASSWORD_HASH = 'passwordHash';
    const PROPERTY_AVATAR = 'avatar';
    const PROPERTY_GENDER = 'gender';
    const PROPERTY_ROLE = 'role';
    const PROPERTY_IS_ACTIVE = 'isActive';
    const PROPERTY_LAST_IP = 'lastIp';
    const PROPERTY_REGISTERED_DATETIME = 'registeredDatetime';
    const PROPERTY_LAST_VISIT_DATETIME = 'lastVisitDatetime';

    const GENDER_MALE = 'MALE';
    const GENDER_FEMALE = 'FEMALE';

    const ROLE_MEMBER = 'MEMBER';
    const ROLE_SPORTSMAN = 'SPORTSMAN';
    const ROLE_TRAINER = 'TRAINER';
    const ROLE_ADMIN = 'ADMIN';

    protected $_objectId = self::PROPERTY_ID;

    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="first_name", type="string", nullable=false)
     * @FormElement(label="First name", type="Text", required=true)
     */
    protected $firstName;

    /**
     * @Column(column="last_name", type="string", nullable=false)
     * @FormElement(label="Last name", type="Text", required=true)
     */
    protected $lastName;

    /**
     * @Column(column="username", type="string", nullable=false)
     * @FormElement(label="Email", type="Email", required=true)
     */
    protected $username;

    /**
     * @Column(column="password_hash", type="string", nullable=true)
     * @FormElement(label="Password", type="Password", required=false)
     */
    protected $passwordHash;

    /**
     * @Column(column="avatar", type="string", nullable=true)
     * @FormElement(label="Avatar", type="File", required=false)
     */
    protected $avatar;

    /**
     * @Column(column="gender", type="string", nullable=true)
     * @FormElement(label="Gender", type="Select", required=false, options={'MALE':'Male', 'FEMALE':'Female'})
     */
    protected $gender;

    /**
     * @Column(column="role", type="string", nullable=true)
     */
    protected $role;

    /**
     * @Column(column="is_active", type="integer", nullable=false)
     * @FormElement(label="Is active", type="Check", required=true)
     */
    protected $isActive;

    /**
     * @Column(column="last_ip", type="string", nullable=true)
     */
    protected $lastIp;

    /**
     * @Column(column="registered_datetime", type="dateTime", nullable=true)
     */
    protected $registeredDatetime;

    /**
     * @Column(column="last_visit_datetime", type="dateTime", nullable=true)
     */
    protected $lastVisitDatetime;

    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_FIRST_NAME => true,
        self::PROPERTY_LAST_NAME => true,
        self::PROPERTY_USERNAME => true,
        self::PROPERTY_PASSWORD_HASH => false,
        self::PROPERTY_AVATAR => true,
        self::PROPERTY_GENDER => true,
        self::PROPERTY_IS_ACTIVE => true,
        self::PROPERTY_LAST_IP => false,
        self::PROPERTY_REGISTERED_DATETIME => false,
        self::PROPERTY_LAST_VISIT_DATETIME => false,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return !is_null($this->id) ? intval($this->id) : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = !is_null($id) ? intval($id) : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordHash()
    {
        return $this->passwordHash;
    }

    /**
     * @param string $passwordHash
     * @return $this
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return $this
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return (bool)$this->isActive;
    }

    /**
     * @param int|bool $isActive
     * @return $this
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive ? 1 : 0;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastIp()
    {
        return $this->lastIp;
    }

    /**
     * @param string $lastIp
     * @return $this
     */
    public function setLastIp($lastIp)
    {
        $this->lastIp = $lastIp;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getRegisteredDatetime()
    {
        return $this->registeredDatetime;
    }

    /**
     * @param string|DateTime $registeredDatetime
     * @return $this
     */
    public function setRegisteredDatetime($registeredDatetime)
    {
        $this->registeredDatetime = $registeredDatetime;

        return $this;
    }

    /**
     * @return null|DateTime
     */
    public function getLastVisitDatetime()
    {
        return $this->lastVisitDatetime;
    }

    /**
     * @param string|DateTime $lastVisitDatetime
     * @return $this
     */
    public function setLastVisitDatetime($lastVisitDatetime)
    {
        $this->lastVisitDatetime = $lastVisitDatetime;

        return $this;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->passwordHash = Hash::password($password);

        return $this;
    }

    /**
     * @return string
     */
    public function getAvatarUrl()
    {
        if ($this->getAvatar() && is_file($this->getUploadPath() . $this->getAvatar())) {
            return \Comit\Helper\Common::getBaseUrl() . 'uploads/avatar/' . $this->getAvatar();
        }

        return \Comit\Helper\Common::getBaseUrl() . 'uploads/avatar/avatar.png';
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return APP_PATH . 'var/uploads/avatar/';
    }

    /**
     * @param \Phalcon\Http\Request\File $file
     */
    public function replaceAvatar(\Phalcon\Http\Request\File $file = null)
    {
        if (!is_null($file)) {

            $filePath = $this->getUploadPath() . substr(sha1(rand()), 0, 8) . '.' . $file->getExtension();
            if ($file->moveTo($filePath)) {

                if (is_file($currentAvatar = $this->getUploadPath() . $this->getAvatar())) {
                    @unlink($currentAvatar);
                }

                Image::resizeAndCrop($filePath, 400, 400);

                $this->setAvatar(basename($filePath));
                $this->save();
            }
        }
    }

    /**
     * @param string $username
     * @param string $password
     * @return $this
     */
    public static function authenticateUser($username, $password)
    {
        if (is_null($username) || is_null($password)) {
            return null;
        }

        return self::findFirst(array(
            self::PROPERTY_USERNAME . ' = :username: AND ' . self::PROPERTY_PASSWORD_HASH . ' = :passwordHash:',
            'bind' => array(
                'username' => $username,
                'passwordHash' => Hash::password($password)
            )
        ));
    }

    /**
     * @param string $password
     * @return bool
     */
    public function isValidPassword($password)
    {
        return $this->passwordHash === Hash::password($password);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role === self::ROLE_ADMIN;
    }

    /**
     * @return bool
     */
    public function isAllowed()
    {
        $allowed = true;
        $role = $this->getRole() ? $this->getRole() : Acl::ROLE_MEMBER;

        $aclPlugin = new Acl();
        if(!$aclPlugin->checkAccess($this->getDI()->get('dispatcher'), $role)) {
            $allowed = false;
        }

        return $allowed;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return trim($this->firstName . ' ' . $this->lastName);
    }

    /**
     * @return string
     */
    public function getGenderDisplayName()
    {
        $genders = array(
            self::GENDER_FEMALE => 'Female',
            self::GENDER_MALE => 'Male',
        );

        if (array_key_exists($this->gender, $genders)) {
            return $genders[$this->gender];
        }

        return ucfirst(strtolower($this->gender));
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->isDeletable()) {

        }

        return parent::beforeDelete();
    }

    /**
     *
     */
    public function beforeValidationOnCreate()
    {
        $this->setRegisteredDatetime(new DateTime());

        if (!$this->getPasswordHash()) {
            $this->setPassword(Hash::crypt(time(), 'md5'));
        }
    }

    /**
     * @return bool
     */
    public function validation()
    {
        $this->validate(
            new \Phalcon\Mvc\Model\Validator\Uniqueness(array(
                'field' => self::PROPERTY_USERNAME,
                'message' => $this->_translate('This email address is already in use.')
            ))
        );

        return !$this->validationHasFailed();
    }

}

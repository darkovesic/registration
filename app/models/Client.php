<?php

use Comit\Helper\DateTime;
use Comit\Model\Base as BaseModel;
use Phalcon\Mvc\Model\Resultset as ResultSet;

/**
 * Client Model Class
 *
 * @Source('clients')
 * @HasMany('id', 'Purchase', 'clientId', {'alias':'Purchases'})
 */
class Client extends BaseModel
{

    const PROPERTY_ID = 'id';
    const PROPERTY_FIRST_NAME = 'firstName';
    const PROPERTY_LAST_NAME = 'lastName';

    const PROPERTY_JMBG = 'jmbg';
    const PROPERTY_PHONE_NUMBER = 'phoneNumber';

    const PROPERTY_CREATED_DATETIME = 'createdDatetime';
    const PROPERTY_SMS_NOTIFICATION = 'smsNotification';



    const ALIAS_PURCHASES = 'Purchases';

    protected $_objectId = self::PROPERTY_ID;



    /**
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", nullable=false)
     */
    protected $id;

    /**
     * @Column(column="first_name", type="string", nullable=false)
     * @FormElement(label="Ime", type="Text", required=true)
     */
    protected $firstName;

    /**
     * @Column(column="last_name", type="string", nullable=true)
     * @FormElement(label="Prezime", type="Text", required=false)
     */
    protected $lastName;

    /**
     * @Column(column="jmbg", type="string", nullable=true)
     * @FormElement(label="JMBG", type="Text", required=false)
     */
    protected $jmbg;

    /**
     * @Column(column="created_datetime", type="DateTime", nullable=true)
     */
    protected $createdDatetime;

    /**
     * @Column(column="sms_notification", type="bool", nullable=true)
     * @FormElement(label="Obavesti SMS-om", type="Check",required=false)
     */
    protected $smsNotification;


    protected $_restRepresentationDefinition = array(
        self::PROPERTY_ID => true,
        self::PROPERTY_FIRST_NAME => true,
        self::PROPERTY_LAST_NAME => true,
        //self::PROPERTY_GUARANTOR => true,
        self::PROPERTY_PHONE_NUMBER => false,
    );

    /**
     * @return int
     */
    public function getId()
    {
        return !is_null($this->id) ? intval($this->id) : null;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = !is_null($id) ? intval($id) : null;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getJmbg()
    {
        return $this->jmbg;
    }

    /**
     * @param string $jmbg
     * @return $this
     */
    public function setJmbg($jmbg)
    {
        $this->jmbg = $jmbg;

        return $this;
    }

    /**
     * @return string
     */
    public function getSmsNotification()
    {
        return $this->smsNotification;
    }

    /**
     * @param string $smsNotification
     * @return $this
     */
    public function setSmsNotification($smsNotification)
    {
        $this->smsNotification = $smsNotification;

        return $this;
    }



    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }


    /**
     * @return string
     */
    public function getDisplayName()
    {
        return trim($this->firstName . ' ' . $this->lastName);
    }




    public function initialize()
    {
        $this->addBehavior(
            new \Phalcon\Mvc\Model\Behavior\Timestampable(
                array(
                    'beforeValidationOnCreate' => array(
                        'field' => 'createdDatetime',
                        'format' => 'Y-m-d H:i:s'
                    )
                )
            )
        );

        parent::initialize();

    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->isDeletable()) {

        }

        return parent::beforeDelete();
    }

    /**
     * @param null|string|array $arguments
     * @return ResultSet|Purchase[]
     */
    public function getPurchases($arguments = null)
    {
        return $this->getRelated(self::ALIAS_PURCHASES, $arguments);
    }

}

<?php
require_once '../vendor/autoload.php';

//$client = new infobip\api\client\SendSingleTextualSms(new infobip\api\configuration\BasicAuthConfiguration('bebakids', 'adminabc123'));
//
//$requestBody = new infobip\api\model\sms\mt\send\textual\SMSTextualRequest();
//$requestBody->setFrom('BEBAKIDS');
//$requestBody->setTo('381648382015');
//$requestBody->setText("This is an example message.");
//
//$response = $client->execute($requestBody);


use OnlineCity\Transport\SocketTransport;
use OnlineCity\SMPP\SMPP;
use OnlineCity\SMPP\SmppClient;
use OnlineCity\SMPP\SmppAddress;
use OnlineCity\Encoder\GsmEncoder;

// Construct transport and client
SmppClient::$system_type = '901039';
$transport = new SocketTransport(array('bulk.mobile-gw.com'), 7900);
$transport->setRecvTimeout(60);
$smpp = new SmppClient($transport);

// Activate binary hex-output of server interaction
$smpp->debug = true;
$transport->debug = true;

// Open the connection
$transport->open();
$smpp->bindTransmitter("bebaapi", "dKkvWMoH");

// Optional connection specific overrides
//SmppClient::$sms_null_terminate_octetstrings = false;
//SmppClient::$csms_method = SmppClient::CSMS_PAYLOAD;
//SmppClient::$sms_registered_delivery_flag = SMPP::REG_DELIVERY_SMSC_BOTH;

// Prepare message
$message = 'Agencija ONESTA Vas obavestava da uskoro istice registracija vaseg vozila, reg. oznake: Da obnovimo saradnju, pozovite nas na tel:064-668-7983, 011-4074-584';
$encodedMessage = GsmEncoder::utf8_to_gsm0338($message);
$from = new SmppAddress('Onesta', SMPP::TON_ALPHANUMERIC);
$to = new SmppAddress(381648382015, SMPP::TON_INTERNATIONAL, SMPP::NPI_E164);

// Send
$smpp->sendSMS($from, $from, $encodedMessage);

// Close connection
//$smpp->close();


// Read SMS and output
$sms = $smpp->readSMS();
echo "SMS:\n";
var_dump($sms);

// Close connection
$smpp->close();

SmppClient::$system_type = '901039';
$transport2 = new SocketTransport(array('bulk.mobile-gw.com'), 7900);
$transport2->setRecvTimeout(6000);
$smpp2 = new SmppClient($transport2);

// Activate binary hex-output of server interaction
$smpp2->debug = true;
$transport2->debug = true;

// Open the connection
$transport2->open();
$smpp2->bindReceiver("bebaapi", "dKkvWMoH");

$sms = $smpp2->readSMS();
echo "SMS:\n";
var_dump($sms);

// Close connection
$smpp2->close();